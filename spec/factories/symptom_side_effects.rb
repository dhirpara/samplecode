# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :symptom_side_effect do
    symptom nil
    side_effect nil
  end
end
