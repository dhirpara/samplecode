# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order_item_report do
    order_item_id 1
    quantity 1
  end
end
