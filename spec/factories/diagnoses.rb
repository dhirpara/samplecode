# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :diagnosis do
    disid 1
    notes "MyText"
    date "2013-07-31 09:27:33"
    tracked "MyString"
    doctor_diagnosed "MyString"
    ssid 1
    deleted false
    ssid_deleted 1
    deleted_date "2013-07-31 09:27:33"
  end
end
