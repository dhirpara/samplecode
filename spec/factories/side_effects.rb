# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :side_effect do
    name "MyText"
    description "MyText"
  end
end
