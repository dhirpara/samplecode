# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :handling_charge do
    handling_charge_method
    handling_charge 5
    min 5
  end
end
