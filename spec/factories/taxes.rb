# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tax do
  end
  factory :taxis, :class => 'Tax' do
    rate 10
  end
end
