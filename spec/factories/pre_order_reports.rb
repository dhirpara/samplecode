# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :pre_order_report do
    order nil
    payment_profile nil
  end
end
