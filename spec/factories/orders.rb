# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order do
    client

    ignore do
      pay_for_cc false
    end

    before(:create) do |order, evaluator|
      order.pricing_type = order.client.pricing_type
    end


    after(:create) do |order, evaluator|
      order.currency = order.client.currency!
      order.save
      canada = Country.find_by_name("Canada")
      Country.find_by_name("United States") || create(:usa)
      create(:payment_type, name: "other")
      create(:tax, country: canada, province_abbr: 'BC', name: 'GST', rate: 10)
    end

    factory :cart_order do
      ignore do
        have_items false
        have_address false
        have_shipping_method false
      end

      after(:create) do |order, evaluator|
        if evaluator.have_items
          order.add_item(build(:order_item))
          order.add_item(build(:order_item))
        end
        if evaluator.have_address
          address = create(:address, country: order.client.country)
          order.create_shipment!
          order.update_ship_address(address)
        end
        if evaluator.have_shipping_method
          order.update_shipping_method(ShippingMethod.find_by_name("Landmark Standard"))
        end
      end

      factory :address_order do
        ignore do
          have_items true
        end

        after(:create) do |order|
          order.next
        end

        factory :delivery_order do
          ignore do
            have_address true
          end

          after(:create) do |order|
            order.next
          end

          factory :payment_order do
            ignore do
              have_shipping_method true
            end

            after(:create) do |order|
              order.next
            end

            factory :pending_order

            ignore do
              with_failure_payment false
            end
            factory :complete_order do
              after(:create) do |order, evaluator|
                if evaluator.pay_for_cc
                  address = order.shipment.address
                  if evaluator.with_failure_payment
                    #amount超过100，支付失败
                    credit_card = ActiveMerchant::Billing::CreditCard.new({ first_name: "liang", last_name: "mincong", number: "4504481742333", verification_value: "123", year: Time.now.year+10, month: 1 })
                    payment1 = order.payments.new(amount: order.total)
                    payment1.purchase(credit_card, address)
                  end
                  credit_card = ActiveMerchant::Billing::CreditCard.new({ first_name: "liang", last_name: "mincong", number: "4030000010001234", verification_value: "123", year: Time.now.year+10, month: 1 })
                  payment2 = order.payments.new(amount: order.total)
                  resp, payment_profile = payment2.store_payment_profile(credit_card, address)
                  payment2.payment_profile_purchase(payment_profile)
                else
                  payment_type = PaymentType.find_by_name("other")
                  payment = order.payments.new(amount: order.total, payment_type_id: payment_type.id, received: true)
                  payment.create_by_other_payment_type_payment
                end
              end

              factory :paid_order do
                factory :ready_order do
                  after(:create) do |order|
                    order.reload.shipment.ready
                  end

                  factory :shipped_order do
                    after(:create) do |order|
                      order.reload.shipment.ship
                    end
                  end
                end
              end

              factory :balance_due_order do
                after(:create) do |order|
                  order.add_item(build(:order_item))
                end
              end

              factory :credit_owing_order do
                after(:create) do |order|
                  order.remove_order_item(build(:order_item))
                end
              end
            end
          end
        end
      end
    end
  end
end
