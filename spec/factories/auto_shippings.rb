# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :auto_shipping do
    client client
    active true
    interval 30
  end
end
