# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order_item do
    order_id { create(:order).id }
    product_id { create(:product).id }
    quantity 1
    price 100

    #after(:create) do |order_item|
      #order_item.price = order_item.product.price_for_user(order_item.order.client)
      #order_item.save
    #end
  end
end
