# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :seo do
    url "MyString"
    title "MyString"
    keywords "MyString"
    description "MyString"
  end
end
