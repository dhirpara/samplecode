FactoryGirl.define do
  factory :support_user do
    ignore do
      roles []
    end
    sequence(:email) {|n| "admin#{n}@gmail.com" }
    user_name "admin"
    sequence(:first_name) {|n| "first name #{n}" }
    sequence(:last_name) {|n| "last name #{n}" }
    active true
    password '12345678'
    password_confirmation '12345678'
    after(:create) do |user, evaluator|
      if evaluator.roles.any?
        evaluator.roles.each{|r| user.roles << r}
      else
        user.roles << (Role.find_by_code("ADM") || Role.create(name: "admin", code: "ADM"))
      end
    end
  end
end
