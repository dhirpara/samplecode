# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :global_language do
    name "MyString"
    abbr "MyString"
  end
end
