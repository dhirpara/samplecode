# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :client_diagnosis do
    client nil
    diagnosis nil
  end
end
