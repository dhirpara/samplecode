# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :daily_symptom_total do
    cid 1
    diaid 1
    total 1
    date "2013-07-31 09:31:21"
    date_entered "2013-07-31 09:31:21"
  end
end
