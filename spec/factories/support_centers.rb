FactoryGirl.define do
  factory :support_center do
    name "support center"
    support_center_type
    #country
    country_id { (Country.find_by_name("Canada") || country).id }
  end
end
