# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :email do
    first_name "first name"
    email "1@gmail.com"
  end
end
