# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :auto_ship_adjustment, :class => 'AutoShipAdjustments' do
    auto_shipping nil
    amount "9.99"
    adjustment_type nil
    note "MyString"
  end
end
