# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :auto_ship_setting_pricing_type do
    auto_ship_setting nil
    pricing_type nil
  end
end
