# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cart do
    factory :cart_with_items do
      ignore do
        items_count 5
      end

      after(:create) do |cart, evaluator|
        FactoryGirl.create_list(:cart_item, evaluator.items_count, cart_id: cart.id)
      end
    end
  end
end
