# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :referral_code_order do
    referral_code nil
    order nil
  end
end
