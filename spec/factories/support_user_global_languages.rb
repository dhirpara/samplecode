# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :support_user_global_language, :class => 'SupportUserGlobalLanguages' do
    support_user nil
    global_language nil
  end
end
