# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :age_group do
    name_of_group "MyString"
    range_of_group "MyString"
  end
end
