# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :country do
    name "Canada"
    abbr "CA"
    currency_id { (Currency.find_by_name("CAD") || create(:currency, name: "CAD")).id }

    factory :canada
    factory :usa do
      name "United States"
      abbr "US"
      currency_id { (Currency.find_by_name("USD") || create(:currency, name: "USD")).id }

      factory :norway do
        name "Norway"
        abbr "NO"
      end
    end
  end
end
