# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :referral_code do
    code "asd"
    percentage false
    start_date "2015-03-13"
    end_date "2015-03-13"
  end
end
