# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :variant do
    unit "unit"
    price "9.99"
    product
  end
end
