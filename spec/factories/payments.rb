# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment do
    order_id { create(:order).id }
    amount "9.99"
    received true

    factory :card_payment do
      message "Approved"
      trn_id "000001"
      trn_type "P"
      payment_type_id { PaymentType.find_by_name("Visa").id }
    end

    factory :non_card_payment do
      payment_type_id { PaymentType.find_by_name("Promotional").id }
    end
  end
end
