FactoryGirl.define do
  factory :shipping_carrier do
    name Faker::Name.name
  end
end
