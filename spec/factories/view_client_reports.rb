# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :view_client_report do
    client nil
    date "2014-08-06 09:56:17"
    support_user nil
  end
end
