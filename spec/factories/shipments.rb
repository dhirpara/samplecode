FactoryGirl.define do
  factory :shipment do
    order
    state "pending"
  end
end
