# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cart_item do
    quantity 1
    association :cartable, factory: :product
  end
end
