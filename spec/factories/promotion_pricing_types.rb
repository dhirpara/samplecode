# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :promotion_pricing_type do
    promotion_id 1
    promotion_type "MyString"
    pricing_type_id nil
  end
end
