# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :shipping_promotion do
    min_amount "9.99"
    start_date "2013-08-21"
    end_date "2013-08-21"
    amount_off "9.99"
  end
end
