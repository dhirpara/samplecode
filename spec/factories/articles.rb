# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :article do
    title "article"
    content "content"
    author "liang"
    article_category
  end
end
