FactoryGirl.define do
  factory :shipping_method do
    name Faker::Name.name
    shipping_carrier
  end
end
