# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :support_user_role do
    support_user_id 1
    role_id 1
  end
end
