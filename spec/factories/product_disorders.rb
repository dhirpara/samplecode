# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_disorder do
    disorder
    product
  end
end
