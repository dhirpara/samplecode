# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :commission_rate do
    min 1
    max 9999
    rate 10
    support_user
  end
end
