# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    sequence(:name) {|n| "Product#{n}" }
    weight_lbs 0.5
    quantity 20
    active true
    allow_order_online true
    product_category

    ignore do
      have_product_caselot false
      #"Retail", "Staff", "Health Professionals", "Shareholder", "Wholesale 1", "Wholesale 2", "Wholesale 3"
      pricing_types [{ name: "Retail", available: true, price: 100}]
    end

    after(:create) do |product, evaluator|
      currency1 = Currency.find_by_name("CAD") || Currency.create(name: "CAD")
      currency2 = Currency.find_by_name("USD") || Currency.create(name: "USD")
      evaluator.pricing_types.each do |hash|
        pricing_type = PricingType.find_by_name(hash[:name]) || PricingType.create(name: hash[:name])
        product_pricing_type = build(:product_pricing_type, pricing_type_id: pricing_type.id, available: hash[:available])
        product_pricing_type.product_prices << build(:product_price, currency_id: currency1.id, price: hash[:price] || 100)
        product_pricing_type.product_prices << build(:product_price, currency_id: currency2.id, price: (hash[:price] || 100) * 2)
        product.product_pricing_types << product_pricing_type
      end

      if evaluator.have_product_caselot
        product_caselot = product.product_caselots.build(count: 5)
        product_pricing_type = build(:product_pricing_type, pricing_type_id: (PricingType.find_by_name("Retail") || PricingType.create(name: "Retail")).id, available: true)
        product_pricing_type.product_prices << build(:product_price, currency_id: currency1.id, price: 80)
        product_pricing_type.product_prices << build(:product_price, currency_id: currency2.id, price: 160)
        product_caselot.product_pricing_types << product_pricing_type
        product_caselot.save
      end
    end
  end
end
