FactoryGirl.define do
  factory :client do
    #sequence(:email) {|n| "#{n}@gmail.com" }
    first_name "first name"
    last_name "last name"
    password '12345678'
    password_confirmation '12345678'
    sequence(:user_name) {|n| "client#{n}" }
    country_id { (Country.find_by_abbr("CA") || create(:country)).id }
    support_center
    home_phone 1234567
    support_user_id { create(:support_user).id }
    introduction_type_id { create(:introduction_type).id }

    factory :guest do
      last_name "last name"
      email "guest@mail.com"
      guest true
    end

    before(:create) do |client, evaluator|
      PricingType.find_by_name("Retail") || PricingType.create(name: "Retail")
    end
  end
end
