# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :country_shipping_adjustment do
    country_id { FactoryGirl.create(:country).id }
    amount 10
  end
end
