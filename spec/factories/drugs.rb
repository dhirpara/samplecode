# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :drug do
    dlid 1
    label "MyText"
    amount 1.5
    unit "MyString"
    comments "MyText"
    previous "MyString"
    date_stopped "2013-07-31 09:20:22"
    ssid 1
    deleted false
    ssid_deleted 1
    delete_date "2013-07-31 09:20:22"
  end
end
