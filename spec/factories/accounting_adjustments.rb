# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :accounting_adjustment do
    adjustable_id 1
    adjustable_type "MyString"
    amount "9.99"
    note "MyString"
  end
end
