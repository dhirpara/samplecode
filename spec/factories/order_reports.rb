# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order_report do
    order_id 1
    products_price "MyString"
    tax "MyString"
    shipping "MyString"
    handling "MyString"
    other "MyString"
    total "MyString"
    received_total "MyString"
  end
end
