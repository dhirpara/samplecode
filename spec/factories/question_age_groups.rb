# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :question_age_group do
    question nil
    age_group nil
  end
end
