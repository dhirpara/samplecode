# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :auto_ship_report do
    client nil
    country nil
    email "MyString"
    date_of_shipment "2015-02-16"
    interval 1
    state "MyString"
    total "9.99"
  end
end
