# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :commission_report do
    order nil
    support_user nil
    date "2014-07-22"
    amount "9.99"
    rate 1
  end
end
