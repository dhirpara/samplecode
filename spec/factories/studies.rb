# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :study do
    title "MyString"
    image "MyString"
    pdf "MyString"
    link "MyString"
  end
end
