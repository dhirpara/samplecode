# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :referral_code_product_promotion, :class => 'ReferralCodeProductPromotions' do
    referral_code nil
    product_promotion nil
  end
end
