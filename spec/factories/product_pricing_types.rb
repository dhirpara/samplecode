# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_pricing_type do
    pricing_type_id 1
    priceable_id 1
    priceable_type "MyString"
    available false
  end
end
