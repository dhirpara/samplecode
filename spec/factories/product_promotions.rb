# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_promotion do
    start_date 10.day.ago
    end_date 10.day.from_now
  end
end
