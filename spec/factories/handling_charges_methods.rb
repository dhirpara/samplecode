FactoryGirl.define do
  factory :handling_charge_method do
    name "Range based"
    active true
  end
end
