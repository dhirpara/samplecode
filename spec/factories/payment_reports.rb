# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment_report do
    order_id 1
    support_user_id 1
    payment_type_id 1
    amount "MyString"
    note "MyText"
  end
end
