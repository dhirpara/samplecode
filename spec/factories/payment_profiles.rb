# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment_profile do
    customer_code "MyString"
    last_digits "MyString"
    client_id 1
  end
end
