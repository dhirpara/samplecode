# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :auto_shipping_item do
    product product
    auto_shipping auto_shipping
    quantity 1
  end
end
