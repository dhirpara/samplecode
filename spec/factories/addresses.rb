FactoryGirl.define do
  factory :address do
    name Faker::Name.name
    address1 Faker::Address.street_address
    address2 Faker::Address.street_address
    phone Faker::PhoneNumber.phone_number
    state "AB"
    postal_code "T2N 2A1"
    city "city"
    active true
    country_id { (Country.find_by_abbr("CA") || FactoryGirl.create(:canada)).id }

    factory :canadian_address do
      city 'Calgary'
      state 'AB'
    end
    factory :american_address do
      country_id { (Country.find_by_abbr("US") || FactoryGirl.create(:usa)).id }
      address1 "Brenda Huang"
      city "Tallahassee"
      state "FL"
      postal_code "32312"
    end
    factory :norway_address do
      country_id { FactoryGirl.create(:norway).id }
      city "town"
      state "Trondheim"
      postal_code "7321"
    end
    factory :address_with_foreign_key do
      country_id { FactoryGirl.create(:country).id }
    end
  end
end
