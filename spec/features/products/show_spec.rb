require 'spec_helper'

describe "show page" do
  let(:product) { create(:product) }

  before do
    create_initial_data
  end

  it do
    visit product_url(product)
  end
end
