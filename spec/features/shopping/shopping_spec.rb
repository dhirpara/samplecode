require 'spec_helper'
#require 'features/shopping/address_other_scene_spec.rb'
#require 'features/shopping_shared/payment_spec.rb'

describe "create order" do
  let(:pre_order_product) { false } 
  let(:product) { create(:product, pre_order: pre_order_product ) }
  let(:client) { create :client, active: true, country_id: canada.id, support_center: create(:support_center, country: canada) }
  let(:canada) { Country.find_by_abbr("CA") }
  before do
    create_initial_data
    product

    client.support_user.update_attributes(receive_commission: true)
    create(:commission_rate, support_user: client.support_user, commission_type: CommissionType.find_by_code("NMB"))

    client.addresses << create(:address, state: "AB", country_id: canada.id)
  end

  context "add product to cart" do
    before do
      visit "/products/#{product.id}"

      fill_in "cart_item[quantity]", with: 1
      if product.pre_order?
        click_on "PRE-ORDER NOW"
      else
        click_on "ADD TO CART"
      end
    end

    it "redirect to correct url"  do
      current_path.should == "/shopping/cart"
    end

    context "checkout" do
      before do
        click_on("Checkout")
      end

      it "redirect to correct url"  do
        current_path.should == "/clients/sign_in"
      end

      context "login as guest" do
        before do
          fill_in "guest_form[first_name]", with: "first name"
          fill_in "guest_form[last_name]", with: "last name"
          fill_in "guest_form[email]", with: "guest@mail.com"
          select canada.name, from: "guest_form[country_id]"
          select IntroductionType.first.name, from: "guest_form[introduction_type_id]"
          click_on("CONTINUE")
        end

        it "redirect correct page" do
          current_path.should == "/clients/newsletter"
        end

        context "click continute" do
          before do
            click_on("Go to shopping cart without subscribing")
            #click_on("Continue")
          end

          it "redirect correct page" do
            current_path.should == "/shopping/cart"
          end

          context "click checkout" do
            before do
              click_on("Checkout")
            end

            it "redirect correct page" do
              current_path.should == "/shopping/addresses"
            end
          end
        end

        #include_context "other scene for address"
      end

      context "sign up" do
        before do
          click_on("Sign up")

          fill_in "client[user_name]", with: "client username"
          fill_in "client[first_name]", with: "first name"
          fill_in "client[last_name]", with: "last name"
          fill_in "client[password]", with: "12345678"
          fill_in "client[password_confirmation]", with: "12345678"
          select canada.name, from: "client[country_id]"
          select IntroductionType.first.name, from: "client[introduction_type_id]"
          fill_in "client[home_phone]", with: "12345678"

          click_on("Sign up")
        end

        it "redirect correct page" do
          current_path.should == "/clients/newsletter"
        end

        context "click shopping cart" do
          before do
            click_on("Go to shopping cart without subscribing")
          end

          it "redirect correct page" do
            current_path.should == "/shopping/cart"
          end

          context "click checkout" do
            before do
              click_on("Checkout")
            end

            it "redirect correct page" do
              current_path.should == "/shopping/addresses"
            end
          end

        end
      end

      context "login client" do
        before do
          within("form#new_client") do
            fill_in "client[user_name]", with: client.user_name
            fill_in "client[password]", with: client.password
            click_on("SIGN IN")
          end
        end

        it "redirect correct page" do
          current_path.should == "/shopping/addresses"
        end

        #include_context "other scene for address"

        context "Submit form" do
          let(:order) { client.orders.first }
          before do
            within('form#new_address') do
              fill_in "address_name", with: "lmc"
              fill_in "address_address1", with: "address1"
              fill_in "address_address2", with: "address2"
              fill_in "address_city", with: "city"
              select "Canada", from: "address_country_id"
              select "Alberta", from: "address_state"
              fill_in "address_postal_code", with: "60001"
              fill_in "address_phone", with: "1234567"
              click_on "Submit"
            end
          end

          it "created address" do
            address = order.shipment.address
            address.name.should == "lmc"
            address.address1.should == "address1"
            address.address2.should == "address2"
            address.city.should == "city"
            address.postal_code.should == "60001"
            address.phone.should == "1234567"
          end
        end


        context "use address" do
          let(:order) { client.orders.first }

          before do
            click_on("Use")
          end

          it "created address" do
            address = order.shipment.address
            address.name.should          == client.addresses.first.name
            address.address1.should      == client.addresses.first.address1
            address.address2.should      == client.addresses.first.address2
            address.city.should          == client.addresses.first.city
            address.postal_code.should   == client.addresses.first.postal_code
            address.phone.should         == client.addresses.first.phone
          end

          context "select standard shipping methods" do
            before do
              choose("shipping_method_id_#{ShippingMethod.find_by_name("Landmark Standard").id}")
              click_button("CONTINUE")
            end

            it "g shipping method" do
              order.reload.shipment.shipping_method.name.should == "Landmark Standard"
            end

            #include_context "other scene for payment"

            context "submit payment form" do
              before do
                fill_in "payment_new_form[address][address1]", with: "address1"
                fill_in "payment_new_form[address][address2]", with: "address2"
                fill_in "payment_new_form[address][city]", with: "city"
                fill_in "payment_new_form[address][postal_code]", with: "T1Y 3Z5"
                fill_in "payment_new_form[address][phone]", with: "1234567"
                select "Alberta", from: "payment_new_form[address][state]"
                fill_in "payment_new_form[credit_card][first_name]", with: "first name"
                fill_in "payment_new_form[credit_card][last_name]", with: "last name"
                fill_in "payment_new_form[credit_card][number]", with: "4030000010001234"
                fill_in "payment_new_form[credit_card][verification_value]", with: "123"
                select(Time.zone.now.year + 1, from: 'payment_new_form[credit_card][year]')
                click_button("Submit Payment")
              end

              it do
                order.payments.count.should == 1
                CommissionReport.count.should == 1
              end

              context "with pre order" do
                let(:pre_order_product) { true }

                it do
                  client.payment_profiles.count.should == 1
                  order.payments.count.should == 0
                end
              end
            end
          end
        end
      end
    end
  end
end
