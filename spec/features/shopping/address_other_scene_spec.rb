shared_context "other scene for address" do
  context "Submit form without fill" do
    before do
      click_on "SUBMIT"
    end

    it "get error message" do
      page.should have_content "Address1 can't be blank"
      page.should have_content "City can't be blank"
      page.should have_content "State can't be blank"
      page.should have_content "Postal code can't be blank"
      page.should have_content "Phone can't be blank"
      page.should have_content "Phone is too short (minimum is 7 characters)"

      current_path.should == "/shopping/addresses"
    end
  end
end
