require 'spec_helper'

describe "show page" do
  let(:video) { create(:video) }

  before do
    create(:article_category, name: "Clinical Reference")
    create(:article_category, name: "Our Vision")
  end

  it do
    visit "/videos/#{video.id}"
  end
end
