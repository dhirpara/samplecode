require 'spec_helper'

describe "ta level" do
  let(:support_user) do
    support_user = create :support_user
  end

  before do
    login_as support_user, scope: :support_user
  end

  context "index" do
    before do
      visit "/admin/configuration/ta_levels"
    end

    context "go to new page" do
      before do
        click_on "Add New Ta Level"
      end

      it "redirect to correct page" do
        current_path.should == "/admin/configuration/ta_levels/new"
      end

      context "submit with invalid form" do
        before do
          click_on "Create Ta level"
        end

        it "not g resource" do
          TaLevel.count.should == 0
        end

        it "show error message" do
          page.should have_content "Name can't be blank"
        end
      end

      context "submit form" do
        before do
          fill_in "ta_level[name]", with: "ta level"

          click_on "Create Ta level"
        end

        it "g resource" do
          TaLevel.count.should == 1
        end

        it "redirect to correct page" do
          current_path.should == "/admin/configuration/ta_levels"
        end

        context "go to edit page" do
          let(:ta_level) { TaLevel.first }
          before do
            click_on "Edit"
          end

          it "redirect to correct page" do
            current_path.should == "/admin/configuration/ta_levels/#{ta_level.id}/edit"
          end

          context "click delete" do
            before do
              click_on "Delete"
            end

            it "not deleted resource" do
              TaLevel.count.should == 1
            end

            it "show message" do
              page.should have_content "Cannot delete record because the record is not marked as deleted"
            end
          end

          context "update resource to deleted" do
            before do
              ta_level.update_attributes(deleted: true)
            end

            context "click delete" do
              before do
                click_on "Delete"
              end

              it "deleted resource" do
                TaLevel.count.should == 0
              end
            end

            context "set related with client" do
              before do
                create(:client, ta_level_id: ta_level.id)
              end

              context "click delete" do
                before do
                  click_on "Delete"
                end

                it "not deleted resource" do
                  TaLevel.count.should == 1
                end

                it "show message" do
                  page.should have_content "Cannot delete record because of dependent clients"
                end
              end
            end
          end
        end
      end
    end
  end
end
