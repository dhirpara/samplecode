require 'spec_helper'

describe "Call list" do
  let(:support_user) { support_user = create :support_user }

  before do
    login_as support_user, scope: :support_user
  end

  context "index" do
    before do
      create :client, next_contact_date: Date.yesterday
      visit "/admin/report/call_lists"
    end

    context "click filter" do
      before do
        click_on "filter"
      end
      
      it "response 200" do
        page.status_code.should == 200
      end
    end
  end
end
