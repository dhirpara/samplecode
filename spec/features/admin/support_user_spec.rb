require 'spec_helper'

describe "support user CRUD" do
  let(:support_user) { create :support_user }

  before do
    login_as support_user, scope: :support_user
  end

  context "index" do
    before do
      visit "/admin/support_users"
    end

    context "go to new page" do
      before do
        click_on "Add New Support User"
      end

      it "redirect to correct page" do
        current_path.should == "/admin/support_users/new"
      end

      context "submit with invalid form" do
        before do
          click_on "Create Support user"
        end

        it "not g resource" do
          SupportUser.count.should == 1
        end

        it "show error message" do
          page.should have_content "Email can't be blank"
          page.should have_content "Password can't be blank"
          page.should have_content "User name can't be blank"
          page.should have_content "First name can't be blank"
          page.should have_content "Last name can't be blank"
        end
      end

      context "submit form" do
        before do
          fill_in "support_user[email]", with: "lmc@andertec.ca"
          fill_in "support_user[password]", with: "12345678"
          fill_in "support_user[password_confirmation]", with: "12345678"
          fill_in "support_user[user_name]", with: "lmc"
          fill_in "support_user[first_name]", with: "liang"
          fill_in "support_user[last_name]", with: "mc"
          check('support_user[active]')


          click_on "Create Support user"
        end

        it "g resource" do
          SupportUser.count.should == 2
        end

        it "redirect to correct page" do
          current_path.should == "/admin/support_users"
        end

        context "go to edit page" do
          let(:new_support_user) { SupportUser.order("id").last }
          before do
            visit "/admin/support_users/#{new_support_user.id}/edit"
          end

          it "redirect to correct page" do
            current_path.should == "/admin/support_users/#{new_support_user.id}/edit"
          end

          context "submit form" do
            before do
              click_on "Update Support user"
            end

            it "redirect to correct page" do
              current_path.should == "/admin/support_users"
            end
          end
        end
      end
    end
  end
end
