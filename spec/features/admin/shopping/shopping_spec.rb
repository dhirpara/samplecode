require 'spec_helper'
require 'features/admin/shopping/address_other_scene_spec.rb'
require 'features/admin/shopping/payment_other_scene_spec.rb'
require 'features/shopping_shared/payment_spec.rb'

describe "create order" do
  let(:support_center) { create :support_center, country: canada }
  let(:support_user) do
    support_user = create :support_user
    support_center.support_users << support_user
    support_user
  end
  let(:client) { create :client, support_center_id: support_center.id, active: true, country: canada }
  let(:canada) { Country.find_by_abbr("CA") }


  #生成项目基本数据
  let(:s_order_type) { OrderType.find_by_code("S") }
  let(:product) { create(:product) }
  before do
    create_initial_data
    product

    client.support_user.update_attributes(receive_commission: true)
    create(:commission_rate, support_user: client.support_user, commission_type: CommissionType.find_by_code("NMB"))

    client.addresses << create(:address, state: "AB", country: canada)
  end

  before do
    login_as support_user, scope: :support_user
  end

  it "not see create order link" do
    visit "/admin/orders"
    page.should_not have_link("Create New Order")
  end

  #skip_to_select_client
  #选中了client
  context "when select client" do
    before do
      visit "/admin/clients"
      within(".admin>form") do
        click_on("Search")
      end

      click_link("Select")

      current_path.should == "/admin/orders"
    end

    it "see create order link"  do
      visit "/admin/orders"
      page.should have_link("Create New Order")
    end

    context "when click create new order button" do
      let(:order) { client.orders.first }
      before do
        click_link("Create New Order")
      end

      it "g order" do
        order.order_type.should == s_order_type
        order.state.should == "cart"
        order.support_user.should == support_user

        current_path.should == "/admin/orders/#{order.id}/order_items"
      end

      context "cancel order" do
        before do
          click_link("Cancel Order")
        end

        it "changed order" do
          order.reload.state.should == "canceled"
        end

        it "changed page" do
          page.should_not have_link("Cancel Order")
          page.should_not have_selector("form#add_item_form")
        end

        context "visit shipment page" do
          before do
            click_link("Shipments")
          end

          it "redirect to back" do
            current_path.should == "/admin/orders/#{order.id}/order_items"
          end

          it "show message" do
            page.should have_content(I18n.t("canceled_order"))
          end
        end
      end

      #skip_to_add_item
      context "add order_item" do
        let(:order_item) { order.reload.order_items.first }
        before do
          page.driver.post("/admin/orders/#{order.id}/order_items", order_item: { product_id: product.id, quantity: 1 })
          #fill_in 'product_name', with: product.name
          #sleep(1)
          #page.execute_script "$('.ui-menu-item a:first').trigger(\"mouseenter\").click();"
          #click_button "ADD"
          #sleep(0.5)
        end

        it "g order_item" do
          order_item.price.should == 100
          order_item.pricing_type_id.should == client.pricing_type_id
          order_item.product_id.should == product.id
          order_item.quantity.should == 1
        end

        #skip_to_update_currency
        #context "update currency" do
          #before do
            #visit "/admin/orders/#{order.id}/order_items"
            #within(".edit_order") do
              #select('USD', from: 'order[currency_id]')
            #end

            #click_button("Update Currency")
          #end

          #it "update order" do
            #order.reload.currency.name.should == "USD"
          #end

          #it "updated order_items" do
            #order.order_items.each do |item|
              #item.price.should == 200
            #end
          #end
        #end

        #skip_to_remove_item
        context "remove order_item" do
          before do
            click_link("DELETE")
          end

          it "remove order_item" do
            order.reload.order_items.count.should == 0
          end
        end

        #skip_to_visit_shipment_page
        context "visit shipment page" do
          before do
            visit "/admin/orders/#{order.id}/shipments"
          end

          it "update order state" do
            order.reload.state.should == "address"
          end

          it "show correct form" do
            page.has_css?('form#new_address').should == true
          end

          it "created shipment" do
            order.shipment.present?.should == true
          end

          include_context "other scene for address"

          context "with completion form" do
            let(:address) { order.reload.shipment.address }
            before do
              within('form#new_address') do
                fill_in "address_name", with: "lmc"
                fill_in "address_address1", with: "address1"
                fill_in "address_address2", with: "address2"
                fill_in "address_city", with: "city"
                select "Canada", from: "address_country_id"
                select "Alberta", from: "address_state"
                fill_in "address_postal_code", with: "60001"
                fill_in "address_phone", with: "1234567"
                click_on "Submit"
              end
            end

            it "created address" do
              address.name.should == "lmc"
              address.address1.should == "address1"
              address.address2.should == "address2"
              address.city.should == "city"
              address.postal_code.should == "60001"
              address.phone.should == "1234567"

              #visit correct page
              current_path.should == "/admin/orders/#{order.id}/shipments"

              #change order adjustmnet
              order.db_tax_charge.should == 5
            end
          end

          context "with select address" do
            let(:address) { order.reload.shipment.address }
            before do
              click_on("Use")
            end

            include_context "other scene for after create address"

            it "created address" do
              address.name.should          == client.addresses.first.name
              address.address1.should      == client.addresses.first.address1
              address.address2.should      == client.addresses.first.address2
              address.city.should          == client.addresses.first.city
              address.postal_code.should   == client.addresses.first.postal_code
              address.phone.should         == client.addresses.first.phone
            end

            it "visit correct page" do
              current_path.should == "/admin/orders/#{order.id}/shipments"
            end

            context "select standard shipping methods" do
              let(:shipping_method) { order.reload.shipment.shipping_method }
              before do
                choose("shipping_method_id_#{ShippingMethod.find_by_name("Landmark Standard").id}")
                click_button("CONTINUE")
              end

              it "g shipping method" do
                shipping_method.name.should == "Landmark Standard"
              end

              context "cancel order" do
                before do
                  visit "/admin/orders/#{order.id}/shipments"
                  click_link("Cancel Order")
                end

                it "changed order" do
                  order.reload.state.should == "canceled"
                end

                it "changed page" do
                  page.should_not have_link("EDIT")
                  page.should_not have_selector("form.edit_order")
                end
              end

              context "select express shipping methods" do
                #TODO
              end

              context "visit adjustments page" do
                before do
                  click_link("Adjustments")
                end

                context "click new" do
                  before do
                    click_link("New")
                  end

                  it "go to adjustment new page" do
                    current_path.should == "/admin/orders/#{order.id}/order_adjustments/new"
                  end

                  context "create adjustment" do
                    let(:order_adjustment) { order.order_adjustments.last }
                    before do
                      within("form#new_order_adjustment")do
                        fill_in "order_adjustment[amount]", with: 99.11
                        fill_in "order_adjustment[note]", with: "test note"
                        click_on "Create Order adjustment"
                      end
                    end

                    it "created adjustment" do
                      order_adjustment.amount.should == 99.11
                      order_adjustment.note.should == "test note"
                    end

                    context "go to edit adjustment" do
                      before do
                        visit "/admin/orders/#{order.id}/order_adjustments/#{order_adjustment.id}/edit"
                      end

                      it "go to adjustment new page" do
                        current_path.should == "/admin/orders/#{order.id}/order_adjustments/#{order_adjustment.id}/edit"
                      end

                      context "update adjustment" do
                        before do
                          within("form.edit_order_adjustment")do
                            fill_in "order_adjustment[amount]", with: 98.11
                            fill_in "order_adjustment[note]", with: "update note"
                            click_on "Update Order adjustment"
                          end
                        end

                        it "updated adjustment" do
                          order_adjustment.reload.amount.should == 98.11
                          order_adjustment.reload.note.should == "update note"
                        end
                      end

                      context "delete adjustment" do
                        before do
                          within("table tr:eq(1)") do
                            click_link("DELETE")
                          end

                          it "deleted adjustment" do
                            order.order_adjustments.count.should == 3
                          end
                        end
                      end
                    end
                  end
                end

                context "cancel order" do
                  before do
                    visit "/admin/orders/#{order.id}/order_adjustments"
                    click_link("Cancel Order")
                  end

                  it "changed order" do
                    order.reload.state.should == "canceled"
                  end

                  it "changed page" do
                    page.should_not have_selector("form.edit_order")
                    #page.should_not have_link("New")
                    within("table") do
                      page.should_not have_link("EDIT")
                      page.should_not have_link("DELETE")
                    end
                  end
                end
              end

              include_context "other scene for payment"

              context "visit payments page" do
                before do
                  click_link("Payments")
                end

                it "get correct page" do
                  current_path.should == "/admin/orders/#{order.id}/payments"
                end

                context "cancel order" do
                  before do
                    click_link("Cancel Order")
                  end

                  it "changed order" do
                    order.reload.state.should == "canceled"
                  end

                  it "changed page" do
                    page.should_not have_selector("form.edit_order")
                    page.should_not have_link("Add New Payment")
                  end
                end

                context "go to new payment" do
                  before do
                    click_link("Add New Payment")
                  end

                  it "get correct page" do
                    current_path.should == "/admin/orders/#{order.id}/payments/new"
                  end

                  context "created payment" do
                    before do
                      fill_in "payment_new_form[payment][note]", with: "payment note"
                      fill_in "payment_new_form[address][address1]", with: "address1"
                      fill_in "payment_new_form[address][address2]", with: "address2"
                      fill_in "payment_new_form[address][city]", with: "city"
                      fill_in "payment_new_form[address][postal_code]", with: "T1Y 3Z5"
                      fill_in "payment_new_form[address][phone]", with: "1234567"
                      select "Alberta", from: "payment_new_form[address][state]"
                      fill_in "payment_new_form[credit_card][first_name]", with: "first name"
                      fill_in "payment_new_form[credit_card][last_name]", with: "last name"
                      fill_in "payment_new_form[credit_card][number]", with: "4030000010001234"
                      fill_in "payment_new_form[credit_card][verification_value]", with: "123"
                      select(Time.zone.now.year + 1, from: 'payment_new_form[credit_card][year]')
                      click_button("Submit Payment")
                    end

                    it "created payment profile" do
                      payment_profile = client.reload.payment_profiles.last
                      payment_profile.customer_code.should_not == nil
                      payment_profile.address1.should == "address1"
                      payment_profile.address2.should == "address2"
                      payment_profile.city.should == "city"
                      payment_profile.postal_code.should == "T1Y 3Z5"
                      payment_profile.phone.should == "1234567"
                      payment_profile.name.should == "first name last name"
                      payment_profile.state.should == "AB"
                      payment_profile.country.should == canada
                    end


                    it do
                      CommissionReport.count.should == 1
                    end

                    shared_examples "refund and void credit card payment" do
                      context "go to refund page" do
                        before do
                          click_link("refund")
                        end

                        it "get correct page" do
                          payment = order.payments.first
                          current_path.should == "/admin/orders/#{order.id}/payments/#{payment.id}/new_refund"
                        end

                        #context "click create payment" do
                          #before do
                            #click_on("Create Payment")
                          #end

                          #it "created refund payment" do
                            #order.payments.count.should == 2
                          #end

                          #context "click void" do
                            #before do
                              #click_on("void")
                            #end

                            #it "created void payment" do
                              #order.payments.count.should == 3
                            #end
                          #end
                        #end
                      end

                      context "click void" do
                        before do
                          click_on("void")
                        end

                        it "created void payment" do
                          order.payments.count.should == 2
                        end
                      end
                    end

                    it_behaves_like "refund and void credit card payment"

                    
                    context "ready order" do
                      before do
                        click_on("Ship Order")
                      end

                      it do
                        order.reload.shipment.state.should == "ready"
                      end

                      context "ship order" do
                        before do
                          order.reload.shipment.ship
                        end

                        it do
                          order.shipment.state.should == "shipped"
                        end
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
