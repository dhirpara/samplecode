shared_context "other scene for address" do

  context "Submit form without fill" do
    before do
      click_on "Submit"
    end

    it "get error message" do
      page.should have_content "Address1 can't be blank"
      page.should have_content "City can't be blank"
      page.should have_content "State can't be blank"
      page.should have_content "Postal code can't be blank"
      page.should have_content "Phone can't be blank"
      page.should have_content "Phone is too short (minimum is 7 characters)"

      current_path.should == "/admin/orders/#{order.id}/shipments"
    end

    context "cancel order" do
      before do
        click_link("Cancel Order")
      end

      it "changed order" do
        order.reload.state.should == "canceled"
      end

      it "changed page" do
        page.should_not have_button("Use")
        page.should_not have_selector("form.edit_order")
        page.should_not have_selector("form#new_address")
      end
    end
  end
end

shared_context "other scene for after create address" do
  context "click EDIT" do
    before do
      click_link "EDIT"
    end

    it "visit correct page" do
      current_path.should == "/admin/orders/#{order.id}/shipments/edit"
    end
  end

  context "cancel order" do
    before do
      click_link("Cancel Order")
    end

    it "changed order" do
      order.reload.state.should == "canceled"
    end

    it "changed page" do
      page.should_not have_link("EDIT")
      page.should_not have_selector("form.edit_order")
      page.should_not have_selector(".shippingAddress form")
    end
  end
end
