shared_context "other scene for payment" do
  context "with empty payment type payment" do
    before do
      create(:card_payment, order: order, message: "Authorization Failed", confirmation_code: ";;", payment_type_id: nil)
      click_link("Payments")
    end

    it "get correct page" do
      current_path.should == "/admin/orders/#{order.id}/payments"
    end
  end

  context "success payment with have payment profile" do
    before do
      create(:complete_order, client: client, pay_for_cc: true)
      client.reload.payment_profiles.any?.should == true
      click_link("Payments")
      click_link("Add New Payment")

      choose("payment_new_form_payment_profile_id_")
      fill_in "payment_new_form[address][address1]", with: "address1 update"
      fill_in "payment_new_form[address][address2]", with: "address2 update"
      fill_in "payment_new_form[address][city]", with: "city update"
      fill_in "payment_new_form[address][postal_code]", with: "T1Y 3Z5 update"
      fill_in "payment_new_form[address][phone]", with: "1234567 update"
      select "Alberta", from: "payment_new_form[address][state]"
      fill_in "payment_new_form[credit_card][first_name]", with: "first name update"
      fill_in "payment_new_form[credit_card][last_name]", with: "last name update"
      fill_in "payment_new_form[credit_card][number]", with: "4030000010001234"
      fill_in "payment_new_form[credit_card][verification_value]", with: "123"
      select(Time.zone.now.year + 1, from: 'payment_new_form[credit_card][year]')
      click_button("Submit Payment")
    end

    include_context "get correct result with not use payment profile"
  end
end
