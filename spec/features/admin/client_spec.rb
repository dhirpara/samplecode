require 'spec_helper'

describe "client CRUD" do
  let(:support_center) { create :support_center }
  let(:support_user) do
    support_user = create :support_user
    support_center.support_users << support_user
    support_user
  end


  before do
    create_initial_data
    login_as support_user, scope: :support_user
  end

  context "index" do
    before do
      visit "/admin/clients"
    end

    context "go to new page" do
      before do
        click_on "Add New client"
      end

      it "redirect to correct page" do
        current_path.should == "/admin/clients/new"
      end

      context "submit form" do
        before do
          fill_in "client[user_name]", with: "lmc"
          fill_in "client[first_name]", with: "liang"
          fill_in "client[last_name]", with: "last name"
          fill_in "client[password]", with: "11223344"
          fill_in "client[password_confirmation]", with: "11223344"
          select support_center.name, from: "client[support_center_id]"
          #select "Alberta", from: "client[province]"

          fill_in "client[province]", with: "AB"
          select Country.first.name, from: "client[country_id]"
          fill_in "client[address_1]", with: "address_1"
          fill_in "client[city]", with: "city"
          fill_in "client[postal_code]", with: "T1Y 3Z5"
          fill_in "client[home_phone]", with: "1234567"
          select IntroductionType.first.name, from: "client[introduction_type_id]"

          click_on "Create Client"
        end

        it "g client" do
          Client.count.should == 1
        end

        it "g address" do
          Client.first.addresses.count.should == 1
        end

        it "redirect to correct page" do
          current_path.should == "/admin/clients/#{Client.first.id}/edit"
        end
      end
    end
  end
end
