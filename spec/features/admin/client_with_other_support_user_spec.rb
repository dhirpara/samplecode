#require 'spec_helper'

#describe "client CRUD" do
  #let(:support_center) { create :support_center }

  #["BAS", "ORC"].each do |code|
    #context "with #{code} role" do
      #let(:support_user) do
        #support_user = create :support_user, roles: [create(:role, code: code)]
        #support_center.support_users << support_user
        #support_user
      #end

      #before do
        #login_as support_user, scope: :support_user
      #end

      #context "index" do
        #before do
          #visit "/admin/clients"
        #end

        #context "go to new page" do
          #before do
            #click_on "Add New client"
          #end

          #it "redirect to correct page" do
            #current_path.should == "/admin/clients/new"
          #end

          #it "can not select support user" do
            #page.has_select?('client[support_user_id]').should == false
          #end

          #context "submit with invalid form" do
            #before do
              #click_on "Create Client"
            #end

            #it "not g client" do
              #Client.count.should == 0
            #end

            #it "show error message" do
              #page.should have_content "Email can't be blank"
              #page.should have_content "Password can't be blank"
              #page.should have_content "User name can't be blank"
              #page.should have_content "Support center can't be blank"
              #page.should have_content "First name can't be blank"
              #page.should have_content "Address 1 can't be blank"
              #page.should have_content "City can't be blank"
              #page.should have_content "Province can't be blank"
              #page.should have_content "Postal code can't be blank"
              #page.should have_content "Cell phone is too short (minimum is 7 characters)"
            #end
          #end

          #context "submit form" do
            #before do
              #fill_in "client[email]", with: "lmc@andertec.ca"
              #fill_in "client[password]", with: "11223344"
              #fill_in "client[password_confirmation]", with: "11223344"
              #fill_in "client[user_name]", with: "lmc"
              #fill_in "client[first_name]", with: "liang"
              #fill_in "client[address_1]", with: "address 1"
              #fill_in "client[city]", with: "city"
              #fill_in "client[postal_code]", with: "123456"
              #fill_in "client[cell_phone]", with: "1234567"

              #select support_center.name, from: "client[support_center_id]"

              #click_on "Create Client"
            #end

            #it "g client" do
              #Client.count.should == 1
              #client = Client.first
              #client.support_user.should == support_user
            #end

            #it "redirect to correct page" do
              #current_path.should == "/admin/clients"
            #end

            #context "search client" do
              #let(:client) { Client.first }
              #before do
                #within(".filter form") do
                  #click_on "Search"
                #end
              #end

              #context "go to edit page" do
                #before do
                  #click_on "Edit"
                #end

                #it "redirect to correct page" do
                  #current_path.should == "/admin/clients/#{client.id}/edit"
                #end

                #context "submit form" do
                  #before do
                    #click_on "Update Client"
                  #end

                  #it "redirect to correct page" do
                    #current_path.should == "/admin/clients"
                  #end
                #end
              #end
            #end
          #end
        #end
      #end
    #end
  #end
#end
