require 'spec_helper'

describe "article CRUD" do
  let(:support_user) {  create :support_user }

  before do
    login_as support_user, scope: :support_user
  end

  context "index" do
    before do
      visit "/admin/articles"
    end

    context "go to new page" do
      before do
        click_on "New"
      end

      it "redirect to correct page" do
        current_path.should == "/admin/articles/new"
      end

      context "submit with invalid form" do
        before do
          click_on "Create Article"
        end

        it "not g article" do
          Article.count.should == 0
        end

        it "show error message" do
          page.should have_content "Title can't be blank"
          page.should have_content "Content can't be blank"
          page.should have_content "Author can't be blank"
        end
      end

      context "submit form" do
        before do
          fill_in "article[title]", with: "About Article"
          fill_in "article[content]", with: "content"
          fill_in "article[author]", with: "author"

          click_on "Create Article"
        end

        it "g article" do
          Article.count.should == 1
          article = Article.first
          article.permalink.should == "about-article"
        end

        it "redirect to correct page" do
          current_path.should == "/admin/articles"
        end

        context "go to edit page" do
          let(:article) { Article.first }
          before do
            click_on "Edit"
          end

          it "redirect to correct page" do
            current_path.should == "/admin/articles/#{article.id}/edit"
          end

          context "submit form" do
            before do
              fill_in "article[title]", with: "About Article 1"
              click_on "Update Article"
            end

            it "updated article" do
              article.permalink.should == "about-article-1"
            end

            it "redirect to correct page" do
              current_path.should == "/admin/articles"
            end
          end
        end
      end
    end
  end
end
