require 'spec_helper'

describe "auto ship" do
  let(:canada) { Country.find_by_abbr("CA") }
  let(:client) { create :client, active: true, country_id: canada.id, support_center: create(:support_center, country: canada) }

  before do
    create_initial_data

    login_as client, scope: :client

    visit "/myaccount/flexible_autoship"

    click_on("New Flexible Autoship Order")
    @auto_ship = AutoShipping.first
  end

  it do
    current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}"
  end

  context "add product" do
    before do
      click_on("Add/Edit Products")

      page.driver.post("/myaccount/flexible_autoship/#{@auto_ship.id}/order_items", auto_shipping_item: { product_id: create(:product).id, quantity: 1 })

      visit "/myaccount/flexible_autoship/#{@auto_ship.id}/order_items"
    end

    it do
      current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}/order_items"
    end

    context "back to auto ship detail page" do
      before do
        click_on "Back to Flexible Autoship Details"
      end

      it do
        current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}"
      end
    end

    context "click submit" do
      before do
        click_on("Submit")
      end

      it do
        current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}/address"
      end

      context "back to auto ship detail page" do
        before do
          click_on "Back to Flexible Autoship Details"
        end

        it do
          current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}"
        end
      end

      context "fill in address" do
        before do
          within('form#new_address') do
            fill_in "address_name", with: "lmc"
            fill_in "address_address1", with: "address1"
            fill_in "address_address2", with: "address2"
            fill_in "address_city", with: "city"
            select "Canada", from: "address_country_id"
            select "Alberta", from: "address_state"
            fill_in "address_postal_code", with: "60001"
            fill_in "address_phone", with: "1234567"
            click_on "Submit"
          end
        end

        it do
          current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}/shipping_method"
        end

        context "back to auto ship detail page" do
          before do
            click_on "Back to Flexible Autoship Details"
          end

          it do
            current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}"
          end
        end


        context "select shipping method" do
          before do
            choose("shipping_method_id_#{ShippingMethod.find_by_name("Landmark Standard").id}")
            click_button("Submit")
          end

          it do
            current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}/payment_profile"
          end

          context "back to auto ship detail page" do
            before do
              click_on "Back to Flexible Autoship Details"
            end

            it do
              current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}"
            end
          end

          context "fill in payment form" do
            before do
              fill_in "payment_new_form[address][address1]", with: "address1"
              fill_in "payment_new_form[address][address2]", with: "address2"
              fill_in "payment_new_form[address][city]", with: "city"
              fill_in "payment_new_form[address][postal_code]", with: "T1Y 3Z5"
              fill_in "payment_new_form[address][phone]", with: "1234567"
              select(Time.zone.now.year + 1, from: 'payment_new_form[credit_card][year]')
              fill_in "payment_new_form[credit_card][first_name]", with: "first name"
              fill_in "payment_new_form[credit_card][last_name]", with: "last name"
              fill_in "payment_new_form[credit_card][number]", with: "4030000010001234"
              fill_in "payment_new_form[credit_card][verification_value]", with: "123"
              select "Alberta", from: "payment_new_form[address][state]"
              click_button("Submit")
            end

            it do
              current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}"
            end

            context "fill in auto ship form" do
              before do
                fill_in "auto_shipping[email]", with: "test@andertec.ca"
                click_on "Save"
              end

              it do
                current_path.should == "/myaccount/flexible_autoship/#{@auto_ship.id}"
              end

              context "generate email" do
                it do
                  @auto_ship.reload
                  AutoShipMailer.remind(@auto_ship).deliver
                end
              end

              context "generate order" do
                before do
                  @auto_ship.reload.generate_order
                end

                it do
                  Order.count.should == 1
                end

                context "ship order" do
                  before do
                    @order = Order.first
                    @order.shipment.ready
                    @order.shipment.ship
                  end

                  it do
                    AutoShipReport.where(state: AutoShipReport::SHIPPED, order_id: @order.id).any?.should == true
                    AutoShipReport.where(state: AutoShipReport::PENDING, order_id: nil).any?.should == true
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
