require 'spec_helper'

describe "article" do
  let(:article_category) { ArticleCategory.find_by_name("Clinical Reference") }
  let(:article) { create(:article, article_category: article_category) }

  before do
    create_initial_data
  end

  context "index" do
    before do
      visit "/articles"
    end

    context "go to detail page" do
      before do
        visit "/article/#{article.article_category.permalink}/#{article.id}"
      end

      it "response 200" do
        page.status_code.should == 200
      end
    end
  end
end
