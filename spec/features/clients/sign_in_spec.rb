require 'spec_helper'

feature "client sign in" do
  let(:client) { create(:client) }

  before :each do
    create_initial_data
    client
  end

  it "signs me in" do
    visit '/clients/sign_in'

    within(".checkoutLogin") do
      fill_in 'User name', with: client.user_name
      fill_in 'Password', with: client.password
    end

    click_on 'SIGN IN'
    current_path.should eq '/myaccount/profile'
  end

  context "with client without home phone" do
    before do
      client.home_phone = nil
      client.save(validate: false)
    end

    it "signs me in" do
      visit '/clients/sign_in'

      within(".checkoutLogin") do
        fill_in 'User name', with: client.user_name
        fill_in 'Password', with: client.password
      end

      click_on 'SIGN IN'
      current_path.should eq '/myaccount/profile'
    end
  end
end
