#shared_context "get correct result for success payment first time" do
#  it "created payment profile" do
#    payment_profile = order.payments.first.payment_profile
#    payment_profile.customer_code.should_not == nil
#    payment_profile.address1.should == "address1"
#    payment_profile.address2.should == "address2"
#    payment_profile.city.should == "city"
#    payment_profile.postal_code.should == "T1Y 3Z5"
#    payment_profile.phone.should == "1234567"
#    payment_profile.name.should == "first name last name"
#    payment_profile.state.should == "AB"
#    payment_profile.country.should == canada
#  end
#end

shared_context "get correct result with not use payment profile" do
  it "changed" do
    payment_profile = client.reload.payment_profiles.last
    payment_profile.customer_code.should_not == nil
    payment_profile.address1.should == "address1 update"
    payment_profile.address2.should == "address2 update"
    payment_profile.city.should == "city update"
    payment_profile.postal_code.should == "T1Y 3Z5 update"
    payment_profile.phone.should == "1234567 update"
    payment_profile.name.should == "first name update last name update"
    payment_profile.state.should == "AB"
    payment_profile.country.should == canada
  end
end
