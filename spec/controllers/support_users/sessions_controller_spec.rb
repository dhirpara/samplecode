require 'spec_helper'

describe SupportUsers::SessionsController do
  describe ".create" do
    before do
      request.env["devise.mapping"] = Devise.mappings[:support_user]

      support_user = create(:support_user, active: false, encrypted_password: "")
      post :create, support_user: { user_name: support_user.user_name }
    end

    it do
      response.status.should == 302
    end
  end
end
