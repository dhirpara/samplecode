require 'spec_helper'

describe Client do
  describe "submit?" do
    let(:client) { build(:client, client_params.reverse_merge!({ address_1: "address 1", city: "city", province: "province", country_id: Country.first.id, postal_code: "T1Y 3Z5", home_phone: "1234567" })) }
    let(:options) { {} }
    let(:client_params) { {} }
    let(:generate_newsletters) { false }

    before do
      ActionMailer::Base.delivery_method = :test
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []

      create_initial_data
      Email.create(email: client.email, first_name: client.first_name) if generate_newsletters
      client.submit?(options)
    end

    it do
      Client.count.should == 1
      Client.first.addresses.count.should == 0
      ActionMailer::Base.deliveries.size.should == 1
    end

    context "with generate address" do
      let(:options) { { generate_address: true} }

      it do
        Client.first.addresses.count.should == 1
      end
    end

    context "with already has newsletter" do
      let(:generate_newsletters) { true }

      it do
        Email.count.should == 0
      end
    end

    context "with receive newsletter update" do
      let(:client_params_email) { "test@email.com" }
      let(:client_params) { { receive_update: "1", email: client_params_email } }

      it do
        Email.count.should == 1
        ActionMailer::Base.deliveries.size.should == 2
      end

      context "but email is blank" do
        let(:client_params_email) { "" }

        it do
          Client.count.should == 0
          ActionMailer::Base.deliveries.size.should == 0
        end
      end
    end
  end
end
