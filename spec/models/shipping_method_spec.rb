require 'spec_helper'

describe ShippingMethod do
  before do
    create_initial_data

    @usa = Country.find_by_abbr("US")
    @canada = Country.find_by_abbr("CA")
    @norway = Country.create(name: 'Norway', abbr: 'NO')
  end

  let(:american_address) { create :american_address, country_id: @usa.id }

  let(:canadian_address) { create :canadian_address, country_id: @canada.id }
  let(:norway_address) { create :norway_address, country_id: @norway.id }

  describe ".rate" do
    let(:shipment) { create :shipment, address: address, shipping_method: shipping_method }
    let(:address) { canadian_address }
    let(:shipping_method) { ShippingMethod.find_by_name("Landmark Standard") }

    before do
      ShippingMethod.should_receive(:shipping_rates).and_return([["Landmark Standard", 511.1111], ["Landmark Express", 1111.1111]])
    end

    it do
      ShippingMethod.rate(shipment).should == 5.11
    end
  end

  describe ".shipping_rates" do
    let(:shipment) { create :shipment, address: address, shipping_method: shipping_method }

    context "with Canadian address" do
      let(:address) { canadian_address }
      let(:shipping_method) { ShippingMethod.find_by_name("Landmark Standard") }

      it do
        ShippingMethod.shipping_rates(shipment).map{|r| r[0]}.should =~ ["Landmark Standard", "Landmark Express", "UPS"]
      end
    end

    context "with other country" do
      let(:address) { american_address }

      context "and FedEx shipping method" do
        let(:shipping_method) { ShippingMethod.find_by_name("FedEx Ground Home Delivery") }

        it do
          ShippingMethod.shipping_rates(shipment).map{|r| r[0]}.should include("FedEx Ground Home Delivery", "FedEx 2 Day", "FedEx Express Saver", "FedEx First Overnight")
        end
      end

      context "and USPS shipping method" do
        let(:shipping_method) { ShippingMethod.find_by_name("USPS Priority Mail 1-Day") }

        it do
          rate_names = ShippingMethod.shipping_rates(shipment).map{|r| r[0]}
          rate_names.should be_any {|n| n =~ /USPS Priority Mail/ }
          #rate_names.should be_any {|n| n =~ /USPS Priority Mail \d-Day/ }
          #rate_names.should be_any {|n| n =~ /USPS Priority Mail Express \d-Day/ }
        end
      end
    end
  end

  describe ".available_methods" do
    let(:address) { create(:address, country_id: @usa.id) }
    let(:get_rates_result) { [["FedEx Ground Home Delivery", 1000], ["FedEx 2 Day", 2000]] }
    before do
      @shipment = create(:shipment, address: address)
      @shipment.should_receive(:shipping_method_rates).and_return(get_rates_result)
      @f_g = ShippingMethod.find_by_name("FedEx Ground Home Delivery")
      @f_2_day = ShippingMethod.find_by_name("FedEx 2 Day")
      @u_p = ShippingMethod.find_by_name("USPS Priority Mail International")
    end

    it "get correct methods" do
      ShippingMethod.available_methods(@shipment).should =~ [@f_g, @f_2_day]
    end

    context "with price" do
      it do
        shippng_methods = ShippingMethod.available_methods(@shipment, with_price: true)
        shippng_methods.should =~ [@f_g, @f_2_day]
        shippng_methods[0].price.should == 10
        shippng_methods[1].price.should == 20
      end
    end

    context "with other country" do
      let(:address) { create(:address, country_id: @norway.id) }
      let(:get_rates_result) { [["USPS Priority Mail International", 1000]] }

      it "get correct methods" do
        ShippingMethod.available_methods(@shipment).should == [@u_p]
      end
    end
  end
end
