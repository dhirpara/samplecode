require 'spec_helper'

describe HandlingCharge do
  describe ".rate" do
    let(:shipping_method) { create(:shipping_method) }
    before do
      create_initial_data
      @order = create(:order)
      create(:shipment, order: @order, address: create(:canadian_address), shipping_method: shipping_method)
      @order.stub(:subtotal).and_return(99)
    end

    it do
      HandlingCharge.rate(@order).should == 5
      @order.stub(:subtotal).and_return(100)
      HandlingCharge.rate(@order).should == 10
      @order.stub(:subtotal).and_return(200)
      HandlingCharge.rate(@order).should == 15
      @order.stub(:subtotal).and_return(300)
      HandlingCharge.rate(@order).should == 20
    end

    context "with not shipping method" do
      let(:shipping_method) { nil }
      before do
        @shipping_method = create(:shipping_method)
      end

      it do
        HandlingCharge.rate(@order, shipping_method: @shipping_method).should == 5
      end
    end

    context "with have shipping promotion" do
      before do
        ShippingPromotion.stub(:get_discount_rate).and_return(0.4)
      end

      it do
        HandlingCharge.rate(@order, shipping_method: @shipping_method).should == 2
      end

      context "with options promotion false" do
        it do
          HandlingCharge.rate(@order, shipping_method: @shipping_method, promotion: false).should == 5
        end
      end
    end
  end
end
