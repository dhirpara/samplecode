require 'spec_helper'

describe ShippingCarrier do
  describe ".usps_carrier" do
    subject { ShippingCarrier.usps_carrier }
    its(:class) { should == ActiveMerchant::Shipping::USPS }
  end

  describe ".fedex_carrier" do
    subject { ShippingCarrier.fedex_carrier }
    its(:class) { should == ActiveMerchant::Shipping::FedEx }
  end

  describe ".landmark_carrier" do
    subject { ShippingCarrier.landmark_carrier }
    its(:class) { should == Landmark }
  end
end
