require 'spec_helper'

describe Email do
  let(:client) { build(:client, email: "1@gmail.com") }

  describe ".subscribe" do
    before do
      ActionMailer::Base.delivery_method = :test
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
    end

    it do
      Email.subscribe(client)
      ActionMailer::Base.deliveries.size.should == 1
    end

    context "with have email record" do
      let(:email) { create(:email, email: client.email, token: "token") }

      before do
        email
      end

      it do
        Email.subscribe(client)
        ActionMailer::Base.deliveries.size.should == 1
      end

      #context "but token is empty" do
        #let(:token) { "" }

        #it do
          #Email.subscribe(client)
          #ActionMailer::Base.deliveries.size.should == 0
        #end
      #end

      #context "but email record have token" do
        #let(:token) { "token" }

        #it do
          #Email.subscribe(client)
          #ActionMailer::Base.deliveries.size.should == 1
        #end
      #end
    end

    context "with have account on Mail Chimp" do
      before do
        MailChimp.subscribe(client)
      end

      after do
        MailChimp.unsubscribe(client)
      end

      it do
        Email.subscribe(client)
        ActionMailer::Base.deliveries.size.should == 0
      end
    end
  end

  describe "unsubscribe" do
    context "when has email record" do
      before do
        create(:email, email: client.email, token: "token")
      end

      it do
        Email.count.should == 1
        Email.unsubscribe(client)
        Email.count.should == 0
      end
    end

    context "when has Mail Chimp record" do
      before do
        MailChimp.subscribe(client)
      end

      it do
        MailChimp.exist_subscriber?(client).should == true
        Email.unsubscribe(client)
        MailChimp.exist_subscriber?(client).should == false
      end
    end
  end
end
