require 'spec_helper'

describe Payment do
  let(:order) { create(:order) }
  before do
    create_initial_data
  end

  describe "#check_credit_amount" do
    let(:amount) { -10 }
    let(:payment_type) { PaymentType.credit_type }
    let(:payment) do
      payment = build(:payment, payment_type_id: payment_type.id, amount: amount)
      payment.save(validate: false)
      payment
    end

    context "with change amount -$9.0" do
      before do
        payment.amount = -9
      end

      it do
        payment.check_credit_amount
        payment.errors.messages.should == {}
      end
    end

    context "with change amount $1.0" do
      before do
        payment.amount = 1
      end

      it do
        payment.check_credit_amount
        payment.errors.messages.should == { base: ["Client only have $0.0 credit amount"] }
      end
    end

    context "with have other credit amount" do
      before do
        other_payment = build(:payment, payment_type_id: payment_type.id, amount: -20, order_id: payment.order_id)
        other_payment.save(validate: false)
      end

      context "and pay $19 credit amount" do
        before do
          payment.amount = 19
        end

        it do
          payment.check_credit_amount
          payment.errors.messages.should == {}
        end
      end

      context "and pay $21 credit amount" do
        before do
          payment.amount = 21
        end

        it do
          payment.check_credit_amount
          payment.errors.messages.should == { base: ["Client only have $20.0 credit amount"] }
        end
      end
    end
  end

  describe "#can_refund_amount" do
    before do
      @payment = create(:card_payment, order: order, amount: 10.0)
    end

    it do
      create(:card_payment, order: order, amount: 2, trn_type: "R", ref_trn_id: @payment.trn_id)
      @payment.can_refund_amount.should == 8
    end

    it do
      create(:non_card_payment, order: order, amount: -2, ref_trn_id: @payment.trn_id)
      @payment.can_refund_amount.should == 8
    end
  end

  describe "#can_void?" do
    let(:payment) { create(:card_payment, order: order, payment_type_id: payment_type.id, trn_type: trn_type, message: message) }
    let(:payment_type) { PaymentType.find_by_name("Visa") }
    let(:message) { "Approved" }
    let(:trn_type) { "P" }

    it do
      payment.can_void?.should == true
    end

    context "with failure payment" do
      let(:message) { "failure" }

      it do
        payment.can_void?.should == false
      end
    end

    context "with timeout to void" do
      before do
        payment.created_at = 1.day.ago
        payment.save
      end

      it do
        payment.can_void?.should == false
      end
    end

    context "with already void" do
      before do
        create(:card_payment, order: order, ref_trn_id: payment.trn_id, trn_type: "VP")
      end

      it do
        payment.can_void?.should == false
      end
    end

    context "with already refund" do
      before do
        create(:card_payment, order: order, amount: 2, ref_trn_id: payment.trn_id, trn_type: "R")
      end

      it do
        payment.can_void?.should == false
      end
    end

    context "with refund payment" do
      let(:trn_type) { "R" }

      it do
        payment.can_void?.should == true
      end

      context "with failure payment" do
        let(:message) { "failure" }

        it do
          payment.can_void?.should == false
        end
      end

      context "with timeout to void" do
        before do
          payment.created_at = 1.day.ago
          payment.save
        end

        it do
          payment.can_void?.should == false
        end
      end

      context "with already void" do
        before do
          create(:card_payment, order: order, trn_type: "VR", ref_trn_id: payment.trn_id)
        end

        it do
          payment.can_void?.should == false
        end
      end
    end

    context "with void purchase payment" do
      let(:trn_type) { "VP" }

      it do
        payment.can_void?.should == false
      end
    end

    context "with void refund payment" do
      let(:trn_type) { "VR" }

      it do
        payment.can_void?.should == false
      end
    end

    context "with not cc type" do
      let(:payment_type) { PaymentType.find_by_name("Promotional") }

      it do
        payment.can_void?.should == false
      end
    end
  end

  shared_examples "success result with login user" do
    it "get success result" do
      order.reload
      payment = order.payments.first

      order.client.payment_profiles.any? == true
      payment.payment_profile_id.should_not == nil

      order.payments.size.should == 1
      payment.payment_type.name.should == "Visa"
      payment.amount.should == amount
      payment.completed_at.should_not == nil
      payment.last_digits.should == "1234"
      payment.source.present?.should == true
    end
  end

  shared_examples "failure result with login user" do
    it "get failure result" do
      order.reload
      payment = order.payments.first

      order.state.should == "payment"
      order.payment_state.should == "pending"
      order.payments.size.should == 1
      payment.amount.should == amount
      payment.completed_at.nil?.should == true
      payment.payment_type.should == nil
      payment.source.present?.should == true
    end
  end

  describe "#payment_profile_purchase" do
    #let(:client) { create :client}
    #let(:order) { create(:payment_order, client: client) }
    let(:order) { create(:payment_order) }
    let(:address) { order.shipment.address }
    let(:amount) { order.total }
    let(:invalid_customer_code) { false }
    let(:number) { "4030000010001234" }
    let(:credit_card) do
      ActiveMerchant::Billing::CreditCard.new(first_name: "liang", last_name: "mincong", number: number, verification_value: "123", year: Time.now.year + 10, month: 1)
    end

    before do
      other_order = create(:payment_order)
      other_credit_card = ActiveMerchant::Billing::CreditCard.new({ first_name: "liang", last_name: "mincong", number: "4030000010001234", verification_value: "123", year: Time.now.year+10, month: 1 })
      other_payment = build(:payment, amount: other_order.total, order: other_order)
      other_payment.store_payment_profile(other_credit_card, other_order.shipment.address)

      payment_profile = other_order.client.payment_profiles.first

      if invalid_customer_code
        payment_profile.update_attributes(customer_code: "11")
      end

      payment = build(:payment, amount: amount, order: order)
      payment.payment_profile_purchase(payment_profile)
    end

    it_behaves_like "success result with login user"

    context "with failure purchase" do
      let(:invalid_customer_code) { true }

      it_behaves_like "failure result with login user"

      it "get result" do
        payment = order.reload.payments.first
        payment.payment_profile_id.should_not == nil
        payment.last_digits.should == "1234"
      end
    end
  end

  describe "#guest_purchase" do
    let(:client) do
      client = build(:guest)
      client.save(validate: false)
      client
    end
    let(:order) { create(:payment_order, client: client) }
    let(:address) { order.shipment.address }
    let(:amount) { order.total }
    let(:number) { "4030000010001234" }
    let(:credit_card) { ActiveMerchant::Billing::CreditCard.new(first_name: "liang", last_name: "mincong", number: number, verification_value: "123", year: Time.now.year + 10, month: 1) }

    before do
      payment = build(:payment, amount: amount, order: order)
      payment.guest_purchase(credit_card, address)
    end

    it "get success result" do
      order.reload
      payment = order.payments.first

      order.payments.size.should == 1

      payment.payment_type.name.should == "Visa"
      payment.amount.should == amount
      payment.completed_at.should_not == nil
      payment.last_digits.should == "1234"
      payment.source.present?.should == true
    end

    context "with failure purchase" do
      let(:number) { "51000" }
      it "get failure result" do
        order.reload
        payment = order.payments.first

        order.state.should == "payment"
        order.payment_state.should == "pending"

        order.payments.size.should == 1
        payment.amount.should == amount
        payment.completed_at.should == nil
        payment.payment_type.should == nil
        payment.last_digits.should == "1000"
        payment.source.present?.should == true
      end
    end
  end
end
