require 'spec_helper'

describe PaymentProfile do
  before do
    create_initial_data
  end

  describe ".generate" do
    let(:order) { create(:order) }
    let(:credit_card) { ActiveMerchant::Billing::CreditCard.new(first_name: "l", last_name: "mc", verification_value: "123", year: 2100, month: 12, number: "4030000010001234") }
    let(:address) { create(:address) }

    before do
      gateway = Payment.gateway_by_currency(order.currency)
      @resp = gateway.store(credit_card, build(:payment, order: order).gateway_options(address))
      @payment_profile = PaymentProfile.generate(order, address, credit_card, @resp)
    end

    it do
      @payment_profile.customer_code.should == @resp.params["customerCode"]
      @payment_profile.last_digits.should == credit_card.last_digits
      @payment_profile.currency_id.should == order.currency_id
      @payment_profile.address1.should == address.address1
    end
  end
end
