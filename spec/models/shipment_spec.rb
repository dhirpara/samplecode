require 'spec_helper'

describe Shipment do
  before do
    create_initial_data
  end
  describe "#update_address" do
    let(:address) { create(:address, name: "name 1", client: create(:client)) }
    let(:shipment) { create(:shipment, address: address)}
    let(:attr) { attributes_for(:address, name: "name 2") }

    before do
      shipment.order.stub(:update_ship_address)
      shipment.stub(:can_update_address?).and_return(true)
    end

    it do
      shipment.update_address(attr)
      shipment.address.name.should == "name 2"
      shipment.address.client.should_not == nil
      Address.count.should == 1
    end

    context "with invalid attributes" do
      let(:attr) { {name: "name 3"} }

      it do
        shipment.update_address(attr)
        shipment.address.name.should == "name 1"
      end
    end

    context "with other shipment used address" do
      let(:attr) { attributes_for(:address, name: "name 4") }
      before do
        shipment.stub(:can_update_address?).and_return(false)
      end

      it do
        shipment.update_address(attr)
        shipment.address.name.should == "name 4"
        shipment.address.client.should == shipment.order.client
        Address.count.should == 2
        address.reload
        address.client.should == nil
        address.name = "name 3"
      end
    end
  end

  describe "#shipping_method_rates" do
    before do
      @usa = Country.find_by_abbr("US")
      @canada = Country.find_by_abbr("CA")
      @norway = Country.create(name: 'Norway', abbr: 'NO')
    end

    let(:shipment) { create :shipment, address: address }
    let(:american_address) { create :american_address, country_id: @usa.id }
    let(:canadian_address) { create :canadian_address, country_id: @canada.id }
    let(:norway_address) { create :norway_address, country_id: @norway.id }


    context "with American address" do
      let(:address) { american_address }

      it "get the vaild rates" do
        rate_names = shipment.shipping_method_rates.map{|r| r[0]}
        rate_names.should include("FedEx Ground Home Delivery", "FedEx 2 Day", "FedEx Express Saver", "FedEx First Overnight")
        rate_names.should be_any {|n| n =~ /USPS Priority Mail/ }
        #rate_names.should be_any {|n| n =~ /USPS Priority Mail \d-Day/ }
        #rate_names.should be_any {|n| n =~ /USPS Priority Mail Express \d-Day/ }
      end
    end

    context "with Canadian address" do
      let(:address) { canadian_address }

      it "get the vaild rates" do
        shipment.shipping_method_rates.map{|r| r[0]}.should =~ ["Landmark Standard", "Landmark Express", "UPS"]
      end
    end

    context "with address of Other country" do
      let(:address) { norway_address }

      it "get the vaild rates" do
        shipment.shipping_method_rates.map{|r| r[0]}.should include("USPS Priority Mail International", "USPS Priority Mail Express International")
      end
    end
  end
end
