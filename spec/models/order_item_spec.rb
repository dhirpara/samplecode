require 'spec_helper'

describe OrderItem do
  describe "#update_quantity" do
    context "with new item" do
      before do
        @order_item = build(:order_item)
      end

      it do
        @order_item.update_quantity.quantity.should == 1
      end
    end

    context "with already exist item" do
      before do
        exist_item = create(:order_item, quantity: 11)
        @order_item = build(:order_item, quantity: 2, order_id: exist_item.order_id, product_id: exist_item.product_id)
      end

      it do
        @order_item.update_quantity.quantity.should == 13
      end
    end

    context "with reset quantity options" do
      before do
        exist_item = create(:order_item, quantity: 11)
        @order_item = build(:order_item, quantity: 2, order_id: exist_item.order_id, product_id: exist_item.product_id)
      end

      it do
        @order_item.update_quantity(reset_quantity: true).quantity.should == 2
      end
    end
  end

  describe "#caselot" do
    let(:quantity) { 1 }
    before do
      @product_caselot = create(:product_caselot)
      @order_item = build(:order_item, quantity: quantity)
      @order_item.stub_chain(:product, :product_caselots, :for_pricing_type, :order).and_return([@product_caselot])
    end

    it do
      @order_item.caselot.should == nil
    end

    context "with quantity is 10" do
      let(:quantity) { 10 }

      it do
        @order_item.caselot.should == @product_caselot
      end
    end

    context "with 10 quantity options" do
      it do
        @order_item.caselot(quantity: 10).should == @product_caselot
      end
    end

    context "with have same product item" do
      before do
        create(:order_item, quantity: 9, order_id: @order_item.order_id, product_id: @order_item.product_id)
      end

      it do
        @order_item.caselot.should == @product_caselot
      end
    end

    context "with have free item" do
      before do
        create(:order_item, quantity: 10, rate: 0, order_id: @order_item.order_id, product_id: @order_item.product_id)
      end

      it do
        @order_item.caselot.should == nil
      end
    end
  end
end
