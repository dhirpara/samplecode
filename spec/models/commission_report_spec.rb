require 'spec_helper'

describe CommissionReport do
  before do
    create_initial_data
  end

  describe ".add_report" do
    let(:order) { create :order }
    let(:commission_type) { CommissionType.find_by_code("NMB") }

    it do
      CommissionReport.add_report(order)
      CommissionReport.count.should == 0
    end

    context "When support is receive commission" do
      let(:support_user) { order.client.support_user }
      let(:commission_rates) { [create(:commission_rate, support_user: support_user, commission_type: commission_type )] }

      before do
        support_user.update_attributes(receive_commission: true)
        support_user.stub(:commission_rates_by_type).and_return(commission_rates)

        order.payments.create(amount: 10, received: true, payment_type_id: PaymentType.find_by_name("Money Order/Cheque").id)
        order.stub(:subtotal).and_return(5)
      end

      it do
        CommissionReport.add_report(order)
        CommissionReport.count.should == 1
      end

      context "when commission type is order amount" do

        before do
          support_user.update_attributes(commission_by_amount: true)
        end

        it do
          CommissionReport.add_report(order)
          CommissionReport.count.should == 1
        end
      end
    end
  end
end
