require 'spec_helper'

describe PaymentReport do
  before do
    create_initial_data
  end

  describe "#generate_report" do
    let(:payment) { build(:card_payment, trn_type: trn_type) }
    let(:order) { payment.order }
    let(:payment_report) { PaymentReport.new(payment: payment) }
    let(:trn_type) { "P" }

    before do
      order.stub(:need_purchase_total).and_return(100)
      payment_report.generate_report
    end

    it do
      payment_report.amount.should == 9.99
    end

    context "when return payment type" do
      let(:trn_type) { "R" }

      it do
        payment_report.amount.should == - 9.99
      end
    end

    context "when void purchase payment type" do
      let(:trn_type) { "VP" }

      it do
        payment_report.amount.should == - 9.99
      end
    end
  end

  #describe "#report_amount" do
  #let(:received) { false }
  #let(:payment_type) { PaymentType.find_by_name("Promotional") }
  #let(:amount) { 10 }
  #let(:payment_report) { PaymentReport.new(payment: payment) }
  #let(:trn_type) { "" }
  #let(:payment) { build(:payment, amount: amount, payment_type_id: payment_type.id, received: received, trn_type: trn_type) }

  #let(:update_received) { received }
  #let(:update_amount) { amount }
  #let(:update_payment_type) { payment_type }

  #before do
  #payment.save

  #payment.assign_attributes(amount: update_amount, payment_type_id: update_payment_type.id, received: update_received)
  #end

  #context "with changed received" do
  #let(:update_received) { true }

  #it { payment_report.report_amount.should == 10 }

  #context "with credit card payment" do
  #context "and purchase payment" do
  #let(:trn_type) { "P" }

  #it { payment_report.report_amount.should == 10 }
  #end

  #context "and refund payment" do
  #let(:trn_type) { "R" }

  #it { payment_report.report_amount.should == -10 }
  #end

  #context "and void purchase payment" do
  #let(:trn_type) { "VP" }

  #it { payment_report.report_amount.should == -10 }
  #end

  #context "and void refund payment" do
  #let(:trn_type) { "VR" }

  #it { payment_report.report_amount.should == 10 }
  #end
  #end

  #context "with changed amount" do
  #let(:update_amount) { 20 }

  #it { payment_report.report_amount.should == 20 }
  #end
  #end

  #context "with payment init received" do
  #let(:received) { true }

  #context "with changed amount" do
  #let(:update_amount) { 20 }

  #it { payment_report.report_amount.should == 10 }
  #end

  #context "with changed payment type" do
  #let(:update_payment_type) { PaymentType.find_by_name("Replacement") }

  #it { payment_report.report_amount.should == -10 }

  #context "with changed amount" do
  #let(:update_amount) { 20 }

  #it { payment_report.report_amount.should == -10 }
  #end
  #end

  #context "with changed received" do
  #let(:update_received) { false }

  #it { payment_report.report_amount.should == -10 }

  #context "with changed amount" do
  #let(:update_amount) { 30 }

  #it { payment_report.report_amount.should == -10 }
  #end
  #end
  #end
  #end

  describe ".can_generate_report?" do
    let(:received) { false }
    let(:payment_type) { PaymentType.find_by_name("Promotional") }
    let(:payment) { build(:payment, amount: 10, payment_type_id: payment_type.id, received: received) }

    it { PaymentReport.can_generate_report?(payment).should == false }

    context "with received payment" do
      let(:received) { true }

      it { PaymentReport.can_generate_report?(payment).should == true }
    end

    context "with exist payment" do
      let(:update_received) { received }

      before do
        payment.save

        payment.assign_attributes(amount: 10, payment_type_id: payment_type.id, received: update_received)
      end

      it { PaymentReport.can_generate_report?(payment).should == false }

      context "with change received" do
        let(:update_received) { true }

        it { PaymentReport.can_generate_report?(payment).should == true }
      end
    end
  end

  #describe ".can_generate_addition_report?" do
  #let(:received) { false }
  #let(:payment_type) { PaymentType.find_by_name("Promotional") }
  #let(:amount) { 10 }
  #let(:payment) { build(:payment, amount: amount, payment_type_id: payment_type.id, received: received) }

  #it { PaymentReport.can_generate_addition_report?(payment).should == false }

  #context "with received payment" do
  #let(:received) { true }

  #it { PaymentReport.can_generate_addition_report?(payment).should == false }
  #end

  #context "with exist payment" do
  #let(:update_received) { received }
  #let(:update_amount) { amount }
  #let(:update_payment_type) { payment_type }

  #before do
  #payment.save

  #payment.assign_attributes(amount: update_amount, payment_type_id: update_payment_type.id, received: update_received)
  #end

  #it { PaymentReport.can_generate_addition_report?(payment).should == false }

  #context "with changed received" do
  #let(:update_received) { true }

  #it { PaymentReport.can_generate_addition_report?(payment).should == false }
  #end

  #context "with received payment" do
  #let(:received) { true }

  #it { PaymentReport.can_generate_addition_report?(payment).should == false }

  #context "with changed payment_type_id" do
  #let(:update_payment_type) { PaymentType.find_by_name("Replacement") }

  #it { PaymentReport.can_generate_addition_report?(payment).should == true }

  #context "with changed received" do
  #let(:update_received) { false }

  #it { PaymentReport.can_generate_addition_report?(payment).should == false }
  #end
  #end

  #context "with changed received" do
  #let(:update_received) { false }

  #it { PaymentReport.can_generate_addition_report?(payment).should == false }
  #end
  #end
  #end
  #end
end
