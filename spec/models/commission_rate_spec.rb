require 'spec_helper'

describe CommissionRate do
  let(:support_user) { create(:support_user, receive_commission: true, commission_by_amount: true)}

  before do
    create_initial_data
  end

  describe "#calculate_amount_by_payment" do
    let(:order) { create :order }

    before do
      @rate = create(:commission_rate, min: 0, max: 200, commission_type: CommissionType.amount_type)
    end

    it do
      @rate.calculate_amount_by_payment(order, 0, 150).should == 150
    end

    context "when payment amount more than 200" do
      let(:amount) { 210 }

      it do
        @rate.calculate_amount_by_payment(order, 0, 150).should == 150
      end

      context "and has more rate" do
        before do
          @rate_1 = create(:commission_rate, min: 200, max: 300, commission_type: CommissionType.amount_type)
          @rate_2 = create(:commission_rate, min: 300, max: 99999, commission_type: CommissionType.amount_type)
        end

        it do
          @rate_1.calculate_amount_by_payment(order, 0, 210).should == 10
        end
      end
    end
  end

  describe ".valid_amount_type_range" do
    let(:order) { create(:order) }

    before do
      order.stub(:commission_support_user).and_return(support_user)
      @rate = create(:commission_rate, min: 0, max: 200, support_user: support_user, commission_type: CommissionType.amount_type)
      @rate_1 = create(:commission_rate, min: 200, max: 400, support_user: support_user, commission_type: CommissionType.amount_type)
      @rate_2 = create(:commission_rate, min: 400, max: 999999, support_user: support_user, commission_type: CommissionType.amount_type)
    end

    it do
      CommissionRate.valid_amount_type_range(order, 400).should include(@rate, @rate_1)
    end
  end
end
