require 'spec_helper'

describe Order do
  describe "#add_item" do
    let(:product) { create :product, have_product_caselot: true }
    let(:order) { create :order }
    let(:quantity) { 1 }
    let(:order_item) { build :order_item, quantity: quantity, product_id: product.id }

    shared_examples "add_item result" do |opts|
      opts ||= {}
      opts.reverse_merge!({
        order_item_price: 100
      })
      before do
        order.add_item(order_item)
      end

      it "g order item" do
        order.reload.order_items.count.should == 1
      end

      it "have correct price" do
        order.reload.order_items.each do |item|
          item.price.should == opts[:order_item_price]
        end
      end
    end

    it_behaves_like "add_item result"

    context "once add 5 items" do
      let(:quantity) { 5 }

      it_behaves_like "add_item result", order_item_price: 80
    end

    context "with exist item" do
      before do
        order.add_item(build :order_item, quantity: 4, product_id: product.id)
      end

      it_behaves_like "add_item result", order_item_price: 80
    end
  end

  describe "#payment_total" do
    before do
      create_initial_data
      @order = create(:order)
      @order.payments.create(amount: 100, trn_type: "P", message: Payment::APPROVED, received: false, payment_type: PaymentType.find_by_name("Visa"))
      @order.payments.create(amount: 10,  trn_type: "R", message: Payment::APPROVED, received: false, payment_type: PaymentType.find_by_name("Visa"))
      @order.payments.create(amount: -10, received: false, payment_type: PaymentType.find_by_name("Promotional"))
    end

    it do
      @order.payment_total.should == 80
    end
  end
end
