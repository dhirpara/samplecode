require 'spec_helper'

describe Payment::NewForm do
  let(:client) { create(:client) }
  let(:order) { create(:payment_order, client: client) }
  let(:credit_cart_attributes) { {
    first_name: "first name",
    last_name: "last name",
    number: "5100000010001004",
    verification_value: "123",
    year: 2100,
    month: 1
  }}
  let(:credit_card) { ActiveMerchant::Billing::CreditCard.new(credit_cart_attributes) }
  let(:address) { build(:address) }
  let(:attributes) { {
    address: {
      address1: address.address1,
      country_id: address.country_id,
      city: address.city,
      state: address.state,
      postal_code: address.postal_code,
      phone: address.phone
    },
    credit_card: credit_cart_attributes
  } }



  before do
    create_initial_data
  end

  describe ".initialize" do
    before do
      @payment_new_form = Payment::NewForm.new(order, attributes)
    end

    it do
      @payment_new_form.address.class.should == Address
      @payment_new_form.credit_card.class.should == ActiveMerchant::Billing::CreditCard
    end


    context "with no attributes" do
      let(:attributes) { {} }

      it do
        @payment_new_form.address.class.should == Address
        @payment_new_form.credit_card.class.should == ActiveMerchant::Billing::CreditCard
      end
    end

    context "with just payment_profile_id attributes" do
      let(:attributes) { { payment_profile_id: payment_profile.id } }
      let(:payment_profile) do
        order.payments.build.store_payment_profile(credit_card, address)[1]
      end

      it do
        @payment_new_form.address.class.should == Address
        @payment_new_form.credit_card.class.should == ActiveMerchant::Billing::CreditCard
      end
    end
  end

  describe "#purchase" do
    let(:payment_new_form) { Payment::NewForm.new(order, attributes) }

    it do
      payment_new_form.purchase.should == true
      order.payments.count.should == 1
      client.payment_profiles.count.should == 1
    end

    context "can pre order" do
      #let(:has_back_orderd_product) { false }
      #before do
      #  order.order_items.stub(:any?).and_return(has_back_orderd_product)
      #end

      #it do
      #  payment_new_form.purchase(pre_order: true).should == true
      #  order.payments.count.should == 1
      #  client.payment_profiles.count.should == 1
      #end

    end

    context "with back orderd product" do
      before do
        order.order_items.stub(:any?).and_return(true)
      end

      it do
        payment_new_form.purchase.should == true
        order.payments.count.should == 0
        client.payment_profiles.count.should == 1
      end
    end

    context "when client is guest" do
      let(:client) { create(:guest) }

      it do
        payment_new_form.purchase.should == true
        order.payments.count.should == 1
        client.payment_profiles.count.should == 0
      end
    end

    context "has payment profile" do
      let(:attributes) { { payment_profile_id: payment_profile.id } }
      let(:payment_profile) do
        order.payments.build.store_payment_profile(credit_card, address)[1]
      end

      it do
        payment_new_form.purchase.should == true
        order.payments.count.should == 1
        client.payment_profiles.count.should == 1
      end
    end
  end
end
