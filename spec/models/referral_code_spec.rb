require 'spec_helper'

describe ReferralCode do
  describe ".rate" do
    let(:referral_code) { create(:referral_code, discount: discount, percentage: percentage) }
    let(:discount) { nil }
    let(:percentage) { false }
    before do
      @order = create(:order, referral_code: referral_code )
    end

    it do
      ReferralCode.rate(@order).should == 0
    end

    context "with discount" do
      let(:discount) { 10 }

      before do
        @order.stub(:subtotal).and_return(200)
      end

      it do
        ReferralCode.rate(@order).should == -10
      end

      context "with percentage" do
        let(:percentage) { true }

        it do
          ReferralCode.rate(@order).should == -20
        end
      end
    end
  end

  describe ".out_of_stock_for_resource?" do
    let(:resource) { create(:order, referral_code: referral_code) }
    let(:referral_code) { create(:referral_code, number_of_times: number_of_times) }
    let(:number_of_times) { nil }

    it do
      ReferralCode.out_of_stock_for_resource?(resource).should == false
    end

    context "when number of times is not nil" do
      let(:number_of_times) { 1 }

      it do
        ReferralCode.out_of_stock_for_resource?(resource).should == false
      end

      context "and resource is cart" do
        let(:resource) { create(:cart, referral_code: referral_code) }

        it do
          ReferralCode.out_of_stock_for_resource?(resource).should == false
        end
      end

      context "and has referral code order" do
        let(:referral_code_order_order) { resource }

        before do
          create(:referral_code_order, order: referral_code_order_order, referral_code: referral_code)
        end

        it do
          ReferralCode.out_of_stock_for_resource?(resource).should == false
        end

        context "and order id is not this resource" do
          let(:referral_code_order_order) { create(:order) }

          it do
            ReferralCode.out_of_stock_for_resource?(resource).should == true
          end
        end
      end
    end
  end
end
