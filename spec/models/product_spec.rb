require 'spec_helper'

describe Product do
  before do
    create_initial_data
  end

  describe ".for_client" do
    let(:products) do
      if defined? admin
        if defined? conditions
          Product.for_client(client, admin, conditions)
        else
          Product.for_client(client, admin)
        end
      else
        Product.for_client(client)
      end
    end
    let(:client) { create(:client, pricing_type: pricing_type) }
    let(:pricing_type) { PricingType.find_by_name("Retail") }

    before do
      @product1  = create(:product)
      @product2  = create(:product, pricing_types: [{ name: "Staff"               , available: true  }])
      @product3  = create(:product, pricing_types: [{ name: "Health Professionals", available: true  }])
      @product4  = create(:product, pricing_types: [{ name: "Shareholders"        , available: true  }])
      @product5  = create(:product, pricing_types: [{ name: "Wholesale 1"         , available: true  }])
      @product6  = create(:product, pricing_types: [{ name: "Wholesale 2"         , available: true  }])
      @product7  = create(:product, pricing_types: [{ name: "Wholesale 3"         , available: true  }])
      @product11 = create(:product, pricing_types: [{ name: "Rentail"             , available: false }])
      @product12 = create(:product, pricing_types: [{ name: "Staff"               , available: false }])
      @product13 = create(:product, pricing_types: [{ name: "Health Professionals", available: false }])
      @product14 = create(:product, pricing_types: [{ name: "Shareholders"        , available: false }])
      @product15 = create(:product, pricing_types: [{ name: "Wholesale 1"         , available: false }])
      @product16 = create(:product, pricing_types: [{ name: "Wholesale 2"         , available: false }])
      @product17 = create(:product, pricing_types: [{ name: "Wholesale 3"         , available: false }], allow_order_online: false)
      @product21 = create(:product, pricing_types: [{ name: "Rentail"             , available: false }], allow_order_online: false)
      @product22 = create(:product, pricing_types: [{ name: "Staff"               , available: false }], allow_order_online: false)
      @product23 = create(:product, pricing_types: [{ name: "Health Professionals", available: false }], allow_order_online: false)
      @product24 = create(:product, pricing_types: [{ name: "Shareholders"        , available: false }], allow_order_online: false)
      @product25 = create(:product, pricing_types: [{ name: "Wholesale 1"         , available: false }], allow_order_online: false)
      @product26 = create(:product, pricing_types: [{ name: "Wholesale 2"         , available: false }], allow_order_online: false)
      @product27 = create(:product, pricing_types: [{ name: "Wholesale 3"         , available: false }], allow_order_online: false)
      @product31 = create(:product, pricing_types: [{ name: "Rentail"             , available: false }], allow_order_online: false, active: false)
      @product32 = create(:product, pricing_types: [{ name: "Staff"               , available: false }], allow_order_online: false, active: false)
      @product33 = create(:product, pricing_types: [{ name: "Health Professionals", available: false }], allow_order_online: false, active: false)
      @product34 = create(:product, pricing_types: [{ name: "Shareholders"        , available: false }], allow_order_online: false, active: false)
      @product35 = create(:product, pricing_types: [{ name: "Wholesale 1"         , available: false }], allow_order_online: false, active: false)
      @product36 = create(:product, pricing_types: [{ name: "Wholesale 2"         , available: false }], allow_order_online: false, active: false)
      @product37 = create(:product, pricing_types: [{ name: "Wholesale 3"         , available: false }], allow_order_online: false, active: false)

      @product_array = [
        @product1 , @product2 , @product3 , @product4 , @product5 , @product6 , @product7 ,
        @product11, @product12, @product13, @product14, @product15, @product16, @product17,
        @product21, @product22, @product23, @product24, @product25, @product26, @product27
      ]
    end

    it "get correct products" do
      products.should == [@product1]
    end

    context "with admin" do
      let(:admin) { true }
      it "get correct products" do
        products.should include(@product1)
        products.should_not include(@product_array - [@product1, @product11])
      end
    end

    context "with other client" do
      let(:pricing_type) { PricingType.find_by_name("Staff") }
      it "get correct products" do
        products.should include(@product1, @product2)
        products.should_not include(@product_array - [@product1, @product2])
      end

      context "with admin" do
        let(:admin) { true }
        it "get correct products" do
          products.should include(@product1, @product2)
          products.should_not include(@product_array - [@product1, @product2, @product11, @product12])
        end
      end
    end

    context "with other client" do
      let(:pricing_type) { PricingType.find_by_name("Health Professionals") }
      it "get correct products" do
        products.should include(@product1, @product3)
        products.should_not include(@product_array - [@product1, @product3, @product11, @product13])
      end
      context "with admin" do
        let(:admin) { true }
        it "get correct products" do
          products.should include(@product1, @product3)
          products.should_not include(@product_array - [@product1, @product3, @product11, @product13])
        end
      end

    end
    context "with other client" do
      let(:pricing_type) { PricingType.find_by_name("Shareholders") }
      it "get correct products" do
        products.should include(@product1, @product4)
        products.should_not include(@product_array - [@product1, @product4])
      end
      context "with admin" do
        let(:admin) { true }
        it "get correct products" do
          products.should include(@product1, @product4)
          products.should_not include(@product_array - [@product1, @product4, @product11, @proeduct14])
        end
      end
    end
    context "with other client" do
      let(:pricing_type) { PricingType.find_by_name("Wholesale 1") }
      it "get correct products" do
        products.should include(@product1, @product5)
        products.should_not include(@product_array - [@product1, @product5])
      end
      context "with admin" do
        let(:admin) { true }
        it "get correct products" do
          products.should include(@product1, @product5)
          products.should_not include(@product_array - [@product1, @product5, @product11, @product15])
        end
      end
    end
    context "with other client" do
      let(:pricing_type) { PricingType.find_by_name("Wholesale 2") }
      it "get correct products" do
        products.should include(@product1, @product6)
        products.should_not include(@product_array - [@product1, @product6])
      end
      context "with admin" do
        let(:admin) { true }
        it "get correct products" do
          products.should include(@product1, @product6)
          products.should_not include(@product_array - [@product1, @product6, @product11, @product16])
        end
      end
    end
    context "with other client" do
      let(:pricing_type) { PricingType.find_by_name("Wholesale 3") }
      it "get correct products" do
        products.should include(@product1, @product7)
        products.should_not include(@product_array - [@product1, @product7])
      end
      context "with admin" do
        let(:admin) { true }
        it "get correct products" do
          products.should include(@product1, @product7)
          products.should_not include(@product_array - [@product7, @product7, @product11, @product17])
        end
      end
    end
  end

  describe ".grep_by_disorder_ids" do
    before do
      @disorder1 = create :disorder
      @disorder2 = create :disorder
      @disorder3 = create :disorder
      3.times do
        product = create(:product)
        create :product_disorder, product: product, disorder: @disorder1
      end
      3.times do
        product = create(:product)
        create :product_disorder, product: product, disorder: @disorder2
      end
      4.times do
        product = create(:product)
        create :product_disorder, product: product, disorder: @disorder3
      end
      2.times do
        product = create(:product)
        create :product_disorder, product: product, disorder: @disorder1
        create :product_disorder, product: product, disorder: @disorder3
      end
      product = create(:product, active: false)
      create :product_disorder, product: product, disorder: @disorder1
    end

    context "when disorder_ids is empty array" do
      let(:disorder_ids) { [] }
      it "should get all products" do
        Product.grep_by_disorder_ids(disorder_ids).count.should == 13
      end
    end
    context "when disorder_ids is nil" do
      let(:disorder_ids) { nil }
      it "should get all products" do
        Product.grep_by_disorder_ids(disorder_ids).count.should == 13
      end
    end
    context "when disorder_ids is array" do
      context "with vaild ids" do
        let(:disorder_ids) { [@disorder1.id, @disorder2.id] }
        it "should get all products" do
          Product.grep_by_disorder_ids(disorder_ids).count.should == 9
        end
      end
      context "with vaild ids" do
        let(:disorder_ids) { [@disorder1.id, @disorder3.id] }
        it "should get all products" do
          Product.grep_by_disorder_ids(disorder_ids).count.should == 10
        end
      end
      context "with invaild ids" do
        let(:disorder_ids) { [@disorder1.id, @disorder2.id, 999] }
        it "should get all products" do
          Product.grep_by_disorder_ids(disorder_ids).count.should == 9
        end
      end
    end
  end

  describe "#pricing_type_for_user" do
    let(:get_pricing_type) { product.pricing_type_for_user(user) }
    let(:pricing_type) { PricingType.find_by_name("Retail") || PricingType.create(name: "Retail") }
    let(:user) { create(:client, pricing_type: pricing_type) }
    let(:retail_available) { true }
    let(:staff_available) { true }
    let(:health_professionals_available) { true }
    let(:shareholder_available) { true }
    let(:wholesale_1_available) { true }
    let(:wholesale_2_available) { true }
    let(:wholesale_3_available) { true }
    let(:product) do
      create(:product, pricing_types: [
        { name: "Retail"                    , available: true                           },
        { name: "Staff"                     , available: staff_available                },
        { name: "Health Professionals"      , available: health_professionals_available },
        { name: "Shareholder"               , available: shareholder_available          },
        { name: "Wholesale 1"               , available: wholesale_1_available          },
        { name: "Wholesale 2"               , available: wholesale_2_available          },
        { name: "Wholesale 3"               , available: wholesale_3_available          }
      ])
    end

    it "get correct type" do
      get_pricing_type.should == PricingType.find_by_name("Retail")
    end

    ["Staff", "Health Professionals", "Shareholder", "Wholesale 1", "Wholesale 2", "Wholesale 3"].each do |name|
      context "with #{name} type client" do
        let(:pricing_type) { PricingType.find_by_name(name) || create(:pricing_type, name: name) }

        it "get correct type" do
          get_pricing_type.should == pricing_type
        end

        context "with #{name} type not available" do
          let("#{name.gsub(" ", "_").downcase}_available") { false }
          it "get correct type" do
            get_pricing_type.should == PricingType.find_by_name("Retail")
          end
        end
      end
    end
  end

  describe "#price_for" do
    let(:product) do
      product = create(:product, pricing_types: [])

      currency1 = Currency.find_by_name("CAD")
      currency2 = Currency.find_by_name("USD")
      ["Retail", "Staff"].each_with_index do |name, index|
        pricing_type = PricingType.find_by_name(name)
        product_pricing_type = build(:product_pricing_type, pricing_type_id: pricing_type.id, available: true)
        product_pricing_type.product_prices << build(:product_price, currency_id: currency1.id, price: 100 - index * 10)
        product_pricing_type.product_prices << build(:product_price, currency_id: currency2.id, price: 95 - index * 10 )
        product.product_pricing_types << product_pricing_type
      end

      product
    end

    it "get correct result" do
      product.price_for.should == 100
    end

    context "with Staff type client" do
      let(:client) { create(:client, pricing_type_id: pricing_type.id) }
      let(:pricing_type) { PricingType.find_by_name("Staff") }

      it "get correct result" do
        product.price_for(user: client).should == 90
      end
    end

    context "with USD currency" do
      it "get correct result" do
        product.price_for(currency: Currency.find_by_name("USD")).should == 95
      end
    end
  end
end
