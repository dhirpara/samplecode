 # coding: utf-8

module TestHelper
  def create_initial_data
    create(:commission_type, name: "Order Number Based", code: "NMB")
    create(:commission_type, name: "Order Amount Based", code: "AMT")
    create(:order_type, name: "Web", code: "W")
    create(:order_type, name: "Guest", code: "G")
    usd = create(:currency, name: 'USD')
    cad = create(:currency, name: 'CAD')
    canada = create(:country, name: "Canada", abbr: "CA", currency_id: cad.id)
    usa = create(:country, name: "United States", abbr: "US", currency_id: usd.id)

    create(:province, country: canada, name: 'Alberta', abbr: 'AB')
    #为了测试
    create(:province, country: usa, name: 'Alberta', abbr: 'AB')
    create(:tax, country: canada, province_abbr: 'AB', name: 'GST', rate: 5)

    create(:order_type, name: "Support", code: "S")
    create(:adjustment_type, name: "tax")
    create(:adjustment_type, name: "handling")
    create(:adjustment_type, name: "shipping")
    create(:adjustment_type, name: "shipping adjustment")
    create(:adjustment_type, name: "promotion")

    rb = create(:handling_charge_method, name: 'Range based', active: true)
    create(:handling_charge, min: 300, handling_charge: 20, handling_charge_method: rb)
    create(:handling_charge, min: 200, max: 299.99, handling_charge: 15, handling_charge_method: rb)
    create(:handling_charge, min: 100, max: 199.99, handling_charge: 10, handling_charge_method: rb)
    create(:handling_charge, min: 0, max: 99.99, handling_charge: 5, handling_charge_method: rb)

    landmark = create(:shipping_carrier, name: 'Landmark')
    usps = create(:shipping_carrier, name: 'USPS')
    fedex = create(:shipping_carrier, name: 'FedEx')

    f_g = create(:shipping_method, name: 'FedEx Ground Home Delivery', shipping_carrier: fedex, default: false)
    f_2 = create(:shipping_method, name: 'FedEx 2 Day', shipping_carrier: fedex, default: false)
    fes = create(:shipping_method, name: 'FedEx Express Saver', shipping_carrier: fedex, default: false)
    fpo = create(:shipping_method, name: 'FedEx First Overnight', shipping_carrier: fedex, default: false)
    upm1 = create(:shipping_method, name: 'USPS Priority Mail 1-Day', shipping_carrier: usps, default: false)
    upme1 = create(:shipping_method, name: 'USPS Priority Mail Express 1-Day', shipping_carrier: usps, default: false)
    upm2 = create(:shipping_method, name: 'USPS Priority Mail 2-Day', shipping_carrier: usps, default: false)
    upm3 = create(:shipping_method, name: 'USPS Priority Mail 3-Day', shipping_carrier: usps, default: false)
    upme2 = create(:shipping_method, name: 'USPS Priority Mail Express 2-Day', shipping_carrier: usps, default: false)

    create(:shipping_method, name: 'USPS Priority Mail International', shipping_carrier: usps, default: true)
    create(:shipping_method, name: 'USPS Priority Mail Express International', shipping_carrier: usps, default: true)

    ls = create(:shipping_method, name: 'Landmark Standard', shipping_carrier: landmark, default: false)
    le = create(:shipping_method, name: 'Landmark Express', shipping_carrier: landmark, default: false)
    canpar = create(:shipping_method, name: 'Canpar', shipping_carrier: landmark, default: false)
    ups = create(:shipping_method, name: 'UPS', shipping_carrier: landmark, default: false)

    #create(:country_shipping_method, country: canada, shipping_method: ls )
    #create(:country_shipping_method, country: canada, shipping_method: le)
    #create(:country_shipping_method, country: canada, shipping_method: canpar)
    #create(:country_shipping_method, country: canada, shipping_method: ups)
    #create(:country_shipping_method, country: usa, shipping_method: f_g)
    #create(:country_shipping_method, country: usa, shipping_method: f_2)
    #create(:country_shipping_method, country: usa, shipping_method: fes)
    #create(:country_shipping_method, country: usa, shipping_method: fpo)
    #create(:country_shipping_method, country: usa, shipping_method: upm1)
    #create(:country_shipping_method, country: usa, shipping_method: upme1)
    #create(:country_shipping_method, country: usa, shipping_method: upm2)
    #create(:country_shipping_method, country: usa, shipping_method: upm3)
    #create(:country_shipping_method, country: usa, shipping_method: upme2)


    create(:landmark_shipping_rate, shipping_method: ls, description: 'Standard rate', base_rate: 5, base_weight_lbs: 3, rate_per_lbs: 1)
    create(:landmark_shipping_rate, shipping_method: le, description: 'Express rate', base_rate: 10, base_weight_lbs: 3, rate_per_lbs: 2)

    create(:payment_type, name: "Visa")
    create(:payment_type, name: "Discover")
    create(:payment_type, name: "Master")
    create(:payment_type, name: "Money Order/Cheque")
    create(:payment_type, name: "Promotional")
    create(:payment_type, name: "Replacement")
    create(:payment_type, name: "Credit")

    create(:role, name: "admin", code: "ADM")
    create(:role, name: "order completion", code: "ORC")

    ["Clinical Use", "General Health Use", "Truehope Products"].each do |name|
      create(:product_category, name: name)
    end

    ["Clinical Reference", "Our Vision"].each do |name|
      create(:article_category, name: name, permalink: name.parameterize)
    end

    ["Retail", "Staff", "Health Professionals", "Shareholders", "Wholesale 1", "Wholesale 2", "Wholesale 3"].each do |name|
      create(:pricing_type, name: name)
    end

    shipping_promotion1 = build(:shipping_promotion, min_amount: 1, start_date: Time.now, end_date: 1.year.from_now, amount_off: 100, area_id: 1)
    shipping_promotion1.pricing_type_ids = PricingType.pluck(:id)
    shipping_promotion1.save
    shipping_promotion2 = build(:shipping_promotion, min_amount: 1, start_date: Time.now, end_date: 1.year.from_now, amount_off: 50, area_id: 2)
    shipping_promotion2.pricing_type_ids = PricingType.pluck(:id)
    shipping_promotion2.save

    create(:auto_ship_setting, pricing_type_ids: [PricingType.first.id])
    create(:introduction_type, name: "None")
    create(:seo, url: "/")
    create(:seo, url: "/videos")
    create(:seo, url: "/products")
    create(:seo, url: "/articles")
  end
end
