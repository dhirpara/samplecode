require 'spec_helper'

shared_examples "have authorizer" do |opts|
  opts ||= {}
  opts.reverse_merge!({codes: ["ADM"], status: 200})
  opts[:codes].each do |code|
    context "with #{code} role" do
      let(:support_user) { create :support_user, roles: [create(:role, code: code)] }

      it "response with #{opts[:status]}" do
        response.status.should == opts[:status]
      end
    end
  end
end

shared_examples "have not authorizer" do |opts|
  opts ||= {}
  opts.reverse_merge!({codes: ["BAS", "ORC", "EXO"], status: 200})
  opts[:codes].each do |code|
    context "with #{code} role" do
      let(:support_user) { create :support_user, roles: [create(:role, code: code)] }

      it "response with 403" do
        response.status.should == 403
      end
    end
  end
end
