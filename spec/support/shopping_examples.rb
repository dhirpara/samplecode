require 'spec_helper'

shared_examples "to be redirected" do
  it { response.status.should == 302 }
  it { should redirect_to redirect_page }
end

shared_context "without login" do
  let(:need_sign_in?) { false }
  it_behaves_like "to be redirected" do
    let(:redirect_page) { new_client_session_url }
  end
end
