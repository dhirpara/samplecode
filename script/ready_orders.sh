#!/bin/bash

cd /var/www/apps/hardy
source /home/dev/.rvm/scripts/rvm

rvm use ruby-1.9.3-p429

RAILS_ENV=production rake ready_orders
