#!/bin/bash

cd ~/workspace/hardy
rake db:drop db:create db:migrate
RAILS_ENV=production rake db:drop db:create db:migrate

cd ~/hardy_db
pg_restore -a -d  hardy_development hardy_stage_latest.dump

for NAME in products product_types product_categories product_caselots product_images product_pricing_types product_prices product_disorders articles article_disorders article_categories videos video_disorders studies study_disorders faqs faq_types disorders pricing_types currencies handling_charges handling_charge_methods country_shipping_adjustments
do
  filename="stage_"$NAME".sql"
  pg_dump hardy_development -a -t $NAME > $filename
  psql hardy_production < $filename
  rm $filename
done

pg_dump -Fc -a hardy_production > hardy_production_s.dump
