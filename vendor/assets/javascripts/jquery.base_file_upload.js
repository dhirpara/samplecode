$(document).on('click', '.delete_file', function(){
  $(this).siblings('.delete_value').val("1");
  $(this).parents(".file_container").hide();
});

(function($){
  $.fn.baseFileUpload = function(options) {
    var defaults,
    base = {
      url: "/admin/"+options.table+"/upload_"+options.column,
      dataType: "json",
      progressInterval: 50,
      pasteZone: null,
      progress: function(e, data){
        if(data.context){
          progress = parseInt(data.loaded / data.total * 100, 10);
          data.context.find('.bar').css('width', progress + '%');
        }
      }
    };

    if(options.mode == "base"){
      defaults = {
        add: function(e, data){
          //data.context = $($.parseHTML(tmpl(options.table+'_'+options.column+"_upload_template", data.files[0])));
          data.context = $(tmpl(options.table+'_'+options.column+"_upload_template", data.files[0]));
          data.old_context = $('#'+options.table+'_'+options.column+'_list').html()
          $('#'+options.table+'_'+options.column+'_list').html(data.context);
          data.context.find('.cancel_file').click(function(){
            data.jqXHR.abort();
            $('#'+options.table+'_'+options.column+'_list').html(data.old_context);
            data.context.remove();
          })
          data.submit();
        },
        done: function(e, data){
          data.context.find('.bar').css('width', '100%');
          var $delete_link = $("<a href='javascript:void(0)' class='delete_file btn2 flBtn'>Delete</a>");
          var $cache_hidden_field = $('<input value='+data.result+' type="hidden" name="'+options.name+'['+options.column+'_cache]">');
          var $remove_hidden_field = $('<input type="hidden" name="'+options.name+'[remove_'+options.column+']" class="delete_value">')

          if (options.alt) {
            var $alt_field = $('<input type="text" name="'+options.name+'['+options.column+'_alt]" placeholder="alt" style="width:100%">');
            data.context.find('.file_name').append($alt_field);
          }

          data.context.find('.operate_buttons').html($delete_link)
          data.context.find('.operate_buttons').append($cache_hidden_field, $remove_hidden_field)
          data.context.find('.upload_image').attr("src", "/uploads/tmp/"+data.result)
        }
      }
    }else if(options.mode == undefined){
      defaults = {
        change: function(e, data){
          if(options.maxFileNumber != undefined){
            var num = (data.files.length + $('#'+options.table+"_"+options.column+"_list .file_container:visible").length) - options.maxFileNumber
            if(num > 0){
              alert("Select too many File upload. The maximum upload files is "+options.maxFileNumber+".")
              while(num > 0){
                data.files.pop();
                num --;
              }
            }
          }
        },
        add: function(e, data){
          //data.context = $($.parseHTML(tmpl(options.table+'_'+options.column+"_upload_template", data.files[0])));
          data.context = $(tmpl(options.table+'_'+options.column+"_upload_template", data.files[0]));
          $('#'+options.table+'_'+options.column+'_list').append(data.context);
          data.context.find('.cancel_file').click(function(){
            data.jqXHR.abort();
            data.context.remove();
          })
          data.submit();
        },
        done: function(e, data){
          var date = new Date();
          var time = date.getTime();
          data.context.find('.bar').css('width', '100%');
          var $link = $("<a href='javascript:void(0)' class='delete_file btn2 flBtn'>Delete</a>");
          var $cache_hidden_field = $('<input value='+data.result+' type="hidden" name="'+options.name+'['+time+']['+options.column+'_cache]">');
          //var $sort_hidden_field = $('<input type="hidden" name="'+options.name+'['+time+'][index]" value='+time+' class="index_sort">');
          var $delete_hidden_field = $('<input type="hidden" name="'+options.name+'['+time+'][_destroy]" class="delete_value"> ');
          data.context.find('.operate_buttons').html($link)
          data.context.find('.operate_buttons').append($cache_hidden_field, $delete_hidden_field)
          //data.context.find('.operate_buttons').append($sort_hidden_field)
          data.context.find('.upload_image').attr("src", "/uploads/tmp/"+data.result)
        }
      };
    }
    var defaultsConfig = $.extend(base, defaults)
    this.fileupload($.extend(defaultsConfig, options))
  }
})( jQuery );
