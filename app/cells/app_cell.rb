class AppCell < Cell::Rails
  def error_msg(opts)
    resources = opts[:resource]
    resources = [] << resources unless resources.is_a?(Array)
    resources.delete_if { |resource| resource.nil? }
    @error_messages = if opts[:without_title] || false
                        resources.inject([]){ |array, object| array + object.errors.values.flatten }
                      else
                        resources.inject([]){ |array, object| array + object.errors.full_messages }
                      end
    if opts[:msg]
      if opts[:msg].is_a? Array
        opts[:msg].each do |msg|
          @error_messages << msg
        end
      else
        @error_messages << opts[:msg]
      end
    end
    render
  end
end
