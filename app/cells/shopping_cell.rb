class ShoppingCell < Cell::Rails

  def progress_bar(opts)
    states = { cart: 0, address: 1, delivery: 2, payment: 3, feedback: 4 }
    @state_index = states[opts[:state]]

    render
  end

end
