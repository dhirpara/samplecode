class ProductImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    "/assets/fallback/#{[version_name, "default.jpg"].compact.join('_')}"
  end

  version :large do
    process resize_and_pad: [284, 411]
  end

  version :medium do
    process resize_and_pad: [200, 282]
  end

  version :thumb do
    process resize_and_pad: [80, 80]
  end

  version :hover do
    process resize_and_pad: [306, 203]
  end
end
