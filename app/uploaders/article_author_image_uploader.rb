class ArticleAuthorImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    "/assets/fallback/articles/author_#{[version_name, "default.jpg"].compact.join('_')}"
  end

  process resize_to_fit: [96, 96]
end
