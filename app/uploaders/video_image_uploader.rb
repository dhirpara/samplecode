class VideoImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    "/assets/fallback/videos/#{[version_name, "default.jpg"].compact.join('_')}"
  end

  version :medium do
    process resize_and_pad: [295, 221]
  end

  version :thumb do
    process resize_and_pad: [66, 66]
  end
end
