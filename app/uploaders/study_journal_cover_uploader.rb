class StudyJournalCoverUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    "/assets/fallback/studies/#{[version_name, "default.jpg"].compact.join('_')}"
  end

  #process resize_to_fit: [800, 545]

  #version :hover do
    #process resize_and_pad: [306, 203]
  #end
  version :gallery do
    process resize_and_pad: [110, 145]
  end
end
