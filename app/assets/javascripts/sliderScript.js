$(document).ready(function() {
	$('.slidewrap').carousel({
		slider: '.slider',
		slide: '.slide',
		slideHed: '.slidehed',
		//nextSlide : '.next',
		//prevSlide : '.prev',
		addPagination: true,
		addNav : false,
		speed: 800 // ms.
	});
	$('.miniSlidewrap').carousel({
		slider: '.slider',
		slide: '.slide',
		slideHed: '.slidehed',
		//nextSlide : '.next',
		//prevSlide : '.prev',
		addPagination: true,
		addNav : false,
		speed: 800 // ms.
	});
	$('.reviewSlidewrap').carousel({
		slider: '.slider',
		slide: '.slide',
		slideHed: '.slidehed',
		//nextSlide : '.next',
		//prevSlide : '.prev',
		addPagination: true,
		addNav : false,
		speed: 800 // ms.
	});
});
