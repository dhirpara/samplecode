$(function(){
  $(document).on("keyup", '#cart_code', function(){
      if ($(this).val()) {
        $.ajax({
          url: "/shopping/cart/licence_field",
          data: { code: $('#cart_code').val() },
          success: function(data){
            $('#licence_container').html(data)
          }
        })
      }else{
        $('#licence_container').html("");
      }
  });
})
