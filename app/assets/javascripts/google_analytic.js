$(function(){
  $(document).on("click", ".ga_footer_link", function(){
    var label = $(this).text();
    ga('send', 'event',  "Footer", "click", label);
  });

  $(document).on("click", ".ga_navigation_link", function(){
    var label = $(this).data("event-label");
    if(!label){
      label = $(this).text();
    }
    ga('send', 'event',  "Navigation", "click", label);
  });

  $(document).on("click", ".ga_product_detail", function(){
    ga('ec:addProduct', {
      'id': $(this).data("id"),
      'name': $(this).data("name")
    });

    ga('ec:setAction', 'click', { list: $(this).data("list") });

    var href = $(this).attr("href");

    var label = $(this).data("event-label");
    var category = $(this).data("event-category");

    ga('send', 'event', category, "click", label, {
      'hitCallback': function() {
        document.location = href;
      }
    });

    return !ga.loaded;
  });

  $(document).on("submit", "#cart_form", function(){
    ga_checkout_action(1);
  })

  $('.checkout_address form').submit(function(){
    ga_checkout_action(2);
  });

  $('.checkout_address .address_use_link').click(function(){
    ga_checkout_action(2);
  });

  $('.checkout_shipping_method form').submit(function(){
    ga_checkout_action(3);
  });

  $('.checkout_payment form').submit(function(){
    ga_checkout_action(4);
  });
})
