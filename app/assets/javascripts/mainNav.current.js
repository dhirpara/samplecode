$(document).ready(function() {
	$(".nav li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})
	
	$(".toggleMenu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(".nav").toggle();
	});

	$(".touch-button").click(function(){
		$(this).toggleClass("activeItem").parent(".item-with-ul").children("a").toggleClass("activeUlItem");
	});
	adjustMenu();

})

$(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	adjustMenu();
});

var adjustMenu = function() {
//var ww = document.body.clientWidth;
var ww = (window.innerWidth) ? window.innerWidth : (document.documentElement && document.documentElement.clientWidth) ? document.documentElement.clientWidth : document.body.offsetWidth;
	ww = ww-12;
	if (ww < 768) {
		$(".toggleMenu").css({"display":"inline-block","position":"absolute","left":ww-70,"top":"20px"});
		$(".nav").css("width",$("header").width());

		if (!$(".toggleMenu").hasClass("active")) {
			$(".nav").hide();
		} else {
			$(".nav").show();
		}
		$(".nav li").unbind('mouseenter mouseleave');
		$(".nav li a.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 768) {
		$(".nav").css("width","900px");
		$(".toggleMenu").css("display", "none");
		$(".nav").show();
		$(".nav li").removeClass("hover");
		$(".nav li a").unbind('click');
		$(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
	var HWidth = $("header").width();
	var NWidth = $(".nav").width();
	if(HWidth<NWidth){
		//$(".nav").css("width","851px");console.log("1-HWidth:"+HWidth,"1-NWidth:"+NWidth);
	}else{
		//$(".nav").css("width",HWidth);console.log("HWidth:"+HWidth,"NWidth:"+NWidth);
	}
	
}
