$(function(){
  $('.delete_item').live('ajax:success', function(event, data, status){
    $('.order_items_list').html(data);
  });

  $('#add_item_form')
  .on('ajax:beforeSend', function(event, xhr, setting){
    if($('#search_name').val() == ""){
      alert("please enter name");
      return false;
    };

    if($('#search_id').val() == ""){
      alert("please select product");
      return false;
    };
    $(".search_submit").attr("disabled", true)
  })
  .on('ajax:success', function(event, data, status){
    $('.order_items_list').html(data);
    $('#search_name').val("");
    $('#search_id').val("");
    $('#search_quantity').val(1);
  })
  .on('ajax:error', function(event, data, status){
    alert(data.responseText);
    $('#search_name').val("");
    $('#search_id').val("");
  })
  .on('ajax:complete', function(event, data, status){
    $(".search_submit").attr("disabled", false);
    $('#search_name').focus();
  })

  $('#dropdown_item_form')
  .on('ajax:beforeSend', function(event, xhr, setting){
    if($('#search_id').val() == ""){
      alert("please select product");
      return false;
    };
    $(".search_submit").attr("disabled", true)
  })
  .on('ajax:success', function(event, data, status){
    $('.order_items_list').html(data);
    $('#search_quantity').val(1);
  })
  .on('ajax:error', function(event, data, status){
    alert(data.responseText);
  })
  .on('ajax:complete', function(event, data, status){
    $(".search_submit").attr("disabled", false);
  })
})
