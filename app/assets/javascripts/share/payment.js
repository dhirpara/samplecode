$(function(){
  $("[name='payment_new_form[payment_profile_id]']").change(function() {
    if($(this).val()) {
      $('.payment_form').hide();
    } else {
      $('.payment_form').show();
    }
  })

  $("[name='payment_new_form[payment_profile_id]'][checked]").change();

  $('#payment_new_form_use_shipping_address').change(function() {
    if($(this).attr("checked") == "checked") {
      $('.billing_address_form').hide();
      $('.shipping_address_detail').show();
    } else {
      $('.billing_address_form').show();
      $('.shipping_address_detail').hide();
    }
  })
})
