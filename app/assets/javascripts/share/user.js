$(function(){
  $('#client_email, #client_work_email, #guest_form_email').blur(function(){
    $this = $(this);
    $label = $this.parents("div.field").find("label");
    if($this.val() != ""){
      email = $this.val().replace("@", "%40");
      escape($this.val());
      $label.text($label.data("title") + "(checking...)")
      $.ajax({
        url: 'https://api.mailgun.net/v2/address/validate?address='+email+'&api_key=pubkey-7b2aqhwftoinz0sa-mis-y789zm4ks21',
        success: function(data){
          if(!data.is_valid){
            $label.text($label.data("title") + "(invalid)")
          }else{
            $label.text($label.data("title") + "(valid)")
          }
        }
      })
    }else{
      $label.text($label.data("title"))
    }
  });
})
