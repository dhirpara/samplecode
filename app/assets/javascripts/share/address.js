$(function(){
  $('#payment_address_country_id').change(function() {
    var country_id = $(this).find("option:selected").val();
    updateProvinces(country_id, "payment_new_form[address][state]");
  });

  $('#address_country_id').change(function() {
    var country_id = $(this).find("option:selected").val();
    updateProvinces(country_id, "address[state]");
  });

  $('#client_country_id').change(function() {
    var country_id = $(this).find("option:selected").val();
    updateProvinces(country_id, "client[province]");
  });

  $('#back_order_client_country_id').change(function() {
    var country_id = $(this).find("option:selected").val();
    updateProvinces(country_id, "back_order_client[province]");
  });

  function updateProvinces(country_id, fieldName) {
    jQuery.ajax({
      url: "/countries/"+country_id+"/provinces",
      type: "GET",
      data: { field_name: fieldName },
      success: function(data) {
        $('#state-field').replaceWith(data);
      }
    })
  };
})
