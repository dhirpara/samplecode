$(function(){
  $('#show_more_caselots').click(function(){
    $(this).hide();
    $('.hide_caselot').show();
  });

  var cache = {};
  if($('#search_name').length != 0){
    $('#search_name').autocomplete({
      delay: 300,
      source: function( request, response ) {
        var term = request.term;
        if ( term in cache ) {
          response( cache[term] );
          return;
        }
        $.ajax({
          url: $("#search_name").data("url"),
          data: {
            name_cont: term,
            client_id: $("#client_id").val()
          },
          success: function(data) {
            var hash = data.map(function(v){return { label: v.name, value: v.name, id: v.id }})
            cache[term] = hash;
            response(hash);
          },
        });
      },
      select: function( event, ui ) {
        $('#search_id').val(ui.item.id)
      }
    });
  };

  $('body')
  .on("click", ".count_operate", function(){
    $parent = $(this).parents(".nested");
    var value = $parent.find(".count_number").val() * 0.01;
    $.each($parent.find(".caselot_price"), function(){
       $(this).val(($("#"+$(this).data("target")).val() * value).toFixed(2))
    });
  })
  .on("click", ".price_available", function(){
    var id = $(this).attr("id");
    if($(this).attr("checked") == "checked"){
      $(this).parents("table").find(".price_field_"+id).attr("readonly", null)
    }else{
      $(this).parents("table").find(".price_field_"+id).attr("readonly", "readonly")
    }
  })
  .on("change", "#product_featured", function(){
    if($(this).attr("checked") == "checked"){
      $("#product_featured_text").attr("readonly", null);
    }
    else{
      $("#product_featured_text").attr("readonly", "readonly");
    }
  });
})
