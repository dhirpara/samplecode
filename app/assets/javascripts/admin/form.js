$(function(){
  //$('[data-preview-url]').click(function(){
  $(document).on('cssmodal:show', function() {
    var $previewField = $("#preview_url");
    $.ajax({
      url: $previewField.val(),
      type: 'post',
      data: $previewField.parents("form").serialize(),
      success: function(data, status, code){
        $('#modal-show .modal-content').html(data);
      }
    })
  });

  $(document).on('cssmodal:hide', function() {
    $('#modal-show .modal-content').html("loading...");
  });

  $('[data-form-save]').click(function(){
    $(this).parents("form").submit();
  });
})
