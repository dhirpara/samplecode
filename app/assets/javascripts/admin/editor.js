$(function(){
  KindEditor.ready(function(K) {
    var array = [
      "#product_highlights", "#product_description", "#product_suggested_use", "#product_supplement_facts", "#product_quality", "#product_verbiage"
    , "#article_content", "#video_description", "#study_brief_description", "#faq_answer"
    ];
    $.each(array, function(i){
      K.create(array[i], {
        langType : 'en',
        uploadJson:"/kindeditor/upload",
        afterBlur: function(){
          this.sync();
        }
      });
    })
  });
})
