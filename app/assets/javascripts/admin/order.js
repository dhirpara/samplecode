$(function(){
  $('input[name="q[state_value]"]').change(function(){
    if($('input[name="q[state_value]"]:checked').val() == "complete") {
      $('.filterDate').show();
      $('#q_only_pending').parent().show();
    }else{
      $('.filterDate').hide();
      $('#q_only_pending').parent().hide();
    }
  });
  $('input[name="q[state_value]"]').change();

  $('#select_all_orders').change(function(){
    if($(this).attr("checked")){
      $('.order_ids').attr("checked", "checked")
    }else{
      $('.order_ids').attr("checked", false)
    }
  });

  $('.order_ids').change(function(){
    if($(this).attr("checked")){
      if($('.order_ids').not("input:checked").size() == 0){
        $('#select_all_orders').attr("checked", "checked")
      }
    }else{
      $('#select_all_orders').attr("checked", false)
    }
  })
})
