$(function(){
  $('[data-role-code="LAN"]').change(function(){
    if($('[data-role-code="LAN"]').attr("checked") == "checked"){
      $('.language_collection_check_boxes').show();
    }else{
      $('.language_collection_check_boxes').hide();
    }
  })

  $('[data-role-code="LAN"]').change();
})
