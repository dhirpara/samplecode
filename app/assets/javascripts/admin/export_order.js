$(function(){
  $('[data-print-invoice]').click(function(){
    $this = $(this);
      
    $.ajax({
      url: $this.attr('href'),
      data: $('[data-export-order]').serialize(),
      success: function(data){
        $('[data-invoices]').html(data);
        $.each($(".barcode"), function(){
          id = $(this).data('id')
          $(this).barcode(id.toString(), "code39");
        })
        javascript:print();
      }
    })

    return false;
  })
})
