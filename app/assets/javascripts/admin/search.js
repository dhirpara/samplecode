$(function(){
  var loadPageRun = true;

  var $selectProvinceCont = $('#q_province_eq');
  var $textProvinceCont = $('#q_province_cont');

  $('#q_country_id_eq').change(function(){
    var provinceList = $(this).data("province-list")[$(this).val()];
    if(provinceList == undefined){
      $selectProvinceCont.next().hide();
      $textProvinceCont.show()

      loadPageRun = false;

      return
    }



    if (provinceList.length > 0) {
      $selectProvinceCont.next().show();
      $textProvinceCont.hide();

      if(!loadPageRun){
        $selectProvinceCont.find('option').remove();
        $selectProvinceCont.selectbox("detach");
        $selectProvinceCont.append("<option value=''>All</option>");
        $.each(provinceList, function(){
          $selectProvinceCont.append("<option value="+this[1]+">"+this[0]+"</option>");
        });
        $selectProvinceCont.selectbox();
      }
    }else{
      $selectProvinceCont.next().hide();
      $textProvinceCont.show();
    }

     loadPageRun = false;
  });

  $('#q_country_id_eq').change();
})
