function showAndHideCardInfo(){
  var payment_type_name = $('#payment_new_form_payment_payment_type_id').find("option:selected").text()
  if(payment_type_name != ""){
    if( payment_type_name != "Visa" && payment_type_name != "Mastercard" && payment_type_name != "Discover"){
      $('.payment_form, .payment_profile_form').hide();
      $('.received_field').show();
    }else{
      if($("#payment_new_form_payment_profile_id_").attr("checked") == "checked"){
        $('.payment_form').show();
      }else{
        $('.payment_form').hide();
      };
      $('.payment_profile_form').show();
      $('.received_field').hide();
    };
  }
}
$(function(){
  showAndHideCardInfo();
  $('#payment_new_form_payment_payment_type_id').change(function(){
    showAndHideCardInfo();
  });
})
