$(document).ready ->
  $('.product_image_upload').baseFileUpload
    name: "product[product_images_attributes]"
    table: "product_images"
    column: "image"
    done: (e, data) ->
      date = new Date()
      time = date.getTime()
      data.context.find('.bar').css('width', '100%')
      $link = $("<a href='javascript:void(0)' class='delete_file btn2 flBtn'>Delete</a>")
      $cache_hidden_field = $('<input value='+data.result+' type="hidden" name="product[product_images_attributes]['+time+'][image_cache]">')
      $delete_hidden_field = $('<input type="hidden" name="product[product_images_attributes]['+time+'][_destroy]" class="delete_value"> ')
      $index_field = $('<input type="text" name="product[product_images_attributes]['+time+'][index]" placeholder="index", style="width:100%;">')
      $alt_field = $('<input type="text" name="product[product_images_attributes]['+time+'][alt]" placeholder="alt", style="width:100%;">')
      data.context.find('.operate_buttons').html($link)
      data.context.find('.operate_buttons').append($cache_hidden_field, $delete_hidden_field)
      data.context.find('.upload_image').attr("src", "/uploads/tmp/"+data.result)
      data.context.find('.file_name').append($index_field)
      data.context.find('.file_name').append($alt_field)

  $('.articles_image_upload').baseFileUpload
    mode: "base"
    name: "article"
    table: "articles"
    column: "image"
    alt: true

  $('.articles_author_image_upload').baseFileUpload
    mode: "base"
    name: "article"
    table: "articles"
    column: "author_image"
    alt: true

  $('.videos_image_upload').baseFileUpload
    mode: "base"
    name: "video"
    table: "videos"
    column: "image"
    alt: true

  $('.studies_journal_cover_upload').baseFileUpload
    mode: "base"
    name: "study"
    table: "studies"
    column: "journal_cover"
    alt: true
  $('.studies_pdf_upload').baseFileUpload
    mode: "base"
    name: "study"
    table: "studies"
    column: "pdf"
  $('.documents_file_upload').baseFileUpload
    mode: "base"
    name: "document"
    table: "documents"
    column: "file"

  $('.products_label_file_upload').baseFileUpload
    mode: "base"
    name: "product"
    table: "products"
    column: "label_file"
