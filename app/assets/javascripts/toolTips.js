
 $(function(){
    $('.non-hoverBox').each(function(){
       var _top, _left; //tips's position
       var content = $(this).attr('tips-data')+'<span class="arrow"></span>';
       var pleft = $(this).offset().left;
       var ptop = $(this).offset().top;
       _left = -(pleft)+5;
       _top = -(ptop)-45;

        $(this).simpletip({ 
            fixed: false, 
            content: content ,
            position: 'top', 
            offset: [_left, _top]
         });
     });

 });
