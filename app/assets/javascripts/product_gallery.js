$(document).ready(function() {
  /*productGallery page*/
  bindProductHoverEvents();
  $(".symptoms").click(function(){
    $(".symptomTags").slideToggle("5000",function(){
      $(".symptoms").parent(".iconBg").toggleClass("active");
    });
  });

  $(".symptomTags a").click(function(){
    var url;
    var $this = $(this);
    if($this.hasClass("active"))
      if($this.data("base-route"))
        url = $this.data("base-route")
      else
        url = window.location.pathname + window.location.search
    else
      url = $this.attr("href")

    $.ajax({
      url: url,
      success: function(body){
        $(".productGallery").replaceWith(body)
        $(".symptomTags a").not($this).removeClass("active");
        $this.toggleClass("active");
      }
    })
    return false
  });
});

function bindProductHoverEvents() {
  $(".product .col1").hover(function(){
    $(this).children(".hover").show();
  },function(){
    $(this).children(".hover").hide();
  });
  $(".productShow .slide, .hover img, .product").mousemove(function(e){
    var x=e.pageX;
    var y=e.pageY;
    var top = $(this).offset().top;
    var left = $(this).offset().left;
    var width = $('.col1').width() ? $('.col1').width() : $(this).width();
    // var offsetImg = ($(".hover").width() - $(this).width())/2;
    var offsetImg = ($(".hover").width() - width)/2;
    $(".toolTips,.toolTip").css("display","block");
    $(".toolTips,.toolTip").css("left",(x-left+offsetImg)).css("top",(y-top-40));
  }).mouseout(function(){
    $(".toolTips,.toolTip").css("display","none");
  });
}
