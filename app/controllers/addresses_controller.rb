class AddressesController < ApplicationController
  def destroy
    Address.find(params[:id]).delete_record
    
    redirect_to :back
  end

  private
  def use_https?
    true
  end
end
