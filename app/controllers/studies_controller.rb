class StudiesController < ApplicationController
  def index
    @seo = Seo.find_by_url("/studies")
    @disorders = Disorder.related_with_study
    @studies = Study.search(params[:q]).result(distinct: true).default_sort.page(params[:page]).per(BASE_CONFIG[:page])

    if request.xhr?
      render partial: "study_list"
    else
      render :index
    end
  end
end
