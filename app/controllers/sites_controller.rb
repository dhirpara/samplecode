class SitesController < ApplicationController
  skip_before_filter :https_redirect, only: :select_language

  def index
    @seo = Seo.find_by_url("/")
    @side_disorders = Disorder.where(id: [1, 2, 4, 5, 6, 7, 13]).order(:id)
    @featured_products = Product.featured_list
  end

  def test_mandrill_send
    MandrillMailer.template(params[:email], Mandrill.new.template_info("Test Mandrill")).deliver

    redirect_to :back
  end

  def select_language
    cookies[:locale] = if GlobalLanguage.active.pluck(:abbr).include? params[:language]
                         params[:language]
                       else
                         I18n.default_locale
                       end
    redirect_to :back
  end

  def transfer_page
    if transfer_page = TransferPage.all.detect{|t| t.name == params[:transfer_name]}
      redirect_to transfer_page.url
    else
      goto_404
    end
  end
end
