class ContactsController < ApplicationController
  def show
    @seo = Seo.find_by_url("/contact_us")
    @contact = Contact.new
  end

  def create
    @seo = Seo.find_by_url("/contact_us")
    @contact = Contact.new(params[:contact])
    if verify_recaptcha
      if @contact.valid?
        ContactMailer.contact(@contact).deliver
        render "confirmation"
      else
        render :show
      end
    else
      flash.now[:notice] = "Captcha is not correct"
      render :show
    end
  end
end
