class ProductsController < ApplicationController
  before_filter :transfer_page_to, only: [:show]

  def index
    @seo = Seo.find_by_url("/products")
    @products = Product.for_client(current_client).search(params[:q]).result(distinct: true).default_sort.page(params[:page]).per(BASE_CONFIG[:page])
    @disorders = Disorder.related_with_product(@products)

    if request.xhr?
      render partial: "product_list"
    else
      render :index
    end
  end

  def show
    @product = Product.find(params[:id])
    redirect_to action: :index, status: 403 and return unless Product.for_client(current_client).include? @product
    @cart_item = CartItem.new
  end

  def search
    products = Product.for_client(current_client, false, name_cont: params[:name_cont])
    render json: products.map{|p| {id: p.id, name: p.name}}
  end
end
