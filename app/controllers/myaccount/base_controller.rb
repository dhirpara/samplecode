class Myaccount::BaseController < ApplicationController
  layout 'myaccount'
  before_filter :authenticate_client!
  skip_before_filter :set_referral_code

  private
  def use_https?
    true
  end
end
