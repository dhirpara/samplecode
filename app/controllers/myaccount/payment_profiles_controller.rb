class Myaccount::PaymentProfilesController < Myaccount::BaseController
  def destroy
    resource = PaymentProfile.find(params[:id])
    authorize_action_for(resource)

    resp = Payment.gateway_by_currency(resource.currency).delete(resource.customer_code)

    if resp.success?
      resource.destroy
    end

    flash[:notice] = resp.message

    redirect_to :back
  end
end
