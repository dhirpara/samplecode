class Myaccount::OrdersController < Myaccount::BaseController
  def index
    @orders = current_client.orders.where(state: "complete").order("completed_at desc").page(params[:page]).per(BASE_CONFIG[:page])
  end

  def show
    @order = current_client.orders.find(params[:id])
  end
end
