class Myaccount::PreOrdersController < Myaccount::BaseController
  def index
    @pre_orders = current_client.pre_orders.page(params[:page]).per(BASE_CONFIG[:page])
  end

  def show
    @pre_order = current_client.pre_orders.find(params[:id])
  end

  def destroy
    @pre_order = current_client.pre_orders.find(params[:id])
    @pre_order.destroy

    redirect_to :back
  end
end
