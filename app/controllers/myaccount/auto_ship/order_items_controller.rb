class Myaccount::AutoShip::OrderItemsController < Myaccount::AutoShip::BaseController
  include ::AutoShip::OrderItemsC

  protected
  def update_items_link
    update_items_myaccount_auto_ship_url(current_auto_ship)
  end

  def detail_link(order_item)
    myaccount_auto_ship_order_item_url(current_auto_ship, order_item)
  end

  def list_link
    myaccount_auto_ship_order_items_url(current_auto_ship)
  end

  def address_link
    myaccount_auto_ship_address_url(current_auto_ship)
  end

  def search_link
    "/products/search.json"
  end
end
