class Myaccount::AutoShip::AddressesController < Myaccount::AutoShip::BaseController
  include ::AutoShip::AddressC
  include AddressC

  protected
  def detail_link
    myaccount_auto_ship_address_url(current_auto_ship)
  end

  def use_address_link(address)
    select_myaccount_auto_ship_address_url(current_auto_ship, id: address.id)
  end

  def shipping_method_link
    myaccount_auto_ship_shipping_method_url(current_auto_ship)
  end
end
