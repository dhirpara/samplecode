class Myaccount::AutoShip::BaseController < Myaccount::BaseController
  include ::AutoShip::BaseC

  #helper_method :client, :current_order, :order

  protected
  def client
    current_client
  end

  def auto_shipping_detail_link(auto_ship = current_auto_ship)
    myaccount_auto_ship_url(auto_ship)
  end

  def authenticate_auto_shipping
    render file: "#{Rails.root}/public/403.html", status: 403, layout: false unless client.auto_shippings.include? current_auto_ship
  end
end
