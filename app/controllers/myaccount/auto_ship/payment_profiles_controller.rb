class Myaccount::AutoShip::PaymentProfilesController < Myaccount::AutoShip::BaseController
  include PaymentFormInfo
  include ::AutoShip::PaymentProfileC
  helper_method :delete_payment_profile_url

  protected
  def detail_link
    myaccount_auto_ship_payment_profile_url(current_auto_ship)
  end

  def delete_payment_profile_url(payment_profile)
    myaccount_payment_profile_url(payment_profile)
  end
end
