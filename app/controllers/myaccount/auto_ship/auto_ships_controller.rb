class Myaccount::AutoShip::AutoShipsController < Myaccount::AutoShip::BaseController
  include ::AutoShip::AutoShipC

  def generate_order
    #TODO
    if Rails.env.development?
      auto_ship = AutoShipping.find(params[:id])
      if params[:test] == "email"
        AutoShipMailer.remind(auto_ship).deliver
      else
        auto_ship.generate_order
      end
      redirect_to :back
    end
  end

  protected
  def new_link
    myaccount_auto_ships_url
  end

  def detail_link
    myaccount_auto_ship_url(current_auto_ship)
  end

  def invoice_link
    invoice_myaccount_auto_ship_url(current_auto_ship)
  end

  def order_items_link
    myaccount_auto_ship_order_items_url(current_auto_ship)
  end

  def address_link
    myaccount_auto_ship_address_url(current_auto_ship)
  end

  def shipping_method_link
    myaccount_auto_ship_shipping_method_url(current_auto_ship)
  end

  def payment_profile_link
    myaccount_auto_ship_payment_profile_url(current_auto_ship)
  end
end
