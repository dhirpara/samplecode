class Myaccount::AutoShip::ShippingMethodsController < Myaccount::AutoShip::BaseController
  include ::AutoShip::ShippingMethodC

  protected
  def detail_link
    myaccount_auto_ship_shipping_method_url(current_auto_ship)
  end

  def payment_link
    myaccount_auto_ship_payment_profile_url(current_auto_ship)
  end
end
