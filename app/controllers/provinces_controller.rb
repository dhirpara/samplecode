class ProvincesController < ApplicationController
  def index
    provinces = Country.find(params[:country_id]).provinces.order(:name)

    render partial: "provinces/field", locals: { field_name: params[:field_name], provinces: provinces }
  end

  private
  def use_https?
    true
  end
end
