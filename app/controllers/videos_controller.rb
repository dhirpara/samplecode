class VideosController < ApplicationController
  def index
    @seo = Seo.find_by_url("/videos")
    @disorders = Disorder.related_with_video
    @videos = Video.search(params[:q]).result(distinct: true).default_sort.page(params[:page]).per(BASE_CONFIG[:page])

    if request.xhr?
      render partial: "video_list"
    else
      render :index
    end
  end

  def show
    @video = Video.find(params[:id])
  end
end
