class SupportUsers::SessionsController < Devise::SessionsController
  before_filter :check_password, only: :create

  def check_password
    support_user = SupportUser.find_for_database_authentication(params[:support_user])
    redirect_to new_support_user_session_url, alert: "This user is inactive, please contact admin" and return if support_user && !support_user.active?
    if support_user && support_user.encrypted_password.try(:blank?)
      redirect_to new_support_user_password_url, alert: "please reset password" and return
    end
  end

  private
  def use_https?
    true
  end
end
