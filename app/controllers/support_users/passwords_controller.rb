class SupportUsers::PasswordsController < Devise::PasswordsController
  private
  def use_https?
    true
  end
end
