class ArticlesController < ApplicationController
  def index
    @seo = Seo.find_by_url("/articles")
    @articles = Article.search(params[:q]).result(distinct: true).default_sort.page(params[:page]).per(BASE_CONFIG[:page])
  end

  def show
    @article = Article.find(params[:id])
    #goto_404 and return if @article.nil?
    #@related_articles = Article.search(id_not_eq: @article.id, article_disorders_disorder_id_in: @article.disorder_ids).result(distinct: true).default_sort
  end
end
