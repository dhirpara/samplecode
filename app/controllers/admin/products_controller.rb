class Admin::ProductsController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  include FileUpload
  include TranslatesManagement
  before_filter :form_info, only: [:edit]
  authorize_actions_for Product, actions: { search: :search, upload_label_file: :update, preview: :update }
  upload_action column: :label_file, params: "[:product][:label_file]"
  before_filter :check_destroy, only: :destroy

  def search
    client = Client.find(params[:client_id])
    products = Product.for_client(client, true, name_cont: params[:name_cont])
    render json: products.map{|p| {id: p.id, name: p.name}}
  end

  def new
    build_resource
    resource.g_product_prices
    form_info
  end

  def update
    if resource.update_attributes(resource_params)
      #resource.delete_non_essential_product_disorders
      redirect_to admin_products_url
    else
      form_info
      render :edit
    end
  end

  def preview
    build_resource if params[:id] == "0"
    resource.assign_attributes(params[:product])
    if resource.valid?
      render partial: "preview"
    else
      render partial: "admin/share/error_messages", locals: { resource: resource }
    end
  end


  protected
  def check_destroy
    redirect_to :back, notice: I18n.t("errors.messages.cannot_destroy.active") if resource.active?
  end

  def form_info
    @product_categories = ProductCategory.order("id")
    @currencies = Currency.where("name in (?)", ["USD", "CAD"]).order("id")
    @disorders = Disorder.order(:name)

    #selected_product_disorders = resource.product_disorders.select do |product_disorder|
      #product_disorder.choose.nil? || product_disorder.choose
    #end

    #selected_product_disorders.each{|product_disorder| product_disorder.choose = true }

    #@product_disorders = (Disorder.where(deleted: false) - selected_product_disorders.map(&:disorder)).inject([]) do |array, disorder|
      #product_disorder = ProductDisorder.new
      #product_disorder.disorder = disorder
      #array << product_disorder
    #end + selected_product_disorders
    #@product_disorders.sort!{|x, y| x.disorder.name <=> y.disorder.name }


    @product_types = ProductType.all
  end

  def search_q
    if params[:q]
      params[:q].keep_if{|k, v| ["active_eq"].include? k}
    else
      { active_eq: true }
    end
  end

  def collection
    get_collection_ivar || begin
    set_collection_ivar(end_of_association_chain.search(search_q).result.order("#{sort_column(nil, "index")} #{sort_direction}").page(params[:page]).per(BASE_CONFIG[:page]))
    end
  end

  def resource_params
    normal_params = params[resource_request_name] || params[resource_instance_name] || {}

    if current_support_user.is_admin?
      normal_params
    else
      normal_params.select{|a| (resource_class::TRANSLATES_ATTRIBUTES + [:label_file_cache, :remove_label_file, :disorder_ids, :product_images_attributes]).include?(a.to_sym)  }
    end
  end
end
