class Admin::ProductPromotionsController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  authorize_actions_for ProductPromotion
  before_filter :form_info, only: :edit

  def new
    build_resource
    form_info
  end

  def update
    if resource.update_attributes(params[:product_promotion])
      resource.delete_non_essential_promotion_pricing_type
      redirect_to admin_product_promotions_url
    else
      form_info
      render :edit
    end
  end

  protected
  def form_info
    @products = Product.where(active: true).default_sort

    selected_promotion_pricing_types = resource.promotion_pricing_types.select do |resource|
      resource.choose.nil? || resource.choose
    end

    selected_promotion_pricing_types.each{|resource| resource.choose = true }

    @promotion_pricing_types = (PricingType.all - selected_promotion_pricing_types.map(&:pricing_type)).inject([]) do |array, pricing_type|
      resource = PromotionPricingType.new
      resource.pricing_type = pricing_type
      array << resource
    end + selected_promotion_pricing_types
    @promotion_pricing_types.sort!{|x, y| x.pricing_type.id <=> y.pricing_type.id }
  end
end
