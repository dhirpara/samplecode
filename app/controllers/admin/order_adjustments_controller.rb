class Admin::OrderAdjustmentsController < Admin::ChangeOrderBaseController
  inherit_resources
  include BaseAdmin
  belongs_to :order
  authorize_actions_for OrderAdjustment
  before_filter :check_cancel_state, except: :index

  def new
    redirect_to [:admin, order], status: 403 and return unless order.can_change_order_adjustment?
    build_resource
    form_info
  end

  def create
    redirect_to [:admin, order], status: 403 and return unless order.can_change_order_adjustment?
    resource = build_resource
    if resource.save
      redirect_to admin_order_order_adjustments_url
    else
      form_info
      render :new
    end
  end

  def edit
    redirect_to [:admin, order], status: 403 and return unless order.can_change_order_adjustment?
    form_info
    edit!
  end

  def update
    redirect_to [:admin, order], status: 403 and return unless order.can_change_order_adjustment?
    if resource.update_attributes(params[:order_adjustment])
      redirect_to admin_order_order_adjustments_url
    else
      form_info
      render :edit
    end
  end

  def destroy
    redirect_to [:admin, order], status: 403 and return unless order.can_change_order_adjustment?
    resource.destroy
    redirect_to action: :index
  end

  private
  def form_info
    @adjustment_types = resource.can_select_adjustment_types
  end
end
