class Admin::ShippingMethodsController < Admin::ChangeOrderBaseController
  authorize_actions_for ShippingMethod, actions: { select: :select }
  before_filter :check_cancel_state, only: :select
  def select
    redirect_to [:admin, order], status: 403 and return unless order.can_change_shipping_method?
    shipping_method = order.shipment.available_shipping_methods.find_by_id(params[:shipping_method_id])
    if shipping_method.present?
      order.update_shipping_method(shipping_method)
      if current_support_user.can_read? Payment
        redirect_to admin_order_payments_url(order)
      else
        redirect_to [:admin, order]
      end
    else
      form_info
      redirect_to edit_shipping_method_admin_order_shipments_url(order), notice: "Please select shipping method"
    end
  end

  private
  def form_info
    @shipping_methods = order.shipment.available_shipping_methods(with_price: true)
  end
end
