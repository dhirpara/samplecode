class Admin::CommissionRatesController < Admin::ApplicationController
  authorize_actions_for CommissionRate
  inherit_resources
  include BaseAdmin
  belongs_to :support_user

  protected
  def search_q
    { commission_type_id_eq: parent.commission_type.id }
  end

  def collection
    get_collection_ivar || begin
    set_collection_ivar(end_of_association_chain.search(search_q).result.order("#{sort_column(nil, "min")} #{sort_direction}").page(params[:page]).per(BASE_CONFIG[:page]))
    end
  end
end
