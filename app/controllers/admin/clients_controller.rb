class Admin::ClientsController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  authority_actions search: 'search'
  authorize_actions_for Client, actions: { select: :select, cancel_select: :select, receive_newletter: :update, impersonate: :select }

  def index
    if search_q.present?
      @clients = current_support_user.clients.search(search_q).result(distinct: true).order("#{sort_column} #{sort_direction}").page(params[:page]).per(BASE_CONFIG[:page])
      @view_client_reports = []
    else
      @clients = []
      @view_client_reports = current_support_user.view_client_reports.order("datetime desc").page(params[:page]).per(15)
    end
  end

  def new
    @client = Client.new(active: true, contact_date: Time.zone.now, support_center_id: SupportCenter.find_by_code("HNC").try(:id))
    form_info
  end

  def edit
    authorize_action_for(resource)
    ViewClientReport.generate_or_update_report(resource, current_support_user)
    form_info
    edit!
  end

  def create
    build_resource
    resource.checkout_address = true
    resource.check_home_phone = true
    resource.back_end = true
    resource.support_user = current_support_user unless current_support_user.is_admin?
    resource.check_support_center = true
    if resource.submit?({generate_address: true})
      session[:select_client_id] = resource.id
      redirect_to edit_admin_client_url(resource), notice: "Created Successful"
    else
      form_info
      render :new
    end
  end

  def update
    authorize_action_for(resource)
    resource.check_home_phone = true
    if params[:client][:password].blank?
      params[:client].delete("password")
      params[:client].delete("password_confirmation")
    end

    resource.assign_attributes(params[:client])

    if resource.submit?
      session[:select_client_id] = resource.id
      redirect_to edit_admin_client_url(resource), notice: "Updated Successful"
    else
      form_info
      render :edit
    end
  end

  def destroy
    authorize_action_for resource
    if resource.deleted? && !resource.active?
      destroy!(notice: "#{resource_class.name} was successfully deleted.")
    elsif resource.active?
      redirect_to :back, notice: I18n.t("errors.messages.cannot_destroy.active")
    else
      redirect_to :back, notice: I18n.t("errors.messages.cannot_destroy.delete")
    end
  end

  def select
    authorize_action_for resource
    ViewClientReport.generate_or_update_report(resource, current_support_user)
    session[:select_client_id] = params[:id]
    #redirect_to
    redirect_to redirect_to_or_default_url(admin_orders_url)
  end

  def cancel_select
    authorize_action_for resource
    session[:select_client_id] = nil
    redirect_to :back
  end

  def receive_newletter
    authorize_action_for resource

    resource.assign_attributes(params[:client])

    unless resource.is_guest?
      resource.save
    end

    if resource.receive_update?
      Email.subscribe(resource)
    else
      Email.unsubscribe(resource)
    end

    redirect_to :back
  end

  def impersonate
    @client = Client.find_by_id(params[:id])
    sign_in(:client, @client)
    session[:select_client_id] = @client.id
    redirect_to after_sign_in_path_for(@client)
  end

  protected
  def form_info
    @countries = Country.order(:id)
    @contact_times = ContactTime.active
    @introduction_types = IntroductionType.active.default_sort
    @register_methods = RegistrationMethod.active
    @call_frequency = CallFrequency.active.order("priority")
    @pricing_types = PricingType.all
    @support_users = SupportUser.active if current_support_user.is_admin?
    @support_centers = current_support_user.support_centers
    @languages = Language.all
    @currencies = Currency.all
    @quit_reasons = QuitReason.all
    @age_categories = AgeCategory.all
    @ta_levels = TaLevel.active
    @provinces = resource.country.try(:provinces)# || @countries.first.provinces
    @limiting_factors = LimitingFactor.all
    @diagnoses = Diagnosis.all
  end

  def redirect_to_or_default_url(default_url)
    params[:redirect_to_url] || default_url
  end

  def search_q
    params[:q]
  end

  def default_sort_column
    "first_name"
  end
end
