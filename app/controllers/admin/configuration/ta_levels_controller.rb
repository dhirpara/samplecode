class Admin::Configuration::TaLevelsController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for TaLevel
  before_filter :check_destroy, only: :destroy
end
