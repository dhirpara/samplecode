class Admin::Configuration::HandlingChargesController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for HandlingCharge
  before_filter :check_destroy, only: :destroy

  def create
    resource = build_resource
    resource.handling_charge_method_id = 2
    if resource.save
      redirect_to admin_configuration_handling_charges_url, notice: "Handling charge was successfully created."
    else
      render :new
    end
  end
end
