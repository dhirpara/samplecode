class Admin::Configuration::UserGroupsController < Admin::ApplicationController
	inherit_resources
  include BaseAdmin

	def index
		@user_groups = UserGroup.all
	end

	def show
  	@user_group = UserGroup.find(params[:id])
  end

	def new
    @user_group = UserGroup.new
	end

	def create
    @user_group = UserGroup.new(params[:user_group])

    if @user_group.save
      redirect_to admin_configuration_user_groups_url
    else
      render :new, notice: 'create user group failed'
    end
  end


	def edit
		@user_group = UserGroup.find(params[:id])
	end
end
