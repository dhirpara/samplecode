class Admin::Configuration::IntroductionTypesController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for IntroductionType
  before_filter :check_destroy, only: :destroy

  private
  def default_sort_column
    "index"
  end
end
