class Admin::Configuration::CountryShippingAdjustmentsController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for CountryShippingAdjustment
  before_filter :form_info, only: [:new, :edit]
  before_filter :check_destroy, only: :destroy

  private
  def form_info
    @countries = Country.all
  end
end
