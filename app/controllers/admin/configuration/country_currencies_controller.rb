class Admin::Configuration::CountryCurrenciesController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for CountryCurrency

  def show
    form_info
    @country_currency = CountryCurrency.new
  end

  def create
    @country_currency = CountryCurrency.new(params[:country_currency])
    @country_currency.submit
    redirect_to admin_configuration_country_currency_url, notice: "Updated Successful"
  end

  private
  def form_info
    @currencies = Currency.all
  end
end
