class Admin::Configuration::TransferPagesController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for TransferPage
end
