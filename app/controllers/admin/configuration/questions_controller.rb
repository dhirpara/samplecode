class Admin::Configuration::QuestionsController < Admin::ApplicationController
	inherit_resources
  include BaseAdmin

	def index
		@questions = Question.all
	end

	def show
  	@question = Question.find(params[:id])
  end

	def new
    @question = Question.new
	end

	def create
		
    @question = Question.new(params[:question])

    if @question.save
      redirect_to admin_configuration_questions_url
    else
      render :new, notice: 'create question failed'
    end
  end

	def edit
		@question = Question.find(params[:id])
	end
end
