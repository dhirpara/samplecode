class Admin::Configuration::SeosController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for Seo
  include TranslatesManagement
end
