class Admin::Configuration::QuestionnairesController < Admin::ApplicationController
	inherit_resources
  include BaseAdmin

	def index
		@questionnaires = Questionnaire.all
	end

	def show
  	@questionnaire = Questionnaire.find(params[:id])
  end

	def new
    @questionnaire = Questionnaire.new
	end

	def create
    @questionnaire = Questionnaire.new(params[:questionnaire])

    if @questionnaire.save
      redirect_to admin_configuration_questionnaires_url
    else
      render :new, notice: 'create questionnaire failed'
    end
  end

	def edit
		@questionnaire = Questionnaire.find(params[:id])
	end
end
