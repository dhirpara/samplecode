class Admin::Configuration::CallFrequenciesController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for CallFrequency
  before_filter :check_destroy, only: :destroy
end
