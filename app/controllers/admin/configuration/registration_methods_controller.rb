class Admin::Configuration::RegistrationMethodsController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for RegistrationMethod
  before_filter :check_destroy, only: :destroy
end
