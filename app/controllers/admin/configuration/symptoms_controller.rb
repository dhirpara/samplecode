class Admin::Configuration::SymptomsController < Admin::ApplicationController
	inherit_resources
  include BaseAdmin

	def index
		@symptoms = Symptom.all
	end

	def show
  	@symptom = Symptom.find(params[:id])
  end

	def new
    @symptom = Symptom.new
	end

	def create
    @symptom = Symptom.new(params[:symptom])

    if @symptom.save
      redirect_to admin_configuration_symptoms_url
    else
      render :new, notice: 'create symptom failed'
    end
  end


	def edit
		@symptom = Symptom.find(params[:id])
	end
end
