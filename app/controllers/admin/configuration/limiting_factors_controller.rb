class Admin::Configuration::LimitingFactorsController < Admin::ApplicationController
  authorize_actions_for LimitingFactor

  inherit_resources
  include BaseAdmin
end
