class Admin::Configuration::DiagnosesController < Admin::ApplicationController
  authorize_actions_for Diagnosis

  inherit_resources
  include BaseAdmin
end
