class Admin::Configuration::DisordersController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for Disorder
  #before_filter :check_destroy, only: :destroy
  include TranslatesManagement
end
