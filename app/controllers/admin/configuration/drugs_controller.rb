class Admin::Configuration::DrugsController < Admin::ApplicationController
	inherit_resources
  include BaseAdmin

	def index
		@drugs = Drug.all
	end

	def show
  	@drug = Drug.find(params[:id])
  end

	def new
    @drug = Drug.new
	end

	def create
    @drug = Drug.new(params[:drug])

    if @drug.save
      redirect_to admin_configuration_drugs_url
    else
      render :new, notice: 'create drug failed'
    end
  end


	def edit
		@drug = Drug.find(params[:id])
	end
end
