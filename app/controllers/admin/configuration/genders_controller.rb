class Admin::Configuration::GendersController < Admin::ApplicationController
	inherit_resources
  include BaseAdmin

	def index
		@genders = Gender.all
	end

	def show
  	@gender = Gender.find(params[:id])
  end

	def new
    @gender = Gender.new
	end

	def create
    @gender = Gender.new(params[:gender])

    if @gender.save
      redirect_to admin_configuration_genders_url
    else
      render :new, notice: 'create gender failed'
    end
  end


	def edit
		@question = Question.find(params[:id])
	end
end
