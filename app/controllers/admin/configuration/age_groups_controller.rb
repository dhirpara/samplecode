class Admin::Configuration::AgeGroupsController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin

	def index
		@age_group = AgeGroup.all
	end

	def show
  	@age_group = AgeGroup.find(params[:id])
  end

	def new
    @age_group = AgeGroup.new
	end

	def create
    @age_group = AgeGroup.new(params[:age_group])

    if @age_group.save
      redirect_to admin_configuration_age_groups_url
    else
      render :noticew, notice: 'create age group failed'
    end
  end


	def edit
		@age_group = AgeGroup.find(params[:id])
	end
end
