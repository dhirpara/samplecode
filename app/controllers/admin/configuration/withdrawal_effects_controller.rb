class Admin::Configuration::WithdrawalEffectsController < Admin::ApplicationController
	inherit_resources
  include BaseAdmin

	def index
		@withdrawal_effects = WithdrawalEffect.all
	end

	def show
  	@withdrawal_effect = WithdrawalEffect.find(params[:id])
  end

	def new
    @withdrawal_effect = WithdrawalEffect.new
	end

	def create
    @withdrawal_effect = WithdrawalEffect.new(params[:withdrawal_effect])

    if @withdrawal_effect.save
      redirect_to admin_configuration_withdrawal_effects_url
    else
      render :new, notice: 'create withdrawal effect failed'
    end
  end


	def edit
		@withdrawal = WithdrawalEffect.find(params[:id])
	end
end
