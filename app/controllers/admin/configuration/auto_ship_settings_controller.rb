class Admin::Configuration::AutoShipSettingsController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  defaults singleton: true

  authorize_actions_for AutoShipSetting

  def show
    @auto_ship_setting = AutoShipSetting.first
    form_info
  end

  def update
    @auto_ship_setting = AutoShipSetting.first
  
    if @auto_ship_setting.update_attributes(params[:auto_ship_setting])
      flash[:notice] = "Auto Ship Setting was successfully updated."
      redirect_to admin_configuration_auto_ship_settings_path
    else
      form_info
      render :show
    end
  end

  private
  def form_info
    @pricing_types = PricingType.all
  end
end
