class Admin::Configuration::BaseConfiguationController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin

  private
  def search_q
    if params[:q]
      params[:q].keep_if{|k, v| ["deleted_eq"].include? k}
    else
      { deleted_eq: false }
    end
  end

  def check_destroy
    redirect_to :back, notice: I18n.t("errors.messages.cannot_destroy.delete") unless resource.deleted?
  end
end
