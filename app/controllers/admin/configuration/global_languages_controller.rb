class Admin::Configuration::GlobalLanguagesController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for GlobalLanguage

  def index
    @can_select_languages = GlobalLanguage.where(active: false).order(:name)
    index!
  end

  def create
    if language = GlobalLanguage.find_by_abbr(params[:abbr])
      language.update_attributes(active: true)
      flash.notice = "#{language.name} was successfully added"
    end

    redirect_to :back
  end

  def destroy
    if language = GlobalLanguage.find_by_id(params[:id])
      language.update_attributes(active: false)
      flash.notice = "#{language.name} was successfully deleted"
    end

    redirect_to :back
  end

  protected
  def search_q
    { active_true: true }
  end
end
