class Admin::Configuration::SideEffectsController < Admin::ApplicationController
	inherit_resources
  include BaseAdmin

	def index
		@side_effects = SideEffect.all
	end

	def show
  	@side_effect = SideEffect.find(params[:id])
  end

	def new
    @side_effect = SideEffect.new
	end

	def create
    @side_effect = SideEffect.new(params[:side_effect])

    if @side_effect.save
      redirect_to admin_configuration_side_effects_url
    else
      render :new, notice: 'create side effects failed'
    end
  end


	def edit
		@side_effect = SideEffect.find(params[:id])
	end
end
