class Admin::Configuration::ContactTimesController < Admin::Configuration::BaseConfiguationController
  authorize_actions_for ContactTime
  before_filter :check_destroy, only: :destroy
end
