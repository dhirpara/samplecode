class Admin::NotesController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  authorize_actions_for Note
  before_filter :check_current_client
  before_filter :check_destroy, only: :destroy

  def index
    @notes = current_client.notes.search(search_q).result.order("#{sort_column(Note, "created_at")} #{sort_direction('desc')}")#.page(params[:page]).per(BASE_CONFIG[:page])
    @note = Note.new
  end

  def create
    build_resource
    resource.client = current_client
    resource.support_user = current_support_user
    if resource.save
      redirect_to collection_url, notice: "Create success"
    else
      render :index
    end
  end

  def update
    update!(location: "#{collection_url}#note_#{resource.id}")
  end

  protected
  def check_current_client
    redirect_to edit_user_registration_path and return unless current_client
  end

  def check_destroy
    redirect_to :back, notice: I18n.t("errors.messages.cannot_destroy.delete") unless resource.deleted?
  end

  def search_q
    if params[:q]
      params[:q].keep_if{|k, v| ["deleted_eq"].include? k}
    else
      { deleted_eq: false }
    end
  end
end
