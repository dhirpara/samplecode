class Admin::ChangeOrderBaseController < Admin::ApplicationController
  helper_method :order, :current_order

  protected
  def check_cancel_state
    redirect_to :back, notice: I18n.t("canceled_order") if order.canceled?
  end

  def order
    @order ||= Order.find(params[:order_id])
  end
  alias :current_order :order

  helper_method :can_create_order_item?, :can_update_order_item?, :can_destroy_order_item?, :can_return_order_item?

  def can_create_order_item?
     order.can_change_order_items? && current_support_user.can_create?(OrderItem)
  end

  def can_update_order_item?
    order.can_change_order_items? && current_support_user.can_update?(OrderItem)
  end

  def can_destroy_order_item?(item)
    order.can_change_order_items? && current_support_user.can_delete?(OrderItem) && item.parent.nil?
  end

  def can_return_order_item?(item)
    !order.can_change? && item.quantity > 0 && item.can_return_quantity > 0 && current_support_user.can_update?(OrderItem)
  end
end
