class Admin::PaymentProfilesController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin

  belongs_to :client

  def destroy
    authorize_action_for(resource)

    resp = Payment.gateway_by_currency(resource.currency).delete(resource.customer_code)

    if resp.success?
      resource.destroy
    end

    flash[:notice] = resp.message

    redirect_to :back
  end
end
