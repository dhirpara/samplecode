class Admin::FaqsController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  include TranslatesManagement
  before_filter :form_info, only: [:new, :edit]
  authorize_actions_for Faq, actions: { preview: :update }

  def preview
    build_resource if params[:id] == "0"
    resource.assign_attributes(params[:faq])
    if resource.valid?
      render partial: "faqs/block", locals: { faq: resource }
    else
      render partial: "admin/share/error_messages", locals: { resource: resource }
    end
  end

  protected
  def form_info
    @faq_types = FaqType.all
  end

  def collection
    if params[:sort]
      super
    else
      get_collection_ivar || begin
      set_collection_ivar(end_of_association_chain.joins(:faq_type).search(search_q).result.order("faq_types.index").order("faqs.index").page(params[:page]).per(BASE_CONFIG[:page]))
      end
    end
  end
end
