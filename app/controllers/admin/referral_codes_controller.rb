class Admin::ReferralCodesController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  authorize_actions_for ReferralCode
  before_filter :form_info, only: :edit

  def new
    @referral_code = ReferralCode.new(start_date: Date.current, end_date: Date.current)
    form_info
  end

  protected
  def form_info
    @pricing_types = PricingType.all
    @support_users = SupportUser.active
    @product_promotions = ProductPromotion.where(use_only_for_referral_code: true)
  end
end
