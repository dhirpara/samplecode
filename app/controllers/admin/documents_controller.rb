class Admin::DocumentsController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  include FileUpload
  authorize_actions_for Study, actions: { upload_file: :update }
  upload_action column: :file, params: "[:document][:file]"

  before_filter :form_info, only: :edit

  def new
    form_info
    build_resource
  end

  protected
  def form_info
    @article_categories = ArticleCategory.all
  end
end
