class Admin::OrderItemsController < Admin::ChangeOrderBaseController
  inherit_resources
  include BaseAdmin
  belongs_to :order
  authorize_actions_for OrderItem, actions: { new_return: :return, return: :return }

  before_filter :state_filter, only: :index

  def index
    @order_item = OrderItem.new(quantity: 1)
    @referral_codes = ReferralCode.all
  end

  def create
    redirect_to [:admin, order], status: 403 and return unless can_create_order_item?
    Product.find(params[:order_item][:product_id])
    order_item = OrderItem.new(params[:order_item])
    order_item.quantity = order_item.quantity == 0 ? 1 : order_item.quantity.abs
    #order_item.pricing_type = order.client.pricing_type
    if order_item.valid?
      order.add_item(order_item)
      @referral_codes = ReferralCode.all
      render partial: "list"
    else
      render text: "something were wrong, please try again", status: 422
    end
  end

  def destroy
    redirect_to [:admin, order], status: 403 and return unless can_destroy_order_item?(resource)
    order.remove_order_item(resource)
    @referral_codes = ReferralCode.all
    render partial: "list"
  end

  def new_return
  #  redirect_to [:admin, order], status: 403 and return unless can_return_order_item?(resource)

    authorize_action_for(resource)
    @return_form = OrderItem::ReturnForm.new(resource)
  end

  def return
  #  redirect_to [:admin, order], status: 403 and return unless can_return_order_item?(resource)
    authorize_action_for(resource)
    @return_form = OrderItem::ReturnForm.new(resource, params[:order_item_return_form])
    if @return_form.submit?
      redirect_to admin_order_order_items_url(order)
    else
      render :new_return
    end
  end

  protected
  def state_filter
    return if order.canceled?
    order.rollback_to_cart unless order.complete?
  end
end
