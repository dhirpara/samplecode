class Admin::ArticlesController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  include FileUpload
  include TranslatesManagement
  before_filter :form_info, only: [:edit]
  authorize_actions_for Article, actions: { upload_image: :update, upload_author_image: :update, preview: :update }
  upload_action column: :image, params: "[:article][:image]"
  upload_action column: :author_image, params: "[:article][:author_image]"

  def new
    build_resource
    form_info
  end

  def update
    if resource.update_attributes(resource_params)
      #resource.delete_non_essential_article_disorders
      redirect_to admin_articles_url
    else
      form_info
      render :edit
    end
  end

  def preview
    build_resource if params[:id] == "0"
    resource.assign_attributes(params[:article])
    if resource.valid?
      render partial: "articles/detail", locals: { article: resource }
    else
      render partial: "admin/share/error_messages", locals: { resource: resource }
    end
  end

  protected
  def form_info
    @article_categories = ArticleCategory.all
    @disorders = Disorder.order(:name)


    #selected_article_disorders = resource.article_disorders.select do |article_disorder|
      #article_disorder.choose.nil? || article_disorder.choose
    #end

    #selected_article_disorders.each{|article_disorder| article_disorder.choose = true }

    #@article_disorders = (Disorder.all - selected_article_disorders.map(&:disorder)).inject([]) do |array, disorder|
      #article_disorder = ArticleDisorder.new
      #article_disorder.disorder = disorder
      #array << article_disorder
    #end + selected_article_disorders
    #@article_disorders.sort!{|x, y| x.disorder.name <=> y.disorder.name }
  end

  def collection
    get_collection_ivar || begin
    set_collection_ivar(end_of_association_chain.search(search_q).result.order("#{sort_column(nil, "index")} #{sort_direction}").page(params[:page]).per(BASE_CONFIG[:page]))
    end
  end
end
