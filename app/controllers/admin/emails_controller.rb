class Admin::EmailsController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin

  authorize_actions_for Email, actions: { export: :read }

  def export
    data = CSV.generate do |csv|
      csv << ["First Name", "Email", "Date Registered"]
      Email.search(search_q).result.each do |email|
        csv << [email.first_name, email.email, email.created_at.strftime("%Y-%m-%d")]
      end
    end
    send_data data, filename: "New_Letters_#{Time.zone.now.strftime("%Y%m%d%H%M%S")}.csv"
  end

  def search_q
    (params[:q] || {}).reverse_merge!({ token_blank: "1" })
  end
end
