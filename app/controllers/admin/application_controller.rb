class Admin::ApplicationController < ApplicationController
  layout 'admin'
  before_filter :authenticate_support_user!
  helper_method :current_client
  #再加载一次是为了在authenticate_support_user后执行
  before_filter :https_redirect, except: :root
  skip_before_filter :set_referral_code

  alias :user_for_this_request :current_support_user

  def root
    redirect_to support_user_redirect_to(current_support_user)
  end

  def current_client
    @current_client ||= Client.find_by_id(session[:select_client_id])
  end

  def use_https?
    true
  end
end
