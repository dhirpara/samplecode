class Admin::PaymentsController < Admin::ChangeOrderBaseController
  inherit_resources
  include BaseAdmin
  include PaymentFormInfo
  belongs_to :order
  before_filter :can_change_by_user?, only: [:new_refund, :refund, :void, :edit, :update]
  before_filter :check_cancel_state, only: [:new, :create]
  before_filter :check_referral_code, only: [:new, :create]

  helper_method :delete_payment_profile_url

  authorize_actions_for Payment, actions: { void: :create, refund: :create, new_refund: :create, pay_by_credit: :create }

  def index
    if order.canceled?
      if order.order_items.any?
        if order.shipment.address.present? && order.shipment.shipping_method.present?
          @client = order.client
          render :index
        else
          redirect_to admin_order_shipments_url(order)
        end
      else
        redirect_to admin_order_order_items_url(order)
      end

      return
    end

    unless order.cart?
      if order.shipment.address.present? && order.shipment.shipping_method.present?
        if order.address?
          order.next
          order.next
        elsif order.delivery?
          order.next
        end
        @client = order.client
        index!
      else
        redirect_to admin_order_shipments_url(order)
      end
    else
      if order.order_items.any?
        if order.shipment.try(:address).try(:present?) && order.shipment.try(:shipping_method).try(:present?)
          order.next
          order.next
          order.next
          @client = order.client
        else
          redirect_to admin_order_shipments_url(order)
        end
      else
        redirect_to admin_order_order_items_url(order)
      end
    end
  end

  def new
    redirect_to [:admin, order], status: 403 and return unless order.can_purchase?
    if session[:payment_params].try(:[], order.id) && session[:payment_params][order.id][:expire_at] > Time.zone.now
      @payment_new_form = Payment::NewForm.new(order, session[:payment_params][order.id][:payment_new_form], back_end: true)
    end
    form_info
  end

  def create
    redirect_to [:admin, order], status: 403 and return unless order.can_purchase?
    if params.has_key?(:save_params)
      save_params

      flash[:notice] = "Form information has been saved"
      redirect_to action: :new
    else
      @payment_new_form = Payment::NewForm.new(order, params[:payment_new_form], back_end: true, support_user: current_support_user)
      if @payment_new_form.submit?
        if @payment_new_form.purchase
          clear_payment_params

          if @payment_new_form.is_pre_order_purchase?
            flash[:notice] = "Successful add to Pre-Order"
          end

          redirect_to action: :index and return
        else
          flash.now[:notice] = @payment_new_form.resp.message
        end
      end

      form_info
      render :new
    end
  end

  def refund
    redirect_to [:admin, order], status: 403 and return unless resource.can_refund?
    #@refund_payment = order.payments.new(params[:payment])
    #@refund_payment.support_user = current_support_user
    @refund_form = Payment::RefundForm.new(resource, params[:payment_refund_form], support_user: current_support_user)
    #if @refund_payment.valid?
    if @refund_form.submit?
      #resp = resource.refund(@refund_payment)
      #redirect_to collection_url(order), notice: resp.message
      redirect_to collection_url(order)#, notice: @refund_form.notice
    else
      flash.now.alert = @refund_form.alert
      render :new_refund
    end
  end

  def new_refund
    redirect_to [:admin, order], status: 403 and return unless resource.can_refund?
    #@refund_payment = Payment.new(amount: resource.can_refund_amount)
    @refund_form = Payment::RefundForm.new(resource)
  end

  def void
    redirect_to [:admin, order], status: 403 and return unless resource.can_void?
    resource.support_user = current_support_user
    resp = resource.void
    flash[:notice] = resp.message
    redirect_to collection_url(order)
  end

  def edit
    authorize_action_for(resource)
    @payment_types = PaymentType.non_cc_type
  end

  def update
    authorize_action_for(resource)
    resource.support_user = current_support_user
    resource.assign_attributes(params[:payment])
    if resource.valid?
      resource.create_by_other_payment_type_payment

      redirect_to admin_order_payments_url(order), notice: "Update success"
    else
      @payment_types = PaymentType.non_cc_type
      render :edit
    end
  end

  def pay_by_credit
    redirect_to [:admin, order], status: 403 and return unless order.can_purchase?
    Payment.pay_by_credit(order, current_support_user)

    redirect_to :back
  end

  private
  def check_referral_code
    unless ReferralCodeOrder.find_by_order_id_and_referral_code_id(order.id, order.referral_code_id)
      if order.referral_code && ReferralCode.out_of_stock_for_resource?(order)
        redirect_to admin_order_order_items_url(order), notice: "The Referral Code cannot be used."
      end
    end
  end

  def save_params
    session[:payment_params] ||= {}
    session[:payment_params][order.id] = {}
    session[:payment_params][order.id][:payment_new_form] = params[:payment_new_form]
    session[:payment_params][order.id][:expire_at] = Time.zone.now + 15.minutes
  end

  def clear_payment_params
    if session[:payment_params].try(:[], order.id)
      session[:payment_params][order.id] = nil
    end
  end

  def can_change_by_user?
    redirect_to :back, notice: I18n.t("canceled_order") and return if order.canceled? && !current_support_user.is_admin?
  end

  #  def change_form_info
  #    @payment = order.payments.new(params[:payment])
  #    @payment.support_user = current_support_user
  #    super if @payment.payment_type.is_cc_type?
  #  end

  def form_info
    @payment_new_form ||= Payment::NewForm.new(order, {}, back_end: true)
    @payment_types = if @payment_new_form.is_pre_order_purchase?
                       PaymentType.where("name in (?)", ["Visa", "Mastercard", "Discover"])
                     elsif order.client.has_credit?
                       PaymentType
                     else
                       PaymentType.where("name != ?", "Credit")
                     end.order(:index)

                     if order.currency.name == "USD"
                       @payment_types = @payment_types.where("name != ?", "Discover")
                     end

                     #@payment_new_form.payment.payment_type ||= @payment_types.first
                     super
  end

  def delete_payment_profile_url(payment_profile)
    admin_client_payment_profile_url(order.client, payment_profile)
  end
end
