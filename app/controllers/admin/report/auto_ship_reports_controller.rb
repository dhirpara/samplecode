class Admin::Report::AutoShipReportsController < Admin::ApplicationController
  authorize_actions_for AutoShipReportAuthorizer

  def index
    @search = AutoShipReport.search(search_q)
    @auto_ship_reports = @search.result.order("date_of_shipment DESC")#.page(params[:page]).per(BASE_CONFIG[:page])
  end

  private
  def search_q
    search_q = params[:q] || { date_of_shipment_gteq: Date.current, date_of_shipment_lteq: Date.current }
    unless current_support_user.is_admin?
      search_q.merge!(client_id_in: current_support_user.client_ids)
    end
    search_q
  end
end
