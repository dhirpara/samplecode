class Admin::Report::CommissionsController < Admin::ApplicationController
  authorize_actions_for CommissionAuthorizer

  def index
    @support_users = SupportUser.commissioned

    if current_support_user.is_admin?
      @search = CommissionReport.search(search_q)
      @commission_reports =  if search_q[:support_user_id_eq].present?
                               @search.result.order("id desc")
                             else
                               []
                             end
    else
      @search = CommissionReport.search(search_q.merge!({support_user_id_eq: current_support_user.id}))
      @commission_reports = @search.result.order("id desc")
    end

    if search_q[:support_user_id_eq].present?
      @selected_support_user = @support_users.find_by_id(search_q[:support_user_id_eq])
    end
  end

  private
  #灵异问题
  def search_q
    params[:q] ||{ date_gteq: 5.days.ago, date_lteq: Date.current }
    #{
      #"date_gteq(1i)" => params[:q].try(:[], "date_gteq(1i)") || 5.days.ago.year,
      #"date_gteq(2i)" => params[:q].try(:[], "date_gteq(2i)") || 5.days.ago.month,
      #"date_gteq(3i)" => params[:q].try(:[], "date_gteq(3i)") || 5.days.ago.day,
      #"date_lteq(1i)" => params[:q].try(:[], "date_lteq(1i)") || Date.current.year,
      #"date_lteq(2i)" => params[:q].try(:[], "date_lteq(2i)") || Date.current.month,
      #"date_lteq(3i)" => params[:q].try(:[], "date_lteq(3i)") || Date.current.day,
    #}.merge({support_user_id_eq: params[:q].try(:[], :support_user_id_eq)})
  end

  def calculate_commissions(clients, from_date, to_date)
    commissioned_orders=[]
    clients.each do |client|
      if client.orders.any?
        orders = client.commission_calculated_orders(from_date, to_date)
        commissioned_orders << {client:client, orders: orders}
      end
    end
    commissioned_orders
  end
end
