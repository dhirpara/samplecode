class Admin::Report::LocationsController < Admin::ApplicationController
  authorize_actions_for LocationAuthorizer, actions: { export: :read, to_client: :read, back_to: :read  }
  helper_method :search_message
  def index
    @search = Client.search(search_q)
    @clients = @search.result(distinct: true).
      includes(:country).order("countries.name, clients.province, clients.city, clients.last_name").
      page(params[:page]).per(BASE_CONFIG[:page])


    @provinces = Country.find_by_id(@search.country_id_eq).try(:provinces) || []
    @countries = Country.all
    @ta_levels = TaLevel.active
    @support_centers = SupportCenter.available
    @support_users = SupportUser.active
    @pricing_types = PricingType.all
  end

  def to_client
    session[:location_report_url] = request.env["HTTP_REFERER"]
    redirect_to edit_admin_client_url(Client.find(params[:client_id]))
  end

  def back_to
    redirect_to session[:location_report_url]
    session.delete(:location_report_url)
  end

  def export
    search = Client.search(search_q)
    clients = search.result(distinct: true).
      includes(:country).order("countries.name, clients.province, clients.city, clients.last_name")

    file = CSV.generate do |csv|
      csv << [
        search_message(clients, search)
      ]
      csv << [
        "First Name",
        "Last Name",
        "Address 1",
        "Address 2",
        "City",
        "State/Province",
        "Country",
        "Postal Code/Zip",
        "Email",
        "Home Phone",
        "Cell Phone",
        "TA Level",
        "Support Center",
        "Support User",
        "Order #",
        "Pricing Type",
        "Product Purchased in Period",
        "Introduction Type"
      ]
      clients.each do |c|
        product_purchased_in_period = if c.order_ids.any?
                                        OrderReport.search({
                                          order_id_in: c.order_ids,
                                          "created_at_gteq(1i)" => params[:q]["created_at_gteq(1i)"],
                                          "created_at_gteq(2i)" => params[:q]["created_at_gteq(2i)"],
                                          "created_at_gteq(3i)" => params[:q]["created_at_gteq(3i)"],
                                          "created_at_lteq(1i)" => params[:q]["created_at_lteq(1i)"],
                                          "created_at_lteq(2i)" => params[:q]["created_at_lteq(2i)"],
                                          "created_at_lteq(3i)" => params[:q]["created_at_lteq(3i)"]
                                        }).result.sum(&:subtotal)
                                      else
                                        0
                                      end
        csv << [
          c.first_name,
          c.last_name,
          c.address_1,
          c.address_2,
          c.city,
          c.province,
          c.country.name,
          c.postal_code,
          c.email,
          c.home_phone,
          c.cell_phone,
          c.ta_level.try(:name),
          c.support_center.name,
          c.support_user.try(:first_last_name),
          c.orders.where(state: "complete").count,
          c.pricing_type.name,
          product_purchased_in_period,
          c.introduction_type.try(:name)
        ]
      end
    end

    send_data file, filename: "Location Report.csv"
  end

  private

  def search_message(clients, search)
    return '' unless params[:q]
    active_message = case search.active_eq
                     when true
                       "in Active"
                     when false
                       "in Inactive"
                     else
                       ""
                     end
    address_message = [
      Country.find_by_id(search.country_id_eq).try(:name) || "All Country",
      search.province_eq || search.province_cont || "All Province",
      search.city_cont || "All City"
    ].join(", ")

    "#{search.result(distinct: true).count} records found for #{active_message} in #{address_message} in #{TaLevel.find_by_id(search.ta_level_id_eq).try(:name) || "All"} TA Levels"
  end

  def search_q
    @search_q ||= params[:q] || {
      created_at_gteq: Date.today,
      :"created_at_lteq(1i)"=> Time.zone.now.year,
      :"created_at_lteq(2i)"=> Time.zone.now.month,
      :"created_at_lteq(3i)"=> Time.zone.now.day
    }
    if Country.find_by_id(@search_q[:country_id_eq])
      @search_q.delete(:province_cont)
    else
      @search_q.delete(:province_eq)
    end
    @search_q.merge!({:"created_at_lteq(4i)"=>"23", :"created_at_lteq(5i)"=>"59", :"created_at_lteq(6i)"=>"59"})
    @search_q
  end
end
