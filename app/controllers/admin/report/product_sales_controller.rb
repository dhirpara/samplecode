class Admin::Report::ProductSalesController < Admin::ApplicationController
  authorize_actions_for ProductSaleAuthorizer
  def index
    @order_item_reports = OrderItemReport.search(search_q).result.order("created_at DESC")
  end

  private
  def search_q
    hash = {}
    hash[:created_at_gteq] = Time.new(*params[:from_date].values, 0, 0, 0, Time.zone.now.formatted_offset) if params[:from_date]
    hash[:created_at_lteq] = Time.new(*params[:to_date].values, 23, 59, 59, Time.zone.now.formatted_offset) if params[:to_date]
    hash.reverse_merge!({
      created_at_gteq: Time.zone.now.beginning_of_day
    })
  end
end
