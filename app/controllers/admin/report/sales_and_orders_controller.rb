class Admin::Report::SalesAndOrdersController < Admin::ApplicationController
  def index
    redirect_to [:admin, order], status: 403 and return unless current_support_user.is_admin?
    @order_reports = OrderReport.search(search_q).result.order("created_at DESC")
    @payment_reports = PaymentReport.search(payment_report_search_q).result.order("created_at DESC")
  end

  private
  def search_q
    hash = params[:q] || {}
    #hash.delete(params[:q][:currency_name_eq]) if params[:q].try(:[], :currency_name_eq) == ""
    hash[:created_at_gteq] = Time.new(*params[:from_date].values, 0, 0, 0, Time.zone.now.formatted_offset) if params[:from_date]
    hash[:created_at_lteq] = Time.new(*params[:to_date].values, 23, 59, 59, Time.zone.now.formatted_offset) if params[:to_date]
    hash.reverse_merge!({
      created_at_gteq: Time.zone.now.beginning_of_day
    })
  end

  def payment_report_search_q
    hash = params[:q] || {}
    #hash = {}
    #hash[:order_currency_name_eq] = params[:q][:currency_name_eq] if params[:q].try(:[], :currency_name_eq).try(:present?)
    hash[:created_at_gteq] = Time.new(*params[:from_date].values, 0, 0, 0, Time.zone.now.formatted_offset) if hash[:from_date]
    hash[:created_at_lteq] = Time.new(*params[:to_date].values, 23, 59, 59, Time.zone.now.formatted_offset) if hash[:to_date]
    hash.reverse_merge!({
      created_at_gteq: Time.zone.now.beginning_of_day
    })
  end
end
