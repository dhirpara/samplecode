class Admin::Report::PreOrdersController < Admin::ApplicationController
  authorize_actions_for PreOrderAuthorizer, actions: { export: :read }

  def index
   # @back_order_reports = BackOrderReport.search(search_q).result.page(params[:page]).per(BASE_CONFIG[:page])
    @pre_orders = PreOrder.search(search_q).result.page(params[:page]).per(BASE_CONFIG[:page])
  end

  def export
    search = BackOrderReport.search(search_q)
    back_order_reports = search.result(distinct: true)

    file = CSV.generate do |csv|
      csv << [
        "Client ID #",
        "First Name",
        "Last Name",
        "Email",
        "Home Phone",
        "Product Name",
        "Product QTY",
        "Date & Time stamp"
      ]
      back_order_reports.each do |r|
        csv << [
          r.client_id,
          r.client.first_name,
          r.client.last_name,
          r.client.email,
          r.client.home_phone,
          r.product.name,
          r.quantity,
          l(r.updated_at, format: :long)
        ]
      end
    end

    send_data file, filename: "Pre-Order Report.csv"
  end

  protected
  def search_q
    params[:q] || {}
  end
end
