class Admin::Report::CallListsController < Admin::ApplicationController
  authorize_actions_for CallListAuthorizer

  def index
    @clients = Client.search(search_q).result.order("next_contact_date").page(params[:page]).per(BASE_CONFIG[:page])
    @support_users = SupportCenter.available
    @ta_levels = TaLevel.active
  end

  protected
  def search_q
    hash = {}
    hash[:next_contact_date_lteq] = Time.new(*params[:to_date].values, 23, 59, 59, Time.zone.now.formatted_offset) if params[:to_date]
    hash[:support_center_id_eq] = params[:q][:support_center_id_eq] if params[:q].try(:[], :support_center_id_eq).try(:present?)
    hash[:ta_level_id_eq] = params[:q][:ta_level_id_eq] if params[:q].try(:[], :ta_level_id_eq).try(:present?)
    hash.reverse_merge!({
      contact_time_id_not_eq: 21,
      next_contact_date_lteq: Time.zone.now.end_of_day,
      active_true: true,
      deleted_false: true
    })
  end
end
