class Admin::SupportCentersController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  authorize_actions_for SupportCenter
  before_filter :form_info, only: [:new, :edit]
  before_filter :check_destroy, only: :destroy

  private
  def check_destroy
    redirect_to :back, notice: I18n.t("errors.messages.cannot_destroy.delete") unless resource.deleted?
  end

  def form_info
    @countries = Country.all
    @support_center_types = SupportCenterType.all
  end

  def search_q
    if params[:q]
      params[:q].keep_if{|k, v| ["deleted_eq"].include? k}
    else
      { deleted_eq: false }
    end
  end
end
