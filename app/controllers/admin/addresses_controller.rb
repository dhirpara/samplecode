class Admin::AddressesController < Admin::ChangeOrderBaseController
  authorize_actions_for Address, actions: { select: :select }
  before_filter :check_cancel_state, only: :select
  def select
    if order.can_change_address?
      address = order.client.addresses.find(params[:id])
      order.update_ship_address(address)
      redirect_to admin_order_shipments_url(order)
    else
      redirect_to [:admin, order], status: 403
    end
  end

  def destroy
    Address.find(params[:id]).delete_record

    redirect_to :back
  end
end
