class Admin::InvoicesController < Admin::ApplicationController
  def show
    @order = Order.find(params[:order_id])
    authorize_action_for(@order)
  end
end
