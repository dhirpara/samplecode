class Admin::ProfilesController < Devise::RegistrationsController
  layout 'admin'
  #这里需要与application同样的方法
  before_filter :authenticate_support_user!
  helper_method :current_client

  #def current_ability
    #@current_ability ||= Ability.new(current_support_user)
  #end

  def current_client
    @current_client ||= Client.find_by_id(session[:select_client_id])
  end

  private
  def use_https?
    true
  end

  def after_update_path_for(resource)
    # TODO
    '/admin/profile'
  end
end
