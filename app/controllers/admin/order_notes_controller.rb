class Admin::OrderNotesController < Admin::ChangeOrderBaseController
  inherit_resources
  include BaseAdmin
  authorize_actions_for OrderNote

  def index
    @order_notes = order.client.order_notes.search(search_q).result.order("#{sort_column(OrderNote, 'updated_at')} #{sort_direction('desc')}").page(params[:page]).per(BASE_CONFIG[:page])
  end

  def create
    build_resource
    resource.create_support_user = current_support_user
    resource.client = order.client
    if resource.save
      redirect_to admin_order_order_notes_url(order), notice: "Create success"
    else
      render :new
    end
  end

  def update
    if resource.update_attributes(params[:order_note])
      redirect_to admin_order_order_notes_url(order), notice: "Update success"
    else
      render :edit
    end
  end

  def destroy
    resource.destroy
    redirect_to admin_order_order_notes_url(order), notice: "Delete success"
  end

  protected
  def search_q
    if params[:q]
      params[:q].keep_if{|k, v| ["deleted_eq"].include? k}
    else
      { deleted_eq: false }
    end
  end
end
