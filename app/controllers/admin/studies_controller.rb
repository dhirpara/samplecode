class Admin::StudiesController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  include FileUpload
  include TranslatesManagement
  authorize_actions_for Study, actions: { upload_journal_cover: :update, upload_pdf: :update, preview: :create }
  upload_action column: :journal_cover, params: "[:study][:journal_cover]"
  upload_action column: :pdf, params: "[:study][:pdf]"
  before_filter :form_info, only: [:edit]

  def new
    build_resource
    form_info
  end

  def update
    if resource.update_attributes(resource_params)
      #resource.delete_non_essential_study_disorders
      redirect_to admin_studies_url
    else
      form_info
      render :edit
    end
  end

  def preview
    build_resource if params[:id] == "0"
    resource.assign_attributes(params[:study])
    if resource.valid?
      render partial: "admin/studies/preview"
    else
      render partial: "admin/share/error_messages", locals: { resource: resource }
    end
  end


  protected
  def form_info
    @disorders = Disorder.order(:name)
    #selected_study_disorders = resource.study_disorders.select do |study_disorder|
      #study_disorder.choose.nil? || study_disorder.choose
    #end

    #selected_study_disorders.each{|study_disorder| study_disorder.choose = true }

    #@study_disorders = (Disorder.all - selected_study_disorders.map(&:disorder)).inject([]) do |array, disorder|
      #study_disorder = StudyDisorder.new
      #study_disorder.disorder = disorder
      #array << study_disorder
    #end + selected_study_disorders
    #@study_disorders.sort!{|x, y| x.disorder.name <=> y.disorder.name }
  end

  def collection
    get_collection_ivar || begin
    set_collection_ivar(end_of_association_chain.search(search_q).result.order("#{sort_column(nil, "index")} #{sort_direction}").page(params[:page]).per(BASE_CONFIG[:page]))
    end
  end
end
