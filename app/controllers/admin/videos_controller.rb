class Admin::VideosController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  include FileUpload
  include TranslatesManagement
  before_filter :form_info, only: [:edit]
  authorize_actions_for Video, actions: { upload_image: :update, preview: :update }
  upload_action column: :image, params: "[:video][:image]"

  def new
    build_resource
    form_info
  end

  def update
    if resource.update_attributes(resource_params)
      #resource.delete_non_essential_video_disorders
      redirect_to admin_videos_url
    else
      form_info
      render :edit
    end
  end

  def preview
    build_resource if params[:id] == "0"
    resource.assign_attributes(params[:video])
    if resource.valid?
      render partial: "videos/detail", locals: { resource: resource }
    else
      render partial: "admin/share/error_messages", locals: { resource: resource }
    end
  end


  protected
  def form_info
    @disorders = Disorder.order(:name)
    #selected_video_disorders = resource.video_disorders.select do |video_disorder|
      #video_disorder.choose.nil? || video_disorder.choose
    #end

    #selected_video_disorders.each{|video_disorder| video_disorder.choose = true }

    #@video_disorders = (Disorder.all - selected_video_disorders.map(&:disorder)).inject([]) do |array, disorder|
      #video_disorder = VideoDisorder.new
      #video_disorder.disorder = disorder
      #array << video_disorder
    #end + selected_video_disorders
    #@video_disorders.sort!{|x, y| x.disorder.name <=> y.disorder.name }
  end

  def collection
    get_collection_ivar || begin
    set_collection_ivar(end_of_association_chain.search(search_q).result.order("#{sort_column(nil, "index")} #{sort_direction}").page(params[:page]).per(BASE_CONFIG[:page]))
    end
  end
end
