class Admin::OrdersController < Admin::ChangeOrderBaseController
  helper_method :order
  authorize_actions_for Order, actions: { invoices: :export, export_list: :export, export_shipping_for_canada: :export, export_shipping_for_other_country: :export, update_total: :update, cancel: :cancel, refund_all: :update, ready_all: :ready }

  def index
    @search_order = Search::Order.new(params[:q], orders: (current_client || current_support_user).orders)
    #@search_order = Search::Order.new(current_support_user.orders, params[:q])
    @orders = @search_order.orders.includes(:shipment, :client).order("#{sort_column(params[:sort_class].try(:constantize) || Order, "created_at")} #{sort_direction('desc')}").page(params[:page]).per(BASE_CONFIG[:page])

    #@orders = if current_client
                #current_client.orders
              #else
                #current_support_user.orders
              #end.search(index_q).result.order("#{sort_column(params[:sort_class].try(:constantize) || Order, "created_at")} #{sort_direction('desc')}").page(params[:page]).per(BASE_CONFIG[:page])

  end

  def show
    authorize_action_for(order)
    session[:select_client_id] = order.client_id
  end

  def create
    order_type = OrderType.find_by_code("S")
    order = current_client.orders.create(order_type: order_type, currency_id: current_client.currency_id!, support_user: current_support_user, pricing_type: current_client.pricing_type)
    redirect_to admin_order_order_items_url(order)
  end

  def update
    redirect_to [:admin, order], status: 403 and return unless can_update_order_item?
    redirect_to :back, notice: I18n.t("canceled_order") and return if order.canceled?
    if params[:order].try(:[], :currency_id)
      order.update_attributes(params[:order])
      order.order_items.each(&:update_price)
    else
      order.assign_attributes(params[:order])
      unless order.submit?
        redirect_to admin_order_order_items_url(order), notice: order.errors.full_messages.join(", ") and return
      end
    end
    order.sync_tax_charge!
    order.sync_handling_charge!

    if params.has_key?(:update_and_continue)
      redirect_to admin_order_shipments_url(order)
    else
      redirect_to admin_order_order_items_url(order)
    end
  end

  def update_total
    order.sync_latest_total
    redirect_to :back
  end

  def export_shipping_for_canada
    orders = params[:order].select{|k, v| v[:id].present? }.values.inject([]) do |array, hash|
      order = Order.find(hash[:id])
      order.package_number = hash[:package_number]
      order.save
      array << order
    end
    if orders.any? && (params.has_key?(:export) || params.has_key?(:export_like_us))
      if params.has_key?(:export)
        send_data Order.export_for_canada(orders), filename: "NT_#{Time.zone.now.strftime("%Y%m%d%H%M%S")}.csv"
      else
        send_data Order.export_for_other_country(orders).gsub("\"", ""), filename: "NT_#{Time.zone.now.strftime("%Y%m%d%H%M%S")}.tab"
      end
    else
      orders.map{ |o| o.shipment }.each(&:ship)
      redirect_to :back
    end
  end

  def export_shipping_for_other_country
    orders = Order.find_all_by_id(params[:ids])
    if orders.any? && params.has_key?(:export)
      send_data Order.export_for_other_country(orders).gsub("\"", ""), filename: "NT_#{Time.zone.now.strftime("%Y%m%d%H%M%S")}.tab"
    else
      orders.map{ |o| o.shipment }.each(&:ship)
      redirect_to :back
    end
  end

  def export_list
    @orders = Order.joins(:client).search(export_list_q).result.order("#{sort_column(params[:sort_class].try(:constantize) || Order, "created_at")} #{sort_direction("desc")}").page(params[:page]).per(BASE_CONFIG[:page])
  end

  def cancel
    #if order.canceled?
      #redirect_to :back, notice: I18n.t("canceled_order")
    #else
      order.cancel
      order.add_report
      PreOrder.find_by_order_id(order.id).try(:destroy)
      #order.remove_payment_reports
      redirect_to :back
    #end
  end

  def refund_all
    Payment.refund_all(order, current_support_user)

    redirect_to :back
  end

  def invoices
    orders = params[:order].select{|k, v| v[:id].present? }.values.inject([]) do |array, hash|
      order = Order.find(hash[:id])
      order.package_number = hash[:package_number]
      order.save
      array << order
    end

    render partial: "admin/invoices/detail", collection: orders, as: :order
  end

  def ready_all
    shipments = Shipment.includes(:order).where("orders.id in (?)", params[:order_ids])
    if params[:ready]
      shipments.each(&:ready)
    else
      shipments.each(&:pending)
    end

    redirect_to :back
  end

  protected
  def order
    @order ||= Order.find(params[:id])
  end

  def export_list_q
    hash = {}
    if params[:q].try(:[], :shipment_address_country_abbr_in)
      hash[:shipment_address_country_abbr_in] = ["CA", "AU", "NZ"] + Country::EUROPEAN_UNION
    else
      hash[:shipment_address_country_abbr_not_in] = ["CA", "AU", "NZ"] + Country::EUROPEAN_UNION
    end
    hash[:shipment_state_eq] = params[:q].try(:[], :shipment_state_eq) || "ready"
    hash
  end
end
