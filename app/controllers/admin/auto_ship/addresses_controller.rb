class Admin::AutoShip::AddressesController < Admin::AutoShip::BaseController
  include ::AutoShip::AddressC
  include AddressC

  protected
  def detail_link
    admin_client_auto_ship_address_url(client, current_auto_ship)
  end

  def use_address_link(address)
    select_admin_client_auto_ship_address_url(client, id: address.id)
  end

  def shipping_method_link
    admin_client_auto_ship_shipping_method_url(client, current_auto_ship)
  end
end
