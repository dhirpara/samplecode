class Admin::AutoShip::PaymentProfilesController < Admin::AutoShip::BaseController
  include PaymentFormInfo
  include ::AutoShip::PaymentProfileC

  helper_method :delete_payment_profile_url

  protected
  def detail_link
    admin_client_auto_ship_payment_profile_url(client, current_auto_ship)
  end

  def delete_payment_profile_url(payment_profile)
    admin_client_payment_profile_url(current_auto_ship.client, payment_profile)
  end
end
