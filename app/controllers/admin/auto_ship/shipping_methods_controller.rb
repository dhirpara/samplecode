class Admin::AutoShip::ShippingMethodsController < Admin::AutoShip::BaseController
  include ::AutoShip::ShippingMethodC

  protected
  def detail_link
    admin_client_auto_ship_shipping_method_url(client, current_auto_ship)
  end

  def payment_link
    admin_client_auto_ship_payment_profile_url(client, current_auto_ship)
  end
end
