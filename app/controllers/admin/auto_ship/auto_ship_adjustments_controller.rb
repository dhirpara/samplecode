class Admin::AutoShip::AutoShipAdjustmentsController < Admin::AutoShip::BaseController
  inherit_resources
  include BaseAdmin
  belongs_to :auto_shipping, param: :auto_ship_id

  def new
    build_resource
    form_info
  end

  def edit
    form_info
  end

  def create
    create! do |success, failure|
      success.html { redirect_to admin_client_auto_ship_url(current_auto_ship.client, current_auto_ship) }
    end
  end

  def update
    update! do |success, failure|
      success.html { redirect_to admin_client_auto_ship_url(current_auto_ship.client, current_auto_ship)}
    end
  end

  def destroy
    destroy!(notice: "#{resource_class.name} was successfully deleted.") do |format|
      format.html { redirect_to admin_client_auto_ship_url(current_auto_ship.client, current_auto_ship) }
    end
  end



  protected
  def form_info
    @adjustment_types = AdjustmentType.where("name not in(?)", ["shipping adjustment", "handling", "tax", "shipping"])
  end

  def collection_url
    admin_client_auto_ship_auto_ship_adjustments_url(current_auto_ship.client, current_auto_ship)
  end
end
