class Admin::AutoShip::BaseController < Admin::ApplicationController
  include ::AutoShip::BaseC

  protected
  def back_end
    true
  end

  def client
    @client ||= Client.find(params[:client_id])
  end

  def auto_shipping_detail_link(auto_ship = current_auto_ship)
    admin_client_auto_ship_url(client, auto_ship)
  end

  def authenticate_auto_shipping
    render file: "#{Rails.root}/public/403.html", status: 403, layout: false unless current_support_user.auto_shippings.include? current_auto_ship
  end
end
