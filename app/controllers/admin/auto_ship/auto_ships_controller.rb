class Admin::AutoShip::AutoShipsController < Admin::AutoShip::BaseController
  include ::AutoShip::AutoShipC

  def update_items
    current_auto_ship.assign_attributes(params[:auto_shipping])
    current_auto_ship.save(validate: false)
    AutoShipReport.add_report(current_auto_ship)
    if params[:update]
      redirect_to order_items_link
    else
      redirect_to address_link
    end
  end

  protected
  def new_link
    admin_client_auto_ships_url(client)
  end

  def detail_link
    admin_client_auto_ship_url(client, current_auto_ship)
  end

  def invoice_link
    invoice_admin_client_auto_ship_url(client, current_auto_ship)
  end

  def order_items_link
    admin_client_auto_ship_order_items_url(client, current_auto_ship)
  end

  def address_link
    admin_client_auto_ship_address_url(client, current_auto_ship)
  end

  def shipping_method_link
    admin_client_auto_ship_shipping_method_url(client, current_auto_ship)
  end

  def payment_profile_link
    admin_client_auto_ship_payment_profile_url(client, current_auto_ship)
  end
end
