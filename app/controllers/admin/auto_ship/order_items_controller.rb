class Admin::AutoShip::OrderItemsController < Admin::AutoShip::BaseController
  include ::AutoShip::OrderItemsC
  def index
    @order_item = AutoShippingItem.new(quantity: 1)
    @products = Product.where(active: true, pre_order: false).default_sort

    render "shared/auto_ship/order_items/index"
  end


  protected
  def update_items_link
    update_items_admin_client_auto_ship_url(client, current_auto_ship)
  end

  def detail_link(order_item)
    admin_client_auto_ship_order_item_url(client, current_auto_ship, order_item)
  end

  def list_link
    admin_client_auto_ship_order_items_url(client, current_auto_ship)
  end

  def address_link
    admin_client_auto_ship_address_url(client, current_auto_ship)
  end

  def search_link
    "/admin/products/search.json"
  end
end
