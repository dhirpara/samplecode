class Admin::ShipmentsController < Admin::ChangeOrderBaseController
  inherit_resources
  include BaseAdmin
  include AddressC
  authorize_actions_for Shipment, actions: { edit_shipping_method: :update, ready: :ready, pickup: :ready }
  belongs_to :order, singleton: true
  before_filter :check_cancel_state, only: [:create, :update]
  before_filter :state_filter, only: :show
  before_filter :set_name, only: :create

  def show
    if resource.address.try(:persisted?)
      unless resource.shipping_method.try(:persisted?)
        shipping_method_form_info
      end
    else
      address_form_info
    end
  end

  def create
    redirect_to [:admin, order], status: 403 and return unless order.can_change_address?
    @address = order.client.addresses.new(params[:address])
    if @address.valid?
      order.update_ship_address(@address)

      redirect_to admin_order_shipments_url(order)
    else
      address_form_info
      render :show
    end
  end

  def edit
    redirect_to [:admin, order], status: 403 and return unless order.can_change_address?
    @address = resource.address
    address_form_info
  end

  def update
    redirect_to [:admin, order], status: 403 and return unless order.can_change_address?
    @address = resource.update_address(params[:address])
    if @address.valid?
      redirect_to admin_order_shipments_url(order)
    else
      #需要重构
      @address = resource.address
      @address.assign_attributes(params[:address])
      @address.valid?
      address_form_info
      render "admin/shipments/edit"
    end
  end

  def edit_shipping_method
    redirect_to [:admin, order], status: 403 and return unless order.can_change_shipping_method?
    @shipment = order.shipment
    shipping_method_form_info
  end

  def ready
    resource.ready
    #resource.order.order_items.each do |i|
      #OrderItemReport.add_report(i)
    #end
    redirect_to :back
  end

  def pickup
    resource.pickup
    #resource.order.order_items.each do |i|
      #OrderItemReport.add_report(i)
    #end
    redirect_to :back
  end

  protected
  def shipping_method_form_info
    begin
      @shipping_methods = order.shipment.available_shipping_methods(with_price: true)
    rescue ActiveMerchant::Shipping::ResponseError => ex
      flash.now[:notice] = ex.message
    end
  end

  def state_filter
    redirect_to :back, notice: I18n.t("canceled_order") if order.canceled? && order.shipment.nil?
    if order.cart?
      if order.order_items.present?
        order.next
      else
        redirect_to admin_order_order_items_url(order) and return
      end
    elsif order.payment?
      order.rollback_to_address
    end
  end

  def set_name
    unless params[:address][:name].present?
      params[:address][:name] = order.client.first_last_name
    end
  end

end
