class Admin::SupportUsersController < Admin::ApplicationController
  inherit_resources
  include BaseAdmin
  before_filter :form_info, only: [:edit]
  before_filter :check_destroy, only: :destroy
  authorize_actions_for SupportUser

  def index
    @support_users = SupportUser.search(search_q).result.order("#{sort_column} #{sort_direction}").page(params[:page]).per(BASE_CONFIG[:page])
  end

  def new
    build_resource
    form_info
  end

  def update
    if params[:support_user][:password].blank?
      params[:support_user].delete("password")
      params[:support_user].delete("password_confirmation")
    end
    if resource.update_attributes(params[:support_user])
      resource.delete_non_essential_support_center_permissions
      resource.delete_non_essential_support_user_roles
      redirect_to admin_support_users_url
    else
      form_info
      render :edit
    end
  end

  private
  def check_destroy
    redirect_to :back, notice: I18n.t("errors.messages.cannot_destroy.active") if resource.active?
  end

  def search_q
    if params[:q]
      params[:q].keep_if{|k, v| ["active_eq"].include? k}
    else
      { active_eq: true }
    end
  end


  def form_info
    @global_languages = GlobalLanguage.active
    selected_support_center_permissions = resource.support_center_permissions.select do |obj|
      obj.choose.nil? || obj.choose
    end

    selected_support_center_permissions.each{|obj| obj.choose = true }

    @support_center_permissions = (SupportCenter.available - selected_support_center_permissions.map(&:support_center)).inject([]) do |array, obj|
      support_center_permission = resource.support_center_permissions.new
      support_center_permission.support_center = obj
      array << support_center_permission
    end + selected_support_center_permissions
    @support_center_permissions.sort!{|x, y| x.support_center_id <=> y.support_center_id }

    selected_support_user_roles = resource.support_user_roles.select do |obj|
      obj.choose.nil? || obj.choose
    end

    selected_support_user_roles.each{|obj| obj.choose = true }

    @support_user_roles = (Role.all - selected_support_user_roles.map(&:role)).inject([]) do |array, obj|
      support_user_role = resource.support_user_roles.new
      support_user_role.role = obj
      array << support_user_role
    end + selected_support_user_roles

    @support_user_roles.sort!{|x, y| x.role_id <=> y.role_id }
  end
end
