class Admin::ProductImagesController < Admin::ApplicationController
  include FileUpload
  authorize_actions_for Product, actions: { upload_image: :update }
  upload_action column: :image, params: "[:image]"
end
