class Shopping::AddressesController < Shopping::BaseController
  include AddressC
  before_filter :authenticate_client_or_guest!
  before_filter :state_filter
  before_filter :set_name, only: [:create, :update]

  def index
    @address = session_order.shipment.address
    address_form_info
  end

  def create
    @address = current_or_guest_client.addresses.new(params[:address])

    begin
      Order.transaction do
        @address.save!
        session_order.update_ship_address(@address)
      end
      redirect_to shopping_shipping_methods_url, notice: 'Address was successfully created.'
    rescue Exception => ex
      #NotificationMailer.address_exception(@address, session_order).deliver
      address_form_info
      flash.now[:error] = Shipment::SHIPPING_RATE_ERROR
      render action: :index
    end
  end

  def update
    @address = session_order.shipment.update_address(params[:address])

    if @address.valid?
      redirect_to shopping_shipping_methods_url, notice: 'Address was successfully updated.'
    else
      address_form_info
      render action: :index
    end
  end

  def select
    begin
      Order.transaction do
        @address = current_or_guest_client.addresses.find(params[:id])
        session_order.update_ship_address(@address)
      end
      redirect_to shopping_shipping_methods_url
    rescue Exception => ex
      #NotificationMailer.address_exception(@address, session_order).deliver
      address_form_info
      flash.now[:error] = Shipment::SHIPPING_RATE_ERROR
      render action: "index"
    end
  end

  private
  def state_filter
    if session_order.cart?
      if session_order.order_items.present?
        session_order.next
      else
        redirect_to shopping_cart_url
      end
    elsif session_order.delivery? || session_order.payment?
      session_order.rollback_to_address
    end
  end

  def set_name
    unless params[:address][:name].present?
      params[:address][:name] = current_or_guest_client.first_last_name
    end
  end
end
