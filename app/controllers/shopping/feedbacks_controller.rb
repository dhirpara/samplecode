class Shopping::FeedbacksController < Shopping::BaseController
  before_filter :authenticate_client_or_guest!
  #before_filter :state_filter

  def new
    @feedback = Feedback.new
    form_info
  end

  def create
    @feedback = Feedback.new(params[:feedback])
    if @feedback.valid?
      FeedbackMailer.feedback(@feedback).deliver
      render "confirmation"
    else
      form_info
      render :new
    end
  end

  private
  def form_info
    @last_completed_order = current_or_guest_client.orders.find_by_id(session[:last_completed_order_id])
  end

  #private
  #def state_filter
    #if session_order.payment?
      #redirect_to shopping_payment_url
    #elsif session_order.delivery?
      #redirect_to shopping_shipping_methods_url
    #elsif session_order.address?
      #redirect_to shopping_addresses_url
    #elsif session_order.cart?
      #redirect_to shopping_cart_url
    #elsif session_order.complete?
    #end
  #end
end
