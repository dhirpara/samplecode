class Shopping::ShippingMethodsController < Shopping::BaseController
  before_filter :authenticate_client_or_guest!
  before_filter :state_filter

  # GET /shopping/shipping_methods
  def index
    form_info
  end

  # PUT /shopping/shipping_methods/select
  def select
    shipping_method = session_order.shipment.available_shipping_methods.find_by_id(params[:shipping_method_id])

    if shipping_method.present?
      session_order.update_shipping_method(shipping_method)
      redirect_to shopping_payment_url
    else
      form_info
      render :action => "index"
    end
  end

  private
  def form_info
    begin
      @shipment = session_order.shipment
      @shipping_methods = @shipment.available_shipping_methods(with_price: true)
    rescue Exception => ex
      flash[:error] = Shipment::SHIPPING_RATE_ERROR
      redirect_to shopping_addresses_url
    end
  end

  def state_filter
    if session_order.cart?
      redirect_to shopping_cart_url
    elsif session_order.address?
      if session_order.ship_address.present?
        session_order.next
      else
        redirect_to shopping_addresses_url
      end
    elsif session_order.payment?
      session_order.rollback_to_delivery
    end
  end
end
