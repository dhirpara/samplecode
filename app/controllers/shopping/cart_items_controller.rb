class Shopping::CartItemsController < Shopping::BaseController
  def create
    @product = Product.find(params[:product_id])
    redirect_to :back, status: 403 and return unless Product.for_client(current_client).include? @product
    # 兼容product index页面直接add to cart
    cart_item = CartItem.new((params[:cart_item] || {}).reverse_merge!({quantity: 1, cartable_id: @product.id, cartable_type: "Product"}))
    authorize_action_for(cart_item)
    if cart_item.valid?
      cart = find_or_create_cart
      cart.add_cartable(cart_item)
      redirect_to shopping_cart_url
    else
      redirect_to :back, alert: cart_item.errors.full_messages.join(",")
    end
  end

  def destroy
    # TODO
    @cart_item = CartItem.find(params[:id])
    @cart_item.remove_from_order

    respond_to :js
  end
end
