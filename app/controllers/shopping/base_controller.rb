class Shopping::BaseController < ApplicationController
  helper_method :session_order, :session_order_id
  skip_before_filter :set_referral_code

  private
  def use_https?
    true
  end

  def session_order
    find_or_create_order
  end

  def session_order_id
    session[:order_id] ? session[:order_id] : find_or_create_order.id
  end
  alias :current_order :session_order

  def find_or_create_order
    return @session_order if @session_order
    if session[:order_id]
      @session_order = current_or_guest_client.orders.include_checkout_objects.find_by_id(session[:order_id])
      create_order unless @session_order.present?
    else
      #@session_order = current_or_guest_client.orders.include_checkout_objects.last
      #if @session_order.try(:in_progress?)
        #session[:order_id] = @session_order.id
      #else
        create_order
      #end
    end

    @session_order
  end

  def create_order
    code = current_or_guest_client.is_guest? ? "G" : "W"
    order_type = OrderType.find_by_code(code)
    @session_order = current_or_guest_client.orders.create(number: Time.now.to_i, currency_id: current_or_guest_client.currency_id!, order_type: order_type)
    @session_order.sync_by_cart(current_cart)
    session[:order_id] = @session_order.id
  end

  def finish_session_order
    session[:order_id] = nil
    cookies[:cart_id] = nil
    current_cart.destroy
    current_or_guest_client.cart.try(:destroy)
  end
end
