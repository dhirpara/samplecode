class Shopping::GuestsController < Shopping::BaseController
  #def create
    #@guest_form = GuestForm.new(params[:guest_form])
    #if @guest_form.submit?
      ##guest = Client.create(email: params[:email], country_id: params[:country_id], guest: true)
      ##guest.save!(:validate => false)
      #session[:guest_client_id] = @guest_form.guest.id
      #redirect_to after_sign_in_path
    #else
      #redirect_to new_client_session_path
    #end
  #end

  def destroy
    session[:guest_client_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end

  #private
  #def after_sign_in_path
    #if session[:checkout]
      #session[:checkout] = false
      #return checkout_shopping_cart_url
    #else
      #return root_url
    #end
  #end
end
