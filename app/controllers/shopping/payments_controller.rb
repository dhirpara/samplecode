class Shopping::PaymentsController < Shopping::BaseController
  include PaymentFormInfo
  before_filter :authenticate_client_or_guest!, :check_referral_code
  before_filter :state_filter
  helper_method :delete_payment_profile_url

  # GET /shopping/payment
  def show
    session_order.sync_latest_total
    form_info
  end

  # POST /shopping/payment
  def create
    if session_order.complete?
      redirect_to new_shopping_feedback_url
    else
      @payment_new_form = Payment::NewForm.new(order, params[:payment_new_form])
      if @payment_new_form.submit?

        if @payment_new_form.purchase
          session[:last_completed_order_id] = order.id
          finish_session_order

          redirect_to new_shopping_feedback_url and return
        else
          flash.now[:notice] = @payment_new_form.resp.message
        end
      end

      form_info
      render :show
    end
  end

  private
  def order; session_order; end

  def check_referral_code
    if order.referral_code && ReferralCode.out_of_stock_for_resource?(order)
      redirect_to shopping_cart_url, notice: "The Referral Code cannot be used."
    end
  end

  def state_filter
    if session_order.cart?
      redirect_to shopping_cart_url
    elsif session_order.address?
      redirect_to shopping_addresses_url
    elsif session_order.delivery?
      if session_order.shipping_method.present?
        session_order.next
      else
        redirect_to shopping_shipping_methods_url
      end
    end
  end

  def delete_payment_profile_url(payment_profile)
    myaccount_payment_profile_url(payment_profile)
  end
end
