class Shopping::CartsController < Shopping::BaseController
  before_filter :authenticate_client_or_guest!, only: :checkout
  before_filter :check_empty_items, only: :update
  before_filter :set_referral_code, only: :show

  # PUT /shopping/cart
  def update
    current_cart.assign_attributes(params[:cart])
    if current_cart.submit?
      #if params[:commit] && params[:commit].downcase == "checkout"
      if current_cart.cart_items.empty?
        redirect_to products_path, notice: I18n.t('cart_is_empty')
      else
        if params[:apply]
          redirect_to shopping_cart_url, notice: "Cart was successfully updated"
        else
          session[:checkout] = true
          redirect_to checkout_shopping_cart_url
        end
      end
      #else
      # redirect_to shopping_cart_url, notice: I18n.t('item_passed_update')
      #end
    else
      render action: :show
      #redirect_to shopping_cart_url, notice: I18n.t('item_failed_update')
    end
  end

  # GET /shopping/cart/checkout
  def checkout
    session_order.sync_by_cart(current_cart)
    redirect_to shopping_addresses_url
  end

  def update_price
    @cart_item = CartItem.find_by_id(params['cart_item_id'])
    @cart_item.quantity = params[:quantity].to_i
    current_cart.add_cartable(@cart_item, reset_quantity: true)
    #@cart_item.add_to_order(current_cart, reset_quantity: true)

    respond_to :js
  end

  def licence_field
    if ReferralCode.where("code ilike ?", params[:code]).first.try(:need_licence?)
      render partial: "licence_field", locals: { licence_value: "" }
    else
      render text: ""
    end
  end

  def show
    if client_or_guest_signed_in?
      session_order.rollback_to_cart unless session_order.cart?
    end
  end

  private
  def check_empty_items
    return unless params[:cart] && params[:cart][:cart_items_attributes]
    params[:cart][:cart_items_attributes].each do |k, v|
      v['_destroy'] = '1' if v['quantity'] == '0'
    end
  end
end
