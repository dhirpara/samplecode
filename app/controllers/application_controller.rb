class ApplicationController < ActionController::Base
  protect_from_forgery
  rescue_from ActiveRecord::RecordNotFound, with: :goto_404 if Rails.env.production?
  rescue_from ActiveRecord::DeleteRestrictionError do |exception|
    message_prefix = exception.message.slice(0..41)
    relative_name = exception.message.slice(42..-1)
    message_suffix = if relative_name == "product_disorders"
                       "products"
                     elsif relative_name == "article_disorders"
                       "articles"
                     elsif relative_name == "created_clients"
                       "clients"
                     elsif relative_name == "video_disorders"
                       "videos"
                     elsif relative_name == "support_center_permissions"
                       "support users"
                     else
                       relative_name.titleize.downcase
                     end
    redirect_to :back, notice: message_prefix + message_suffix
  end

  Warden::Manager.after_authentication do |user,auth,opts|
    user.update_attributes(active: true) if user.class == Client
  end

  include SortableC
  before_filter :https_redirect
  before_filter :set_locale
  before_filter :set_referral_code


  helper_method :current_cart, :current_or_guest_client, :client_or_guest_signed_in?, :current_guest, :guest_signed_in?, :articles, :article_categories, :current_products, :product_categories, :faq_types, :study_disorders, :params_q, :wp_posts


  def goto_404
    render file: "#{Rails.root}/public/404.html", status: 404, layout: false
  end

  def current_products
    Product.for_client(current_client)
  end

  def client_or_guest_signed_in?
    !!current_or_guest_client
  end

  def current_or_guest_client
    if current_client
      if session[:guest_client_id]
        if current_guest.present?
          logging_in
          current_guest.destroy
        end
        session[:guest_client_id] = nil
      end
      current_client
    else
      current_guest
    end
  end

  def guest_signed_in?
    !!current_guest
  end

  def current_guest
    @cached_guest_client ||= Client.find_by_id(session[:guest_client_id])
  end

  def authenticate_client_or_guest!
    redirect_to new_client_session_path unless current_or_guest_client.present?
  end

  def after_sign_in_path_for(user)
    if user.class == Client
      sync_current_cart
      if session[:checkout]
        session[:checkout] = false
        return checkout_shopping_cart_url
      else
        return myaccount_profile_url
      end
    else
      if session[:support_user_return_to].nil?
        return support_user_redirect_to(user)
      else
        super
      end
    end
  end

  def support_user_redirect_to(user)
    if user.is_admin? || user.is_basic? || user.is_order_completion?
      admin_clients_url
    elsif user.is_product_sales_report?
      admin_report_product_sales_url
    elsif user.is_language?
      edit_user_registration_path
    else
      export_list_admin_orders_url
    end
  end

  def sync_current_cart
    if cart = Cart.find_by_id(cookies[:cart_id])
      # current_cart 存在
      if cart.cart_items.present?
        ## current_cart 可用
        if cart.shopper.present?
          ### current_cart 有归属
          cookies[:cart_id] = nil
          set_available_cart
        else
          ### current_cart 无归属
          current_client.cart = cart
        end
      else
        ## current_cart 不可用
        cart.destroy
        cookies[:cart_id] = nil
        set_available_cart
      end
    else
      # current_cart 不存在
      set_available_cart
    end
  end

  def set_available_cart
    if current_client.cart.present?
      # 当前用户存在未清空的cart
      set_cookies(:cart_id, current_client.cart.id)
    #else
      ## 当前用户不存在未清空的cart
      #latest_order = current_client.orders.last
      #if latest_order.try(:in_progress?)
        ## 当前用户存在未完成的订单
        #@current_cart = latest_order.generate_cart
        #set_cookies(:cart_id, @current_cart.id)
      #end
    end
  end

  def current_cart
    return @current_cart if @current_cart
    @current_cart = Cart.find_by_id(cookies[:cart_id])
    if client_or_guest_signed_in?
      if @current_cart.present?
        if @current_cart.shopper != current_or_guest_client
          current_or_guest_client.cart = @current_cart
        end
      else
        @current_cart = current_or_guest_client.build_cart(pricing_type: current_or_guest_client.try(:pricing_type) || PricingType.default)
      end
    else
      if @current_cart.present?
        @current_cart = Cart.new(pricing_type: current_or_guest_client.try(:pricing_type) || PricingType.default) if @current_cart.shopper
      else
        @current_cart = Cart.new(pricing_type: current_or_guest_client.try(:pricing_type) || PricingType.default)
      end
    end

    @current_cart
  end

  def find_or_create_cart
    return current_cart if current_cart.persisted?

    if client_or_guest_signed_in?
      @current_cart = current_or_guest_client.create_cart(pricing_type: current_or_guest_client.pricing_type)
    else
      @current_cart = Cart.create(pricing_type: PricingType.default)
    end

    set_cookies(:cart_id, @current_cart.id)
    @current_cart
  end

  def set_cookies(key, value, expires=1.hour.from_now)
    cookies[key] = { value: value, expires: expires }
  end

  private
  def https_redirect
    #TODO
    if (request.ssl? && !use_https? || !request.ssl? && use_https?) && Rails.env.production?
      protocol = request.ssl? ? "http" : "https"
      flash.keep
      redirect_to protocol: "#{protocol}://", status: :moved_permanently
    end
  end

  def use_https?
    false
  end

  def logging_in
    current_guest.orders.each do |order|
      order_type = OrderType.find_by_code('W')
      order.client_id = current_client.id
      order.order_type = order_type
      order.save!
    end
    current_guest.addresses.each do |address|
      address.client_id = current_client.id
      address.save!
    end
  end

  def product_categories
    @product_categories = ProductCategory.order("id")
  end

  def article_categories
    @article_categories = ArticleCategory.all
  end

  def articles
    @articles = Article.all
  end

  def faq_types
    @faq_types ||= FaqType.default_sort
  end

  def study_disorders
    Disorder.related_with_study.where(show_in_navigation: true)
  end

  def params_q
    params[:q].nil? ? {} : params[:q].dup
  end

  def set_locale
    I18n.locale =  cookies[:locale] || setup_locale
  end

  def setup_locale
    #if user_signed_in?
      #current_user.update_attribute(:locale, guess_browser_language) if current_user.locale.blank?
      #return cookies[:locale] = current_user.locale
    #else
      return cookies[:locale] = guess_browser_language
    #end
  end

  def guess_browser_language
    request.accept_language.split(/,/).each{|language|
      if language =~ /zh-CN/i
        return 'zh-CN'
      else
        return I18n.default_locale
      end
    } unless request.accept_language.blank?
    return I18n.default_locale
  end

  def wp_posts
    return @wp_posts if @wp_posts
    language_code = case I18n.locale
                    when :"zh-CN"
                      'cn'
                    when :"zh-TW"
                      'ch'
                    else
                      'en'
                    end
    @wp_posts = WpPost.publish_posts.includes(:terms).where("wp_terms.slug = ?", language_code).order("post_date desc").limit(3)
  end

  def user_for_this_request
    current_client || Client.new
  end

  def set_referral_code
    if referral_code = ReferralCode.find_referral_code_through_http_referer(request.env["HTTP_REFERER"])
      if find_or_create_cart.referral_code.nil?
        current_cart.code = referral_code.code
        if current_cart.submit?
          if referral_code.product_promotions.any?
            referral_code.product_promotions.each do |promotion|
              current_cart.add_cartable(CartItem.new(quantity: 1, cartable_id: promotion.x_product_id, cartable_type: "Product"))
            end

            redirect_to shopping_cart_url
          end
        end
      end
    end
  end

  private
  def transfer_page_to
    if transfer_page = TransferPage.where(name: request.path).first
      redirect_to transfer_page.url
    end
  end
end
