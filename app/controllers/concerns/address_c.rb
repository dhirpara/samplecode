module AddressC
  extend ActiveSupport::Concern
  included do
    before_filter :set_name, only: :create
  end

  protected
  def address_form_info
    @address ||= Address.new(country: Country.find_by_abbr("US"))
    @addresses = current_order.client.addresses(true)
    @countries = Country.order(:id)
    @provinces = @address.country.provinces.order(:name)
  end

  def set_name
    unless params[:address][:name].present?
      params[:address][:name] = order.client.first_last_name
    end
  end
end
