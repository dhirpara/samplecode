module AutoShip::AddressC
  extend ActiveSupport::Concern

  included do
    helper_method :detail_link, :use_address_link, :shipping_method_link
  end

  def show
    @address = current_auto_ship.address
    address_form_info

    render "shared/auto_ship/addresses/show"
  end

  def create
    @address = current_auto_ship.client.addresses.new(params[:address])

    if @address.valid?
      current_auto_ship.update_ship_address(@address)

      redirect_to shipping_method_link
    else
      address_form_info
      render "shared/auto_ship/addresses/show"
    end
  end

  def update
    @address = current_auto_ship.update_address(params[:address], sync_charge: false)
    if @address.valid?
      current_auto_ship.shipping_method_id = nil
      current_auto_ship.save(validate: false)
      AutoShipReport.add_report(current_auto_ship)

      redirect_to shipping_method_link
    else
      address_form_info
      render "shared/auto_ship/addresses/show"
    end
  end

  def select
    @address = current_auto_ship.client.addresses.find(params[:id])
    current_auto_ship.update_ship_address(@address)
    current_auto_ship.shipping_method_id = nil
    current_auto_ship.save(validate: false)
    AutoShipReport.add_report(current_auto_ship)

    redirect_to shipping_method_link
  end
end
