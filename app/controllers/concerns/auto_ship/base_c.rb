module AutoShip::BaseC
  extend ActiveSupport::Concern

  included do
    #helper_method :auto_shipping_detail_link, :client, :current_order, :order, :current_auto_ship
    helper_method :back_end, :auto_shipping_detail_link, :client, :current_auto_ship#, :current_order
    before_filter :authenticate_auto_shipping
  end

  protected
  def back_end
    false
  end

  def current_auto_ship
    @current_auto_ship ||= ::AutoShipping.find(params[:auto_ship_id])
  end
  def current_order
    @current_order ||= current_auto_ship.order
  end
  alias :order :current_order
  #def current_order
    #@current_order ||= ::AutoShipping.find(params[:auto_shipping_id])
  #end
  #alias :order :current_order
  #alias :current_auto_ship :current_order
end
