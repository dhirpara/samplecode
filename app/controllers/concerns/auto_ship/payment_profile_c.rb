module AutoShip::PaymentProfileC
  extend ActiveSupport::Concern

  included do
    helper_method :detail_link
  end

  def show
    form_info

    render "shared/auto_ship/payment_profiles/show"
  end

  def create
    @payment_new_form = Payment::NewForm.new(order, params[:payment_new_form])

    if @payment_new_form.submit?
      if @payment_new_form.payment_profile
        current_auto_ship.payment_profile = @payment_new_form.payment_profile
        current_auto_ship.save(validate: false)

        AutoShipReport.add_report(current_auto_ship)

        redirect_to auto_shipping_detail_link and return
      else
        resp, payment_profile = @payment_new_form.payment.store_payment_profile(@payment_new_form.credit_card, @payment_new_form.address)
        if resp.success?
          current_auto_ship.payment_profile = payment_profile
          current_auto_ship.save(validate: false)

          AutoShipReport.add_report(current_auto_ship)

          redirect_to auto_shipping_detail_link and return
        else
          flash.now[:notice] = resp.message
        end
      end
    end

    form_info
    render "shared/auto_ship/payment_profiles/show"
  end

  def form_info
    super

    @payment_new_form.payment_profile = current_auto_ship.payment_profile
  end
end
