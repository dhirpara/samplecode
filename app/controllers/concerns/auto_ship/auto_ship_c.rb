module AutoShip::AutoShipC
  extend ActiveSupport::Concern

  included do
    helper_method :new_link, :invoice_link, :order_items_link, :address_link, :shipping_method_link, :payment_profile_link
    skip_before_filter :authenticate_auto_shipping, only: [:index, :create]
  end


  def index
    @auto_shippings = client.auto_shippings.page(params[:page]).per(BASE_CONFIG[:page])

    render "shared/auto_ship/auto_ships/index"
  end

  def show
    render "shared/auto_ship/auto_ships/show"
  end

  def create
    auto_shipping = client.auto_shippings.new(active: true, email: client.email, interval: 30, next_order_date: Date.tomorrow)
    auto_shipping.save(validate: false)

    redirect_to auto_shipping_detail_link(auto_shipping)
  end

  def update
    if current_auto_ship.update_attributes(params[:auto_shipping])
      AutoShipReport.add_report(current_auto_ship)
      flash[:notice] = "Auto Ship Order Successfully Updated"
      redirect_to detail_link
    else
      render "shared/auto_ship/auto_ships/show"
    end
  end

  def update_items
    redirect_to address_link
  end

  def destroy
    current_auto_ship.destroy
    
    redirect_to action: :index
  end

  def invoice
    render "shared/auto_ship/auto_ships/invoice"
  end

  protected
  #def current_order
    #@current_order ||= ::AutoShipping.find(params[:id])
  #end
  def current_auto_ship
    @current_auto_ship ||= ::AutoShipping.find(params[:id])
  end
end
