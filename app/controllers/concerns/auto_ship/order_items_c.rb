module AutoShip::OrderItemsC
  extend ActiveSupport::Concern

  included do
    helper_method :update_items_link, :list_link, :detail_link, :search_link, :address_link
  end

  def index
    #@order_items = current_auto_ship.auto_shipping_items
    @order_item = AutoShippingItem.new(quantity: 1)
    @products = Product.for_client(current_auto_ship.client).where(pre_order: false).default_sort

    render "shared/auto_ship/order_items/index"
  end

  def create
    item = AutoShippingItem.new(params[:auto_shipping_item])
    if item.valid?
      current_auto_ship.add_item(item)
      render partial: 'shared/auto_ship/order_items/list'
    else
      render text: "something were wrong, please try again", status: 422
    end
  end

  def destroy
    current_auto_ship.remove_order_item(current_auto_ship.auto_shipping_items.find(params[:id]))

    render partial: "shared/auto_ship/order_items/list"
  end
end
