module AutoShip::ShippingMethodC
  extend ActiveSupport::Concern

  included do
    helper_method :detail_link, :payment_link
  end

  def show
    @shipping_methods = ShippingMethod.available_methods(current_auto_ship.order.shipment, with_price: true, date: current_auto_ship.next_order_date)

    render "shared/auto_ship/shipping_methods/show"
  end

  def create
    @shipping_methods = ShippingMethod.available_methods(current_auto_ship.order.shipment, with_price: true, date: current_auto_ship.next_order_date)
    shipping_method = @shipping_methods.detect{|s| s.id == params[:shipping_method_id].to_i }
    flash.now[:notice] = "Please select shipping method" if shipping_method.nil?

    if shipping_method && current_auto_ship.update_column(:shipping_method_id, shipping_method.id)
      current_auto_ship.order.shipment.shipping_method = shipping_method

      AutoShipReport.add_report(current_auto_ship)
      redirect_to payment_link
    else
      render "shared/auto_ship/shipping_methods/show"
    end
  end
end
