module TranslatesManagement
  extend ActiveSupport::Concern

  included do
    before_filter :check_language_permissions, only: [:update]
  end

  #def resource_params
    #normal_params = params[resource_request_name] || params[resource_instance_name] || {}

    #if current_support_user.is_admin?
      #normal_params
    #else
      #normal_params.select{|a| resource_class::TRANSLATES_ATTRIBUTES.include? a.to_sym }
    #end
  #end

  protected
  def check_language_permissions
    return if current_support_user.is_admin?
    unless current_support_user.global_languages.map(&:abbr).include?(I18n.locale.to_s)
      flash[:alert] = t('ban_translations', language: GlobalLanguage.find_by_abbr(I18n.locale.to_s).name)
      redirect_to :back and return
    end
  end
end
