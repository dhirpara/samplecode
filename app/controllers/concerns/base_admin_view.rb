module BaseAdminView
  extend ActiveSupport::Concern
  included do
    rescue_from ActionView::MissingTemplate do |exception|
      render "admin/share/new" and return if ['create', 'new'].include? action_name
      render "admin/share/edit" and return if ['update', 'edit'].include? action_name
    end
  end
end
