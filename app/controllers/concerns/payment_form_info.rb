module PaymentFormInfo
  extend ActiveSupport::Concern

  def form_info
    @payment_new_form ||= Payment::NewForm.new(order)
    @countries = Country.form_selector
    @provinces = @payment_new_form.address.country.provinces
    @payment_profiles = order.client.payment_profiles.where(currency_id: order.currency_id).sort do |x, y|
      y.payments.where(trn_type: "P").first.try(:created_at).to_i <=> x.payments.where(trn_type: "P").first.try(:created_at).to_i
    end
    @payment_new_form.payment_profile ||= @payment_profiles.first
  end
end
