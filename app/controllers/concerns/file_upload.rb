module FileUpload
  extend ActiveSupport::Concern

  included do
    self.class_attribute :resource_class

    self.resource_class = self.name.split('::').last.sub(/Controller/, '').singularize.constantize
  end

  module ClassMethods
    def upload_action(options)
      class_eval <<-CLASS
        def upload_#{options[:column]}
          obj = #{self.resource_class}.new(#{options[:column]}: params#{options[:params]})
            if request.env["HTTP_ACCEPT"].index("application/json")
              render json: obj.#{options[:column]}_cache.to_json
            else
              render json: obj.#{options[:column]}_cache.to_json, content_type: 'text/plain'
            end
        end
      CLASS
    end
  end
end
