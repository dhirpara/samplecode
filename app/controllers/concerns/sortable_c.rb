module SortableC
  extend ActiveSupport::Concern
  included do
    helper_method :sort_column, :sort_direction
  end

  def sort_column(model = nil, column = nil)
    model ||= resource_class
    column ||= default_sort_column
    model.column_names.include?(params[:sort].try(:downcase)) ? "#{model.name.tableize}.#{params[:sort]}" : "#{model.name.tableize}.#{column}"
  end

  def sort_direction(direction = 'asc')
    %w[asc desc].include?(params[:direction]) ? params[:direction] : direction
  end

  def default_sort_column
    "id"
  end
end
