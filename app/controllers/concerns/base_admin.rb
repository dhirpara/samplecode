module BaseAdmin
  extend ActiveSupport::Concern
  include BaseAdminView
  included do
    helper_method :can_delete?
  end


  def create
    create! do |success, failure|
      success.html { redirect_to collection_url }
    end
  end

  def update
    update! do |success, failure|
      success.html { redirect_to collection_url }
    end
  end

  def destroy
    destroy!(notice: "#{resource_class.name} was successfully deleted.") do |format|
      format.html { redirect_to collection_url }
    end
  end

  def collection
    get_collection_ivar || begin
    @search = end_of_association_chain.search(search_q)
    set_collection_ivar(@search.result.order("#{sort_column} #{sort_direction}").page(params[:page]).per(BASE_CONFIG[:page]))
    end
  end

  def create_resource(object)
    form_info and return false unless object.save
  end

  def update_resource(object, attributes)
    form_info and return false unless object.update_attributes(attributes)
  end

  def can_delete?; true; end

  def form_info
  end
  def search_q
    {}
  end

  #别加resource_params方法, translates controller里有
end
