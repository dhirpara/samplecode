module BaseMailerC
  extend ActiveSupport::Concern
  included do
    self.class_attribute :resource_class_name
    self.resource_class_name = name.split('::').last.sub(/Controller/, '').singularize
  end

  def create
    @contact = self.class.resource_class.new(params[:"#{resource_name}"])
    if @contact.valid?
      "#{self.class.resource_class}Mailer".constantize.contact(@contact).deliver
      if request.xhr?
        @ajax = true
        render "confirmation", layout: false
      else
        render "confirmation"
      end
    else
      form_info
      if request.xhr?
        render partial: "form", locals: ajax_params
      else
        render :new
      end
    end
  end

  def new
    @contact = self.class.resource_class.new
    form_info
  end

  private
  def ajax_params; {}; end
  def form_info; end

  def resource_name
    self.class.resource_class.name.underscore
  end

  module ClassMethods
    attr_writer :resource_class
    def resource_class
      @resource_class ||= self.resource_class_name.constantize
    end
  end
end
