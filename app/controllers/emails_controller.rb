class EmailsController < ApplicationController
  def create
    goto_404 unless request.xhr?
    @email = Email.new(first_name: params[:first_name], last_name: params[:last_name], email: params[:email])

    if @email.valid?
      Email.subscribe(@email)
    end
  end

  def destroy
    @email = Email.new(params[:email])

    begin
      MailChimp.unsubscribe(@email)

      redirect_to unsubscription_email_url, notice: "Unsubscription Successful"
    rescue Gibbon::MailChimpError => e
      form_info
      flash.now.alert = "Invalid email."

      render :unsubscription
    end
  end

  def unsubscription
    form_info
  end

  def subscription
    if email = Email.where(token: params[:token]).first
      begin
        MailChimp.subscribe(email)
        #email.update_attributes(token: nil)
        email.destroy
        flash.now[:notice] = "Thank you for subscribing to the Hardy Nutritionals NutraTalk Newsletter"
      rescue Gibbon::MailChimpError
        flash.now[:alert] = "Please try again"
      end
    else
      flash.now[:alert] = "Sorry, the token is invalid"
    end
  end

  private
  def form_info
    @email ||= Email.new
  end
end
