class Clients::NewlettersController < DeviseController
  before_filter :authenticate_client_or_guest!, except: :confirmation

  def show
    @client = current_or_guest_client
  end

  def update
    @client = current_or_guest_client

    @client.assign_attributes(params[:client])
    unless @client.is_guest?
      @client.save
    end

    if @client.receive_update?
      Email.subscribe(@client)
    end

    redirect_to action: :show
  end

  def confirmation
    @client = Client.new(receive_update: "1")

    render action: :show
  end

  def use_https?
    true
  end
end
