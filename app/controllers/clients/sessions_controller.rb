class Clients::SessionsController < Devise::SessionsController

  def new
    form_info
    super
  end

  def create
    if params.has_key?(:guest_login)
      @guest_form = GuestForm.new(params[:guest_form])
      if @guest_form.submit?
        session[:guest_client_id] = @guest_form.guest.id
        if session[:checkout]
          session[:checkout] = false
          #redirect_to checkout_shopping_cart_url
          redirect_to client_newletter_url
        else
          redirect_to root_url
        end
      else
        form_info
        render :new
      end
    else
      check_password
      #super
      self.resource = warden.authenticate!(auth_options)
      set_flash_message(:notice, :signed_in) if is_navigational_format?
      sign_in(resource_name, resource)
      flash[:notice] = "#{I18n.t("devise.sessions.client.signed_in")}, #{current_client.full_name}"

      redirect_to after_sign_in_path_for(resource)
    end
  end

  def check_password
    client = Client.find_by_user_name(params[:client][:user_name])
    if client && client.encrypted_password.try(:blank?)
      redirect_to new_client_password_url, alert: "please reset password" and return
    end
  end

  private
  def use_https?
    true
  end

  def form_info
    @guest_form ||= GuestForm.new
    @countries = Country.order(:id)
    @introduction_types = IntroductionType.active.default_sort
  end
end
