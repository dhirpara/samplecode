class Clients::BackOrderedsController < DeviseController
  def new
    @back_order_client = BackOrderClient.new(params[:back_order_client])
    if @back_order_client.product_quantity.to_i <= 0
      redirect_to :back, alert: "Quantity must be greater than 0"
    else
      if current_client
        report = BackOrderReport.find_or_create_by_client_id_and_product_id(current_client.id, @back_order_client.product_id)
        report.quantity = @back_order_client.product_quantity
        report.save

        redirect_to confirmation_client_back_ordered_url
      else
        form_info
      end
    end
  end

  def create
    @back_order_client = BackOrderClient.new(params[:back_order_client])
    if @back_order_client.submit?
      sign_in(:client, @back_order_client.client)
      redirect_to confirmation_client_back_ordered_url
    else
      form_info
      render :new
    end
  end

  #def update
  #current_client.update_attributes(back_ordered_time: Time.zone.now)

    #redirect_to confirmation_client_back_ordered_url
  #end

  private
  def form_info
    @countries = Country.order(:id)
    @provinces = @back_order_client.country.try(:provinces)
    @introduction_types = IntroductionType.active.default_sort
  end

  def use_https?
    true
  end
end
