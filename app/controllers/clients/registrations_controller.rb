class Clients::RegistrationsController < Devise::RegistrationsController
  protected
  def build_resource(hash=nil)
    hash ||= resource_params.reverse_merge!(back_ordered_customer: true, support_center_id: SupportCenter.find_by_code("HNW").try(:id)) || {}
    self.resource = resource_class.new_with_session(hash, session)
    self.resource.check_home_phone = true
    self.resource
  end

  private
  def after_sign_up_path_for(user)
    if user.email.present?
      flash[:notice] = "A registration confirmation email has been sent to you from service@hardynutritionals.com. If it doesn't appear in your in Inbox within 15 minutes, please check your Spam/Junk folder and add service@hardynutritionals.com to your list of safe senders."
      if user.receive_update == "1"
        Email.subscribe(user)
      end
    end

    #edit_client_registration_url
    #shopping_cart_url
    client_newletter_url
  end

  def use_https?
    true
  end
end
