class Clients::PasswordsController < Devise::PasswordsController
  def new
    if Client.find_by_email(params[:email]).try(:encrypted_password).try(:present?)
      redirect_to new_client_session_url
    else
      self.resource = resource_class.new
    end
  end

  private
  def use_https?
    true
  end
end
