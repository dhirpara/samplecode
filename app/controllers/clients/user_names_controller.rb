class Clients::UserNamesController < DeviseController
  def new
    @client = Client.new
  end

  def create
    if @client = Client.where(params[:client].merge(active: true)).first
      UserNameMailer.remind(@client).deliver
      flash[:notice] = "An email with your user name has been sent to you"
      redirect_to new_client_user_name_url
    else
      @client = Client.new(params[:client])
      flash.now[:alert] = "Could't find account, please try again"
      render :new
    end
  end

  def use_https?
    true
  end
end
