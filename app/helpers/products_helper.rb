module ProductsHelper
  def discount_price(product)
    return @discount_price if @discount_price
    product_promotion = ProductPromotion.joins(promotion_pricing_types: :pricing_type).where("x_product_id = ? and y_product_id = ? and start_date <= ? and end_date >= ? and promotion_method_id = 2 and pricing_types.name = ? and use_only_for_referral_code = ?", product.id, product.id, Date.today, Date.today, current_client.try(:pricing_type).try(:name) || "Retail", false).first
    if product_promotion
      product_price = product.price_for(user: current_client)
      @discount_price = ((product_price * product_promotion.x_quantity + product_price * product_promotion.rate) / (product_promotion.x_quantity+ 1)).ceil(2)
    end
  end

  def caselots_show_for_client(product, client)
    return @caselots_show_for_client if @caselots_show_for_client
    pricing_type = client.try(:pricing_type) || PricingType.default
    @caselots_show_for_client = product.product_caselots.for_client(client).joins(:product_pricing_types).where("product_pricing_types.show = ? and product_pricing_types.pricing_type_id = ?", true, pricing_type.id).order("count")
  end

  def caselots_hide_for_client(product, client)
    return @caselots_hide_for_client if @caselots_hide_for_client
    pricing_type = client.try(:pricing_type) || PricingType.default
    @caselots_hide_for_client = product.product_caselots.for_client(client).joins(:product_pricing_types).where("product_pricing_types.show = ? and product_pricing_types.pricing_type_id = ?", false, pricing_type.id).order("count")
  end
end
