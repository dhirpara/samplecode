module PaymentsHelper
  def can_void?(payment)
    payment.can_void? && (!order.canceled? || (order.canceled? && current_support_user.is_admin?))
  end

  def can_refund?(payment)
    payment.can_refund? && (!order.canceled? || (order.canceled? && current_support_user.is_admin?))
  end
end
