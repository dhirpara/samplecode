module ClientsHelper
  def active_search_condition
    condition = params[:q].dup
    condition.delete :deleted_false
    condition.delete :deleted_true
    { q: { "active_true" => "true" }.reverse_merge!(condition) }
  end

  def deleted_search_condition
    condition = params[:q].dup
    condition.delete :active_true
    { q: { deleted_true: "true" }.reverse_merge!(condition) }
  end
end
