module ArticlesHelper
  def show_article_message?(article = nil)
    if article
      article.article_category.name == "Clinical Reference"
    else
      params[:q].try(:[], :article_category_name_eq) == "Clinical Reference"
    end
  end
end
