module ApplicationHelper
  include Sortable

  def destroy_session_path
    if client_signed_in?
      return destroy_client_session_path
    elsif guest_signed_in?
      return shopping_guest_path
    end
  end

  def active_nav(val)
    cont = params[:controller].split('/').last
    if val.is_a? String
      cont == val ? 'active' : ''
    else
      val.include?(cont) ? 'active' : ''
    end
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render( partial: association.to_s.singularize + "_fields", locals: {:s => builder})
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")", :class => "button")
  end

  def link_to_add_product_caselot_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    new_object.g_product_prices

    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render association.to_s.singularize + "_fields",  s: builder
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")", :class => "button")
  end


  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)", :class => "button")
  end

  def show_need_total(order)
    if order.credit_owing? || order.balance_due?
      "#{number_to_currency(order.balance_amount, locale: I18n.default_locale)}"
    end
  end

  def show_retail_price(item)
    if item.order.client.pricing_type.name != "Retail"
      "(Retail #{number_to_currency item.product.retail_price, locale: I18n.default_locale})"
    end
  end

  def show_selected_client_info
    [current_client.first_last_name, current_client.email, current_client.city, current_client.province, current_client.country.name].select(&:present?).join(", ").html_safe
  end

  ["Clinical Reference", "Our Vision"].each do |name|
    define_method "#{name.downcase.gsub(" ", "_")}" do
      ArticleCategory.find_by_name(name)
    end
  end

  def truncate_content(content, length = 90)
    truncate strip_tags(content).gsub(/&nbsp;/i,""), length: length, separator: ' '
  end

  def use_https?
    #TODO
    #"https"
    Rails.env.production? ? "https" : "http"
  end

  def report_from_date
    if params[:from_date]
      Time.new(params[:from_date]["date(1i)"].to_i, params[:from_date]["date(2i)"].to_i, params[:from_date]["date(3i)"].to_i, 0, 0, 0, Time.zone.now.formatted_offset)
    end
  end

  def report_to_date
    if params[:to_date]
      Time.new(params[:to_date]["date(1i)"].to_i, params[:to_date]["date(2i)"].to_i, params[:to_date]["date(3i)"].to_i, 23, 59, 59, Time.zone.now.formatted_offset)
    end
  end

  def merge_order_item_reports(reports)
    reports.inject([]) do |array, report|
      if item_report = array.detect{|r| r.order_item.pricing_type_id == report.order_item.pricing_type_id &&
                                    r.order_item.product_id == report.order_item.product_id &&
                                    r.order_item.price == report.order_item.price &&
                                    r.order_item.order.currency_id == report.order.currency_id }
      item_report.quantity +=  report.quantity
      else
        array << report.dup
      end
      array
    end
  end

  def show_amount(amount)
    if amount < 0
      "(#{number_to_currency(-amount, locale: I18n.default_locale)})"
    else
      number_to_currency(amount, locale: I18n.default_locale)
    end
  end

  def show_payment_amount(payment)
    if payment.is_cc_type?
      if ["Purchase", "Void Refund", ""].include? payment.action
        number_to_currency payment.amount, locale: I18n.default_locale
      else
        "(#{number_to_currency payment.amount, locale: I18n.default_locale})"
      end
    else
      show_amount(payment.amount)
    end
  end

  def show_payments_total(payments)
    total = if payments.first.payment_type.is_cc_type?
              payments.map do |payment|
                payment.amount = ["Purchase", "Void Refund"].include?(payment.action) ? payment.amount : -payment.amount
                payment
              end
            else
              payments
            end.sum(&:amount)
            show_amount(total)
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def shipping_method_amount_message(shipment, shipping_method)
    if shipping_method.promotion_price == shipping_method.price
      "(#{number_to_currency shipping_method.price, locale: I18n.default_locale})"
    else
      "(#{number_to_currency shipping_method.promotion_price, locale: I18n.default_locale},  Because you purchased a minimum order of #{number_to_currency ShippingPromotion.get_promotion(shipment.order).min_amount, locale: I18n.default_locale}, you qualify for #{number_to_currency(shipping_method.price- shipping_method.promotion_price, locale: I18n.default_locale)} off ground shipping)"
    end
  end

  def order_report_class(report)
    if report.is_cancel_report?
      "cnclOrder"
    elsif report.is_return_report?
      "rtrnOrder"
    elsif report.is_add_report?
      "addOrder"
    end
  end

  def showLanguageSelect?
    [
      "admin/products",
      "admin/articles",
      "admin/videos",
      "admin/studies",
      "admin/faqs",
      "admin/documents",
      "admin/configuration/disorders",
      "admin/configuration/seos"
    ].include?(params[:controller])
  end

  def preview_url(resource)
    id = resource.try(:id) || 0
    "/admin/#{resource.class.name.underscore.pluralize}/#{id}/preview"
  end

  def current_language
    GlobalLanguage.find_by_abbr(I18n.locale.to_s)
  end

  def can_visit_report_nav?
    current_support_user.is_admin? ||
      current_support_user.is_basic? ||
      current_support_user.is_order_completion? ||
      current_support_user.is_export_orders?
  end

  def can_visit_management_nav?
    current_support_user.is_admin? ||
      current_support_user.is_language?
  end

  def can_visit_configurations_nav?
    current_support_user.is_admin? ||
      current_support_user.is_language?
  end
end
