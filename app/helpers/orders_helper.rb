module OrdersHelper
  def order_item_reports_sort(reports)
    reports.sort! do |x, y|
      if (x.order_item.product.product_code <=> y.order_item.product.product_code) == 0
        y.quantity <=> x.quantity
      else
        x.order_item.product.product_code <=> y.order_item.product.product_code
      end
    end
  end

  def get_products(order_item_reports)
    array = order_item_reports.group_by{|report| report.order_item.product }.to_a
    array.sort!{|x, y| x[0].product_code <=> y[0].product_code }
  end

  def hide_total_column?(search_order)
    search_order.state_value.to_s == "complete" && search_order.only_balance
  end
end
