module Sortable
  extend ActiveSupport::Concern
  def sortable_class(column, direction)
    if column == params[:sort]
      "sortable sortable-#{direction}"
    else
      "sortable"
    end
  end


  def sortable(model, column, title = nil)
    title ||= column.titleize
    direction = ("#{model.name.tableize}.#{column}" == sort_column(model) && sort_direction == 'asc') ? 'desc' : 'asc'
    content_tag :th, :class => sortable_class(column, direction) do
      link_to title, { q: params[:q], :page => params[:page], sort_class: model.name, :sort => column, :direction => direction }
    end
  end
end
