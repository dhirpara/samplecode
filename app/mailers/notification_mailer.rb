# coding: utf-8

class NotificationMailer < BaseMailer

  helper ApplicationHelper

  def client_signup(client)
    @client = client
    mail(to: client.email_or_default, from: BASE_CONFIG[:from], bcc: BASE_CONFIG[:email]["client_signup"]["bcc"], subject: "Welcome to Hardy Nutritionals!")
  end

  def order_checkout(order, address)
    @order = order
    @address = address
    if Rails.env.production?
      mail(to: order.client.email_or_default, from: BASE_CONFIG[:email]["order_checkout"]["from"], bcc: BASE_CONFIG[:email]["order_checkout"]["bcc"], subject: "Hardy Nutritionals™ order confirmation")
    else
      mail(from: BASE_CONFIG[:email]["order_checkout"]["from"], bcc: BASE_CONFIG[:email]["order_checkout"]["bcc"], subject: "Hardy Nutritionals™ order confirmation")
    end
  end

  #def address_exception(address, order)
    #@address = address
    #@order = order
    #mail(from: BASE_CONFIG[:email]["order_checkout"]["from"], to: BASE_CONFIG[:system], subject: "Error looking up shipping rates")
  #end

  #private
  #def admin_emails
    #SupportUser.all.select {|user| user.is_admin?}.collect{|user| user.email }
  #end
end
