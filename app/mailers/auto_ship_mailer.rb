class AutoShipMailer < ActionMailer::Base
  helper ApplicationHelper
  def remind(auto_shipping)
    @auto_ship = auto_shipping
    @address = Address.new(
      name: @auto_ship.payment_profile.name,
      address1: @auto_ship.payment_profile.address1,
      address2: @auto_ship.payment_profile.address2,
      city: @auto_ship.payment_profile.city,
      state: @auto_ship.payment_profile.state,
      postal_code: @auto_ship.payment_profile.postal_code,
      country: @auto_ship.payment_profile.country,
      phone: @auto_ship.payment_profile.phone
    )
    mail(to: @auto_ship.email, from: BASE_CONFIG[:email]["auto_ship"]["from"], subject: "Hardy Nutritionals Auto Ship Order Reminder")
  end

  def failure_notice(auto_ship)
    @auto_ship = auto_ship
    mail(to: @auto_ship.email, from: BASE_CONFIG[:email]["auto_ship"]["from"], subject: "Auto Ship Credit Card Failure Notice")
  end
end
