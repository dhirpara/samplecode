class BaseMailer < ActionMailer::Base
  def contact(contact)
    @contact= contact
    mail(to: BASE_CONFIG[:email]["contact"]["to"], from: BASE_CONFIG[:from], subject: "Web Contact Information Received.")
  end
end
