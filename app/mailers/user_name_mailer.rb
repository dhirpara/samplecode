class UserNameMailer < ActionMailer::Base
  def remind(client)
    @client = client
    mail(to: @client.email, from: BASE_CONFIG[:from], subject: "Hardy Nutritionals User Name Remind")
  end
end
