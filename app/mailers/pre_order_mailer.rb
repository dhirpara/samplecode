class PreOrderMailer < ActionMailer::Base
  def declined_notification(pre_order)
    @pre_order = pre_order
    mail(to: @pre_order.payment_profile.client.email, from: BASE_CONFIG[:from], subject: "Your order has been declined")
  end
end
