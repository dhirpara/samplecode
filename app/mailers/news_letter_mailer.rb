class NewsLetterMailer < ActionMailer::Base
  def subscription(email)
    @email = email
    mail(to: @email.email, from: BASE_CONFIG[:email]["news_letter"]["from"], subject: "Hardy Nutritionals NutraTalk Newsletter")
  end
end
