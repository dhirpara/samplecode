class FeedbackMailer < ActionMailer::Base
  def feedback(feedback)
    @feedback = feedback
    mail(to: BASE_CONFIG[:email]["feedback"]["to"], from: BASE_CONFIG[:email]["order_checkout"]["from"], subject: "Customer Feedback")
  end
end
