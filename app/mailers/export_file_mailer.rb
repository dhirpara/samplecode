class ExportFileMailer < ActionMailer::Base
  def usa_and_international_subscription(orders)
    attachments["orders-#{Time.zone.now.strftime("%Y-%m-%d")}.tab"] = Order.export_for_other_country(orders).gsub("\"", "")
    mail(to: BASE_CONFIG[:email]['export_file']['usa_and_international']['to'], from: BASE_CONFIG[:from], subject: "Hardy Nutritionals Export Files")
  end

  def canada_subscription(orders)
    time = Time.zone.now.strftime("%Y-%m-%d")
    attachments["canadian-orders-#{time}.tab"] = Order.export_for_other_country(orders).gsub("\"", "")
    attachments["canadian-orders-#{time}.csv"] = Order.export_for_canada(orders)
    mail(to: BASE_CONFIG[:email]['export_file']['canada']['to'], from: BASE_CONFIG[:from], subject: "Hardy Nutritionals Export Files")
  end
end
