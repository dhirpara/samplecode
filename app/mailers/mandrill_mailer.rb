class MandrillMailer < ActionMailer::Base
  def template(email, json)
    @json = json
    mail(to: email, from: BASE_CONFIG[:email]["from"], subject: @json["subject"])
  end
end
