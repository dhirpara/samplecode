class OrderItemAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
     user.is_admin? || user.is_basic?
  end

  def self.readable_by?(user)
    user.is_admin? || user.is_basic? || user.is_order_completion?
  end

  def returnable_by?(user)
    (resource.order.shipment.try(:pickup?) || resource.order.shipment.try(:shipped?)) &&
      resource.quantity > 0 && resource.can_return_quantity > 0 && user.can_update?(OrderItem)
  end
end
