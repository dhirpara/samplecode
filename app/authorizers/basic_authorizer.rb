class BasicAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_basic? || user.is_admin?
  end
end
