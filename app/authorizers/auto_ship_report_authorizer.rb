class AutoShipReportAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_admin? || user.is_basic?
  end
end
