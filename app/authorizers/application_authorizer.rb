class ApplicationAuthorizer < Authority::Authorizer
  def self.default(adjective, user)
    user.is_admin?
  end
end
