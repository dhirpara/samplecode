class CommissionAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_admin? || SupportUser.commissioned.include?(user)
  end
end
