class SeoAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_admin? || user.is_language?
  end

  def self.deletable_by?(user)
    false
  end
end
