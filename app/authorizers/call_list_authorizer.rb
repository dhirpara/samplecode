class CallListAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_admin? || user.is_basic? || user.is_order_completion? || user.is_export_orders?
  end
end
