class ProductAuthorizer < ApplicationAuthorizer
  def self.searchable_by?(user)
    user.is_admin? || user.is_basic?
  end

  def self.readable_by?(user)
    user.is_admin? || user.is_language?
  end

  def self.updatable_by?(user)
    user.is_admin? || user.is_language?
  end
end
