class CartItemAuthorizer < ApplicationAuthorizer
  def creatable_by?(user)
    if resource.cartable.back_ordered?
      if user.new_record? || user.back_ordered_customer?
        false
      else
        true
      end
    else
      true
    end
  end
end
