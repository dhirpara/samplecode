class OrderCompletionAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_order_completion? || user.is_admin?
  end
end
