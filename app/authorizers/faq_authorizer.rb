class FaqAuthorizer < ApplicationAuthorizer
  def self.readable_by?(user)
    user.is_admin? || user.is_basic? || user.is_language?
  end

  def self.updatable_by?(user)
    user.is_admin? || user.is_language?
  end
end
