class PaymentAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_order_completion? || user.is_admin?
  end

  def updatable_by?(user)
    !resource.received?
  end
end
