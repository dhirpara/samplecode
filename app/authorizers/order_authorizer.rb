class OrderAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_admin? || user.is_basic?
  end

  def self.readable_by?(user)
    user.is_admin? || user.is_basic? || user.is_order_completion?
  end

  def self.updatable_by?(user)
    user.is_admin? || user.is_basic? || user.is_order_completion?
  end

  def self.exportable_by?(user)
    user.is_admin? || user.is_export_orders?
  end

  def readable_by?(user)
    #user.is_admin? || user.clients.map{|c| c.orders }.flatten.include?(resource)
    user.is_admin? || user.support_centers.includes(clients: :orders).where("orders.id = ?", resource.id)
  end

  def readyable_by?(user)
    resource.complete? && (user.is_order_completion? || user.is_admin?) && resource.shipment.try(:state) == "pending"
  end

  def pendingable_by?(user)
    resource.complete? && (user.is_order_completion? || user.is_admin?) && resource.shipment.try(:state) == "ready"
  end

  def cancelable_by?(user)
    (user.is_admin? || user.is_basic? || user.is_order_completion?) &&
      !resource.canceled? && resource.payments.empty?
  end

  def self.readyable_by?(user)
    user.is_order_completion? || user.is_admin?
  end
end
