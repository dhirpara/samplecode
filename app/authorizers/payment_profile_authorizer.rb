class PaymentProfileAuthorizer < ApplicationAuthorizer
  def deletable_by?(user)
    if user.class == SupportUser
      if user.is_admin?
        true
      elsif user.is_order_completion?
        user.client_ids.include?(resource.client_id)
      end
    else
      resource.client == user
    end
  end
end
