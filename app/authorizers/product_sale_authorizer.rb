class ProductSaleAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_admin? || user.is_product_sales_report?
  end
end
