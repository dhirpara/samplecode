class ClientAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_admin? || user.is_basic? || user.is_order_completion?
  end

  def updatable_by?(user)
    auth_for_resource(user, resource)
  end

  def deletable_by?(user)
    auth_for_resource(user, resource)
  end

  def selectable_by?(user)
    auth_for_resource(user, resource)
  end

  def auth_for_resource(user, resource)
    (user.is_admin? || user.is_basic? || user.is_order_completion?) && user.clients.include?(resource)
  end
end
