class ShipmentAuthorizer < ApplicationAuthorizer
  def self.default(adjective, user)
    user.is_basic? || user.is_admin?
  end

  def self.readable_by?(user)
    user.is_admin? || user.is_basic? || user.is_order_completion?
  end

  def self.readyable_by?(user)
    user.is_order_completion? || user.is_admin?
  end
end
