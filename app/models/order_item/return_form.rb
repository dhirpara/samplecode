class OrderItem::ReturnForm
  include ::NonPersistenceModel

  attr_accessor :quantity, :order_item

  validates_presence_of :quantity
  validates_numericality_of :quantity, greater_than: 0, allow_blank: true
  validate :check_quantity


  def initialize(order_item, attributes = nil)
    @order_item = order_item
    unless attributes.nil?
      @quantity = attributes[:quantity].to_i
    end
  end

  def order
    order_item.order
  end

  def submit?
    ActiveRecord::Base.transaction do
      return false unless valid?
      item = order.order_items.new(quantity: -quantity, product_id: order_item.product_id, price: order_item.price,
                                   pricing_type_id: order_item.pricing_type_id, retail_price: order_item.retail_price, parent: order_item,
                                   caselot_count: order_item.caselot_count)
      item.save
      OrderItemReport.add_report(item)

      order.sync_tax_charge!
      order.change_payment_state!
      order.add_report
      true
    end
  end

  def check_quantity
    if quantity > order_item.can_return_quantity
      errors.add(:base, "Most return #{order_item.can_return_quantity} bottles")
    end
  end
end
