class Faq < ActiveRecord::Base
  TRANSLATES_ATTRIBUTES = [:question, :answer]
  translates *TRANSLATES_ATTRIBUTES

  attr_accessible :answer, :question, :faq_type_id, :index

  belongs_to :faq_type
  before_save :change_index
  scope :default_sort, order(:index)

  include Authority::Abilities

  TYPES = [["Standard", "S"], ["Advanced", "A"]]

  def self.grep_by_type(type)
    faq_type = TYPES.find {|faq_type| faq_type[1] if faq_type[0] == type.capitalize}
    return all unless faq_type.present?
    where('faq_type.name' => faq_type)
  end

  def change_index
    self.index = index.present? ? index : Faq.last_index + 1
  end

  def self.last_index
    order("index").last.try(:index) || 0
  end
end
