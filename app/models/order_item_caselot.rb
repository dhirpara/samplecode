class OrderItemCaselot < ActiveRecord::Base
  attr_accessible :count, :order_item_id, :quantity, :unit_price

  def retail_price
    unit_price / count
  end
end
