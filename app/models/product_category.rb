class ProductCategory < ActiveRecord::Base
  translates :name

  attr_accessible :name
  has_many :products
end
