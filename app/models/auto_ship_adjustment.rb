class AutoShipAdjustment < ActiveRecord::Base
  belongs_to :auto_shipping
  belongs_to :adjustment_type
  attr_accessible :amount, :note, :adjustment_type_id

  validates_presence_of :amount
end
