class Email < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :email, :first_name, :last_name, :token

  validates_presence_of :first_name
  #validates :email, presence: true, uniqueness: true, format: { with: Devise.email_regexp }
  validates :email, presence: true, format: { with: Devise.email_regexp }

  def self.subscribe(client)
    return if MailChimp.exist_subscriber?(client)

    token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless exists?(token: random_token)
    end

    email = Email.find_or_initialize_by_email(client.email)
    email.update_attributes(first_name: client.first_name, last_name: client.last_name, email: client.email, token: token)

    NewsLetterMailer.subscription(email).deliver
  end

  def self.unsubscribe(client)
    if exist_email = Email.where("lower(email) = ?", client.email.try(:downcase)).first
      exist_email.destroy
    else
      MailChimp.unsubscribe(client) rescue nil
    end
  end
end
