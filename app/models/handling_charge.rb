class HandlingCharge < ActiveRecord::Base
  include Authority::Abilities
   attr_accessible :min, :max, :handling_charge, :handling_charge_method_id, :handling_charge_method, :deleted
   belongs_to :handling_charge_method
   validates_presence_of :min, :handling_charge

   def self.rate(order, options = {})
     return 0 unless order.subtotal > 0
     options.reverse_merge!({ shipping_method: order.shipment.shipping_method, promotion: true })
     max = where(max: nil).try(:first).try(:min) || order('min DESC').try(:first).try(:min) || 0
     charge = (order.subtotal >= max ? where(min: max) : where('min <= ? AND max > ?', order.subtotal, order.subtotal).order(:handling_charge)).first.try(:handling_charge) || 0
     charge = (charge * ShippingPromotion.get_discount_rate(order, options[:shipping_method], options)) if options[:promotion]

     charge.round(2)
   end
end
