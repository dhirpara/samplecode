class ClientLimitingFactor < ActiveRecord::Base
  belongs_to :client
  belongs_to :limiting_factor
end
