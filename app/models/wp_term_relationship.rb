class WpTermRelationship < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['blog'])
  
  belongs_to :term_taxonomy, class_name: "WpTermTaxonomy"
end
