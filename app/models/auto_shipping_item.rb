class AutoShippingItem < ActiveRecord::Base
  include OrderItemExtension
  default_scope order(:created_at)
  attr_accessible :auto_shipping_id, :product_id, :quantity, :caselot_count
  attr_accessor :product_name, :order_item, :rate

  belongs_to :product
  belongs_to :auto_shipping, inverse_of: :auto_shipping_items
  alias :order :auto_shipping

  validates_presence_of :product_id, :quantity
  validates_numericality_of :quantity, greater_than: 0

#  delegate :price, :retail_price, :pricing_type, :description_after_price, to: :order_item

  def weight
    @weight ||= product.weight_lbs * quantity
  end

  def total
    @total ||= price * quantity
  end

  def pricing_type
    @pricing_type ||= auto_shipping.pricing_type
  end

  def price
    @price ||= current_price
  end

  def retail_price
    @retail_price ||= product.retail_price(auto_shipping.currency)
  end

#  def order_item
#    return @order_item if @order_item
#    #order = auto_shipping.order
#    @order_item = OrderItem.new(product_id: product_id, quantity: quantity,
#                                caselot_count: caselot_count,
#                                pricing_type_id: order.client.pricing_type_id,
#                                retail_price: product.retail_price(order.client.currency!),
#                                order: order,
#                                rate: rate)
#    @order_item.price = @order_item.current_price
#    @order_item
#  end

  def self_with_promotion
    return @self_with_promotion if @self_with_promotion

    @self_with_promotion = []
    @self_with_promotion << self


    if promotion = ProductPromotion.same_product_discount_promotions(self).where("start_date <= ? and end_date >= ? and use_only_for_referral_code = ?", auto_shipping.next_order_date, auto_shipping.next_order_date, false).first
      discount_quantity = promotion.discount_quantity(self)
      self.quantity = quantity - discount_quantity

      auto_ship_item = AutoShippingItem.new(product_id: promotion.y_product_id)
      auto_ship_item.quantity = discount_quantity
      auto_ship_item.rate = promotion.rate
      auto_ship_item.caselot_count = caselot_count
      auto_ship_item.auto_shipping = auto_shipping

      @self_with_promotion << auto_ship_item
    end

    ProductPromotion.free_promotions(self).where("start_date <= ? and end_date >= ? and use_only_for_referral_code = ?", auto_shipping.next_order_date, auto_shipping.next_order_date, false).each do |promotion|
      auto_ship_item = AutoShippingItem.new(product_id: promotion.y_product_id)
      auto_ship_item.quantity = [(quantity/promotion.x_quantity.to_i) * promotion.y_quantity, promotion.repeated_time * promotion.y_quantity].min
      auto_ship_item.rate = 0
      auto_ship_item.caselot_count = caselot_count
      auto_ship_item.auto_shipping = auto_shipping

      @self_with_promotion << auto_ship_item
    end

    if promotion = ProductPromotion.self_product_discount_promotions(self).where("start_date <= ? and end_date >= ? and use_only_for_referral_code = ?", auto_shipping.next_order_date, auto_shipping.next_order_date, false).first
      self.rate = promotion.rate
    end

    @self_with_promotion
  end

  def auto_ship_discount_price
    @auto_ship_discount_price ||= if AutoShipSetting.can_get_discount?(auto_shipping)
                              AutoShipSetting.rate(price)
                            else
                              0
                            end
  end

#  def caselot_count
#    @caselot_count ||= caselot({user: order.client, quantity: quantity }.merge({})).try(:count)
#  end

  def rate
    @rate || 1
  end
end
