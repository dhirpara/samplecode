class ReferralCodeOrder < ActiveRecord::Base
  belongs_to :referral_code
  belongs_to :order
  attr_accessible :order, :referral_code
end
