class ProductPromotion < ActiveRecord::Base
  attr_accessible :name, :x_product_id, :x_quantity, :y_product_id, :promotion_quantity_or_rate_off,
    :promotion_method_id, :repeated_time, :end_date, :start_date, :promotion_pricing_types_attributes, :use_only_for_referral_code

  include Authority::Abilities

  validates_presence_of :name, :x_product_id
  validates_numericality_of :x_quantity, :promotion_quantity_or_rate_off, greater_than: 0
  validates_presence_of :y_product_id, :promotion_method_id
  validates_presence_of :repeated_time

  belongs_to :x_product, class_name: 'Product'
  belongs_to :y_product, class_name: 'Product'
  has_many :promotion_pricing_types, as: :promotion, dependent: :destroy
  has_many :pricing_types, through: :promotion_pricing_types
  accepts_nested_attributes_for :promotion_pricing_types, allow_destroy: true, reject_if: proc { |attributes| attributes["choose"] == "0" && attributes["id"].nil? }
  attr_accessor :x_item, :y_item

  scope :free_promotions, lambda{|item| joins(promotion_pricing_types: :pricing_type).where("x_product_id = ? and promotion_method_id = 1 and pricing_types.name = ? ", item.product_id, item.pricing_type.name) }
  #x_product == y_product
  scope :same_product_discount_promotions, lambda{|item| joins(promotion_pricing_types: :pricing_type).where("x_product_id = ? and y_product_id = ? and x_quantity < ? and promotion_method_id = 2 and pricing_types.name = ?", item.product_id, item.product_id, item.quantity, item.pricing_type.name) }
  scope :self_product_discount_promotions, lambda{|item| joins(promotion_pricing_types: :pricing_type).where("x_product_id = ? and y_product_id = ? and x_quantity <= ? and promotion_method_id = 3 and pricing_types.name = ?", item.product_id, item.product_id, item.quantity, item.pricing_type.name) }

  scope :in_time, where("start_date <= ? and end_date >= ?", Date.today, Date.today)

  after_save :check_use_only_for_referral_code


  def y_quantity
    promotion_quantity_or_rate_off
  end

  def rate
    1 - promotion_quantity_or_rate_off * 0.01
  end

  def delete_non_essential_promotion_pricing_type
    promotion_pricing_types.each do |resource|
      resource.destroy unless resource.choose
    end
  end

  def discount_quantity(item)
    [item.quantity/(x_quantity.to_i + 1), repeated_time].min
  end

  class << self
    def activation(item)
      activation_discount_self_item(item)
      activation_discount_same_items(item)
      generate_free_items(item)
    end

    def activation_discount_self_item(item)
      promotions =  if item.order.referral_code
                      item.order.referral_code.product_promotions.self_product_discount_promotions(item)
                    else
                      []
                    end | self_product_discount_promotions(item).in_time.where(use_only_for_referral_code: false)

                    promotion = promotions.first

                    item.rate = if promotion.present?
                                  promotion.rate
                                else
                                  1
                                end

                    item.update_price
    end

    def activation_discount_same_items(item)
      promotions =  if item.order.referral_code
                      item.order.referral_code.product_promotions.same_product_discount_promotions(item)
                    else
                      []
                    end | same_product_discount_promotions(item).in_time.where(use_only_for_referral_code: false)

                    promotion = promotions.first

                    if promotion.present?
                      discount_quantity = promotion.discount_quantity(item)
                      item.quantity = item.quantity - discount_quantity
                      item.update_price(quantity: item.quantity + discount_quantity)

                      child = item.children.find_by_product_id(item.product_id) || item.children.new(order: item.order, product: item.product, rate: promotion.rate)
                      child.quantity = discount_quantity
                      child.update_price(quantity: item.quantity + discount_quantity)
                    else
                      item.children.find_by_product_id(item.product_id).try(:destroy)
                    end
    end

    #免费product
    def generate_free_items(item)
      promotions = if item.order.referral_code
                     item.order.referral_code.product_promotions.free_promotions(item)
                   else
                     []
                   end | free_promotions(item).in_time.where(use_only_for_referral_code: false)

                   (ProductPromotion.where(x_product_id: item.product_id,  promotion_method_id: 1) - promotions).each do |promotion|
                     item.children.find_by_product_id_and_parent_id_and_rate(promotion.y_product_id, item.id, 0).try(:destroy)
                   end

                   promotions.each do |promotion|
                     child = item.children.find_by_product_id(promotion.y_product_id) || item.children.new(order: item.order, product: promotion.y_product)
                     child.quantity = [(item.quantity/promotion.x_quantity.to_i) * promotion.y_quantity, promotion.repeated_time * promotion.y_quantity].min
                     next if child.quantity == 0

                     child.rate = 0
                     child.update_price(quantity: child.quantity)
                   end
    end
  end

  private
  def check_use_only_for_referral_code
    unless use_only_for_referral_code?
      ReferralCodeProductPromotion.where(product_promotion_id: id).each(&:destroy)
    end
  end
end
