class Tax < ActiveRecord::Base
  attr_accessible :country, :province_abbr, :name, :rate
  belongs_to :country

  def self.rate(order)
    if order.ship_address.country.abbr == "CA"
      province_abbr = order.ship_address.state
      tax = find_by_province_abbr(province_abbr)
      (order.subtotal * tax.rate * 0.01)
    elsif Country::EUROPEAN_UNION.include?(order.ship_address.country.abbr)
      order.subtotal > 25 ? order.subtotal * 0.2 : 0
    else
      0
    end.round(2)
  end
end
