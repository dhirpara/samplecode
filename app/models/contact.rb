class Contact
  include ::NonPersistenceModel
  attr_accessor :name, :email, :email_confirmation, :phone, :company, :message
  validates_presence_of :name, :email, :phone
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,})\z/ }
  validates_confirmation_of :email
end
