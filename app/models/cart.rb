class Cart < ActiveRecord::Base
  include Cart::DatabaseCart #has_many :cart_items, dependent: :destroy
  attr_accessible :cart_items_attributes, :shopper, :code, :pricing_type, :licence
  attr_accessor :code, :check_referral_code
  belongs_to :shopper, polymorphic: true
  belongs_to :pricing_type
  belongs_to :referral_code

  alias :client :shopper

  accepts_nested_attributes_for :cart_items, allow_destroy: true
  before_validation :set_code, if: :check_referral_code
  before_save :set_licence, if: :check_referral_code


  def quantity
    cart_items.inject(0) {|q, i| q + i.quantity }
  end

  def currency
    shopper.try(:currency) || Currency.find_by_name("CAD")
  end

  def g_order_by_user(user)
    transaction do
      order = user.orders.new
      cart_items.each do |cart_item|
        order.add_order_item(cart_item)
      end
      cart_items.destroy_all
      order
    end
  end

  def total_for_user(user=shopper)
    total = 0
    cart_items.each do |item|
      total += item.total_for_user(user)
    end

    total
  end

  def subtotal
    @subtotal ||= cart_items.inject(0) do |sum, item|
      sum += item.total_for_user
    end
  end

  def total
    return @total if @total
    @total = subtotal

    if referral_code
      @total += ReferralCode.rate(self)
    end

    @total
  end

  def submit?
    self.check_referral_code = true
    if valid?
      self.pricing_type = referral_code.try(:pricing_type) || client.try(:pricing_type) || PricingType.default

      cart_items.select{|i| i.parent_id.nil? }.each do |item|
        add_cartable(item, update_order: true)
      end

      save
    else
      false
    end
  end

  def set_code
    ReferralCode.set_referral_code_for_order(self)
    if referral_code.try(:need_licence?)
      validates_presence_of :licence
    end
  end

  def set_licence
    if referral_code.blank?
      self.licence = ""
    end
  end
end
