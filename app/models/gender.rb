class Gender < ActiveRecord::Base
  attr_accessible :gender_type

  has_many :question_for_gender, dependent: :destroy
  has_many :questions, through: :question_for_gender
end
