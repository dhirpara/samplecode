class OrderReport < ActiveRecord::Base
  belongs_to :order
  attr_accessible :handling, :order_id, :other, :subtotal, :payment_received, :shipping, :tax, :total

  #def payment_received
    #@payment_received ||= PaymentReport.where("order_id =? and payment_type_id in (?) and DATE(created_at) = DATE(?)", order_id, PaymentType.actual_type_id, created_at).sum(&:amount)
  #end

  def self.add_report(order)
    if order.payments.received.any?
      order_report = OrderReport.where("order_id = ? and DATE(created_at) = DATE(?)", order.id, Time.zone.now).first || OrderReport.new
      past_reports = where("order_id = ? and DATE(created_at) < DATE(?)", order.id, Time.now)

      if order.canceled?
        subtotal, tax, shipping, handling, other, total =
          - past_reports.sum(&:subtotal),
          - past_reports.sum(&:tax),
          - past_reports.sum(&:shipping),
          - past_reports.sum(&:handling),
          - past_reports.sum(&:other),
          - past_reports.sum(&:total)
      else
        subtotal, tax, shipping, handling, other, total =                                                  order.subtotal - past_reports.sum(&:subtotal),
          order.db_tax_charge - past_reports.sum(&:tax),
          order.db_shipping_charge - past_reports.sum(&:shipping),
          order.db_handling_charge - past_reports.sum(&:handling),
          order.db_other_charge - past_reports.sum(&:other),
          order.total! - past_reports.sum(&:total)
      end

      order_report.update_attributes({
        order_id: order.id,
        subtotal: subtotal,
        tax: tax,
        shipping: shipping,
        handling: handling,
        other: other,
        total: total,
        payment_received: PaymentReport.where("order_id =? and payment_type_id in (?) and DATE(created_at) = DATE(?)", order.id, PaymentType.actual_type_id, Time.now).sum(&:amount)
      })

      if order_report.subtotal == 0 && order_report.tax == 0 && order_report.shipping == 0 &&
        order_report.handling == 0 && order_report.other == 0 && order_report.total == 0 && order_report.payment_received == 0
        order_report.destroy
      end
    end
  end

  def is_cancel_report?
    total == 0
  end

  def is_return_report?
    return false unless prev_report

    prev_report.total > total
  end

  def is_add_report?
    return false unless prev_report

    prev_report.total < total
  end

  def prev_report
    @prev_report ||= OrderReport.where("order_id = ? and created_at < ?", order.id, created_at).order("created_at DESC").first
  end
end
