class SupportUser < ActiveRecord::Base
  include Authority::UserAbilities
  include Authority::Abilities
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  #
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:user_name]

  has_many :support_center_permissions
  has_many :support_centers, through: :support_center_permissions
  has_many :clients, through: :support_centers, select: "clients.email, clients.id, clients.first_name, clients.last_name, clients.active, clients.deleted, clients.guest"
  has_many :auto_shippings, through: :clients
  has_many :orders, through: :clients
  has_many :support_user_roles, dependent: :destroy
  has_many :notes, through: :clients
  has_many :order_notes, through: :clients
  has_many :created_clients, class_name: "Client", dependent: :restrict
  accepts_nested_attributes_for :support_user_roles, allow_destroy: true, reject_if: proc { |attributes| attributes["choose"] == "0" && attributes["id"].nil? }
  attr_accessible :support_user_roles_attributes

  has_many :support_user_global_languages, dependent: :destroy
  accepts_nested_attributes_for :support_user_global_languages, allow_destroy: true
  has_many :global_languages, through: :support_user_global_languages

  has_many :roles, through: :support_user_roles
  has_many :commission_rates
  has_many :view_client_reports, dependent: :destroy


  accepts_nested_attributes_for :support_center_permissions, allow_destroy: true, reject_if: proc { |attributes| attributes["choose"] == "0" && attributes["id"].nil? }
  attr_accessible :support_center_permissions_attributes

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :user_name, :first_name, :last_name, :time_zone, :active, :receive_commission, :commission_by_amount, :global_language_ids
  # attr_accessible :title, :body
  validates_presence_of :user_name, :first_name, :last_name

  scope :active, where(active: true)
  scope :commissioned, where(receive_commission: true)

  def self.find_for_database_authentication(conditions={})
    self.where("user_name = ?", conditions[:user_name]).limit(1).first ||
      self.where("email = ?", conditions[:user_name]).limit(1).first
  end

  def first_last_name
    "#{first_name} #{last_name}"
  end

  def is_admin?
    roles.any?{|r| r.code == "ADM"}
  end

  def is_basic?
    roles.any?{|r| r.code == "BAS"}
  end

  def is_order_completion?
    roles.any?{|r| r.code == "ORC"}
  end

  def is_export_orders?
    roles.any?{|r| r.code == "EXO"}
  end

  def is_product_sales_report?
    roles.any?{|r| r.code == "PSR"}
  end

  def is_language?
    roles.any?{|r| r.code == "LAN"}
  end

  def delete_non_essential_support_center_permissions
    support_center_permissions.each do |obj|
      obj.destroy unless obj.choose
    end
  end

  def delete_non_essential_support_user_roles
    support_user_roles.each do |obj|
      obj.destroy unless obj.choose
    end
  end

  def commission_rates_by_type(type)
    commission_rates.where(commission_type_id: type).order('min')
  end

  def commission_type
    if commission_by_amount?
      CommissionType::AMOUNT
    else
      CommissionType::NUMBER
    end
  end
end
