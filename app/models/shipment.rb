require 'nokogiri'

class Shipment < ActiveRecord::Base
  #include ActiveMerchant::Shipping
  include Authority::Abilities
  include ShipmentHelper
  self.authorizer_name = 'ShipmentAuthorizer'
  attr_accessible :order_id, :address_id, :shipping_method, :address, :weight_lbs, :shipped_at
  attr_accessor :cheapest_shipping_method

  SHIPPING_RATE_ERROR='Error getting shipping rate. Please check address entered or call Toll Free: 1-855-955-1114 (Outside USA or Canada: 1-587-271-1114)'

  belongs_to :order, inverse_of: :shipment
  belongs_to :shipping_method
  belongs_to :address
  #delegate :shipping_carrier, to: :shipping_method
  accepts_nested_attributes_for :order

  after_create :set_weight_lbs

  state_machine initial: 'pending' do
    state 'pending'
    state 'pickup'
    state 'ready'
    state 'shipped'

    event :pending do
      transition to: 'pending', from: 'ready'
    end

    event :ready do
      transition to: 'ready', from: 'pending', :if => :allow_ship?
    end

    event :pickup do
      transition to: 'pickup', from: 'pending', :if => :allow_ship?
    end

    event :ship do
      transition to: 'shipped', from: 'ready'
    end

    after_transition to: 'shipped', :do => :update_shipped_at
    after_transition to: ['shipped', 'pickup'] do |object, transition|
      object.order.order_items.each do |i|
        OrderItemReport.add_report(i)
      end
    end
  end

  def cheapest_shipping_method
    @cheapest_shipping_method ||= ShippingMethod.find_by_name(shipping_method_rates[0][0])
  end

  def allow_ship?
    order.complete?
  end

  def update_shipped_at
    update_attributes(shipped_at: Time.zone.now)

    if report = AutoShipReport.find_by_order_id(order_id)
      report.update_attributes(state: AutoShipReport::SHIPPED)
    end
  end

  def format_shipped_at
    shipped_at.try(:strftime, "%Y-%m-%d %H:%M:%S")
  end

  def ship_type
    if shipping_method.name == "Landmark Standard" || shipping_method.name == "Landmark Express" || shipping_method.name == "UPS"
      "LGINTSTD"
    else
      shipping_method_name = shipping_method.name.split(" ")
      shipping_method_name.shift
      ship_type_name = shipping_method_name.join(" ")
      if ship_type_name == "2 Day" || ship_type_name == "Ground Home Delivery" || ship_type_name == "Express Saver" || ["Landmark Standard", "Canpar", "UPS", "Landmark Express"].include?(shipping_method.name)
        shipping_method.name!
      elsif ship_type_name == "Priority Mail 1-Day" || ship_type_name == "Priority Mail 2-Day" || ship_type_name == "Priority Mail 3-Day"
        "Priority Mail"
      elsif ship_type_name == "Priority Mail Express 2-Day" || ship_type_name == "Priority Mail Express 1-Day"
        "Express Mail"
      else
        ship_type_name
      end
    end
  end

  private
  def set_weight_lbs
    order.sync_shipment_weight!
  end

  def weight
    order.weight
  end
end
