class Symptom < ActiveRecord::Base
  attr_accessible :disid, :question, :name, :description

  has_many :sympton_side_effects, dependent: :destroy
  has_many :side_effects, through: :sympton_side_effects
end
