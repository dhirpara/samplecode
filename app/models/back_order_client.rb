class BackOrderClient
  include ::NonPersistenceModel

  attr_accessor :email, :user_name, :country_id, :first_name, :last_name, :password, :password_confirmation,
    :receive_update, :province, :home_phone, :product_id, :product_quantity, :client, :introduction_type_id

  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,})\z/ }, allow_blank: true
  validates_presence_of :email, if: proc {|client| client.receive_update == "1" }
  validates_presence_of :user_name, :first_name, :last_name, :country_id, :password, :product_id
  validates_confirmation_of :password
  validates_length_of :password, minimum: 8
  validates_length_of :home_phone, in: 7..32

  validate :check_user_name
  validate :check_introduction_type

  def submit?
    return false unless valid?

    self.client = Client.new(email: email, user_name: user_name, first_name: first_name, last_name: last_name,
                        country_id: country_id, home_phone: home_phone, province: province, password: password,
                        password_confirmation: password_confirmation, introduction_type_id: introduction_type_id,
                       support_center_id: SupportCenter.find_by_code("HNW").try(:id), back_ordered_customer: true,
                       active: true)

    if receive_update == "1"
      Email.subscribe(self.client)
    end

    self.client.save!(validate: false)

    BackOrderReport.create(client_id: self.client.id, product_id: product_id, quantity: product_quantity)

    true
  end

  def country
    @country ||= Country.find_by_id(country_id)
  end

  def product
    @product ||= Product.find_by_id(product_id)
  end

  def check_user_name
    errors.add(:user_name, "has already been taken") if Client.find_by_user_name(user_name)
  end

  def check_introduction_type
    if introduction_type_id.blank?
      errors.add(:base, "Please let us know how you heard about us so we can serve you better.")
    end
  end
end
