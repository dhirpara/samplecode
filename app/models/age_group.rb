class AgeGroup < ActiveRecord::Base
  attr_accessible :name_of_group, :range_of_group

  has_many :question_age_groups, dependent: :destroy
  has_many :questions, through: :question_age_groups
end
