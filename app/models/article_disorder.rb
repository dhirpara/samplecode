class ArticleDisorder < ActiveRecord::Base
  attr_reader :choose
  attr_accessible :choose, :disorder_id
  belongs_to :product
  belongs_to :disorder
  belongs_to :article

  def choose=(value)
    @choose = (value == "1" || value == true) ? true : false
  end
end
