class QuestionAgeGroup < ActiveRecord::Base
  attr_accessible :question_id, :age_group_id
  
  belongs_to :question
  belongs_to :age_group
end
