class PaymentReport < ActiveRecord::Base
  attr_accessible :amount, :note, :order_id, :payment_type_id, :support_user_id, :payment_id, :payment, :amount_owing
  belongs_to :order
  belongs_to :payment
  belongs_to :support_user
  belongs_to :payment_type

  def amount_received
    @amount_received ||= PaymentReport.where("order_id = ? and payment_type_id = ? and DATE(created_at) = DATE(?) and created_at <= ?", order_id, payment_type_id, created_at, created_at).sum(&:amount)
  end

  def self.add_report(payment)
    generate_report(payment)
  end

  def self.generate_report(payment)
    return unless can_generate_report?(payment)

    payment_report = PaymentReport.new(payment: payment)
    payment_report.generate_report
  end

  def self.can_generate_report?(payment)
    payment.received?
  end

  def generate_report
    assign_attributes(amount: payment.amount!, note: payment.note, order_id: payment.order_id,
                      payment_type_id: payment.payment_type_id, support_user_id: payment.support_user_id, amount_owing: payment.order.need_purchase_total)
    save
  end
end
