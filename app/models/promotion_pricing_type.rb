class PromotionPricingType < ActiveRecord::Base
  belongs_to :pricing_type
  attr_reader :choose
  attr_accessible :pricing_type_id, :choose

  def choose=(value)
    @choose = (value == "1" || value == true) ? true : false
  end
end
