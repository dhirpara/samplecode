class Province < ActiveRecord::Base
   attr_accessible :country, :abbr, :name
   belongs_to :country
end
