class VideoDisorder < ActiveRecord::Base
  attr_reader :choose
  attr_accessible :disorder_id, :choose

  belongs_to :disorder
  belongs_to :video

  def choose=(value)
    @choose = (value == "1" || value == true) ? true : false
  end
end
