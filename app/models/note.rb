class Note < ActiveRecord::Base
  include Authority::Abilities
  self.authorizer_name = 'NoteAuthorizer'
  belongs_to :client
  belongs_to :support_user

  attr_accessible :note, :deleted
  validates_presence_of :note
end
