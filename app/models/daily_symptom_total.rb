class DailySymptomTotal < ActiveRecord::Base
  attr_accessible :cid, :date, :date_entered, :diaid, :total
end
