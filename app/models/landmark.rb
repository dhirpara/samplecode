class Landmark
  def find_rates(order)
    rates = []
    if order.shipment.address.country.abbr == "CA"
      LandmarkShippingRate.all.each do |rate|
        price = rate.base_rate
        if order.weight > rate.base_weight_lbs
          price += (order.weight - rate.base_weight_lbs) * rate.rate_per_lbs
        end
        rates << [rate.shipping_method.name, price * 100]
      end

      rates << ["UPS", rates.detect{|r| r[0] == "Landmark Standard"}[1] + 200]
    else
      s = Roo::Spreadsheet.open('public/shipping_rates.xls')
      abbr_list = s.row(2)
      weight_list = s.column(1)
      weight_start = 5
      weight_end = 70

      abbr_index = abbr_list.index{|a| a == order.shipment.address.country.abbr }
      price = if weight_list[weight_start] <= order.weight.ceil && order.weight.ceil <= weight_list[weight_end]
               s.cell(s.column(1).index{|a| a == order.weight.ceil } + 1, abbr_index + 1)
             else
               #最高的价格                                   #每加1lbs的价格
               s.cell(weight_end + 1, abbr_index + 1) + (order.weight.ceil - weight_list[weight_end]) * s.cell(weight_end + 1 + 1, abbr_index + 1).round(2)
             end.round(2)
             rates << ["Landmark Standard", price * 100]
    end

    rates
  end
end
