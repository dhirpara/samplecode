class Article < ActiveRecord::Base
  TRANSLATES_ATTRIBUTES = [:title, :content, :seo_title, :seo_keywords, :seo_description]

  translates *TRANSLATES_ATTRIBUTES

  include Authority::Abilities
  attr_accessible :title, :content, :article_disorders_attributes, :author, :publish_date, :column_number, :article_category_id, :index, :seo_title, :seo_keywords, :seo_description
  belongs_to :article_category
  validates_presence_of :title, :content, :author
  validates_uniqueness_of :title
  mount_uploader :image, ArticleImageUploader
  attr_accessible :image, :image_cache, :remove_image
  mount_uploader :author_image, ArticleAuthorImageUploader
  attr_accessible :author_image, :author_image_cache, :remove_author_image, :author_image_alt, :image_alt, :disorder_ids
  has_many :disorders, through: :article_disorders
  has_many :article_disorders, dependent: :destroy
  accepts_nested_attributes_for :article_disorders, allow_destroy: true, reject_if: proc { |attributes| attributes["choose"] == "0" && attributes["id"].nil? }
  before_save :set_permalink

  scope :default_sort, order(:index)
  before_save :change_index

  def title_parameterize
    if title.match(/\p{Han}+/u)
      title.gsub(" ", "-")
    else
      title.parameterize
    end
  end

  def next
    Article.where("index > ?", index).default_sort.first || Article.default_sort.first
  end

  def prev
    Article.where("index < ?", index).default_sort.last || Article.default_sort.last
  end

  #def delete_non_essential_article_disorders
    #article_disorders.each do |article_disorder|
      #article_disorder.destroy unless article_disorder.choose
    #end
  #end

  protected
  def set_permalink
    self.permalink = title.parameterize
  end

  def change_index
    self.index = index.present? ? index : Article.last_index + 1
  end

  def self.last_index
    order("index").last.try(:index) || 0
  end
end
