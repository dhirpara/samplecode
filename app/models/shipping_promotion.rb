class ShippingPromotion < ActiveRecord::Base
  attr_accessible :amount_off, :end_date, :min_amount, :start_date, :promotion_pricing_types_attributes, :area_id
  include Authority::Abilities

  has_many :promotion_pricing_types, as: :promotion, dependent: :destroy
  has_many :pricing_types, through: :promotion_pricing_types
  accepts_nested_attributes_for :promotion_pricing_types, allow_destroy: true, reject_if: proc { |attributes| attributes["choose"] == "0" && attributes["id"].nil? }


  validates_numericality_of :min_amount, :amount_off, greater_than: 0
  validates_presence_of :area_id

  def delete_non_essential_promotion_pricing_type
    promotion_pricing_types.each do |resource|
      resource.destroy unless resource.choose
    end
  end

  class << self
    def get_promotion(order, options = {})
      options.reverse_merge!({ date: Date.today })
      date = options[:date]
      area_id = order.shipment.address.country.abbr == "US" && order.shipment.address.state != "AK" && order.shipment.address.state != "HI" ? 1 : 2
      joins(promotion_pricing_types: :pricing_type).where("start_date <= ? and end_date >= ? and min_amount <= ? and area_id = ? and name = ?", date, date, order.subtotal, area_id, order.pricing_type.name).order("amount_off DESC").first
    end

    def get_discount_rate(order, shipping_method, options = {})
      promotion = get_promotion(order, options)
      if promotion.nil? || order.shipment.cheapest_shipping_method != shipping_method
        1
      else
        (100 - promotion.amount_off) * 0.01
      end
    end

    def discount_rate(order, rates, options = {})
      promotion = get_promotion(order, options)
      return rates if promotion.nil?

      rates.each{|a| a[1] = a[1] * (1 - promotion.amount_off * 0.01) if a[0] == order.shipment.cheapest_shipping_method.name }
    end
  end
end
