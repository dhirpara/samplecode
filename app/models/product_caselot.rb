class ProductCaselot < ActiveRecord::Base
  translates :description

  include ProductExtension
  attr_accessible :count, :description
  attr_accessor :quantity
  has_many :product_pricing_types, dependent: :destroy, as: :priceable
  accepts_nested_attributes_for :product_pricing_types
  attr_accessible :product_pricing_types_attributes

  belongs_to :product

  validates_numericality_of :count, greater_than: 0, message: "Caselot Count must be greater than 0"

  #default_scope order('count DESC')

  def g_product_prices
    PricingType.where(active: true).each do |pricing_type|
      product_pricing_type = product_pricing_types.new(pricing_type_id: pricing_type.id)
      Currency.all.each do |currency|
        product_pricing_type.product_prices.new(currency_id: currency.id)
      end
    end
  end

  def retail_price_for_user(user = nil)
    price_for(user: user)
  end

  #需要修改时用for_pricing_type方法
  def self.for_client(client)
    search({
      product_pricing_types_available_eq: true,
      product_pricing_types_pricing_type_id_eq: client.try(:pricing_type_id) || PricingType.find_by_name("Retail").id
    }).result(distinct: true)
  end

  def self.for_pricing_type(pricing_type)
    search({
      product_pricing_types_available_eq: true,
      product_pricing_types_pricing_type_id_eq: pricing_type.id
    }).result(distinct: true)
  end
end
