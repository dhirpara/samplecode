class Video < ActiveRecord::Base
  TRANSLATES_ATTRIBUTES = [:title, :description, :seo_title, :seo_keywords, :seo_description]

  translates *TRANSLATES_ATTRIBUTES

  include Authority::Abilities
  include NextPreviousHelper
  include DisorderHelper
  attr_accessible :description, :embed_link, :title, :image, :seo_title, :seo_keywords, :seo_description, :index, :image_alt, :disorder_ids
  has_many :video_disorders, dependent: :destroy
  accepts_nested_attributes_for :video_disorders, allow_destroy: true, reject_if: proc { |attributes| attributes["choose"] == "0" && attributes["id"].nil? }
  attr_accessible :video_disorders_attributes
  has_many :disorders, through: :video_disorders
  before_save :change_index

  mount_uploader :image, VideoImageUploader
  attr_accessible :image, :image_cache, :remove_image

  validates_presence_of :title, :embed_link

  scope :default_sort, order("index")

  def related
    Video.search(id_not_eq: id, video_disorders_disorder_id_in: video_disorders.map(&:disorder_id) ).result(distinct: true)
  end

  #def delete_non_essential_video_disorders
    #video_disorders.each do |video_disorder|
      #video_disorder.destroy unless video_disorder.choose
    #end
  #end

  def to_param
    "#{id}-#{title.parameterize}"
  end

  def change_index
    self.index = index.present? ? index : Video.last_index + 1
  end

  def self.last_index
    order("index").last.try(:index) || 0
  end
end
