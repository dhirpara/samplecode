class Drug < ActiveRecord::Base
  attr_accessible :brand_name, :drug_name, :current_dosage, :unit, :api_link, :menufacture, :herbal, :sleep, :exercise, :water, :amount, :comments, :date_stopped, :delete_date, :deleted, :dlid, :label, :previous, :ssid, :ssid_deleted, :unit
end
