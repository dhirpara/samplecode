class Setting < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :name, :value, :code
  validates_presence_of :name, :value
end
