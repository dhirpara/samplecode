class OrderNote < ActiveRecord::Base
  include Authority::Abilities
  self.authorizer_name = 'OrderNoteAuthorizer'
  belongs_to :client
  belongs_to :read_support_user, class_name: "SupportUser"
  belongs_to :create_support_user, class_name: "SupportUser"
  attr_accessible :note, :deleted
  validates_presence_of :note
end
