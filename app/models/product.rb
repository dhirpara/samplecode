class Product < ActiveRecord::Base
  TRANSLATES_ATTRIBUTES = [:name, :seo_title, :seo_keywords, :seo_description, :highlights, :description, :suggested_use, :supplement_facts, :quality]

  translates *TRANSLATES_ATTRIBUTES

  mount_uploader :label_file, ProductLabelFileUploader
  include Authority::Abilities
  include ProductExtension
  include DisorderHelper
  self.authorizer_name = 'ProductAuthorizer'
  attr_accessible :name, :disorder_id, :product_promotions_attributes, :product_images_attributes, :product_disorders_attributes,
                  :quantity, :unit, :description, :highlights, :shipping_description, :weight_lbs, :product_code, :tariff_code, :customs_value,
                  :chargeable, :type_id, :allow_order_online, :active, :product_caselots_attributes, :featured, :product_category_id, :suggested_use, :supplement_facts, :featured_text,
                  :label_file, :label_file_cache, :remove_label_file, :seo_title, :seo_keywords, :seo_description, :index, :quality, :back_ordered, :verbiage, :disorder_ids, :pre_order
  belongs_to :product_category
  belongs_to :product_type, class_name: "ProductType", foreign_key: "type_id"
  has_many :product_disorders, dependent: :destroy
  accepts_nested_attributes_for :product_disorders, allow_destroy: true, reject_if: proc { |attributes| attributes["choose"] == "0" && attributes["id"].nil? }
  has_many :disorders, through: :product_disorders
  has_many :product_images, dependent: :destroy
  accepts_nested_attributes_for :product_images, allow_destroy: true
  has_many :product_pricing_types, dependent: :destroy, as: :priceable
  accepts_nested_attributes_for :product_pricing_types
  attr_accessible :product_pricing_types_attributes

  before_save :change_index

  scope :default_sort, order(:index)
  #scope :clinical_use, where(product_category_id: 1)
  #scope :general_health_use, where(product_category_id: 2)
  #scope :other_products, where(product_category_id: 3)
  scope :featured_list, where(featured: true)

  has_many :product_caselots, dependent: :destroy do
    def sort_for_count
      sort {|x,y| (x.count || 1000 )<=> (y.count || 1000)}
    end
  end
  accepts_nested_attributes_for :product_caselots, allow_destroy: true

  has_many :order_items, dependent: :restrict

  validates_presence_of :name, message: "Name can't be blank"
  validates_numericality_of :quantity, greater_than: 0, message: "Quantity must be greater than 0"
  validates_numericality_of :weight_lbs, greater_than: 0, message: "Weight lbs must be greater than 0"

  scope :available, where("allow_order_online = ? and products.active = ?", true, true)

  UNIT_VALUES=['g', 'mg', 'ml']

  def related_videos
    Video.joins(:video_disorders).where(:"video_disorders.disorder_id" => disorders.pluck(:id)).uniq
  end

  def label_file_name
    label_file_url.split("/")[-1]
  end

  def product_images_or_default
    product_images.any? ? product_images : [ProductImage.new]
  end

  def g_product_prices
    PricingType.where(active: true).each do |pricing_type|
      product_pricing_type = product_pricing_types.new(pricing_type_id: pricing_type.id)
      Currency.all.each do |currency|
        product_pricing_type.product_prices.new(currency_id: currency.id)
      end
    end
   end

   def prev(client)
     Product.for_client(client).where("products.index < ?", index).default_sort.last || Product.for_client(client).default_sort.last
   end

   def next(client)
     Product.for_client(client).where("products.index > ?", index).default_sort.first || Product.for_client(client).default_sort.first
   end

   def self.for_client(client, admin = false, conditions = {})
     hash = {
       active_eq: true,
       product_pricing_types_available_eq: true,
       product_pricing_types_pricing_type_name_in: [client.try(:pricing_type).try(:name)] | ["Retail"],
     }
     hash[:allow_order_online_eq] = true unless admin
     hash.merge!(conditions)
     Product.search(hash).result(distinct: true)
   end

   def retail_price(currency = nil)
     price_for(currency: currency)
   end

   def default_image
     product_images.any? ? product_images.first : ProductImage.new
   end

   def default_secondary_image
     #product_images.where(index: 2).present? ? product_images.where(index: 2).first.image_url(size) : ProductImage.new.image_url(size)
     product_images.second || ProductImage.new
     #product_image.image_url(size)
   end

   #def delete_non_essential_product_disorders
     #product_disorders.each do |product_disorder|
       #product_disorder.destroy unless product_disorder.choose
     #end
   #end

   def group_by_caselot(quantity)
     group = []
     sum = quantity

     product_caselots.order('caselot_count DESC').each do |caselot|
       count = caselot.caselot_count
       if count <= sum
         num = sum / count
         sum -= count * num
         caselot.quantity = num
         group << caselot
       end
     end

     group
   end

   def to_param
    "#{id}-#{name.downcase.strip.gsub(' ', '-').gsub('%', '-').gsub("/", "-").gsub(".", "-")}"
   end

   def display_name
     "#{name}, #{quantity} #{unit.present? ? unit : product_type.try(:name)}"
   end

   def quantity_unit
     "#{quantity} #{unit.present? ? I18n.t("product.unit.#{unit}") : product_type.try(:name)}"
   end

   def change_index
     self.index = index.present? ? index : Product.last_index + 1
   end

   def self.last_index
     order("index").last.try(:index) || 0
   end
end
