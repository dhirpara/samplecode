class CommissionType < ActiveRecord::Base
   attr_accessible :name, :code

   AMOUNT = where(code: 'AMT').first
   NUMBER = where(code: 'NMB').first

   def self.number_type
     where(code: 'NMB').first
   end

   def self.amount_type
     where(code: 'AMT').first
   end
end
