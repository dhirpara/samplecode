class Document < ActiveRecord::Base
  translates :name

  attr_accessible :name, :article_category_id

  include Authority::Abilities

  mount_uploader :file, DocumentFileUploader
  attr_accessible :file, :file_cache, :remove_file

  validates_presence_of :name, :article_category_id
  validate :presence_file

  def presence_file
    if remove_file.nil? || remove_file == "1"
      errors.add(:base, "File can't be blank")
      self.remove_file!
    end
  end

  def file_name
    file_url.split("/")[-1]
  end
end
