class OrderAdjustment < ActiveRecord::Base
  include Authority::Abilities
  self.authorizer_name = 'OrderCompletionAuthorizer'
  attr_accessible :amount, :note, :adjustment_type, :adjustment_type_id
  belongs_to :adjustment_type
  belongs_to :adjustable, polymorphic: true
  validates_presence_of :amount

  after_commit :sync_charge

  def can_select_adjustment_types
    tax = AdjustmentType.find_by_name("tax")
    shipping = AdjustmentType.find_by_name("shipping")
    shipping_adjustment = AdjustmentType.find_by_name("shipping adjustment")
    handling = AdjustmentType.find_by_name("handling")
    array = if adjustment_type == tax
              [tax]
            elsif adjustment_type == shipping
              [shipping]
            elsif adjustment_type == handling
              [handling]
            elsif adjustment_type == shipping_adjustment
              [shipping_adjustment]
            else
              []
            end
    (AdjustmentType.all - adjustable.order_adjustments.map(&:adjustment_type).select{|t| ["shipping adjustment", "handling", "tax", "shipping"].include? t.name} + array).sort_by!(&:id)
  end

  def sync_charge
    adjustable.change_state!("payment")
    adjustable.change_payment_state!
    adjustable.add_report
  end

  def self.other_adjustments
    search(adjustment_type_name_in: ["promotion", "refund", "charge", "discount"]).result
  end
end
