class Seo < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :description, :keywords, :title, :url

  TRANSLATES_ATTRIBUTES = [:title, :keywords, :description]
  translates *TRANSLATES_ATTRIBUTES
end
