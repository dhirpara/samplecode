class Country < ActiveRecord::Base
  attr_accessible :name, :abbr, :currency_id
  attr_accessor :index

  belongs_to :currency

  has_many :country_shipping_methods
  has_many :shipping_methods, through: :country_shipping_methods
  has_many :provinces

  EUROPEAN_UNION = ["AT", "BE", "BG", "CY", "CZ", "DK", "EE", "FI", "FR",
                    "DE", "GR", "HU", "IE", "IT", "LV", "LT", "LU", "MT", "NL",
                    "PL", "PT", "RO", "SK", "SI", "ES", "SE", "GB"]

  def self.form_selector
    find(:all).collect { |country| [country.name, country.id] }
  end
end
