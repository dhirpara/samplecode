class WpTermTaxonomy < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['blog'])
  set_table_name "wp_term_taxonomy"

  belongs_to :term, class_name: "WpTerm"
end
