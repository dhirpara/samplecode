class CountryShippingMethod < ActiveRecord::Base
  attr_accessible :country, :shipping_method
  #
  belongs_to :country
  belongs_to :shipping_method

end
