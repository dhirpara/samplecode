# encoding: utf-8
class CartItem < ActiveRecord::Base
  include OrderItemExtension
  include Authority::Abilities
  attr_accessible :cartable_id, :cartable_type, :quantity, :cart, :cart_id, :order, :product, :rate
  #没用处，只为了和order_item共用一个方法
  attr_accessor :price, :caselot_count, :pricing_type_id, :retail_price
  belongs_to :cart, inverse_of: :cart_items
  belongs_to :pricing_type
  alias :order :cart

  belongs_to :parent, class_name: :CartItem
  has_many :children, class_name: :CartItem, foreign_key: :parent_id, dependent: :destroy

  belongs_to :cartable, polymorphic: true
  alias :product :cartable

  validates :cartable, presence: true
  validates :quantity, presence: true, numericality: { greater_than: 0 }

  def price
    self.caselot_count = caselot(user: cart.client).try(:count)

    current_price
  end

  def retail_price
    cartable.retail_price
  end

  def order=(val)
    self.cart = val
  end


  def self.find_by_product_id_and_parent_id_and_rate(product_id, parent_id, rate)
    self.find_by_cartable_id_and_parent_id_and_rate(product_id, parent_id, rate)
  end

  def self.find_by_product_id_and_parent_id(product_id, parent_id)
    self.find_by_cartable_id_and_parent_id(product_id, parent_id)
  end

  def self.find_by_product_id(product_id)
    self.find_by_cartable_id(product_id)
  end

  def self.find_all_by_product_id(product_id)
    self.find_all_by_cartable_id(product_id)
  end

  def self.find_by_product_id_and_rate(product_id, rate)
    self.find_by_cartable_id_and_rate(product_id, rate)
  end

  def product_id
    cartable_id
  end

  def pricing_type
    order.pricing_type
  end

  def product=(val)
    self.cartable = val
  end

  def total_for_user(user=nil)
    price * quantity
  end
end
