class ProductPricingType < ActiveRecord::Base
  attr_accessible :available, :priceable_id, :priceable_type, :pricing_type_id, :show
  has_many :product_prices, dependent: :destroy
  accepts_nested_attributes_for :product_prices
  attr_accessible :product_prices_attributes

  belongs_to :pricing_type

  validate :check_price
  def check_price
    priceable_name = priceable_type == 'Product' ? 'Product' : 'Caselot'
    errors.add(:base, "#{priceable_name} Price must be greater than 0") if available? && product_prices.any?{|p| p.price.to_f <= 0}
  end
end
