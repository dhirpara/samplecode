class ShippingCarrier < ActiveRecord::Base
  attr_accessible :name

  def carrier_api
    if name == "USPS"
      ShippingCarrier.usps_carrier
    elsif name == "FedEx"
      ShippingCarrier.fedex_carrier
    elsif name == "Landmark"
      ShippingCarrier.landmark_carrier
    end
  end

  def self.usps_carrier
    USPS.new(login: BASE_CONFIG[:shipping_carrier]['usps']['login'])
  end

  def self.fedex_carrier
    FedEx.new(login: BASE_CONFIG[:shipping_carrier]['fedex']['login'],
              password: BASE_CONFIG[:shipping_carrier]['fedex']['password'],
              key: BASE_CONFIG[:shipping_carrier]['fedex']['key'],
              account: BASE_CONFIG[:shipping_carrier]['fedex']['account'])
  end

  def self.landmark_carrier
    Landmark.new
  end
end
