class IntroductionType < ActiveRecord::Base
  include Authority::Abilities
  has_many :clients, dependent: :restrict
  attr_accessible :name, :index, :deleted
  validates_presence_of :name
  scope :active, where(deleted: false)
  scope :default_sort, order('index ASC')
end
