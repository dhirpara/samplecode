class BackOrderReport < ActiveRecord::Base
   include Authority::Abilities
   attr_accessible :client_id, :product_id, :quantity

   belongs_to :client
   belongs_to :product
end
