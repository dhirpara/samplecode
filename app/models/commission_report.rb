class CommissionReport < ActiveRecord::Base
  belongs_to :order
  belongs_to :support_user
  attr_accessible :amount, :date, :description, :rate, :order, :support_user

  def commission_amount
    @commission_amount ||= (amount || 0) * (rate || 0)/100
  end

  def client
    @client ||= order.client
  end

  def rate_and_description
    if rate == 0
      "(#{description})"
    else
      "#{rate}% (#{description})"
    end
  end

  def self.add_init_report(order)
    return if where(order_id: order.id).any?
    return unless order.commission_support_user
    return unless order.complete?

    report = new(order: order, support_user: order.commission_support_user, date: Date.current, amount: 0)

    if order.commission_support_user.commission_by_amount
      report.update_attributes(description: "Amount", rate: 0)
    else
      if rate = CommissionRate.find_rate_by_client(order)
        report.update_attributes(description: "Order #{rate.min}->#{rate.max}", rate: rate.rate)
      end
    end
  end

  def self.add_report(order)
 #   return unless payment.received?
    return unless order.commission_support_user
    # return unless PaymentType::ACTUAL_TYPE_NAME.include?(payment.payment_type.try(:name))

    actual_received_total = order.payments.includes(:payment_type).where("payment_types.name in (?) and received = ?", PaymentType::ACTUAL_TYPE_NAME, true).sum(&:amount!)
    tmp_amount = [actual_received_total, order.subtotal].min - CommissionReport.where(order_id: order.id).sum(&:amount)

    unless tmp_amount == 0
      if order.commission_support_user.commission_by_amount
        generate_order_amount_report(order, tmp_amount)
      else
        generate_order_number_report(order, tmp_amount)
      end
    end
  end

  def self.generate_order_amount_report(order, amount)
    order_received_total = CommissionReport.where(order_id: order.client.order_ids, support_user_id: order.commission_support_user.id).sum(&:amount)

    CommissionRate.valid_amount_type_range(order, amount).each do |rate|
      tmp_amount =  rate.calculate_amount_by_payment(order, order_received_total, amount)

      create(order: order, support_user: order.commission_support_user,
             date: Date.current, amount: tmp_amount, rate: rate.rate,
             description: "Amount #{rate.min} - #{rate.max}")
    end
  end

  def self.generate_order_number_report(order, amount)
    if exist_report = CommissionReport.where(order_id: order.id).first
      report = exist_report.dup
      report.update_attributes(date: Date.current, amount: amount)
    else
      if rate = CommissionRate.find_rate_by_client(order)
        create(order: order, support_user: order.commission_support_user, rate: rate.rate,
               description: "Order #{rate.min}->#{rate.max}", date: Date.current, amount: amount)
      end
    end
  end
end
