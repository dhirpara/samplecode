class ProductPrice < ActiveRecord::Base
  attr_accessible :currency_id, :price
  belongs_to :product_pricing_type
  belongs_to :currency
  belongs_to :priceable, polymorphic: true
end
