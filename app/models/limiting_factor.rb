class LimitingFactor < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :name

  has_many :client_limiting_factors, dependent: :destroy

  validates_presence_of :name
  validates_uniqueness_of :name, allow_blank: true
end
