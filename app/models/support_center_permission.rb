class SupportCenterPermission < ActiveRecord::Base
  attr_accessible :support_user_id, :support_center_id, :choose
  attr_reader :choose

  belongs_to :support_user
  belongs_to :support_center

  def choose=(value)
    @choose = (value == "1" || value == true) ? true : false
  end
end
