class StudyDisorder < ActiveRecord::Base
  attr_reader :choose
  attr_accessible :disorder_id, :study_id, :choose
  belongs_to :disorder
  belongs_to :study

  def choose=(value)
    @choose = (value == "1" || value == true) ? true : false
  end
end
