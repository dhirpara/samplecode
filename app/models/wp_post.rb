class WpPost < ActiveRecord::Base
  has_many :attachments, class_name: "WpPost", foreign_key: "post_parent", conditions: { post_type: "attachment" }, order: "post_date"
  has_many :postmetas, class_name: "WpPostmeta", foreign_key: "post_id"
  has_many :term_relationships, class_name: "WpTermRelationship", foreign_key: "object_id"
  has_many :term_taxonomies, through: :term_relationships
  has_many :terms, through: :term_taxonomies

  self.establish_connection(ActiveRecord::Base.configurations['blog'])

  scope :publish_posts, where("post_type = 'post' and post_status= 'publish'")

  def image
    feature_image || attachments.first
  end

  def feature_image
    @feature_image ||= WpPost.find_by_ID(postmetas.find_by_meta_key("_thumbnail_id").try(:meta_value))
  end
end
