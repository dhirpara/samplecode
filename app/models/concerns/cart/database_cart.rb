module Cart::DatabaseCart
  extend ActiveSupport::Concern

  included do
    has_many :cart_items, dependent: :destroy, inverse_of: :cart
    alias :order_items :cart_items
  end

  def add_cartable(item, options = {})
    options.reverse_merge!({reset_quantity: false, update_order: false})

    tmp_item = if order_item = order_items.detect{|i| i.cartable_id == item.product_id && i.parent_id.nil? }
                 order_item
               else
                 item.order = self
                 item
               end

    tmp_item.quantity = if options[:update_order]
                          if tmp_item.quantity_changed?
                            item.quantity
                          else
                            order_items.where("rate != 0").find_all_by_product_id(item.product_id).sum(&:quantity)
                          end
                        else
                          options[:reset_quantity] ? item.quantity : order_items.where("rate != 0").find_all_by_product_id(item.product_id).sum(&:quantity) + item.quantity
                        end

    tmp_item.add_to_order(self, options)
  end

  def remove_cartable(item)
    cart_items.where(cartable_id: item.cartable, cartable_type: item.cartable.class.name).first.try(:destroy)
  end

  module ClassMethods
    def find_or_create_by_user(user)
      if cart = where(shopper_id: user, shopper_type: user.class.name).first
        cart
      else
        Cart.create(shopper_id: user, shopper_type: user.class.name)
      end
    end
  end
end
