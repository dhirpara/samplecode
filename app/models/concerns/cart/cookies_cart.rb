module Cart::CookiesCart
  extend ActiveSupport::Concern

  included do
    attr_accessor :cart_items
    self.class_attribute :cartable_type
    self.cartable_type = "Varant"
  end

  def initialize(attributes = {})
    @cart_items = if attributes.is_a? Hash
                    if attributes.try(:[], :cart_items_attributes)
                      attributes[:cart_items_attributes].inject([]) do |array, hash|
                        array << CartItem.new(hash.merge!(cartable_type: cartable_type))
                      end
                    else
                      []
                    end
                  else
                    attributes.split(",").inject([]) do |array, str|
                      cartable_id, quantity = str.split("-")
                      array << CartItem.new(cartable_id: cartable_id, cartable_type: cartable_type, quantity: quantity)
                    end
                  end
  end

  def remove_cartable(item)
    cart_items.delete_if {|ci| ci.cartable == item.cartable }
  end

  def add_cartable(item)
    save unless persisted?
    if cart_item = cart_items.detect {|ci| ci.cartable == item.cartable }
      cart_item.quantity += item.quantity
    else
      cart_items << item
    end
  end

  def g_cookies
    cart_items.map {|cart_item| "#{cart_item.cartable_id}-#{cart_item.quantity}" }.join(",")
  end
end
