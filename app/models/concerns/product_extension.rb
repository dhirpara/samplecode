module ProductExtension
  extend ActiveSupport::Concern

  def price_for(options = {})
    user = options[:user]
    currency = options[:currency]
    pricing_type = options[:pricing_type]
    currency_name_eq = currency.try(:name) || user.try(:currency).try(:name) || "CAD"

    if currency_name_eq == "USA"
      currency_name_eq = "USD"
    end

    ProductPrice.search({
      product_pricing_type_priceable_type_eq: self.class.name,
      product_pricing_type_priceable_id_eq: id,
      product_pricing_type_available_eq: true,
      product_pricing_type_pricing_type_id_eq: (pricing_type || pricing_type_for_user(user)).id,
      currency_name_eq: currency_name_eq
    }).result.order(:price).first.price
  end

  def pricing_type_for_user(user = nil)
    return PricingType.find_by_name("Retail") if user.nil?
    PricingType.search({
      product_pricing_types_priceable_type_eq: self.class.name,
      product_pricing_types_priceable_id_eq: id,
      product_pricing_types_available_eq: true,
      id_eq: user.pricing_type_id
    }).result.first || PricingType.find_by_name("Retail")
  end
end
