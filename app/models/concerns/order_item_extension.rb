module OrderItemExtension
  extend ActiveSupport::Concern

  def add_to_order(order, options = {})
 #   self.order = order
 #   item = update_quantity(options)
 #   item.order = order
 #   item.update_price(options)
 #   ProductPromotion.activation(item)
    update_price(options)
    ProductPromotion.activation(self)
  end

  def remove_from_order
    destroy
  end

  def update_quantity(options = {})
    options.reverse_merge!({reset_quantity: false})
    if order_item = order.order_items.find_by_product_id_and_parent_id(product_id, nil)
      order_item.quantity = options[:reset_quantity] ? quantity : order_item.order.order_items.where("rate != 0").find_all_by_product_id(product_id).sum(&:quantity) + quantity
      #order_item.quantity = order_item.same_items_total_quantity(reset_quantity: options[:reset_quantity])
      order_item
    else
      self
    end
    #item.save
    #item
  end

  #def same_items_total_quantity
  #if self.new_record?
  #order.order_items
  #else
  #order.order_items.where("id != ?", id)
  #end.select{|i| i.rate != 0 && i.product_id == product_id}.sum(&:quantity) + quantity
  #end



  def update_price(options = {})
    if quantity_changed?
      self.caselot_count = caselot({user: order.client}.merge(options)).try(:count)
    end
    self.pricing_type = order.pricing_type #pricing_type_for_user(order.client)
    self.price = current_price
    self.retail_price = product.retail_price(order.currency)
    self.save
  end

  def current_price
    #if caselot(user: order.client).present?
    #caselot(user: order.client).price_for(user: order.client, currency: order.currency)
    #else
    #product.price_for(user: order.client, currency: order.currency)
    #end * rate
    (product.product_caselots.for_pricing_type(order.pricing_type).find_by_count(caselot_count) || product).
      price_for(user: order.client, currency: order.currency, pricing_type: order.pricing_type) * rate
  end

  #找机会删除
  def pricing_type_for_user(user = nil)
    #if caselot(user: user).present?
    #caselot(user: user).pricing_type_for_user(user)
    #else
    #product.pricing_type_for_user(user)
    #end
    (product.product_caselots.for_client(user).find_by_count(caselot_count) || product).
      pricing_type_for_user(user)
  end

  def caselot(options = {})
    product.product_caselots.for_pricing_type(order.pricing_type).order("count DESC").detect do |caselot|
      caselot.count <= (options[:quantity] || (order.order_items.select{|i| i.rate != 0 && i.id != id && i.product_id == product_id}.sum(&:quantity) + quantity))
    end
  end

  def rate_off
    1 - rate
  end

  def is_rentail_pricing_type?
    pricing_type.name == "Retail"
  end

  def description_after_price
    if rate != 1
      if rate == 0
        "(FREE)"
      else
        "(#{rate_off * 100}% off)"
      end
    else
      #if caselot
      if item_caselot = product.product_caselots.for_pricing_type(order.pricing_type).find_by_count(caselot_count)
        if !is_rentail_pricing_type?
          "(#{item_caselot.description}, Retail #{retail_price})"
        else
          "(#{item_caselot.description})"
        end
      elsif is_rentail_pricing_type?
        "(Retail)"
      elsif !is_rentail_pricing_type?
        "(Retail #{retail_price})"
      end
    end
  end
end
