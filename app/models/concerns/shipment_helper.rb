require 'nokogiri'

module ShipmentHelper
  extend ActiveSupport::Concern

  def shipping_method_rates
    @shipping_method_rates ||= if address.country.abbr == "US"
                                 ShippingMethod.usps_rates(self) + ShippingMethod.fedex_rates(self)
                               elsif address.country.abbr == "CA"
                                 ShippingMethod.canada_rates(self)
                               elsif (["AU", "NZ"] + Country::EUROPEAN_UNION).include?(address.country.abbr)
                                 ShippingMethod.canada_rates(self) + ShippingMethod.usps_rates(self)
                               else
                                 ShippingMethod.usps_rates(self)
                               end.sort{|x, y| x[1] <=> y[1]}
  end

  def available_shipping_methods(options = {})
    return [] unless address.present?
    ShippingMethod.available_methods(self, options)
  end

  def generate_package(weight)
    num = (weight/70.0).ceil

    if num == 0
      Package.new(weight*16, [6, 6, 6], :units => :imperial)
    else
      per_weight = weight/num
      num.times.inject([]) do |array, _|
        array << Package.new(per_weight*16, [6, 6, 6], :units => :imperial)
      end
    end
  end

  def package
    generate_package(weight)
  end

  def update_address(attr, options = {})
    addr = Address.new(attr)
    return addr unless addr.valid?

    options.reverse_merge!({ sync_charge: true })
    if can_update_address?
      address.update_attributes(attr)
    else
      address.update_attributes(client: nil)
      update_attributes(address: Address.create(attr.merge!(client: order.client)))
    end
    order.update_ship_address(address) if options[:sync_charge]
    address
  end

  def can_update_address?
    if self.class == Shipment
      Shipment.where(address_id: address_id).count < 2 &&
        AutoShipping.where(address_id: address_id).count == 0
    else
      AutoShipping.where(address_id: address_id).count < 2 &&
        Shipment.where(address_id: address_id).count == 0
    end
  end
end
