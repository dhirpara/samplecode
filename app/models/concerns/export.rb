module Export
  extend ActiveSupport::Concern

  module ClassMethods
    def export_for_other_country(orders)
      CSV.generate(row_sep: "\r\n") do |csv|
        array = []
        index = 0
        array[index] = [
          "INVOICE_NO", "PRIMARY_ID", "Ship_Address1", "Ship_Address2", "Ship_City", "Ship_Country",
          "Ship_First", "Ship_Last", "Ship_Company", "Ship_Zip", "Ship_State", "Ship_Phone", "Ship_Email",
          "Ship_Type", "Ship_Via", "Subtotal", "ShipHand", "Tax", "Total_Sale", "Custom1", "Product Code",
          "Quantity", "Weight", "Description", "Price"
        ].join("\t")
        csv << [array[index]]
        orders.each do |o|
          client = o.client
          shipment = o.shipment
          address = shipment.address
          shipping_method = shipment.shipping_method
          ship_via = shipping_method.shipping_carrier.name == "FedEx" ? "FedEx" : "Express 1"
          name_array = address.name.split(" ")
          ship_first = name_array.shift
          ship_last = name_array.join(" ")
          o.order_items.each do |i|
            index += 1
            product = i.product
            array[index] = [
              o.id,
              o.id,
              address.address1,
              address.address2,
              address.city,
              address.country.name,
              ship_first,
              ship_last,
              address.name,
              address.postal_code,
              address.state,
              address.phone,
              client.email,
              shipment.ship_type,
              ship_via,
              o.subtotal,
              o.db_shipping_charge + o.db_handling_charge + o.db_shipping_adjustment_charge,
              o.db_tax_charge,
              o.total,
              product.shipping_description,
              product.product_code,
              i.quantity,
              product.weight_lbs,
              product.name,
              i.price
            ].join("\t")
            csv << [array[index]]
          end
        end
      end
    end

    def export_for_canada(orders)
      CSV.generate do |csv|
        orders.each do |o|
          shipment = o.shipment
          address = shipment.address
          #csv << ["Line Indicator", "Shipment Reference", "Name", "Attention", "Address1", "Address2", "City", "Prov", "Postal", "Country", "Phone", "Service", "Email", "Residential/Commercial"]
          shipping_method_code = case shipment.shipping_method.name
                                 when "Landmark Standard"
                                   "LGCASTD"
                                 when "Landmark Express"
                                   "LGCAEXP"
                                 when "UPS"
                                   "LGCAUPSG"
                                 end
          csv << [
            "H",
            o.id,
            address.name,
            nil,
            address.address1,
            address.address2.blank? ? nil : address.address2,
            address.city,
            address.state.blank? ? nil : address.state,
            address.postal_code.blank? ? nil : address.postal_code,
            address.country.name,
            address.phone,
            shipping_method_code,
            o.client.email,
            nil
          ]
          #csv << ["Line Indicator", "Package Referenct", "Weight", "Length", "Width", "Height"]
                                   o.package_number.times do |i|
                                     csv << [
                                       "P",
              "#{o.id}-#{i+1}",
              (o.weight/o.package_number).round(2),
              "1",
              "1",
              "1"
            ]
          end

          #csv << ["Line Indicator", "SKY", "Quantity", "Unit Price", "Description for Customs", "Country of Origin", "Canadian H.S Code"]
          o.order_items.each do |i|
            csv << [
              "I",
              i.product.product_code.blank? ? nil : i.product.product_code,
              i.quantity,
              i.price,
              [i.product.name, i.product.product_type.name, i.product.quantity].join(" "),
              'US',
              nil
            ]
          end
        end
      end
    end
  end
end
