module NextPreviousHelper
  extend ActiveSupport::Concern

  def next
    self.class.where("index > ?", index).default_sort.first || self.class.default_sort.first
  end

  def prev
    self.class.where("index < ?", index).default_sort.last || self.class.default_sort.last
  end
end
