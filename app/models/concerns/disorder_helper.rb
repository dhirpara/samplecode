module DisorderHelper
  extend ActiveSupport::Concern
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def grep_by_disorder_ids(disorder_ids)
      if disorder_ids.present?
        results = []
        disorder_ids.each do |disorder_id|
          collection = joins("#{to_s.downcase}_disorders".to_sym).where("#{to_s.downcase}_disorders.disorder_id = ?", disorder_id)
          results = results | collection
        end
      else
        results = all
      end

      results
    end
  end
end
