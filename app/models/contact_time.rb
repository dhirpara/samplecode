class ContactTime < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :description, :deleted
  has_many :clients, dependent: :restrict

  scope :active, where(deleted: false)
end
