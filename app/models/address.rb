class Address < ActiveRecord::Base
  include Authority::Abilities
  self.authorizer_name = 'BasicAuthorizer'
  attr_accessible :name,:address1, :address2, :city, :state, :country, :country_id, :postal_code, :active, :use_shipping_address, :phone, :client
  attr_accessor :use_shipping_address, :without_validate_name

  validates_presence_of :name, if: Proc.new {|a| a.without_validate_name.nil? }
  validates_presence_of :address1, :city, :state, :country_id, :postal_code, :phone
  validates_length_of :phone, in: 7..32

  belongs_to :country
  belongs_to :client

  def address_lines(join_chars = ', ')
    [address1, address2].delete_if{|add| add.blank?}.join(join_chars)
  end

  def build_billing_address
    attributes.merge!({
      country: country.abbr,
      zip: postal_code
    }).symbolize_keys
  end

  def use_shipping_address=(value)
    @use_shipping_address = (value == "1" || value == "true" || value == true) ? true : false
  end

  def delete_record
    #order.shipment.update_attributes(address_id: nil, shipping_method: nil) if order.shipment.address == self

    Shipment.where(address_id: id).count + AutoShipping.where(address_id: id).count > 0 ? update_attributes(client: nil) : destroy
  end
end
