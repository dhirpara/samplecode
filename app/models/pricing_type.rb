class PricingType < ActiveRecord::Base
  attr_accessible :name, :default, :active, :allow_auto_shipping, :auto_shipping_discount
  has_many :product_pricing_types

  def self.default
    find_by_name("Retail")
  end
end
