class Search::Order
  include ::NonPersistenceModel
  mattr_accessor :search_columns, :form_columns

  def self.attr_accessor_and_search(*attr)
    attr_accessor *attr
    self.search_columns = self.form_columns = attr.inject([]){|array, column| array << column }
  end

  attr_accessor :orders
  attr_accessor :only_balance, :state_value


  STATE_VALUE = {
    all: "",
    complete: ["complete"],
    incomplete: ["cart", "address", "delivery", "payment"],
    canceled: ["canceled"]
  }

  COUNTRY_VALUE = {
    all: "",
    canada: "CA"
  }

  attr_accessor :completed_at_lteq, :completed_at_gteq, :only_pending

  attr_accessor_and_search :id_eq, :shipment_address_country_abbr_eq, :client_first_name_cont, :client_last_name_cont,
    :client_email_cont, :client_email_or_client_first_name_or_client_last_name_or_client_user_name_cont, :client_home_phone_or_client_work_phone_or_client_cell_phone_cont

  def initialize(attributes, options)
    if attributes
      form_columns.each{ |column| instance_variable_set("@#{column}", attributes[column]) }

      @state_value = attributes[:state_value].to_sym
      @only_balance = attributes[:only_balance] == "1"
      @only_pending = attributes[:only_pending] == "1"

      @completed_at_gteq = Time.new(attributes["completed_at_gteq(1i)"], attributes["completed_at_gteq(2i)"], attributes["completed_at_gteq(3i)"], 0, 0, 0, Time.zone.now.formatted_offset)
      @completed_at_lteq = Time.new(attributes["completed_at_lteq(1i)"], attributes["completed_at_lteq(2i)"], attributes["completed_at_lteq(3i)"], 23, 59, 59, Time.zone.now.formatted_offset)
    else
      @completed_at_gteq = 6.month.ago
      @state_value = :complete
      @shipment_address_country_abbr_eq = ""
    end

    @orders = options[:orders].search(condition(attributes)).result
  end

  def condition(attributes)
    if attributes
      hash = search_columns.inject({}){ |hash, column| hash[column] = attributes[column]; hash }
      hash.merge!({payment_state_in: ['balance_due', 'credit_owing']}) if only_balance
      hash.merge!({state_in: STATE_VALUE[state_value]}) if STATE_VALUE[state_value].present?
      if state_value == :complete
        hash.merge!({
          completed_at_gteq: completed_at_gteq,
          completed_at_lteq: completed_at_lteq
        })

        if attributes["only_pending"] == "1"
          hash.merge!({
            shipment_state_eq: "pending"
          })
        end
      end
      hash
    else
      {state_in: "complete"}
    end
  end
end
