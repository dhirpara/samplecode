class PaymentType < ActiveRecord::Base
  attr_accessible :name

  scope :non_cc_type, where("name not in (?)", ["Visa", "Mastercard", "Discover"])

  CARD_NAME = ["Visa", "Mastercard", "Discover"]
  ACTUAL_OTHER_TYPE_NAME = ["Money Order/Cheque"]
  ACTUAL_TYPE_NAME = CARD_NAME + ACTUAL_OTHER_TYPE_NAME

  def self.actual_type_id
    where("name in (?)", ACTUAL_TYPE_NAME).map(&:id)
  end

  def is_cc_type?
    CARD_NAME.include?(name)
  end

  def self.credit_type
    find_by_name("Credit")
  end
end
