class CommissionRate < ActiveRecord::Base
  include Authority::Abilities
  before_save :change_type

  attr_accessible :commission_type, :max, :min, :rate, :support_user_id
  validates_presence_of :min, :rate, :support_user_id

  belongs_to :support_user
  belongs_to :commission_type

  def self.find_rate_by_client(order)
    order_number = CommissionReport.where(order_id: order.client.order_ids, support_user_id: order.commission_support_user.id).group_by(&:order_id).count + 1

    order.commission_support_user.commission_rates_by_type(CommissionType.number_type).detect do |rate|
      order_number >= rate.min and ( rate.max.blank? or order_number <= rate.max )
    end
  end

  def calculate_amount_by_payment(order, order_received_total, amount)

    order_received_total_with_current = order_received_total + amount

    if amount >= 0
      if max.nil?
        order_received_total_with_current - [order_received_total, min].max
      else
        [order_received_total_with_current, max].min - [order_received_total, min].max
      end
    else
      [order_received_total_with_current, min].max - [order_received_total, max].min
    end
  end

  def self.valid_amount_type_range(order, amount)
    order_received_total = CommissionReport.where(order_id: order.client.order_ids, support_user_id: order.commission_support_user.id).sum(&:amount)
    order_received_total_with_current = order_received_total + amount

    commission_rates = order.commission_support_user.commission_rates_by_type(CommissionType.amount_type)

    array = commission_rates.delete_if do |rate|
      ([order_received_total, order_received_total_with_current].max <= rate.min) ||
        ([order_received_total, order_received_total_with_current].min >= rate.max)
    end

    if amount < 0
      array.sort{|x, y| y.min <=> x.min}
    else
      array.sort{|x, y| x.min <=> y.min}
    end
  end

  def change_type
    self.commission_type_id = commission_type_id ? commission_type_id : support_user.commission_type.id
  end
end
