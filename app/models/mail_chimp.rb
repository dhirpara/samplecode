class MailChimp
  def self.gateway
    @gateway ||= Gibbon::API.new(BASE_CONFIG[:mail_chimp]["api"])
  end

  def self.subscribe(client)
    gateway.lists.subscribe({id: BASE_CONFIG[:mail_chimp]["list_id"],
                             email: { email: client.email },
                             merge_vars: { FNAME: client.first_name, LNAME: client.last_name},
                             double_optin: false})
  end

  def self.unsubscribe(client)
    gateway.lists.unsubscribe(id: BASE_CONFIG[:mail_chimp]["list_id"],
                              email: {email: client.email}, delete_member: true)
  end

  def self.exist_subscriber?(client)
    gateway.lists.member_info({id: BASE_CONFIG[:mail_chimp]["list_id"],
                               emails: [{email: client.email}]})["data"].any?
  end
end
