class ShippingMethod < ActiveRecord::Base
  require 'xmlsimple'
  include Authority::Abilities
  self.authorizer_name = 'BasicAuthorizer'
  attr_accessible :name, :shipping_carrier, :default, :shipping_carrier_id
  attr_accessor :price, :promotion_price
  belongs_to :shipping_carrier

  RENAME = {
    :"FedEx Ground Home Delivery" => "FedEx Ground",
    :"USPS Priority Mail 1-Day" => "USPS Priority Mail",
    :"USPS Priority Mail 2-Day" => "USPS Priority Mail",
    :"USPS Priority Mail 3-Day" => "USPS Priority Mail",
    :"USPS Priority Mail Express 1-Day" => "USPS Express Mail",
    :"USPS Priority Mail Express 2-Day" => "USPS Express Mail"
  }

  def name!
    RENAME.keys.include?(name.to_sym) ? RENAME[name.to_sym] : name
  end

  def subtitle
    { "Landmark Standard" => " - Ground", "UPS" => " - Courier", "Landmark Express" => " - Priority"  }[name]
  end

  class << self
    def rate(shipment, options={})
      rates = shipping_rates(shipment, options)
      (rates.detect{|rate| rate[0] == shipment.shipping_method.name}[1] * 0.01).round(2)
    end

    def available_methods(shipment, options = {})
      options.reverse_merge!({ with_price: false, promotion: true })
      rates = shipment.shipping_method_rates
      promotion_rates = get_promotion_rates(shipment.order, rates.map(&:dup), options) if options[:promotion]
      #shipping_methods = shipment.address.country.shipping_methods.blank? ? where(default: true) : shipment.address.country.shipping_methods
      #shipping_methods = shipping_methods.where(name: rates.map{|r| r[0]}.flatten)
      shipping_methods = ShippingMethod.where(name: rates.map{|r| r[0]}.flatten)

      if options[:with_price]
        shipping_methods = shipping_methods.each do |s|
          s.price = ((rates.detect{|r| r[0] == s.name}[1] * 0.01) + shipment.order.handling_charge(shipping_method: s, promotion: false) + shipment.order.shipping_adjustment_charge(shipping_method: s, promotion: false)).round(2)
          s.promotion_price = ((promotion_rates.detect{|r| r[0] == s.name}[1] * 0.01) + shipment.order.handling_charge(options.merge!(shipping_method: s)) + shipment.order.shipping_adjustment_charge(options.merge!(shipping_method: s))).round(2) if options[:promotion]
        end
        if options[:promotion]
          shipping_methods.sort_by(&:promotion_price)
        else
          shipping_methods.sort_by(&:price)
        end
      else
        shipping_methods
      end
    end

    def get_promotion_rates(order, rates, options = {})
      ShippingPromotion.discount_rate(order, rates, options)
    end

    def shipping_rates(shipment, options = {})
      options.reverse_merge!({ promotion: true })
      rates = shipment.shipping_method_rates
      #options[:promotion] ? ShippingPromotion.discount_rate(shipment.order.reload, rates) : rates
      #TODO
      #TO CHECK
      options[:promotion] ? ShippingPromotion.discount_rate(shipment.order, rates, options) : rates
    end

    def canada_rates(shipment)
      ShippingCarrier.landmark_carrier.find_rates(shipment.order)
    end

    def usps_rates(shipment)
      ShippingCarrier.usps_carrier.find_rates(origin, destination(shipment.address), shipment.package).rates.inject([]) do |array, rate|
        if ShippingMethod.pluck(:name).include?(rate.service_name)
          array << [rate.service_name, rate.price]
        else
          array
        end
      end
    end

    def fedex_rates(shipment)
      array = []
      fedex_response = ShippingCarrier.fedex_carrier.find_rates(origin, destination(shipment.address), shipment.package)
      XmlSimple.xml_in(fedex_response.xml)["RateReplyDetails"].each do |rate_reply_detail|
        rate_type = rate_reply_detail['ServiceType'][0].gsub('_', ' ').titleize.gsub("Fedex", "FedEx")
        rate_type = "FedEx " + rate_type unless rate_type.include?("FedEx")
        rate_reply_detail['RatedShipmentDetails'].each do |rate_shipment_detail|
          shipment_rate_detail = rate_shipment_detail['ShipmentRateDetail'][0]
          if shipment_rate_detail.to_s.include?("PAYOR_ACCOUNT")
            rate = shipment_rate_detail["TotalNetFedExCharge"][0]["Amount"][0].to_f * 100 * 1.17
            if ShippingMethod.pluck(:name).include?(rate_type)
              array << [rate_type, rate]
            else
              array
            end
          end
        end
      end
      array
    end

    def origin
      Location.new(country: BASE_CONFIG[:origin_address]['country'],
                   state: BASE_CONFIG[:origin_address]['state'],
                   city: BASE_CONFIG[:origin_address]['city'],
                   zip: BASE_CONFIG[:origin_address]['zip'])
    end

    def destination(address)
      if address.country.abbr == "CA"
        Location.new(country: address.country.abbr,
                     province: address.state,
                     city: address.city,
                     postal_code: address.postal_code)
      else
        Location.new(country: address.country.abbr,
                     state: address.state,
                     city: address.city,
                     zip: address.postal_code)
      end
    end
  end
end
