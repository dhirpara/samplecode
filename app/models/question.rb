class Question < ActiveRecord::Base
  attr_accessible :description, :title,:gender_ids, :age_group_ids

  has_many :question_age_groups, dependent: :destroy
  has_many :age_groups, through: :question_age_groups

  has_many :question_for_gender, dependent: :destroy
  has_many :genders, through: :question_for_gender
end
