class TransferPage < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :name, :url

  validates_uniqueness_of :name, allow_blank: true
  validates_presence_of :name, :url
end
