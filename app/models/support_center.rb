class SupportCenter < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :support_center_type_id, :support_center_type, :code, :name, :address, :city, :province, :country_id, :country, :postal_code, :phone, :fax, :email, :support_email, :support_phone, :deleted, :default

  has_many :clients, dependent: :restrict
  has_many :support_center_permissions, dependent: :restrict
  has_many :support_users, through: :support_center_permissions
  belongs_to :support_center_type
  belongs_to :country

  validates_presence_of :support_center_type_id, :name, :country_id

  before_save :set_default

  scope :available, where(deleted: false)

  def set_default
    SupportCenter.update_all(default: false) if default
  end
end
