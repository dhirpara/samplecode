class SideEffect < ActiveRecord::Base
  attr_accessible :description, :name, :symptom_ids

  has_many :symptom_side_effects, dependent: :destroy
  has_many :symptoms, through: :symptom_side_effects
end
