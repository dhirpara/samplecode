class CountryCurrency
  include ::NonPersistenceModel
  include Authority::Abilities

  attr_accessor :countries

  def initialize(attributes = nil)
    if attributes
      @countries = []
      attributes[:countries].keys.each do |id|
        country = Country.find(id)
        country.assign_attributes(attributes[:countries][id])
        @countries << country
      end
    else
      @countries = Country.order(:id)
    end
  end

  def submit
    @countries.each(&:save)
  end
end
