class TaLevel < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :deleted, :name
  validates_presence_of :name
  has_many :clients, dependent: :restrict

  scope :active, where(deleted: false)
end
