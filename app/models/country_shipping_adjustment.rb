class CountryShippingAdjustment < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :country_id, :amount, :percentage, :deleted, :country

  validates_presence_of :amount
  belongs_to :country
  def is_deleted?
    deleted? ? "Yes" : "No"
  end

  def is_percentage?
    percentage? ? "Yes" : "No"
  end

  class << self
    def rate(order, options ={})
      options.reverse_merge!({ shipping_method: order.shipment.shipping_method, promotion: true })
      charge = if adjustment = where(country_id: order.shipment.address.country_id).first
                 adjustment.amount
               else
                 0
               end
      charge = charge * ShippingPromotion.get_discount_rate(order, options[:shipping_method], options) if options[:promotion]
      charge.round(2)
    end
  end
end
