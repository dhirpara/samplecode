class ProductType < ActiveRecord::Base
  translates :name
  attr_accessible :name
end
