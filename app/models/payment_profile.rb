class PaymentProfile < ActiveRecord::Base
  include Authority::Abilities
  acts_as_paranoid
  attr_accessible :customer_code, :last_digits, :address1, :address2, :city, :postal_code, :phone, :name, :state, :country_id, :currency_id, :payment_type
  belongs_to :country
  belongs_to :payment_type
  belongs_to :client
  belongs_to :currency
  has_many :payments
  has_many :auto_shippings , dependent: :nullify
  has_many :pre_order, dependent: :destroy

  def self.generate(order, address, credit_card, resp)
    address_attributes = address.attributes.select do |k,v|
      ["address1", "address2", "city", "state", "name", "postal_code", "phone", "country_id"].include? k
    end

    payment_type_name = if resp.params["cardType"] == "VI"
                          "Visa" 
                        elsif resp.params["cardType"] == "MC"
                          "Mastercard"
                        elsif resp.params["cardType"] == "NN"
                          "Discover"
                        else
                          nil
                        end

    payment_type = PaymentType.find_by_name(payment_type_name)

    payment_profile = order.client.payment_profiles.build
    payment_profile.update_attributes(address_attributes.merge({
      customer_code: resp.params["customerCode"], last_digits: credit_card.last_digits, currency_id: order.currency_id,
      payment_type: payment_type
    }))
    payment_profile
  end

  def card_method
    case payment_type.try(:name)
    when "Visa"
      "V(#{last_digits})"
    when "Mastercard"
      "M(#{last_digits})"
    when "Discover"
      "D(#{last_digits})"
    end
  end
end
