class SupportUserRole < ActiveRecord::Base
  attr_accessible :role_id, :support_user_id
  belongs_to :role

  attr_reader :choose
  attr_accessible :choose, :disorder_id
  def choose=(value)
    @choose = (value == "1" || value == true) ? true : false
  end
end
