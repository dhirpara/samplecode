class Mandrill
  include HTTParty

  base_uri 'https://mandrillapp.com/api/1.0'

  def template_info(name)
    self.class.get("/templates/info.json?key=#{BASE_CONFIG[:mandrill]["api_key"]}&name=#{CGI.escape(name)}")
  end
end
