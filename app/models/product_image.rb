class ProductImage < ActiveRecord::Base
  mount_uploader :image, ProductImageUploader
  include Authority::Abilities
  attr_accessible :image, :image_cache, :index, :alt

  default_scope order('index ASC')
end
