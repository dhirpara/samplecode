class Diagnosis < ActiveRecord::Base
  include Authority::Abilities

  has_many :client_diagnoses, dependent: :destroy

  attr_accessible :name
  validates_presence_of :name
  validates_uniqueness_of :name, allow_blank: true
end
