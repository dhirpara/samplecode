# encoding: utf-8
class OrderItem < ActiveRecord::Base
  include OrderItemExtension
  include Authority::Abilities
  self.authorizer_name = 'OrderItemAuthorizer'
  attr_accessible :order_id, :order, :price, :quantity, :product_id, :caselot_count, :retail_price, :product, :rate, :parent, :pricing_type_id
  belongs_to :product
  belongs_to :order, inverse_of: :order_items
  belongs_to :pricing_type

  belongs_to :parent, class_name: :OrderItem
  has_many :children, class_name: :OrderItem, foreign_key: :parent_id, dependent: :destroy

  validates :quantity, presence: true
  validates_presence_of :product
  #before_validation :change_quantity

  scope :similar_item, lambda{|item| where(product_id: item.product_id, parent_id: nil).first }
  def total
    price * quantity
  end

  def total_for_now
    reload_price * quantity
  end

  def weight
    product.weight_lbs * quantity
  end

  def name
    product.name
  end

  #def change_quantity
    #unless skip_valid_quantity
      #self.quantity = quantity.to_i <= 0 ? 1 : quantity
    #end
  #end

  def reload_price
    self.caselot_count = caselot(order.client).try(:count)
    self.price_for(user: order.client, client: order.currency)
  end

  def can_return_quantity
    quantity + children.where("quantity < ?", 0).sum(&:quantity)
  end
end
