class AutoShipping < ActiveRecord::Base
  include ShipmentHelper

  attr_accessible :client_id, :interval, :next_order_date, :active, :support_user_id, :shipping_method_id, :address_id, :address, :payment_address_id, :auto_shipping_items_attributes, :use_shipping_address, :email
  attr_accessor :use_shipping_address, :order, :auto_shipping_items_with_promotion

  belongs_to :client
  belongs_to :support_user
  belongs_to :shipping_method
  belongs_to :address
  belongs_to :payment_profile

  has_many :auto_shipping_items, dependent: :destroy, inverse_of: :auto_shipping
  alias :order_items :auto_shipping_items

  has_many :auto_ship_reports
  has_one :latest_auto_ship_report, conditions: { state: AutoShipReport::PENDING, order_id: nil }, class_name: "AutoShipReport", dependent: :destroy

  has_many :auto_ship_adjustments, dependent: :destroy, inverse_of: :auto_shipping

  accepts_nested_attributes_for :auto_shipping_items, allow_destroy: true
  #validates_presence_of :client_id
  validates_presence_of :auto_shipping_items, :address_id, :shipping_method_id, :payment_profile
  validates_presence_of :email, :interval
  validates :next_order_date, date: { after: Proc.new { Date.current }, message: 'must be after today' }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,})\z/ }, allow_blank: true

  delegate :currency, to: :client
  delegate :shipment, :canceled?, to: :order

  def weight
    @weight ||= auto_shipping_items_with_promotion.sum(&:weight)
  end

  def pricing_type_id
    @pricing_type_id ||= pricing_type.id
  end

  def pricing_type
    @pricing_type ||= client.pricing_type
  end

  def subtotal
    @subtotal ||= auto_shipping_items_with_promotion.sum(&:total)
  end

  def tax_charge
    @tax_charge ||= if address
                      Tax.rate(self)
                    else
                      0
                    end
  end

  def handling_charge
    @handling_charge ||= if address
                           HandlingCharge.rate(self, date: next_order_date)
                         else
                           0
                         end
  end

  def shipping_charge
    @shipping_charge ||= if shipping_method
                           ShippingMethod.rate(self, date: next_order_date)
                         else
                           0
                         end
  end

  def shipping_adjustment_charge
    @shipping_adjustment_charge ||= if shipping_method
                                      CountryShippingAdjustment.rate(self, date: next_order_date)
                                    else
                                      0
                                    end
  end

  def other_charge
    @other_charge ||= (auto_ship_charge + auto_ship_adjustments.sum(&:amount)).round(2)
  end

  def order
    return @order if @order
    @order = Order.new(client_id: client_id,
                       currency_id: client.currency!.id,
                       order_type: OrderType.find_by_code("W"),
                       pricing_type: client.pricing_type
                      )

    #TODO为了让auto_ship的order一开始就拥有order_items
    auto_shipping_items_with_promotion.each do |item|
      @order.order_items.build(product_id: item.product_id, quantity: item.quantity,
                                  caselot_count: item.caselot_count,
                                  pricing_type_id: pricing_type_id,
                                  price: item.price,
                                  retail_price: item.retail_price,
                                  rate: item.rate)
    end

    @order.build_shipment(address: address, shipping_method: shipping_method)

    @order
  end

  def auto_shipping_items_with_promotion
    return @auto_shipping_items_with_promotion if @auto_shipping_items_with_promotion
    @auto_shipping_items_with_promotion = auto_shipping_items.map(&:self_with_promotion).inject([]){|array, items| array + items}
  end

  def has_payment_address?
    payment_address_id.present?
  end

  def has_items?
    auto_shipping_items.present?
  end

  def total
    (subtotal + shipping_handling_charge + tax_charge + other_charge).round(2)
  end

  def shipping_handling_charge
    shipping_charge + handling_charge + shipping_adjustment_charge
  end

  #def shipping_charge
  #ShippingMethod.rate(order.shipment, promotion: false)
  #end
  #  alias db_shipping_charge shipping_charge


  #  def handling_charge(options = {})
  #    options.reverse_merge!({ promotion: false })
  #    HandlingCharge.rate(order, options)
  #  end
  #  alias db_handling_charge handling_charge

  #  def shipping_adjustment_charge(options = {})
  #    options.reverse_merge!({ promotion: false })
  #    CountryShippingAdjustment.rate(order, options)
  #  end

  def ship_address
    address
  end

  def auto_ship_charge
    AutoShipSetting.rate_by_order(self)
  end

  def add_item(item)
    if auto_shipping_item = auto_shipping_items.where(product_id: item.product_id).first
      auto_shipping_item.quantity = auto_shipping_item.quantity + item.quantity
      auto_shipping_item.caselot_count = auto_shipping_item.caselot(user: client, quantity: auto_shipping_item.quantity).try(:count)
      auto_shipping_item.save
    else
      self.auto_shipping_items << item
      item.caselot_count = item.caselot(user: client, quantity: item.quantity).try(:count)
      item.save
    end
    AutoShipReport.add_report(self)
  end

  def remove_order_item(item)
    auto_shipping_items.delete(item)
    AutoShipReport.add_report(self)
    #item.destroy
  end

  def can_generate_order?
    active? && auto_shipping_items.any? && address && shipping_method && payment_profile &&
      email.present? && interval.present?
  end

  def generate_order
    Order.transaction do
      order = client.orders.create(number: Time.now.to_i, currency_id: client.currency!.id,
                                   order_type: OrderType.find_by_code("W"), pricing_type: client.pricing_type)

      # Order Items
      auto_shipping_items.each do |item|
        order_item = order.order_items.new(product_id: item.product.id, quantity: item.quantity)
        order.add_item(order_item)
        order_item.caselot_count = item.caselot_count
        order_item.update_price
        order_item.children.each do |i|
          i.caselot_count = item.caselot_count
          i.update_price
        end
      end

      order.sync_handling_charge!

      if AutoShipSetting.rate_by_order(order) != 0
        order.order_adjustments.create(amount: AutoShipSetting.rate_by_order(order), adjustment_type: AdjustmentType.find_by_name("discount"), note: "Flexible Autoship")
      end

      auto_ship_adjustments.each do |adjustment|
        order.order_adjustments.create(
          amount: adjustment.amount,
          note: adjustment.note,
          adjustment_type_id: adjustment.adjustment_type_id
        )
      end

      # Shipping Address
      order.next
      order.update_ship_address(address)

      # Shipping Method
      order.update_shipping_method(shipping_method)

  #    resp = order.payments.new(amount: order.total).payment_profile_purchase(payment_profile)
  #    if resp.success?
      
     if Payment::NewForm.new(order, payment_profile_id: payment_profile_id).purchase
        billing_address = Address.new(payment_profile.attributes.select{|k, _| ["address1", "address2", "city", "state", "name", "postal_code", "phone", "country_id"].include? k})
        #NotificationMailer.order_checkout(order.reload, billing_address).deliver
        #  order.shipment.ready
        latest_auto_ship_report.update_attributes(total: order.total, order: order)
      else
        latest_auto_ship_report.update_attributes(total: order.total, state: AutoShipReport::FAILED, order: order)
        AutoShipMailer.failure_notice(self).deliver
      end

      update_attributes(next_order_date: next_order_date + interval)
      AutoShipReport.add_report(self)
    end

    order
  end

  def update_ship_address(address)
    self.address = address
    save(validate: false)
  end

  #  def show_price?
  #    if ProductPromotion.joins(promotion_pricing_types: :pricing_type).where("start_date <= ? and end_date >= ? and pricing_types.name = ?", Date.today, Date.today,  client.pricing_type.name).any?
  #      false
  #    elsif ShippingPromotion.where("start_date <= ? and end_date >= ?", Date.today, Date.today)
  #      false
  #    else
  #      true
  #    end
  #  end

end
