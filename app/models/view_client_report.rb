class ViewClientReport < ActiveRecord::Base
  belongs_to :client
  belongs_to :support_user
  attr_accessible :datetime, :client_id, :support_user_id

  def self.generate_or_update_report(client, support_user)
    report = ViewClientReport.where(client_id: client.id, support_user_id: support_user.id).first ||
      ViewClientReport.new(client_id: client.id, support_user_id: support_user.id)
    report.update_attributes(datetime: Time.zone.now)
  end
end
