class GuestForm
  include ::NonPersistenceModel

  attr_accessor :email, :country_id, :first_name, :last_name, :guest, :receive_update, :introduction_type_id
  validates_presence_of :first_name, :last_name
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,})\z/ }
  validates_presence_of :country_id
  validate :check_introduction_type

  def submit?
    return false unless valid?
    self.guest = Client.new(first_name: first_name, last_name: last_name, email: email,
                            country_id: country_id, guest: true, introduction_type_id: introduction_type_id,
                            support_center_id: SupportCenter.find_by_code("HNW").try(:id),
                            back_ordered_customer: true)

    if receive_update == "1"
      Email.subscribe(guest)
    end

    guest.save!(validate: false)

    true
  end

  def check_introduction_type
    if introduction_type_id.blank?
      errors.add(:base, "Please let us know how you heard about us so we can serve you better.")
    end
  end
end
