class RegistrationMethod < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :name, :deleted
  validates_presence_of :name
  has_many :clients, foreign_key: :register_method_id, dependent: :restrict

  scope :active, where(deleted: false)
end
