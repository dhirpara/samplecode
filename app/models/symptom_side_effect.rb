class SymptomSideEffect < ActiveRecord::Base
	  attr_accessible :symptom_id, :side_effect_id


  belongs_to :symptom
  belongs_to :side_effect
  # attr_accessible :title, :body
end
