class AutoShipReport < ActiveRecord::Base
  include Authority::Abilities
  belongs_to :client
  belongs_to :auto_shipping
  belongs_to :country
  belongs_to :order
  attr_accessible :client, :country, :date_of_shipment, :email, :interval, :state, :total, :order

  validates_presence_of :client, :country, :email, :date_of_shipment, :interval, :total

  PENDING = "Pending"
  FAILED = "Failed"
  SHIPPED = "Shipped"

  def self.add_report(auto_ship)
    report = auto_ship.latest_auto_ship_report(true) || auto_ship.build_latest_auto_ship_report
    if auto_ship.can_generate_order?
      report.update_attributes(
        client: auto_ship.client,
        country: auto_ship.address.try(:country),
        email: auto_ship.email,
        date_of_shipment: auto_ship.next_order_date,
        interval: auto_ship.interval,
        total: auto_ship.total
      )
    else
      report.destroy
    end
  end
end
