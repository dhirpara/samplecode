class Feedback
  include ::NonPersistenceModel
  attr_accessor :name, :email, :message
  validates_presence_of :name, :email, :message
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,})\z/ }
end
