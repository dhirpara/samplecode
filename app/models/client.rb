class Client < ActiveRecord::Base
  include Authority::Abilities
  include Authority::UserAbilities
  self.authorizer_name = 'ClientAuthorizer'


  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable, authentication_keys: [:user_name], reset_password_keys: [:user_name, :email]

  attr_accessor :receive_update, :back_end

  validates_format_of :email, allow_blank: true, with: email_regexp
  validates_uniqueness_of :user_name
  validates_presence_of :user_name, :first_name, :last_name, :country
  validates_presence_of :support_center, if: proc {|client| client.check_support_center }
  validates_presence_of :email, if: proc {|client| client.receive_update == "1" }

  validates_presence_of :address_1, :city, :province, :country_id, :postal_code, if: proc {|client| client.check_support_center }
  validates_length_of :home_phone, in: 7..32, if: proc {|client| client.check_home_phone }
  validate :check_introduction_type

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :user_name, :first_name, :middle_name, :last_name, :country_id, :country,
    :date_of_birth, :gender, :guardian, :active, :address_1, :address_2, :address_3, :city, :province, :postal_code,
    :home_phone, :work_phone, :cell_phone, :pager, :fax, :work_email, :contact_day, :contact_time_id, :time_zone, :introduction_type_id,
    :register_method_id, :general_comments, :main_participant, :call_frequency_id, :mental_wellness, :mental_wellness_details, :pricing_type_id, :support_user_id,
    :support_center_id, :language_id, :currency_id, :quit_reason_id, :age_category_id, :support_center, :contact_date, :deleted, :guest, :ta_level_id, :ta_comments, :next_contact_date,
    :receive_update, :limiting_factor_ids, :diagnosis_ids, :back_ordered_customer

  attr_accessor :check_support_center, :checkout_address, :check_home_phone

  has_one :cart, as: :shopper, dependent: :destroy
  has_many :payment_profiles#, dependent: :destroy
  has_many :addresses
  has_many :orders, dependent: :restrict
  has_many :pre_orders, through: :orders
  has_many :payments, through: :orders
  has_many :notes, dependent: :destroy
  has_many :order_notes, dependent: :destroy
  has_many :auto_shippings
  has_many :client_limiting_factors, dependent: :destroy
  has_many :limiting_factors, through: :client_limiting_factors
  accepts_nested_attributes_for :limiting_factors, allow_destroy: true

  has_many :client_diagnoses, dependent: :destroy
  has_many :diagnoses, through: :client_diagnoses
  accepts_nested_attributes_for :diagnoses, allow_destroy: true

  has_many :view_client_reports, dependent: :destroy

  belongs_to :support_center
  belongs_to :currency
  belongs_to :country
  belongs_to :pricing_type
  belongs_to :support_user
  belongs_to :contact_time
  belongs_to :introduction_type
  belongs_to :registration_method, foreign_key: :register_method_id
  belongs_to :call_frequency
  belongs_to :language
  belongs_to :quit_reason
  belongs_to :age_category
  belongs_to :ta_level

  after_create :send_welcome_email
  before_create :set_currency
  before_save :set_pricing_type
  #before_create :set_support_center
  #before_update :set_news_letter

  def submit?(options = {})
    transaction do
      options.reverse_merge!({generate_address: false})
      if save
        if options[:generate_address]
          addresses.create(name: full_name, address1: address_1, address2: address_2, city: city, state: province, country_id: country_id, postal_code: postal_code, active: true, phone: home_phone)
        end

        if receive_update == "1" && Email.where("lower(email) = ?", email.try(:downcase)).empty?
          Email.subscribe(self)
        elsif receive_update != "1"
          Email.unsubscribe(self)
        end

        true
      else
        false
      end
    end
  end

  def receive_update
    @receive_update ||=  if email.blank?
                           "0"
                         elsif Email.where("lower(email) = ?", email.downcase).any?
                           "1"
                         elsif MailChimp.exist_subscriber?(self)
                           "1"
                         else
                           "0"
                         end
  end

  def receive_update?
    receive_update == "1"
  end

  def is_guest?
    guest
  end

  #new first_last_name
  def full_name
    str = "#{first_name} #{last_name}"
    str += " - Guest" if guest?
    str
  end

  def first_last_name
    "#{first_name} #{last_name}"
  end

  #def self.find_for_database_authentication(conditions={})
  #self.where("user_name = ?", conditions[:user_name]).limit(1).first ||
  #self.where("email = ?", conditions[:user_name]).limit(1).first
  #end

  def set_currency
    self.currency = self.country.name == "Canada" ? Currency.find_by_name("CAD") : Currency.find_by_name("USD")
  end

  def currency!
    currency || Currency.find_by_id(currency_id!)
  end

  def currency_id!
    currency_id || country.currency_id
  end

  #def set_support_center
  #self.support_center_id = SupportCenter.find_by_default(true).try(:id) unless self.support_center.present?
  #end

  def commissioned_orders
    orders.where(state: 'complete', payment_state: 'paid').order('completed_at')
  end

  def commission_calculated_orders(from_date, to_date)
    if support_user.commission_by_amount?
      orders_with_commission_calculated_by_amount(from_date, to_date)
    else
      orders_with_commission_calculated_by_order(from_date, to_date)
    end
  end

  def orders_with_commission_calculated_by_amount(from_date, to_date)
    result_orders=[]
    earlier_total = 0
    commissioned_orders.each do |order|
      order.calculate_commission_by_amount(earlier_total, support_user.commission_rates_by_type(CommissionType::AMOUNT))
      earlier_total += order.total
      result_orders.unshift(order) if order.completed_between?(from_date, to_date)
    end
    result_orders
  end

  def orders_with_commission_calculated_by_order(from_date, to_date)
    result_orders=[]
    commissioned_orders.each_with_index do |order, index|
      order.calculate_commission_by_number(index+1, support_user.commission_rates_by_type(CommissionType::NUMBER))
      result_orders.unshift(order) if order.completed_between?(from_date, to_date)
    end
    result_orders
  end

  def email_changed?;  false; end
  def email_required?;  false; end

  def credit_amount
    payments.where(payment_type_id:  PaymentType.credit_type.id, received: true).sum(&:amount).abs
  end

  def has_credit?
    credit_amount > 0
  end

  def email_or_default
    email.present? ? email : "noemail@gethardy.com"
  end

  private
  def send_welcome_email
    return unless self.password
    NotificationMailer.client_signup(self).deliver
  end

  def set_pricing_type
    return unless pricing_type.nil?
    self.pricing_type = PricingType.find_by_name("Retail")
  end

  def check_introduction_type
    if introduction_type.blank?
      if back_end
        validates_presence_of :introduction_type
      else
        errors.add(:base, "Please let us know how you heard about us so we can serve you better.")
      end
    end
  end

  #def set_news_letter
  #if receive_update == "1" && changed.include?("email")
  #if Client.where(email: email_change[0]).count == 1
  ##Email.find_by_email(email_change[0]).try(:destroy)
  #MailChimp.unsubscribe(Client.new(email: email_change[0]))
  #end
  #end
  #end
end
