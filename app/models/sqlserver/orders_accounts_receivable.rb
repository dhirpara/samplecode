class Sqlserver::OrdersAccountsReceivable < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Orders_Accounts_Receivable'
end
