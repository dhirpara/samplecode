class Sqlserver::ClientIntroduction < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Client_Introductions'
end
