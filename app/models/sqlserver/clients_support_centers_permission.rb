class Sqlserver::ClientsSupportCentersPermission < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Clients_Support_Centers_Permissions'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.find_each do |obj|
      unless ::SupportCenterPermission.where(support_user_id: obj.CSCP_SSID, support_center_id: obj.CSCP_CSCID).any?
        s = ::SupportCenterPermission.new
        s.id = obj.CSCP_ID
        s.support_user_id = obj.CSCP_SSID
        s.support_center_id = obj.CSCP_CSCID
        s.save
      end
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE support_center_permissions_id_seq START with #{::SupportCenterPermission.order("id").last.id + 1} RESTART;")
  end
end
