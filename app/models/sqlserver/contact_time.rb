class Sqlserver::ContactTime < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Contact_Time'

  def self.data_to_migrate
    self.find_each do |obj|
      ct = ::ContactTime.new
      ct.id = obj.CT_ID
      ct.description = obj.CT_Description
      ct.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE contact_times_id_seq START with #{::ContactTime.order("id").last.id + 1} RESTART;")
  end
end
