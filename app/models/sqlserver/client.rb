class Sqlserver::Client < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Clients'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.get_pricing_type_id(id)
    #type = Sqlserver::ClientsType.find(id).CTY_Type
    type = {1=>"Normal", 2=>"Staff", 3=>"Wholesale", 4=>"Shareholders", 5=>"CNE Health Professionals", 6=>"Norway", 7=>"Health Professional Referrals", 8=>"Hutterite", 9=>"CCA", 10=>"Amish", 11=>"Spanish"}[id]
    if type == "Normal" || type == "Health Professional Referrals"
      #"Retail"
      1
    elsif type == "Staff"
      #"Staff"
      2
    elsif type == "Shareholders"
      #"Shareholders"
      4
    elsif type == "CNE Health Professionals" || type == "Wholesale"
      #"Health Professionals"
      3
    elsif type == "Norway" || type == "CCA"
      #"Wholesale 1"
      5
    elsif type == "Hutterite"
      #"Wholesale 2"
      6
    elsif type == "Amish" || type == "Spanish"
      #"Wholesale 3"
      7
    end
    #::PricingType.find_by_name(name).id
  end

  def self.valid_clients
    self.where("C_FirstName != ?", "TH").order("C_ID")
  end

  def self.valid_clients_ids
    self.valid_clients.pluck("C_ID")
  end

  def self.data_to_migrate(clients_array, array_index)
    if ::RegistrationMethod.count == 0
      self.select("DISTINCT C_RegisteredBy").each do |obj|
        rm = ::RegistrationMethod.new
        rm.name = obj.C_RegisteredBy
        rm.save
      end
    end

    if ::CallFrequency.count == 0
      self.select("DISTINCT C_CallFrequency").each do |obj|
        cf = ::CallFrequency.new
        cf.name = obj.C_CallFrequency
        cf.save
      end
    end

    index = 0
    clients_array.each_with_index do |obj, i|
      puts "#{array_index}-#{i}"
      c = ::Client.new
      c.id = obj.C_ID
      count = ::Client.where(user_name: obj.C_UserName).count
      if obj.C_UserName.blank?
        c.user_name = "username#{index}"
      else
        c.user_name =  if count > 0
                         "#{array_index}_#{i}_" + obj.C_UserName
                       else
                         obj.C_UserName
                       end
      end
      c.first_name =  obj.C_FirstName
      c.last_name = obj.C_LastName
      c.middle_name = obj.C_MiddleName
      c.date_of_birth = obj.C_DOB
      c.gender = obj.C_Gender
      c.guardian = obj.C_Guardian
      c.active = transform_to_boolean obj.C_Active
      c.address_1 = obj.C_Address
      c.address_2 = obj.C_AptNum
      c.address_3 = obj.C_Address3
      c.city = obj.C_City
      c.province = obj.C_State
      c.country_id = Sqlserver::Country.get_new_id_by_old_id(obj.C_COID)
      c.postal_code = obj.C_PostalCode
      c.home_phone = obj.C_HPhone
      c.work_phone = obj.C_WPhone
      c.cell_phone = obj.C_Cell
      c.pager = obj.C_Pager
      c.fax = obj.C_Fax

      count = ::Client.where(email: obj.C_Email).count
      c.email = if obj.C_Email.blank?
                  "hardy#{index}@example.com"
                elsif obj.C_Email.match(/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/)
                  if count > 0
                    "#{array_index}_#{i}_" + obj.C_Email
                  else
                    obj.C_Email
                  end
                else
                  "hardy#{index}@example.com"
                end
      c.work_email = obj.C_WEmail
      c.contact_day = obj.C_ContactDay
      c.contact_time_id = ::ContactTime.find_by_description(obj.C_ContactTime).try(:id)
      c.time_zone = obj.C_TimeZone
      c.contact_date = obj.C_ContactDate
      c.next_contact_date = obj.C_NextContactdate
      c.start_date = obj.C_StartDate
      c.start_supplement = obj.C_StartSupplement
      c.date_active = obj.C_DateActive
      c.introduction_type_id = ::IntroductionType.find_by_name(obj.C_Introduction).try(:id)
      c.register_method_id = ::RegistrationMethod.find_by_name(obj.C_RegisteredBy).try(:id)
      c.quit_date = obj.C_QuitDate
      c.quit_reason_id = obj.C_QRID
      c.general_comments = obj.C_GeneralComments
      c.admin_order = transform_to_boolean obj.C_AdminOrder
      c.admin_support = transform_to_boolean obj.C_AdminSupport
      c.allow_order = transform_to_boolean obj.C_AllowOrder
      c.require_data = transform_to_boolean obj.C_ReferalDate
      c.data_start_date = obj.C_DataStartDate
      c.data_stop_date = obj.C_DataStopDate
      c.discount = obj.C_Discount
      c.main_participant = transform_to_boolean obj.C_MainParticipant
      c.ta_comments = obj.C_TAComments
      c.call_frequency_id = ::CallFrequency.find_by_name(obj.C_CallFrequency).try(:id)
      c.self_registered = transform_to_boolean obj.C_SelfRegistered
      c.cne_professional = transform_to_boolean obj.C_CNEProfessional
      c.mental_wellness = transform_to_boolean obj.C_MentalWellness
      c.mental_wellness_details = obj.C_MentalWellnessDetails
      c.single_order_client = transform_to_boolean obj.C_SingleOrderClient

      c.pricing_type_id = get_pricing_type_id(obj.C_CTYID) #C_CTYID  client_types(Clients_Types)
      c.contact_time_id = obj.C_CTID
      c.deleted = transform_to_boolean obj.C_Deleted

      c.client_family_id = obj.C_FID #C_FID  client_families(Families)
      c.support_center_id = obj.C_CSCID #C_CSCID support_centers ( Clients_Support_Centers)
      c.ta_level_id = obj.C_CTAID # C_CTAID ta_levels (client_talevel)
      c.support_group_id = obj.C_CSGID #C_CSGID support_groups (Clients_Support_Groups)
      c.language_id = obj.C_LANID #C_LANID languages (Languages)
      c.support_user_id = obj.C_SSIDCreated
      c.quit_reason_id = obj.C_QRID
      c.age_category_id = obj.C_CACID
      c.save(validate: false)
      index += 1

      #Sqlserver::Note.where(N_CID: c.id).each do |obj|
      #n = ::Note.new
      #n.client_id = obj.N_CID
      #n.note = obj.N_Note
      #n.support_user_id = obj.N_SSIDWritten
      #n.deleted = transform_to_boolean(obj.N_Deleted)
      #n.save
      #end
    end

    ActiveRecord::Base.connection.execute("ALTER SEQUENCE clients_id_seq START with #{::Client.order(:id).last.id + 1} RESTART;")
    puts "finish-#{array_index}"
    #ActiveRecord::Base.connection.execute("ALTER SEQUENCE notes_id_seq START with #{::Note.order(:id).last.id + 1} RESTART;")
  end
end
