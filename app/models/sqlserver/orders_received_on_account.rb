class Sqlserver::OrdersReceivedOnAccount < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Orders_Received_On_Account'
end
