class Sqlserver::Note < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Notes'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate(array, i)
    #begin
    self.where(N_CID: array).each_with_index do |obj, index|
      puts "#{i}-#{index}"
      n = ::Note.new
      n.client_id = obj.N_CID
      n.note = obj.N_Note
      n.support_user_id = obj.N_SSIDWritten
      n.deleted = transform_to_boolean(obj.N_Deleted)
      n.created_at = obj.N_Date
      n.updated_at = obj.N_ModifiedDate || obj.N_Date
      n.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE notes_id_seq START with #{::Note.order("id").last.id + 1} RESTART;")
    puts "finish-#{i}"
    #rescue TinyTds::Error
    #binding.pry
    #end
  end

end
