class Sqlserver::ClientsSupportCenter < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Clients_Support_Centers'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.find_each do |obj|
      s = ::SupportCenter.new
      s.id = obj.CSC_ID
      s.code = obj.CSC_Code
      s.name = obj.CSC_Name.blank? ? "(no name)" : obj.CSC_Name
      s.address = obj.CSC_Address
      s.city = obj.CSC_City
      s.province = obj.CSC_State
      s.country_id = Sqlserver::Country.get_new_id_by_old_id(obj.CSC_COID)
      s.postal_code = obj.CSC_PostalCode
      s.phone = obj.CSC_Phone
      s.fax = obj.CSC_Fax
      s.email = obj.CSC_Email
      s.support_email = obj.CSC_SupportEmail
      s.support_phone = obj.CSC_SupportPhone
      s.deleted = transform_to_boolean obj.CSC_Deleted
      s.default = obj.CSC_Name == "Nutratek Support Center" ? true : false
      s.support_center_type_id = obj.CSC_CSCTID
      s.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE support_centers_id_seq START with #{::SupportCenter.order("id").last.id + 1} RESTART;")
  end

end
