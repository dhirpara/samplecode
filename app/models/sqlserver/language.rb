class Sqlserver::Language < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Languages'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.find_each do |obj|
      l = ::Language.new
      l.id = obj.LAN_ID
      l.name = obj.LAN_Name
      l.order = obj.LAN_Order
      l.active = !transform_to_boolean(obj.LAN_Deleted)
      l.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE languages_id_seq START with #{::Language.order("id").last.id + 1} RESTART;")
  end
end
