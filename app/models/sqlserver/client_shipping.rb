class Sqlserver::ClientShipping < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Client_Shipping'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    ids_array = Sqlserver::Client.valid_clients_ids.in_groups(10, false)

    ids_array.each do |ids|
      self.where(CS_CID: ids).each do |obj|
        puts obj.id
        client = ::Client.find_by_id(obj.CS_CID)
        if client
          a = ::Address.new
          a.client_id = obj.CS_CID
          a.phone = if client.home_phone.blank? || client.home_phone.size < 7
                      "1111111"
                    else
                      client.home_phone
                    end
          a.address1 = obj.CS_Address
          a.address2 = obj.CS_Address2
          a.city = obj.CS_City
          a.state = obj.CS_State
          a.name = obj.CS_CompanyName
          a.postal_code = obj.CS_PostalCode
          a.country_id = Sqlserver::Country.get_new_id_by_old_id(obj.CS_COID)
          a.active = !transform_to_boolean(obj.CS_Deleted)
          a.save
        end
      end
    end
    puts "finish"
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE addresses_id_seq START with #{::Address.order("id").last.id + 1} RESTART;")
  end
end
