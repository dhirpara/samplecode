class Sqlserver::ClientsTaLevel < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Clients_TALevel'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.find_each do |obj|
      tl = ::TaLevel.new
      tl.id = obj.CTA_ID
      tl.name = obj.CTA_Name
      tl.deleted = transform_to_boolean obj.CTA_Deleted
      tl.save
    end

    ActiveRecord::Base.connection.execute("ALTER SEQUENCE ta_levels_id_seq START with #{::TaLevel.order(:id).last.id + 1} RESTART;")
  end
end
