class Sqlserver::SupportStaff < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Support_Staff'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    index = 0
    self.find_each do |obj|
      su = ::SupportUser.new
      su.first_name = obj.SS_FirstName
      su.last_name = obj.SS_LastName
      count = ::SupportUser.where(user_name: obj.SS_UserName).count
      if obj.SS_UserName.blank?
        su.user_name = "username#{index}"
      else
        su.user_name =  if count > 0
                         "#{index}_" + obj.SS_UserName
                        else
                          obj.SS_UserName
                        end
      end
      su.support_desk = obj.SS_SupportDesk
      #su.admin = transform_to_boolean obj.SS_Admin
      #su.super_admin =transform_to_boolean obj.SS_SuperAdmin
      su.data_enter = transform_to_boolean obj.SS_DataEnter
      su.admin_orders = transform_to_boolean obj.SS_AdminOrders
      su.order_desk = transform_to_boolean obj.SS_OrderDesk
      su.time_zone = obj.SS_TimeZone
      su.client_id = obj.SS_CID
      su.support_center_id = obj.SS_CSCID
      su.email = "abc#{index}@nutratek.com"
      su.active = !transform_to_boolean(obj.SS_Deleted)
      su.save(validate: false)
      su.roles << Role.find_by_code("BAS")
      index += 1
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE support_users_id_seq START with #{::SupportUser.order("id").last.id + 1} RESTART;")
  end
end
