class Sqlserver::Diagnosis < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Diagnosis'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end
  def self.data_to_migrate
    self.find_each do |obj|
      d = ::Diagnosis.new
      d.id = obj.DIA_ID
      d.cid = obj.DIA_CID
      d.disid= obj.DIA_DISID
      d.notes= obj.DIA_Notes
      d.date= obj.DIA_Date
      d.tracked= obj.DIA_Tracked
      d.doctor_diagnosed= obj.DIA_DoctorDiagnosed
      d.ssid= obj.DIA_SSID
      d.deleted= transform_to_boolean obj.DIA_Deleted
      d.ssid_deleted= obj.DIA_SSIDDeleted
      d.deleted_date= obj.DIA_DeletedDate
      d.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE diagnoses_id_seq START with #{::Diagnosis.last.id + 1} RESTART;")
  end

end
