class Sqlserver::Drug < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Drugs'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.find_each do |obj|
      puts obj.DRU_ID
      d = ::Drug.new
      d.id = obj.DRU_ID
      d.cid = obj.DRU_CID
      d.dlid = obj.DRU_DLID
      d.label = obj.DRU_Label
      d.amount = obj.DRU_Amount
      d.unit = obj.DRU_Unit
      d.comments = obj.DRU_Comments
      d.previous = obj.DRU_Previous
      d.date_stopped = obj.DRU_DateStopped
      d.ssid = obj.DRU_SSID
      d.deleted = transform_to_boolean obj.DRU_Deleted
      d.ssid_deleted= obj.DRU_SSIDDeleted
      d.delete_date = obj.DRU_DeleteDate
      d.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE drugs_id_seq START with #{::Drug.last.id + 1} RESTART;")
  end
end
