class Sqlserver::QuitReason < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Quit_Reasons'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.all.each do |obj|
      q = ::QuitReason.new
      q.id = obj.QR_ID
      q.name = obj.QR_Reason
      q.deleted = obj.QR_Deleted
      q.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE quit_reasons_id_seq START with #{::QuitReason.order("id").last.id + 1} RESTART;")
  end
end
