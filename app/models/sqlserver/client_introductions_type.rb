class Sqlserver::ClientIntroductionsType < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Client_Introductions_Types'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.find_each do |obj|
      i = ::IntroductionType.new
      i.id = obj.CIT_ID
      i.name = obj.CIT_Type
      i.index = obj.CIT_Order
      i.deleted = transform_to_boolean(obj.CIT_Deleted)
      i.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE introduction_types_id_seq START with #{::IntroductionType.order("id").last.id + 1} RESTART;")
  end
end
