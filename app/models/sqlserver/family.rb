class Sqlserver::Family < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Families'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.find_each do |obj|
      cf = ::Family.new
      cf.id = obj.F_ID
      cf.name = obj.F_Name
      cf.cid_head = obj.F_CIDHead
      cf.date_created = obj.F_DateCreated
      cf.ssid_created = obj.F_SSIDCreated
      cf.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE families_id_seq START with #{::Family.last.id + 1} RESTART;")
  end

end
