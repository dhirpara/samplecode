class Sqlserver::DailySymptomTotal < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Daily_SymptomTotals'

  def self.data_to_migrate
    self.find_each do |obj|
      d = ::DailySymptomTotal.new
      d.id = obj.DST_ID
      d.cid = obj.DST_CID
      d.diaid = obj.DST_DIAID
      d.total = obj.DST_Total
      d.date = obj.DST_Date
      d.date_entered = obj.DST_DateEntered
      d.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE daily_symptom_totals_id_seq START with #{::DailySymptomTotal.last.id + 1} RESTART;")
  end
end
