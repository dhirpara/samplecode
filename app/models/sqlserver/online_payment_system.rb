class Sqlserver::OnlinePaymentSystem < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Online_Payment_Systems'
end
