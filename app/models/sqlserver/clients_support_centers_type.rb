class Sqlserver::ClientsSupportCentersType < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Clients_Support_Centers_Types'

  def self.data_to_migrate
    self.find_each do |obj|
      s = ::SupportCenterType.new
      s.id = obj.CSCT_ID
      s.name = obj.CSCT_TypeName
      s.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE support_center_types_id_seq START with #{::SupportCenterType.order("id").last.id + 1} RESTART;")
  end
end
