class Sqlserver::AutoShipping < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Auto_Shipping'


  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.find_each do |obj|
      as = ::AutoShipping.new
      as.id = obj.AS_ID
      as.client_id = obj.AS_CID
      as.date_started = obj.AS_DateStarted
      as.interval = obj.AS_Interval
      as.next_order_date = obj.AS_NextOrderDate
      as.active = transform_to_boolean obj.AS_Active
      as.support_member_id = obj.AS_SMID
      as.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE auto_shippings_id_seq START with #{::AutoShipping.last.id + 1} RESTART;")
  end
end
