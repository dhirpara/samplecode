class Sqlserver::ClientsSupportGroup < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Clients_Support_Groups'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    find_each do |obj|
      csg = ::SupportGroup.new
      csg.id = obj.CSG_ID
      csg.name = obj.CSG_NewName
      csg.order = obj.CSG_Order
      csg.active = !transform_to_boolean(obj.CSG_Deleted)
      csg.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE support_groups_id_seq START with #{::SupportGroup.last.id + 1} RESTART;")
  end
end
