class Sqlserver::NewsletterSubscribeError < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Newsletter_Subscribe_Errors'
end
