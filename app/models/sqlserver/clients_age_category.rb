class Sqlserver::ClientsAgeCategory < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Clients_Age_Categories'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    self.find_each do |obj|
      puts obj.id
      a = ::AgeCategory.new
      a.id = obj.CAC_ID
      a.name = obj.CAC_GategoryName
      a.deleted = obj.CAC_Deleted
      a.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE age_categories_id_seq START with #{::AgeCategory.order("id").last.id + 1} RESTART;")
  end
end
