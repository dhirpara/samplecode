class Sqlserver::Doctor < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Doctors'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    ids_array = Sqlserver::Client.valid_clients_ids.in_groups(10, false)
    ids_array.each do |ids|
    self.where(DR_CID: ids).each do |obj|
      cd = ::ClientDoctor.new
      cd.id = obj.DR_ID
      cd.client_id = obj.DR_CID
      cd.name = obj.DR_Name
      cd.doctor_type = obj.DR_Type
      cd.work_phone = obj.DR_WPhone
      cd.home_phone = obj.DR_HPhone
      cd.fax = obj.DR_Fax
      cd.email = obj.DR_Email
      cd.address = obj.DR_Address
      cd.city = obj.DR_City
      cd.province = obj.DR_State
      cd.postal_code = obj.DR_PostalCode
      cd.support_status = obj.DR_SupportStatus
      cd.active = !transform_to_boolean(obj.DR_Deleted)
      cd.save
    end
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE client_doctors_id_seq START with #{::ClientDoctor.order("id").last.id + 1} RESTART;")
  end
end
