class Sqlserver::ShoppingCartProduct < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Shopping_Cart_Products'
end
