class Sqlserver::Symptom < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Symptoms'

  def self.data_to_migrate
    self.find_each do |obj|
      cf = ::Symptom.new
      cf.id = obj.SYM_ID
      cf.disid = obj.SYM_DISID
      cf.question = obj.SYM_Question
      cf.save
    end
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE symptoms_id_seq START with #{::Symptom.last.id + 1} RESTART;")
  end

end
