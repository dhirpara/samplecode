# coding: utf-8

class Sqlserver::Country < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Countrys'

  COUNTRY_HASH = {
    "CA"=>  "CANADA",
    "US"=>  "UNITED STATES",
    "AF"=> "AFGHANISTAN",
    "AX"=> "ÅLAND ISLANDS",
    "AL"=> "ALBANIA",
    "DZ"=> "ALGERIA",
    "AS"=> "AMERICAN SAMOA",
    "AD"=> "ANDORRA",
    "AO"=> "ANGOLA",
    "AI"=> "ANGUILLA",
    "AQ"=> "ANTARCTICA",
    "AG"=> "ANTIGUA AND BARBUDA",
    "AR"=> "ARGENTINA",
    "AM"=> "ARMENIA",
    "AW"=> "ARUBA",
    "AU"=> "AUSTRALIA",
    "AT"=> "AUSTRIA",
    "AZ"=> "AZERBAIJAN",
    "BS"=> "BAHAMAS",
    "BH"=> "BAHRAIN",
    "BD"=> "BANGLADESH",
    "BB"=> "BARBADOS",
    "BY"=> "BELARUS",
    "BE"=> "BELGIUM",
    "BZ"=> "BELIZE",
    "BJ"=> "BENIN",
    "BM"=> "BERMUDA",
    "BT"=> "BHUTAN",
    "BO"=> "BOLIVIA, PLURINATIONAL STATE OF",
    "BQ"=> "BONAIRE, SINT EUSTATIUS AND SABA",
    "BA"=> "BOSNIA AND HERZEGOVINA",
    "BW"=> "BOTSWANA",
    "BV"=> "BOUVET ISLAND",
    "BR"=> "BRAZIL",
    "IO"=> "BRITISH INDIAN OCEAN TERRITORY",
    "BN"=> "BRUNEI DARUSSALAM",
    "BG"=> "BULGARIA",
    "BF"=> "BURKINA FASO",
    "BI"=> "BURUNDI",
    "KH"=>  "CAMBODIA",
    "CM"=>  "CAMEROON",
    "CV"=>  "CAPE VERDE",
    "KY"=>  "CAYMAN ISLANDS",
    "CF"=>  "CENTRAL AFRICAN REPUBLIC",
    "TD"=>  "CHAD",
    "CL"=>  "CHILE",
    "CN"=>  "CHINA",
    "CX"=>  "CHRISTMAS ISLAND",
    "CC"=>  "COCOS (KEELING) ISLANDS",
    "CO"=>  "COLOMBIA",
    "KM"=>  "COMOROS",
    "CG"=>  "CONGO",
    "CD"=>  "CONGO, THE DEMOCRATIC REPUBLIC OF THE",
    "CK"=>  "COOK ISLANDS",
    "CR"=>  "COSTA RICA",
    "CI"=>  "CÔTE D'IVOIRE",
    "HR"=>  "CROATIA",
    "CU"=>  "CUBA",
    "CW"=>  "CURAÇAO",
    "CY"=>  "CYPRUS",
    "CZ"=>  "CZECH REPUBLIC",
    "DK"=>  "DENMARK",
    "DJ"=>  "DJIBOUTI",
    "DM"=>  "DOMINICA",
    "DO"=>  "DOMINICAN REPUBLIC",
    "EC"=>  "ECUADOR",
    "EG"=>  "EGYPT",
    "SV"=>  "EL SALVADOR",
    "GQ"=>  "EQUATORIAL GUINEA",
    "ER"=>  "ERITREA",
    "EE"=>  "ESTONIA",
    "ET"=>  "ETHIOPIA",
    "FK"=>  "FALKLAND ISLANDS (MALVINAS)",
    "FO"=>  "FAROE ISLANDS",
    "FJ"=>  "FIJI",
    "FI"=>  "FINLAND",
    "FR"=>  "FRANCE",
    "GF"=>  "FRENCH GUIANA",
    "PF"=>  "FRENCH POLYNESIA",
    "TF"=>  "FRENCH SOUTHERN TERRITORIES",
    "GA"=>  "GABON",
    "GM"=>  "GAMBIA",
    "GE"=>  "GEORGIA",
    "DE"=>  "GERMANY",
    "GH"=>  "GHANA",
    "GI"=>  "GIBRALTAR",
    "GR"=>  "GREECE",
    "GL"=>  "GREENLAND",
    "GD"=>  "GRENADA",
    "GP"=>  "GUADELOUPE",
    "GU"=>  "GUAM",
    "GT"=>  "GUATEMALA",
    "GG"=>  "GUERNSEY",
    "GN"=>  "GUINEA",
    "GW"=>  "GUINEA-BISSAU",
    "GY"=>  "GUYANA",
    "HT"=>  "HAITI",
    "HM"=>  "HEARD ISLAND AND MCDONALD ISLANDS",
    "VA"=>  "HOLY SEE (VATICAN CITY STATE)",
    "HN"=>  "HONDURAS",
    "HK"=>  "HONG KONG",
    "HU"=>  "HUNGARY",
    "IS"=>  "ICELAND",
    "IN"=>  "INDIA",
    "ID"=>  "INDONESIA",
    "IR"=>  "IRAN, ISLAMIC REPUBLIC OF",
    "IQ"=>  "IRAQ",
    "IE"=>  "IRELAND",
    "IM"=>  "ISLE OF MAN",
    "IL"=>  "ISRAEL",
    "IT"=>  "ITALY",
    "JM"=>  "JAMAICA",
    "JP"=>  "JAPAN",
    "JE"=>  "JERSEY",
    "JO"=>  "JORDAN",
    "KZ"=>  "KAZAKHSTAN",
    "KE"=>  "KENYA",
    "KI"=>  "KIRIBATI",
    "KP"=>  "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF",
    "KR"=>  "KOREA, REPUBLIC OF",
    "KW"=>  "KUWAIT",
    "KG"=>  "KYRGYZSTAN",
    "LA"=>  "LAO PEOPLE'S DEMOCRATIC REPUBLIC",
    "LV"=>  "LATVIA",
    "LB"=>  "LEBANON",
    "LS"=>  "LESOTHO",
    "LR"=>  "LIBERIA",
    "LY"=>  "LIBYA",
    "LI"=>  "LIECHTENSTEIN",
    "LT"=>  "LITHUANIA",
    "LU"=>  "LUXEMBOURG",
    "MO"=>  "MACAO",
    "MK"=>  "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF",
    "MG"=>  "MADAGASCAR",
    "MW"=>  "MALAWI",
    "MY"=>  "MALAYSIA",
    "MV"=>  "MALDIVES",
    "ML"=>  "MALI",
    "MT"=>  "MALTA",
    "MH"=>  "MARSHALL ISLANDS",
    "MQ"=>  "MARTINIQUE",
    "MR"=>  "MAURITANIA",
    "MU"=>  "MAURITIUS",
    "YT"=>  "MAYOTTE",
    "MX"=>  "MEXICO",
    "FM"=>  "MICRONESIA, FEDERATED STATES OF",
    "MD"=>  "MOLDOVA, REPUBLIC OF",
    "MC"=>  "MONACO",
    "MN"=>  "MONGOLIA",
    "ME"=>  "MONTENEGRO",
    "MS"=>  "MONTSERRAT",
    "MA"=>  "MOROCCO",
    "MZ"=>  "MOZAMBIQUE",
    "MM"=>  "MYANMAR",
    "NA"=>  "NAMIBIA",
    "NR"=>  "NAURU",
    "NP"=>  "NEPAL",
    "NL"=>  "NETHERLANDS",
    "NC"=>  "NEW CALEDONIA",
    "NZ"=>  "NEW ZEALAND",
    "NI"=>  "NICARAGUA",
    "NE"=>  "NIGER",
    "NG"=>  "NIGERIA",
    "NU"=>  "NIUE",
    "NF"=>  "NORFOLK ISLAND",
    "MP"=>  "NORTHERN MARIANA ISLANDS",
    "NO"=>  "NORWAY",
    "OM"=>  "OMAN",
    "PK"=>  "PAKISTAN",
    "PW"=>  "PALAU",
    "PS"=>  "PALESTINE, STATE OF",
    "PA"=>  "PANAMA",
    "PG"=>  "PAPUA NEW GUINEA",
    "PY"=>  "PARAGUAY",
    "PE"=>  "PERU",
    "PH"=>  "PHILIPPINES",
    "PN"=>  "PITCAIRN",
    "PL"=>  "POLAND",
    "PT"=>  "PORTUGAL",
    "PR"=>  "PUERTO RICO",
    "QA"=>  "QATAR",
    "RE"=>  "RÉUNION",
    "RO"=>  "ROMANIA",
    "RU"=>  "RUSSIAN FEDERATION",
    "RW"=>  "RWANDA",
    "BL"=>  "SAINT BARTHÉLEMY",
    "SH"=>  "SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA",
    "KN"=>  "SAINT KITTS AND NEVIS",
    "LC"=>  "SAINT LUCIA",
    "MF"=>  "SAINT MARTIN (FRENCH PART)",
    "PM"=>  "SAINT PIERRE AND MIQUELON",
    "VC"=>  "SAINT VINCENT AND THE GRENADINES",
    "WS"=>  "SAMOA",
    "SM"=>  "SAN MARINO",
    "ST"=>  "SAO TOME AND PRINCIPE",
    "SA"=>  "SAUDI ARABIA",
    "SN"=>  "SENEGAL",
    "RS"=>  "SERBIA",
    "SC"=>  "SEYCHELLES",
    "SL"=>  "SIERRA LEONE",
    "SG"=>  "SINGAPORE",
    "SX"=>  "SINT MAARTEN (DUTCH PART)",
    "SK"=>  "SLOVAKIA",
    "SI"=>  "SLOVENIA",
    "SB"=>  "SOLOMON ISLANDS",
    "SO"=>  "SOMALIA",
    "ZA"=>  "SOUTH AFRICA",
    "GS"=>  "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS",
    "SS"=>  "SOUTH SUDAN",
    "ES"=>  "SPAIN",
    "LK"=>  "SRI LANKA",
    "SD"=>  "SUDAN",
    "SR"=>  "SURINAME",
    "SJ"=>  "SVALBARD AND JAN MAYEN",
    "SZ"=>  "SWAZILAND",
    "SE"=>  "SWEDEN",
    "CH"=>  "SWITZERLAND",
    "SY"=>  "SYRIAN ARAB REPUBLIC",
    "TW"=>  "TAIWAN, PROVINCE OF CHINA",
    "TJ"=>  "TAJIKISTAN",
      "TZ"=>  "TANZANIA, UNITED REPUBLIC OF",
"TH"=>  "THAILAND",
"TL"=>  "TIMOR-LESTE",
"TG"=>  "TOGO",
"TK"=>  "TOKELAU",
"TO"=>  "TONGA",
"TT"=>  "TRINIDAD AND TOBAGO",
"TN"=>  "TUNISIA",
"TR"=>  "TURKEY",
"TM"=>  "TURKMENISTAN",
"TC"=>  "TURKS AND CAICOS ISLANDS",
"TV"=>  "TUVALU",
"UG"=>  "UGANDA",
"UA"=>  "UKRAINE",
"AE"=>  "UNITED ARAB EMIRATES",
"GB"=>  "UNITED KINGDOM",
"UM"=>  "UNITED STATES MINOR OUTLYING ISLANDS",
"UY"=>  "URUGUAY",
"UZ"=>  "UZBEKISTAN",
"VU"=>  "VANUATU",
"VE"=>  "VENEZUELA, BOLIVARIAN REPUBLIC OF",
"VN"=>  "VIET NAM",
"VG"=>  "VIRGIN ISLANDS, BRITISH",
"VI"=>  "VIRGIN ISLANDS, U.S.",
"WF"=>  "WALLIS AND FUTUNA",
"EH"=>  "WESTERN SAHARA",
"YE"=>  "YEMEN", #敏聪自己加的
"YU"=> "YUGOSLAVIA",
"ZM"=>  "ZAMBIA",
"ZW"=>  "ZIMBABWE"
  }

  def self.set_by_hand(name)
    case name
    when "St. Kitts and Nevis"
      "KN"
    when "Scotland (United Kingdom)"
      "GB"
    when "Wales (United Kingdom)"
      "GB"
    when "USA"
      "US"
    when "England"
      "GB"
    when "Saipan (Northern Mariana Islands)"
      "MP"
    when "U.S. Virgin Islands"
      "VI"
    when "St. John (U.S. Virgin Islands)"
      "VI"
    when "Netherlands Antilles"
      "NL"
    when "Holland (Netherlands)"
      "NL"
    when "Saba (Netherlands Antilles)"
      "NL"
    when "Slovak Republic (Slovakia)"
      "SK"
    when "St. Croix (U.S. Virgin Islands)"
      "VI"
    when "Macedonia"
      "MK"
    when "Iran"
      "IR"
    when "Curacao (Netherlands Antillies)"
      "CW"
    when "Netherlands (Holland)"
      "NL"
    when "Taiwan"
      "TW"
    when "Korea (South Korea)"
      "KR"
    when "Bolivia"
      "BO"
    when "Venezuela"
      "VE"
    #以下的是网上搜索的
    when "Channel Islands"
      "GB"
    when "Russia"
      "RU"
    when "Vietnam"
      "VN"
    when "Macau"
      "MO"
      #不确定
    when "Yugoslavia"
      "YU"
    when "Bosnia-Herzegovina"
      "BA"
      #不确定
    when "Great Britain & Northern Ireland"
      "GB"
      #不确定
    when "Canary Islands"
      "CI"
    end
  end

  OLD_ABBR = {1=>"USA", 2=>"Canada", 3=>"Afghanistan", 4=>"Albania", 5=>"Algeria", 6=>"American Samoa", 7=>"Andorra", 8=>"Angola", 9=>"Anguilla", 10=>"Antigua and Barbuda", 11=>"Argentina", 12=>"Armenia", 13=>"Aruba", 14=>"Australia", 15=>"Austria", 16=>"Azerbaijan", 17=>"Azores (Portugal)", 18=>"Bahamas", 19=>"Bahrain", 20=>"Bangladesh", 21=>"Barbados", 22=>"Belarus", 23=>"Belgium", 24=>"Belize", 25=>"Benin", 26=>"Bermuda", 27=>"Bhutan", 28=>"Bolivia", 29=>"Bonaire (Netherlands Antillies)", 30=>"Bosnia-Herzegovina",
   31=>"Botswana", 32=>"Brazil", 33=>"British Virgin Islands", 34=>"Brunei Darussalam", 35=>"Bulgaria", 36=>"Burkina Faso", 37=>"Burma (Myanmar)", 38=>"Burundi ", 39=>"Cambodia", 40=>"Cameroon", 41=>"Canary Islands", 42=>"Cape Verde", 43=>"Cayman Islands", 44=>"Central African Republic", 45=>"Chad ", 46=>"Channel Islands", 47=>"Chile", 48=>"China", 49=>"Colombia", 50=>"Comoros", 51=>"Congo - Democratic Republic of",
   52=>"Congo - Republic of the", 53=>"Cook Islands", 54=>"Costa Rica", 55=>"Cote D'Ivoire (Ivory Coast)", 56=>"Croatia", 57=>"Curacao (Netherlands Antillies)", 58=>"Cyprus", 59=>"Czech Republic", 60=>"Denmark", 61=>"Djibouti", 62=>"Dominica", 63=>"Dominican Republic", 64=>"Ecuador", 65=>"Egypt", 66=>"El Salvador", 67=>"England", 68=>"Equatorial Guinea", 69=>"Eritrea", 70=>"Estonia", 71=>"Ethiopia", 72=>"Faroe Islands (Denmark)",
   73=>"Fiji", 74=>"Finland", 75=>"France", 76=>"French Guiana", 77=>"French Polynesia", 78=>"Gabon", 79=>"Gambia", 80=>"Georgia - Republic of", 81=>"Germany", 82=>"Ghana", 83=>"Gibraltar", 84=>"Great Britain & Northern Ireland", 85=>"Greece", 86=>"Greenland (Denmark)", 87=>"Grenada", 88=>"Guadeloupe", 89=>"Guam", 90=>"Guatemala", 91=>"Guinea", 92=>"Guinea-Bissau", 93=>"Guyana", 94=>"Haiti", 95=>"Holland (Netherlands)", 96=>"Honduras", 97=>"Hong Kong", 98=>"Hungary",
   99=>"Iceland", 100=>"India", 101=>"Indonesia", 102=>"Iran", 103=>"Iraq", 104=>"Ireland", 105=>"Israel", 106=>"Italy", 107=>"Ivory Coast (Cote d'Ivoire)", 108=>"Jamaica", 109=>"Japan", 110=>"Jordan", 111=>"Kazakhstan", 112=>"Kenya", 113=>"Kiribati", 114=>"Korea (South Korea)", 115=>"Kosrae (Federated States of Micronesia)", 116=>"Kuwait", 117=>"Kyrgyzstan", 118=>"Laos", 119=>"Latvia", 120=>"Lebanon", 121=>"Lesotho", 122=>"Liberia", 123=>"Libya", 124=>"Liechtenstein", 125=>"Lithuania", 126=>"Luxembourg", 127=>"Macau", 128=>"Macedonia", 129=>"Madagascar", 130=>"Maderia (Portugal)", 131=>"Malawi", 132=>"Malaysia", 133=>"Maldives", 134=>"Mali", 135=>"Malta",
   136=>"Marshall Islands", 137=>"Martinique", 138=>"Mauritania", 139=>"Mauritius", 140=>"Mexico", 141=>"Micronesia - Federated States of", 142=>"Moldova", 143=>"Monaco", 144=>"Mongolia", 145=>"Montserrat", 146=>"Morocco", 147=>"Mozambique", 148=>"Namibia", 149=>"Nauru", 150=>"Nepal", 151=>"Netherlands (Holland)", 152=>"Netherlands Antilles", 153=>"New Caledonia", 154=>"New Zealand", 155=>"Nicaragua", 156=>"Niger", 157=>"Nigeria", 158=>"Norfolk Island", 159=>"Northern Ireland (UK)", 160=>"Northern Mariana Islands", 161=>"Norway", 162=>"Oman", 163=>"Pakistan", 164=>"Palau", 165=>"Panama", 166=>"Papua New Guinea ", 167=>"Paraguay", 168=>"Peru", 169=>"Philippines", 170=>"Pitcairn Island", 171=>"Poland",
   172=>"Ponape (Federated States of Micronesia)", 173=>"Portugal", 174=>"Qatar", 175=>"Reunion", 176=>"Romania", 177=>"Rota (Northern Mariana Islands)", 178=>"Russia", 179=>"Rwanda", 180=>"Saba (Netherlands Antilles)", 181=>"Saipan (Northern Mariana Islands)", 182=>"San Marino", 183=>"Sao Tome & Principe", 184=>"Saudi Arabia", 185=>"Scotland (United Kingdom)", 186=>"Senegal", 187=>"Seychelles",
   188=>"Sierra Leone ", 189=>"Singapore", 190=>"Slovak Republic (Slovakia)", 191=>"Slovenia", 192=>"Solomon Islands", 193=>"Somalia", 194=>"South Africa", 195=>"Spain", 196=>"Sri Lanka", 197=>"St. Barthelemy (Guadeloupe)", 198=>"St. Christopher (St. Kitts and Nevis)", 199=>"St. Croix (U.S. Virgin Islands)", 200=>"St. Eustatius (Netherlands Antilles)", 201=>"St. Helena", 202=>"St. John (U.S. Virgin Islands)",
   203=>"St. Kitts and Nevis", 204=>"St. Lucia", 205=>"St. Maarten (Netherlands Antilles)", 206=>"St. Martin (Guadeloupe)", 207=>"St. Pierre & Miquelon", 208=>"St. Thomas (U.S. Virgin Islands)", 209=>"St. Vincent and the Grenadines", 210=>"Sudan", 211=>"Suriname", 212=>"Swaziland", 213=>"Sweden", 214=>"Switzerland", 215=>"Syrian Arab Republic (Syria) ", 216=>"Tahiti (French Polynesia)", 217=>"Taiwan", 218=>"Tajikistan", 219=>"Tanzania", 220=>"Thailand", 221=>"Tinian (Northern Mariana Islands)", 222=>"Togo", 223=>"Tonga", 224=>"Tortola (British Virgin Islands)", 225=>"Trinidad and Tobago", 226=>"Tristan da Cunha", 227=>"Truk (Federated States of Micronesia)",
   228=>"Tunisia", 229=>"Turkey", 230=>"Turkmenistan", 231=>"Turks & Caicos Islands", 232=>"Tuvalu", 233=>"U.S. Virgin Islands", 234=>"Uganda", 235=>"Ukraine", 236=>"Union Island (St. Vincent and the Grenadines)", 237=>"United Arab Emirates", 238=>"United Kingdom", 239=>"Uruguay", 240=>"Uzbekistan", 241=>"Vanuatu", 242=>"Vatican City", 243=>"Venezuela", 244=>"Vietnam", 245=>"Virgin Gorda (British Virgin Islands)", 246=>"Wales (United Kingdom)", 247=>"Wallis and Futuna Islands", 248=>"Western Samoa", 249=>"Yap (Federated States of Micronesia)", 250=>"Yemen", 251=>"Yugoslavia", 252=>"Zambia", 253=>"Zimbabwe", 255=>"Puerto Rico", 256=>"Serbia", 257=>"Montenegro"}

  NEW_ABBR = {"CA"=>1, "US"=>2, "AF"=>3, "AX"=>4, "AL"=>5, "DZ"=>6, "AS"=>7, "AD"=>8, "AO"=>9, "AI"=>10, "AQ"=>11, "AG"=>12, "AR"=>13, "AM"=>14, "AW"=>15, "AU"=>16, "AT"=>17, "AZ"=>18, "BS"=>19, "BH"=>20, "BD"=>21, "BB"=>22, "BY"=>23, "BE"=>24, "BZ"=>25, "BJ"=>26, "BM"=>27, "BT"=>28, "BO"=>29, "BQ"=>30, "BA"=>31, "BW"=>32, "BV"=>33, "BR"=>34, "IO"=>35, "BN"=>36, "BG"=>37, "BF"=>38, "BI"=>39, "KH"=>40, "CM"=>41, "CV"=>42, "KY"=>43, "CF"=>44, "TD"=>45, "CL"=>46, "CN"=>47, "CX"=>48, "CC"=>49, "CO"=>50, "KM"=>51, "CG"=>52, "CD"=>53, "CK"=>54, "CR"=>55, "CI"=>56, "HR"=>57, "CU"=>58, "CW"=>59, "CY"=>60, "CZ"=>61, "DK"=>62, "DJ"=>63, "DM"=>64, "DO"=>65, "EC"=>66, "EG"=>67, "SV"=>68, "GQ"=>69, "ER"=>70, "EE"=>71, "ET"=>72, "FK"=>73, "FO"=>74, "FJ"=>75, "FI"=>76, "FR"=>77, "GF"=>78, "PF"=>79, "TF"=>80, "GA"=>81, "GM"=>82, "GE"=>83, "DE"=>84, "GH"=>85, "GI"=>86, "GR"=>87, "GL"=>88, "GD"=>89, "GP"=>90, "GU"=>91, "GT"=>92, "GG"=>93, "GN"=>94, "GW"=>95, "GY"=>96, "HT"=>97, "HM"=>98, "VA"=>99, "HN"=>100, "HK"=>101, "HU"=>102, "IS"=>103, "IN"=>104, "ID"=>105, "IR"=>106, "IQ"=>107, "IE"=>108, "IM"=>109, "IL"=>110, "IT"=>111, "JM"=>112, "JP"=>113, "JE"=>114, "JO"=>115, "KZ"=>116, "KE"=>117, "KI"=>118, "KP"=>119, "KR"=>120, "KW"=>121, "KG"=>122, "LA"=>123, "LV"=>124, "LB"=>125, "LS"=>126, "LR"=>127, "LY"=>128, "LI"=>129, "LT"=>130, "LU"=>131, "MO"=>132, "MK"=>133, "MG"=>134, "MW"=>135, "MY"=>136, "MV"=>137, "ML"=>138, "MT"=>139, "MH"=>140, "MQ"=>141, "MR"=>142, "MU"=>143, "YT"=>144, "MX"=>145, "FM"=>146, "MD"=>147, "MC"=>148, "MN"=>149, "ME"=>150, "MS"=>151, "MA"=>152, "MZ"=>153, "MM"=>154, "NA"=>155, "NR"=>156, "NP"=>157, "NL"=>158, "NC"=>159, "NZ"=>160, "NI"=>161, "NE"=>162, "NG"=>163, "NU"=>164, "NF"=>165, "MP"=>166, "NO"=>167, "OM"=>168, "PK"=>169, "PW"=>170, "PS"=>171, "PA"=>172, "PG"=>173, "PY"=>174, "PE"=>175, "PH"=>176, "PN"=>177, "PL"=>178, "PT"=>179, "PR"=>180, "QA"=>181, "RE"=>182, "RO"=>183, "RU"=>184, "RW"=>185, "BL"=>186, "SH"=>187, "KN"=>188, "LC"=>189, "MF"=>190, "PM"=>191, "VC"=>192, "WS"=>193, "SM"=>194, "ST"=>195, "SA"=>196, "SN"=>197, "RS"=>198, "SC"=>199, "SL"=>200, "SG"=>201, "SX"=>202, "SK"=>203, "SI"=>204, "SB"=>205, "SO"=>206, "ZA"=>207, "GS"=>208, "SS"=>209, "ES"=>210, "LK"=>211, "SD"=>212, "SR"=>213, "SJ"=>214, "SZ"=>215, "SE"=>216, "CH"=>217, "SY"=>218, "TW"=>219, "TJ"=>220, "TZ"=>221, "TH"=>222, "TL"=>223, "TG"=>224, "TK"=>225, "TO"=>226, "TT"=>227, "TN"=>228, "TR"=>229, "TM"=>230, "TC"=>231, "TV"=>232, "UG"=>233, "UA"=>234, "AE"=>235, "GB"=>236, "UM"=>237, "UY"=>238, "UZ"=>239, "VU"=>240, "VE"=>241, "VN"=>242, "VG"=>243, "VI"=>244, "WF"=>245, "EH"=>246, "YE"=>247, "YU"=>248, "ZM"=>249, "ZW"=>250}

  def self.get_new_id_by_old_id(id)
    #name = Sqlserver::Country.find(id).CO_Name
    name = OLD_ABBR[id]
    abbr = if COUNTRY_HASH.invert[name.upcase].nil?
             set_by_hand(name)
           else
             COUNTRY_HASH.invert[name.upcase]
           end
    NEW_ABBR[abbr]
    #country = ::Country.find_by_abbr(abbr)
    #country.id
  end

  def self.data_to_migrate
    COUNTRY_HASH.each do |k, v|
      c = ::Country.new
      c.name = v.titleize
      c.abbr = k
      c.save
    end

    ActiveRecord::Base.connection.execute("ALTER SEQUENCE countries_id_seq START with #{::Country.order("id").last.id + 1} RESTART; ")
  end
end
