class Sqlserver::OrderNote < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  self.table_name = 'Order_Notes'

  def self.transform_to_boolean(str)
    str == "Y" ? true : false
  end

  def self.data_to_migrate
    ids_array = Sqlserver::Client.valid_clients_ids.in_groups(10, false)
    ids_array.each do |ids|
    self.where(ON_CID: ids).each do |obj|
      puts obj.id
      on = ::OrderNote.new
      on.client_id = obj.ON_CID
      on.note = obj.ON_Note
      on.create_support_user_id = obj.ON_SSIDCreated
      on.deleted = transform_to_boolean(obj.ON_Deleted)
      on.created_at = obj.ON_Date
      on.updated_at = obj.ON_Date
      on.save
    end
    end
    puts "finish"
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE order_notes_id_seq START with #{::OrderNote.order("id").last.id + 1} RESTART;")
  end
end
