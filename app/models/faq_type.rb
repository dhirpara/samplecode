class FaqType < ActiveRecord::Base
  attr_accessible :name, :index

  has_many :faqs

  scope :default_sort, order(:index)
end
