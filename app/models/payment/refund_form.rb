class Payment::RefundForm
  include ::NonPersistenceModel

  attr_accessor :payment, :cc_amount, :other_amount, :payment_type_id, :payment_types, :note, :support_user, :alert

  validate :check_amount

  validates_presence_of :note

  def initialize(payment, attributes = nil, options = {})
    @payment = payment
    @payment_types = PaymentType.non_cc_type
    unless attributes.nil?
      @cc_amount = attributes[:cc_amount].present? ? attributes[:cc_amount].to_d : 0
      @other_amount = attributes[:other_amount].present? ? attributes[:other_amount].to_d : 0
      @payment_type_id = attributes[:payment_type_id]
      @note = attributes[:note]
      @support_user = options[:support_user]
    end
  end

  def payment_type
    PaymentType.find(payment_type_id)
  end

  def submit?
    ActiveRecord::Base.transaction do
      return false unless valid?

      gateway = Payment.gateway_by_currency(order.currency)

      if cc_amount > 0
        resp = gateway.refund(cc_amount * 100, payment.confirmation_code)

        if resp.success?
          cc_payment = order.payments.new(amount: cc_amount, note: note, support_user: support_user)
          cc_payment.create_by_resp(resp, ref_trn_id: payment.trn_id)
        else
          self.alert = resp.message
          return false
        end
      end

      if other_amount > 0
        other_payment = order.payments.new(amount: - other_amount, ref_trn_id: payment.trn_id, received: false, note: note, payment_type_id: payment_type_id, support_user: support_user, completed_at: Time.zone.now)
        other_payment.create_by_other_payment_type_payment
      end

      true
    end
  end

  def order
    @order ||= payment.order
  end

  def check_amount
    if cc_amount < 0
      errors.add(:base, "Refund amount to Credit Card must be greater than or equal to $0")
    end

    if other_amount < 0
      errors.add(:base, "Refund amount to #{payment_type.name} must be greater than or equal to $0")
    end

    if cc_amount == 0 && other_amount == 0
      errors.add(:base, "Refund total amount must be greater than $0")
    end

    if cc_amount + other_amount > payment.can_refund_amount
      errors.add(:base, "Just can refund $#{payment.can_refund_amount} for this transaction")
    end
  end

  def refund_total
    @refund_total ||= cc_amount + other_amount
  end
end
