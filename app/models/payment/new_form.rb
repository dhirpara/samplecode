class Payment::NewForm
  include ::NonPersistenceModel

  attr_accessor :order, :payment, :address, :credit_card, :payment_profile, :back_end,
    :use_shipping_address, :resp

  def initialize(order, attributes = {}, options = {})
    options.reverse_merge!(back_end: false)

    @order = order
    @back_end = options[:back_end]

    if attributes.present?
      @use_shipping_address = attributes[:use_shipping_address] == "1"
      @payment_profile_id = attributes[:payment_profile_id]

      @payment = if @back_end
                   order.payments.build(attributes[:payment].reverse_merge(support_user: options[:support_user]))
                 else
                   order.payments.build(amount: order.total!)
                 end

      unless payment_profile
        @credit_card = ActiveMerchant::Billing::CreditCard.new(attributes[:credit_card])

        @address = if @use_shipping_address
                     order.shipment.address.dup
                   else
                     Address.new(attributes[:address].merge({name: @credit_card.name}))
                   end
        @address.without_validate_name = true
      end
    else
      if @back_end
        @payment = Payment.new(received: false, amount: order.need_purchase_total,
                               payment_type: order.client.payments.where(trn_type: "P").first.try(:payment_type))
      end
    end

    @address ||= Address.new(country_id: Country.first.id)
    @credit_card ||= ActiveMerchant::Billing::CreditCard.new(year: Date.current.year, month: Date.current.month)
  end

  def submit?
    if order.payment_profiles.find_by_id(payment_profile_id)
      true
    else
      #front-end不用选payment_type
      if @payment.is_cc_type?
        if back_end
          @payment.valid? & @credit_card.valid? & @address.valid?
        else
          @credit_card.valid? & @address.valid?
        end
      else
        @payment.valid?
      end
    end
  end

  def payment_profile_id
    @payment_profile_id ||= @payment_profile.try(:id)
  end

  def payment_profile
    @payment_profile ||= order.payment_profiles.find_by_id(@payment_profile_id)
  end

  def is_pre_order_purchase?
    order.payments.count == 0 && order.order_items.any?{|i| i.product.pre_order? }
  end

  def purchase(options = {})
    #这里要用count找order存在数据库payment的个数
    if is_pre_order_purchase?
      unless payment_profile
        @resp, @payment_profile = payment.store_payment_profile(credit_card, address)
      end

      if payment_profile
        pre_order = PreOrder.find_or_initialize_by_order_id(order.id)
        pre_order.update_attributes(payment_profile: payment_profile)
        add_referral_code_order
        true
      else
        false
      end
    else
      #前端不能选payment_type,所以payment_type为nil
      if payment.is_cc_type?
        @resp = if payment_profile
                  payment.payment_profile_purchase(payment_profile)
                else
                  if order.client.guest?
                    payment.guest_purchase(credit_card, address)
                  else
                    store_profile_resp, @payment_profile = payment.store_payment_profile(credit_card, address)

                    if @payment_profile
                      payment.payment_profile_purchase(payment_profile)
                    else
                      store_profile_resp
                    end
                  end
                end

        if @resp.success?
          add_referral_code_order
          NotificationMailer.order_checkout(order.reload, billing_address).deliver
          true
        else
          false
        end
      else
        payment.create_by_other_payment_type_payment
        add_referral_code_order
        true
      end
    end
  end

  def add_referral_code_order
    if order.referral_code
      unless ReferralCodeOrder.find_by_order_id_and_referral_code_id(order.id, order.referral_code_id)
        ReferralCodeOrder.create(order: order, referral_code: order.referral_code)
      end
    end
  end

  def billing_address
    if payment_profile
      Address.new(payment_profile.attributes.select{|k, _| ["address1", "address2", "city", "state", "name", "postal_code", "phone", "country_id"].include? k})
    else
      address
    end
  end
end
