class Disorder < ActiveRecord::Base
  TRANSLATES_ATTRIBUTES = [:name]
  translates *TRANSLATES_ATTRIBUTES

  IMAGE_URLS = {
    "1"  => "/assets/adhd_thumb.jpg",
    "2"  => "/assets/anxiety_thumb.jpg",
    "4"  => "/assets/autism_thumb.jpg",
    "5"  => "/assets/bipolar_thumb.jpg",
    "6"  => "/assets/depression_thumb.jpg",
    "7"  => "/assets/ocd_thumb.jpg",
    "13" => "/assets/stress_thumb.jpg"
  }

  include Authority::Abilities
  has_many :product_disorders, dependent: :restrict
  has_many :products, through: :product_disorders

  has_many :article_disorders, dependent: :restrict
  has_many :articles, through: :article_disorders

  has_many :video_disorders, dependent: :restrict
  has_many :videos, through: :video_disorders

  has_many :study_disorders, dependent: :restrict
  has_many :studies, through: :study_disorders

  attr_accessible :name, :deleted, :show_in_navigation


  scope :related_with_product, lambda{|products| joins(:product_disorders).where(:"deleted" => false, :"product_disorders.product_id" => products.map(&:id)).order("name").uniq }

  ["video", "study"].each do |model|
    class_eval %(
      scope :related_with_#{model}, joins(:#{model}_disorders).where(deleted: false).order("name").uniq
    )
  end

  def image_url
    IMAGE_URLS[id.to_s]
  end
end
