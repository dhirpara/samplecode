class Study < ActiveRecord::Base
  TRANSLATES_ATTRIBUTES = [:title, :brief_description, :article_title]

  translates *TRANSLATES_ATTRIBUTES

  include Authority::Abilities
  include DisorderHelper
  attr_accessible :journal_cover, :link, :pdf, :title, :brief_description, :video_id, :article_title, :reference, :index, :journal_cover_alt, :disorder_ids
  mount_uploader :journal_cover, StudyJournalCoverUploader
  attr_accessible :journal_cover, :journal_cover_cache, :remove_journal_cover
  has_many :study_disorders, dependent: :destroy
  accepts_nested_attributes_for :study_disorders, allow_destroy: true, reject_if: proc { |attributes| attributes["choose"] == "0" && attributes["id"].nil? }
  attr_accessible :study_disorders_attributes
  has_many :disorders, through: :study_disorders
  belongs_to :video

  mount_uploader :pdf, StudyPdfUploader
  attr_accessible :pdf, :pdf_cache, :remove_pdf

  scope :default_sort, order(:index)

  validates_presence_of :title

  def pdf_name
    pdf_url.split("/")[-1]
  end

  #def delete_non_essential_study_disorders
    #study_disorders.each do |study_disorder|
      #study_disorder.destroy unless study_disorder.choose
    #end
  #end
end
