class ArticleCategory < ActiveRecord::Base
  attr_accessible :name
  has_many :articles, dependent: :destroy
  has_many :documents, dependent: :destroy
end
