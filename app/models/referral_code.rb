class ReferralCode < ActiveRecord::Base
  include Authority::Abilities
  acts_as_paranoid

  belongs_to :support_user
  belongs_to :pricing_type
  has_many :referral_code_product_promotions, dependent: :destroy
  has_many :product_promotions, through: :referral_code_product_promotions
  attr_accessible :code, :url, :discount, :end_date, :number_of_times, :percentage, :start_date, :support_user_id, :pricing_type_id, :product_promotion_ids, :need_licence

  validates_presence_of :code
  validates_uniqueness_of :code
  validates_numericality_of :discount, greater_than_or_equal_to: 0, allow_blank: true
  validates_numericality_of :discount, less_than_or_equal_to: 100, allow_blank: true, if: :percentage
  validates_presence_of :start_date, :end_date
  validates_numericality_of :number_of_times, greater_than: 0, allow_blank: true

  def self.set_referral_code_for_order(resource)
    if resource.code.present?
      resource.referral_code = where("code ilike ?", resource.code).first
      if resource.referral_code.nil?
        resource.errors.add(:base, "The Referral Code is invalid.")
      elsif ReferralCode.out_of_stock_for_resource?(resource) || resource.referral_code.over_valid_date?
        resource.errors.add(:base, "The Referral Code cannot be used.")
        resource.referral_code = nil
      end
    else
      resource.referral_code = nil
    end
  end

  def self.change_client(order)
    return if order.client.guest?
    return unless order.referral_code

    if tmp_support_user = order.referral_code.support_user
      order.client.support_user = tmp_support_user
    end

    if tmp_pricing_type = order.referral_code.pricing_type
      order.client.pricing_type = tmp_pricing_type
    end

    order.client.save
  end

  def self.update_adjustment(order)
    note_text = "Referral Code"
    if order.referral_code && order.referral_code.discount
      adjustment = order.order_adjustments.find_by_note(note_text) || order.order_adjustments.build(adjustment_type: AdjustmentType.find_by_name("refund"), note: note_text)
      adjustment.amount = rate(order)
      adjustment.save
    else
      order.order_adjustments.find_by_note(note_text).try(:destroy)
    end
  end

  def self.rate(order)
    if order.referral_code.discount
      if order.referral_code.percentage
        - order.subtotal * order.referral_code.discount * 0.01
      else
        - order.referral_code.discount
      end
    else
      0
    end
  end

  def self.find_referral_code_through_http_referer(http_referer)
    if http_referer
      ReferralCode.where(url: http_referer).first
      #if url = /(^https??:\/\/.*?)\//.match(http_referer).try(:[], 1)
      #  ReferralCode.where("url ilike ?", "#{url}%").first
      #end
    end
  end


  def self.out_of_stock_for_resource?(resource)
    if resource.referral_code.number_of_times.blank?
      false
    else
      if resource.class == Order
        if ReferralCodeOrder.find_by_order_id_and_referral_code_id(resource.id, resource.referral_code_id)
          false
        else
          ReferralCodeOrder.where(referral_code_id: resource.referral_code_id).count >= resource.referral_code.number_of_times
        end
      else
        ReferralCodeOrder.where(referral_code_id: resource.referral_code_id).count >= resource.referral_code.number_of_times
      end
    end
  end

  def over_valid_date?
    Date.current < start_date || Date.current > end_date
  end

end
