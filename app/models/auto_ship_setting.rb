class AutoShipSetting < ActiveRecord::Base
  include Authority::Abilities
  has_many :auto_ship_setting_disorders, dependent: :destroy

  attr_accessible :discount, :pricing_type_ids

  validates_numericality_of :discount, greater_than_or_equal_to: 0, less_than: 100

  has_many :auto_ship_setting_pricing_types, dependent: :destroy
  has_many :pricing_types, through: :auto_ship_setting_pricing_types

  def self.rate(total)
    - (total * first.discount * 0.01).round(2)
  end

  def self.rate_by_order(order)
    if can_get_discount?(order)
      rate(order.subtotal)
    else
      0
    end
  end

  def self.can_get_discount?(order)
    first.pricing_type_ids.include? order.pricing_type_id
  end
end
