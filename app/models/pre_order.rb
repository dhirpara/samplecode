class PreOrder < ActiveRecord::Base
  include Authority::Abilities
  belongs_to :order
  belongs_to :payment_profile
  attr_accessible :order, :payment_profile

  scope :pending, where(declined: false)

  def complete_order
    if Payment::NewForm.new(order, payment_profile_id: payment_profile_id, address: {}, credit_card: {}).purchase
      destroy
    else
      self.declined = true
      self.save(false)
      PreOrderMailer.declined_notification(self).deliver
    end
  end
end
