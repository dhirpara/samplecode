class OrderItemReport < ActiveRecord::Base
  attr_accessible :order_item, :quantity

  belongs_to :order_item

  def order
    order_item.order
  end

  def self.add_report(item)
    create(order_item: item, quantity: item.quantity)
  end

  def self.sale_reports
    where("quantity > ?", 0)
  end

  def self.return_reports
    where("quantity < ?", 0)
  end
end
