class GlobalLanguage < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :abbr, :name, :active

  scope :active, where(active: true)
end
