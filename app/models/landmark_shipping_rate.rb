class LandmarkShippingRate < ActiveRecord::Base
  attr_accessible :shipping_method, :description, :base_rate, :base_weight_lbs, :rate_per_lbs

  belongs_to :shipping_method

end
