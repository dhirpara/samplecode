class Payment < ActiveRecord::Base
  include Authority::Abilities
  attr_accessible :amount, :error, :error_code, :message, :order_id, :confirmation_code, :ref_trn_id, :payment_type_id, :payment_type, :received, :note, :support_user, :payment_profile_id, :completed_at, :last_digits, :source, :trn_id, :trn_type
  PAYMENT_ACTION = {
    P: "Purchase",
    R: "Refund",
    VP: "Void Purchase",
    VR: "Void Refund"
  }

  APPROVED = "Approved"
  default_scope order("payments.created_at DESC")

  belongs_to :order, inverse_of: :payments
  belongs_to :payment_type
  belongs_to :support_user
  validates :amount, numericality: { greater_than: 0 }, if: :is_cc_type?
  validates_presence_of :amount
  validate :check_credit_amount, :check_refund_amount, :check_amount


  scope :valid_purchase_payments, where(trn_type: "P", message: APPROVED)

  scope :card_type, includes(:payment_type).where("(payment_types.name in (?) or payment_type_id is null)", PaymentType::CARD_NAME)
  scope :other_type, includes(:payment_type).where("payment_types.name not in (?)", PaymentType::CARD_NAME)
  scope :actual_other_type, includes(:payment_type).where("payment_types.name in (?)", PaymentType::ACTUAL_OTHER_TYPE_NAME)

  scope :received, where(received: true)

  def card_method
    case payment_type.try(:name)
    when "Visa"
      "V(#{last_digits!})"
    when "Mastercard"
      "MC(#{last_digits!})"
    when "Discover"
      "D(#{last_digits!})"
    end
  end

  def last_digits!
    last_digits || order.payments.find_by_trn_id(ref_trn_id).try(:last_digits!)
  end

  def amount!
    if ["R", "VP"].include?(trn_type)
      - amount
    else
      amount
    end
  end

  def self.card_received_total
    (select(&:is_valid_purchase?).sum(&:amount) -
     select(&:is_valid_void_purchase?).sum(&:amount) -
     select(&:is_valid_refund?).sum(&:amount) +
     select(&:is_valid_void_refund?).sum(&:amount)).round(2)
  end

  def check_credit_amount
    return unless payment_type == PaymentType.credit_type
    return if ref_trn_id.present?

    if amount > can_change_credit_amount
      errors.add(:base, "Client only have $#{can_change_credit_amount} credit amount")
    end
  end

  def can_change_credit_amount
    order.client.credit_amount + (payment_type_id_changed? ? 0 : amount_before_change)
  end

  def check_amount
    if amount == 0
      errors.add(:amount, "can't equal $0")
    end
  end

  def check_refund_amount
    return if is_cc_type?
    return if ref_trn_id.blank?

    parent_payment = order.payments.find_by_trn_id(ref_trn_id)

    if amount >= 0
      errors.add(:base, "Amount must be lesser than $0")
    elsif amount.abs > parent_payment.can_refund_amount - amount_before_change
      errors.add(:base, "Amount must be lesser than -$#{parent_payment.can_refund_amount - amount_before_change} for this transaction")
    end
  end

  def amount_before_change
    amount_change.try(:[], 0) || amount
  end

  def create_by_resp(resp, options = {})
    payment_type_name = if resp.params["cardType"] == "VI"
                          "Visa" 
                        elsif resp.params["cardType"] == "MC"
                          "Mastercard"
                        elsif resp.params["cardType"] == "NN"
                          "Discover"
                        else
                          nil
                        end
    payment_type = PaymentType.find_by_name(payment_type_name)
    assign_attributes(source: resp.params,
                      payment_type: payment_type,
                      last_digits: options[:last_digits],
                      payment_profile_id: options[:payment_profile_id],
                      ref_trn_id: options[:ref_trn_id],
                      received: resp.success?,
                      error: resp.params["errorFields"],
                      error_code: resp.params["errorType"],
                      message: resp.message,
                      confirmation_code: resp.authorization,
                      trn_id: resp.params["trnId"],
                      trn_type: resp.params["trnType"]
                     )
    if resp.success?
      self.completed_at = Time.zone.now
    end

    save

    if resp.success?
      change_after_complete
    end
  end

  def is_cc_type?
    return true if payment_type.nil?
    payment_type.is_cc_type?
  end

  def self.gateway_by_currency(currency)
    ActiveMerchant::Billing::BeanstreamGateway.new(login: BASE_CONFIG[:beanstream]["#{currency.name}"]["login"],
                                                   user: BASE_CONFIG[:beanstream]["#{currency.name}"]["user"],
                                                   password: BASE_CONFIG[:beanstream]["#{currency.name}"]["password"],
                                                   secure_profile_api_key: BASE_CONFIG[:beanstream]["#{currency.name}"]["secure_profile_api_key"])
  end

  def change_after_complete
    order.change_state!("payment")
    order.change_payment_state!

    if order.complete?
      ReferralCode.change_client(order)
    end

    CommissionReport.add_report(order)
    PaymentReport.add_report(self)
    order.add_report
  end


  #  def purchase(options)
  #    transaction do
  #      payment_profile = options[:payment_profile]
  #      credit_card = options[:credit_card]
  #      address = options[:address]
  #
  #      return guest_purchase(credit_card, address) if order.client.is_guest?
  #      return payment_profile_purchase(payment_profile) if payment_profile
  #      client_purchase(credit_card, address)
  #    end
  #  end

  def gateway_options(address)
    {
      billing_address: address.build_billing_address,
      email: order.client.email_or_default,
      order_id: order.id
    }
  end

  def store_payment_profile(credit_card, address)
    gateway = Payment.gateway_by_currency(order.currency)
    payment_profile = nil

    resp = gateway.store(credit_card, gateway_options(address))


    if resp.success?
      payment_profile = PaymentProfile.generate(order, address, credit_card, resp)
    else
      create_by_resp(resp, last_digits: credit_card.last_digits)
    end

    [resp, payment_profile]
  end

  def guest_purchase(credit_card, address)
    gateway = Payment.gateway_by_currency(order.currency)

    resp = gateway.purchase(amount * 100, credit_card, gateway_options(address))
    create_by_resp(resp, last_digits: credit_card.last_digits)

    resp
  end

  def payment_profile_purchase(payment_profile)
    gateway = Payment.gateway_by_currency(order.currency)
    options = { order_id: order.id }
    resp = gateway.purchase(amount * 100, payment_profile.customer_code, options)
    create_by_resp(resp, payment_profile_id: payment_profile.id, last_digits: payment_profile.last_digits)

    return resp
  end

  def void
    transaction do
      gateway = Payment.gateway_by_currency(order.currency)
      resp = gateway.void(self.confirmation_code)
      #Rails.logger.info(resp.inspect)
      void_payment = order.payments.new(amount: amount, support_user: support_user)
      void_payment.create_by_resp(resp, ref_trn_id: trn_id)
      resp
    end
  end

  def refund(payment)
    transaction do
      gateway = Payment.gateway_by_currency(order.currency)
      resp = gateway.refund(payment.amount * 100, self.confirmation_code)
      payment.create_by_resp(resp, ref_trn_id: trn_id)

      resp
    end
  end

  #def trn_id
  #confirmation_code.split(";")[0]
  #end

  def action
    return "" if payment_type.nil?
    if payment_type.is_cc_type?
      code = confirmation_code.split(";")[2]
      code.nil? ? "" : PAYMENT_ACTION[code.to_sym]
    else
      payment_type.name
    end
  end

  def success?; message == APPROVED; end

  def can_refund?
    is_cc_type? && is_valid_purchase? && can_refund_amount > 0
  end

  def can_refund_amount
    #already_void? ? 0 : amount - (valid_refund_payments.map(&:amount).inject(&:+) || 0)

    @can_refund_amount ||= already_void? ? 0 : amount - valid_refund_payments.inject(0) do |total, p|
      total += p.is_cc_type? ? p.amount : - p.amount
    end
  end

  def is_valid_purchase?
    trn_type == "P" && success?
  end
  def is_valid_void_purchase?
    trn_type == "VP" && success?
  end
  def is_valid_void_refund?
    trn_type == "VR" && success?
  end
  def is_valid_refund?
    trn_type == "R" && success?
  end

  def already_void?
    #if is_valid_purchase?
    #order.payments.any?{|p| p.ref_trn_id == trn_id && p.is_valid_void_purchase? }
    #else
    #order.payments.any?{|p| p.ref_trn_id == trn_id && p.is_valid_void_refund? }
    #end
    void_payment.present?
  end

  def void_payment
    order.payments.where(ref_trn_id: trn_id, trn_type: "V#{trn_type}", message: APPROVED).first
  end

  def valid_refund_payments
    #is_valid_purchase? ? order.payments.where(ref_trn_id: trn_id, message: APPROVED).delete_if(&:already_void?) : []
    refund_payments.delete_if(&:already_void?)
  end

  def refund_payments
    order.payments.where(ref_trn_id: trn_id).select do |p|
      if p.is_cc_type? 
        p.trn_type == "R" && p.success?
      else
        true
      end
    end
  end

  def can_void?
    is_cc_type? && (is_valid_refund? || is_valid_purchase?) && !already_void? && created_at.strftime("%Y%m%d") == Time.zone.now.strftime("%Y%m%d") && valid_refund_payments.empty?
  end

  def create_by_other_payment_type_payment
    transaction do
      self.completed_at = Time.zone.now if received?
      save

      change_after_complete

      CommissionReport.add_init_report(order)
    end
  end

  class<<self
    def pay_by_credit(order, support_user)
      #TODO
      pay_amount = [order.need_purchase_total, order.client.credit_amount].min
      return if pay_amount <= 0
      payment = order.payments.new(amount: pay_amount, received: true, payment_type_id: PaymentType.credit_type.id, support_user: support_user)
      payment.create_by_other_payment_type_payment
    end

    def refund_all(order, support_user)
      transaction do
        gateway = Payment.gateway_by_currency(order.currency)
        order.payments.valid_purchase_payments.each do |p|
          if p.can_refund_amount > 0
            resp = gateway.refund(p.can_refund_amount * 100, p.confirmation_code)
            cc_payment = order.payments.new(amount: p.can_refund_amount, support_user: support_user)
            cc_payment.create_by_resp(resp, ref_trn_id: p.trn_id)
          end
        end

        completed_time = Time.zone.now
        PaymentType.non_cc_type.each do |pt|
          total = order.payments.where(payment_type_id: pt, ref_trn_id: nil).sum(&:amount)
          if total > 0
            non_cc_payment = order.payments.build(amount: -total, received: false, payment_type_id: pt.id, support_user: support_user, completed_at: completed_time)
            non_cc_payment.create_by_other_payment_type_payment
          end
        end
      end
    end
  end
end
