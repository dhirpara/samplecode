class Order < ActiveRecord::Base
  include Export
  include Authority::Abilities
  include ActionView::Helpers::NumberHelper
  self.authorizer_name = 'OrderAuthorizer'
  attr_accessible :number, :acitve, :order_type, :currency_id, :currency, :calculated_at, :client_id, :completed_at, :shipment_attributes, :ship_address_attributes, :support_user, :order_items_attributes, :pricing_type, :code, :licence
  attr_accessor :commission_rate, :commission_amount, :code, :check_referral_code
  before_save :set_package_number
  before_validation :set_code, if: :check_referral_code
  before_save :set_licence, if: :check_referral_code

  has_many :order_items, :dependent => :destroy, inverse_of: :order
  accepts_nested_attributes_for :order_items

  has_many :order_adjustments, as: :adjustable, :dependent => :destroy
  has_one :shipment, :dependent => :destroy, inverse_of: :order
  has_many :payments, :dependent => :destroy, inverse_of: :order
  has_many :pre_orders, dependent: :destroy, inverse_of: :order
  belongs_to :client
  belongs_to :currency
  belongs_to :order_type
  belongs_to :support_user
  belongs_to :pricing_type
  belongs_to :referral_code

  state_machine initial: 'cart' do
    state 'canceled'
    event :next do
      transition from: 'cart', to: 'address'
      transition from: 'address', to: 'delivery'
      transition from: 'delivery', to: 'payment'
    end

    event :cancel do
      transition to: 'canceled'
    end

    event :paid do
      transition to: 'complete'
    end

    after_transition to: 'address', :do => :create_shipment! # 在进入地址部分时先初始化Shipment

    event :rollback_to_cart do
      transition to: "cart"
    end

    event :rollback_to_address do
      transition to: "address"
    end

    event :rollback_to_delivery do
      transition to: "delivery"
    end

    event :rollback_to_payment do
      transition to: "payment"
    end
  end

  def referral_code
    ReferralCode.unscoped { super }
  end

  def submit?
    self.check_referral_code = true

    if valid?
      self.pricing_type = referral_code.try(:pricing_type) || client.pricing_type

      save

      order_items.where("parent_id is null").each do |item|
        add_item(item, update_order: true)
      end

      true
    else
      false
    end
  end

  def currency_name
    currency.name == "USA" ? "USD" : currency.name
  end

  def payment_state_title
    if payment_state == "balance_due"
      "balance due"
    elsif payment_state == "credit_owing"
      "credit owing"
    else
      payment_state
    end
  end

  def commission_support_user
    #@commission_support_user ||= if referral_code.try(:support_user).try(:receive_commission)
    #referral_code.support_user
    #elsif client.support_user.try(:receive_commission)
    #client.support_user
    #end

    @commission_support_user ||= if referral_code.try(:support_user)
                                   if referral_code.support_user.receive_commission
                                     referral_code.support_user
                                   end
                                 elsif client.support_user.try(:receive_commission)
                                   client.support_user
                                 end
  end

  def format_completed_at
    completed_at.try(:strftime, "%Y-%m-%d %H:%M:%S")
  end

  def quantity
    order_items.map{|item| item.quantity}.sum
  end

  def change_payment_state!
    return unless payments.any?
    state = if total! == payment_total(received: true)
              "paid"
            elsif total! < payment_total(received: true)
              "credit_owing"
            else
              "balance_due"
            end
    self.payment_state = state
    save
  end

  def change_state!(rollback_to_state)
    return if canceled?
    return if shipment.try(:ready?) || shipment.try(:pickup?) || shipment.try(:shipped?)
    return unless payments.any?
    if total! == payment_total
      unless complete?
        update_attributes(completed_at: Time.zone.now)
        #add_report
      end
      paid
    else
      send("rollback_to_#{rollback_to_state}")
    end
  end


  state_machine :payment_state, initial: 'pending' do
    state 'pending'
    state 'paid'
    state 'balance_due'
    state 'credit_owing'
    event :pay do
      transition to: 'paid', from: 'pending'
    end
  end

  # state_machine callback function
  # -*- begin -*-
  def create_shipment!
    build_shipment.save! unless shipment.present?
  end

  ["tax", "shipping", "shipping_adjustment", "handling"].each do |name|
    define_method "#{name}_type" do
      AdjustmentType.find_by_name!(name.gsub("_", " "))
    end
  end


  def change_charge(type, charge)
    if adjustment = order_adjustments.select {|adjustment| adjustment.adjustment_type == type }.first
      adjustment.update_attributes(amount: charge)
    else
      order_adjustments.create!(adjustment_type: type, amount: charge)
    end
  end

  def sync_tax_charge!
    return unless shipment.try(:address)
    change_charge(tax_type, tax_charge)
  end

  def sync_referral_code_charge!
    ReferralCode.update_adjustment(self)
  end

  def sync_shipping_charge!
    return unless shipment.try(:shipping_method)
    change_charge(shipping_type, shipping_charge)
  end

  def sync_shipping_adjustment_charge!
    change_charge(shipping_adjustment_type, shipping_adjustment_charge(shipping_method: shipment.shipping_method))
  end

  def sync_handling_charge!
    return unless shipment.try(:shipping_method)
    change_charge(handling_type, handling_charge)
  end

  def sync_shipment_weight!
    return unless shipment
    shipment.update_attributes!(weight_lbs: weight)
  end

  def shipping_handling_charge
    db_shipping_charge + db_handling_charge + db_shipping_adjustment_charge
  end

  #def in_progress?
  #cart? || address? || delivery? || payment?
  #end
  # -*- end -*-

  # No tax for any other country except Canada. Canada's tax is based on provinces in taxes table.
  def tax_charge
    Tax.rate(self)
  end

  def shipping_charge
    ShippingMethod.rate(shipment)
  end

  def handling_charge(options = {})
    HandlingCharge.rate(self, options)
  end

  def shipping_adjustment_charge(options = {})
    CountryShippingAdjustment.rate(self, options)
  end

  def subtotal
    (order_items.map(&:total).inject(&:+) || 0).round(2)
  end

  def total
    return unless payment? || complete?
    total!
  end

  def total!
    (subtotal + (order_adjustments.map(&:amount).inject(&:+) || 0)).round(2)
  end

  def order_items_changed
    @order_items_changed ||= order_items.inject([]) do |array, item|
      array << item if item.price != item.reload_price
      array
    end
  end

  def sync_latest_total
    order_items.each do |item|
      item.update_price
    end
    sync_tax_charge!
    sync_handling_charge!
    sync_shipping_charge!

    total
  end

  ["tax", "handling", "shipping", "shipping_adjustment"].each do |name|
    define_method "db_#{name}_charge" do
      order_adjustments.detect {|adjustment| adjustment.adjustment_type == send("#{name}_type") }.try(:amount) || 0
    end
  end

  def db_other_charge
    ((order_adjustments.map(&:amount).inject(&:+) || 0) - db_tax_charge - db_shipping_charge - db_handling_charge).round(2)
  end

  def payment_total(options = {})
    options.reverse_merge!({received: false})
    credit_card_payments = payments.select{|p| p.payment_type.nil? || p.payment_type.is_cc_type? }

    credit_card_total = credit_card_payments.select(&:is_valid_purchase?).sum(&:amount) -
      credit_card_payments.select(&:is_valid_void_purchase?).sum(&:amount) -
      credit_card_payments.select(&:is_valid_refund?).sum(&:amount) +
      credit_card_payments.select(&:is_valid_void_refund?).sum(&:amount)

    other_payments = payments - credit_card_payments
    other_total = if options[:received]
                    other_payments.select(&:received?).map(&:amount).inject(&:+) || 0
                  else
                    other_payments.map(&:amount).inject(&:+) || 0
                  end
    (credit_card_total + other_total).round(2)
  end

#  def actual_received_payment_total
#    @actual_received_payment_total ||= payments.card_type.card_received_total +
#      payments.actual_other_type.select(&:received?).sum(&:amount)
#  end

  #计算应该要purchase的金额
  def need_purchase_total
    (total! -  payment_total).round(2)
  end

  def ship_address
    shipment.try(:address)
  end

  def shipping_method
    shipment.try(:shipping_method)
  end

  def sync_by_cart(cart)
    self.pricing_type = cart.pricing_type
    self.referral_code = cart.referral_code
    self.licence = cart.licence
    save
    order_items.delete_all unless order_items.empty?
    cart.cart_items.where("rate != ?", 0).group_by(&:cartable_id).each do |k, v|
      order_item = order_items.new(product_id: k, quantity: v.sum(&:quantity))
      add_item(order_item)
    end
    sync_handling_charge!
  end

  def generate_cart
    cart = client.create_cart
    order_items.each do |order_item|
      cart.cart_items.create(quantity: order_item.quantity, cartable_id: order_item.product_id, cartable_type: "Product")
    end

    cart
  end

  def update_ship_address(address)
    transaction do
      shipment.address = address
      shipment.shipping_method = nil
      shipment.save
      order_adjustments.joins(:adjustment_type).where("adjustment_types.name = ?", "shipping").first.try(:destroy)
      sync_tax_charge!
      self.next if address?
      rollback_to_delivery if complete?
      change_payment_state!
      add_report
    end
  end

  def update_shipping_method(shipping_method)
    transaction do
      shipment.update_attributes(shipping_method: shipping_method)
      if delivery?
        self.next
      elsif address?
        self.next
        self.next
      end
      sync_shipping_charge!
      sync_handling_charge!
      sync_shipping_adjustment_charge!
      change_state!("payment")
      change_payment_state!
      add_report
    end
  end

#  def create_other_payment_type_payment(payment)
#    transaction do
#      payment.completed_at = Time.zone.now if payment.received?
#      payment.save
#
#      payment.change_after_complete
#
#      CommissionReport.add_init_report(self)
#    end
#  end

  def self.include_checkout_objects
    # TODO: Add other relationshop if necessary
    includes([{:order_items => :product}])
  end

  def weight
    order_items.map(&:weight).inject(&:+) || 0
  end

  def add_item(item, options = {})
    transaction do
      tmp_item = if order_item = order_items.detect{|i| i.product_id == item.product_id && i.parent_id.nil? }
                   order_item
                 else
                   item.order = self
                   item
                 end

      tmp_item.quantity = if options[:update_order]
                            options[:quantity] = order_items.where("rate != 0").find_all_by_product_id(item.product_id).sum(&:quantity)
                          else
                            options[:reset_quantity] ? item.quantity : order_items.where("rate != 0").find_all_by_product_id(item.product_id).sum(&:quantity) + item.quantity
                          end

      tmp_item.add_to_order(self, options)

      reload
      sync_tax_charge!
      sync_handling_charge!
      sync_referral_code_charge!
      sync_shipment_weight!
      sync_shipping_charge!
      change_state!("cart")
      change_payment_state!
      add_report
    end
  end

  def remove_order_item(item)
    transaction do
      item.remove_from_order
      sync_tax_charge!
      sync_handling_charge!
      sync_shipment_weight!
      sync_shipping_charge!
      change_state!("cart")
      change_payment_state!
      add_report
    end
  end

  #def has_payment_profile?
  #  client.payment_profiles.where(currency_id: currency_id).any?
  #end

  def payment_profiles
    @payment_profiles ||= client.payment_profiles.where(currency_id: currency_id)
  end

  #后台下订单限制验证
  def can_change_order_items?
    can_change? && !address? && !delivery? && !payment? && !canceled?
  end

  def can_change_address?
    can_change? && !cart? && !payment?
  end

  def can_change_shipping_method?
    can_change? && !cart? && !payment? && !(address? && shipment.address.nil?)
  end

  def can_change_order_adjustment?
    shipment.try(:address) && shipment.try(:shipping_method)
  end

  def can_purchase?
    payment? || complete? || canceled?
  end

  def can_change?
    !shipment.try(:pickup?) && !shipment.try(:ready?) && !shipment.try(:shipped?)
  end

  def set_package_number
    self.package_number = self.package_number.to_i == 0 ? 1 : self.package_number.to_i
  end

  def completed_between?(from_date, to_date)
    ( !completed_at.blank? and completed_at >= from_date and completed_at <= to_date ) ? true : false
  end

  def balance_amount
    if canceled?
      0
    else
      payment_total(received: true) - total!
    end
  end

  def calculate_commission_by_amount(earlier_total, commission_rates)
    @commission_amount = 0
    @commission_rate=''
    start_amount = earlier_total
    amount_to_calculate = total
    commission_rates.each do |rate|
      if start_amount >= rate.min and (rate.max.blank? or start_amount < rate.max )
        end_amount = start_amount + amount_to_calculate
        if rate.max.blank? or end_amount <= rate.max
          @commission_rate += "#{number_with_precision(amount_to_calculate, precision:2)}@#{rate.rate}% (Amount #{rate.min}->#{rate.max}) "
          @commission_amount += amount_to_calculate * rate.rate * 0.01
          break
        else
          amount_to_calculate -= (rate.max - start_amount)
          @commission_rate += "#{number_with_precision(amount_to_calculate, precision:2)}@#{rate.rate}% (Amount #{rate.min}->#{rate.max}) "
          @commission_amount += (amount_to_calculate) * rate.rate * 0.01
          start_amount = rate.max
        end
      end
    end
  end

  def calculate_commission_by_number(order_number, commission_rates)
    commission_rates.each do |rate|
      if order_number >= rate.min and ( rate.max.blank? or order_number <= rate.max )
        @commission_rate = "#{rate.rate}% (Order #{rate.min}->#{rate.max})"
        @commission_amount = total * rate.rate * 0.01
      end
    end
  end

  def add_report
    OrderReport.add_report(self)
  end

  def remove_payment_reports
    PaymentReport.remove_reports(self)
  end

  def set_code
    ReferralCode.set_referral_code_for_order(self)
    if referral_code.try(:need_licence?)
      validates_presence_of :licence
    end
  end

  def set_licence
    if referral_code.blank?
      self.licence = ""
    end
  end
end
