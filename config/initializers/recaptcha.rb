raw_config = File.read("#{Rails.root}/config/base_config.yml")
base_config = YAML.load(raw_config)[Rails.env].symbolize_keys

Recaptcha.configure do |config|
  config.public_key  = base_config[:captcha]["public_key"]
  config.private_key = base_config[:captcha]["private_key"]
  config.proxy = base_config[:captcha]["proxy"]
end
