Hardy::Application.routes.draw do
  root to: 'sites#index'
  #get 'test_mandrill' => 'sites#test_mandrill'
  #post 'test_mandrill' => 'sites#test_mandrill_send'
  get 'about' => 'sites#about'
  get 'order' => 'sites#mailer-dev.html'
  get 'hardy_research' => 'sites#hardy_research'
  get 'policies' => 'sites#policies'
  get 'checkout' => 'sites#checkout'
  get 'Optimus-DSD' => 'sites#Optimus-DSD'
  match '/admin' => redirect("/admin/root")
  match '/myaccount' => redirect("/myaccount/profile"), as: :myaccount
  get "/contact_us", to: "contacts#show", as: :contact
  post "/contact_us", to: "contacts#create", as: :contact
  resources :faqs, only: [:index]
  resource :email, only: [:create, :destroy] do
    collection do
      get :subscription
      get :unsubscription
    end
  end

  devise_for :clients, controllers: { registrations: "clients/registrations", sessions: "clients/sessions", passwords: "clients/passwords" }
  as :client do
    get 'myaccount/profile' => 'myaccount/profiles#edit', :as => 'edit_client_registration'
    put 'myaccount/profile' => 'myaccount/profiles#update', :as => 'client_registration'
    post 'myaccount/profile' => 'clients/registrations#create'
    get 'clients/user_name/new' => 'clients/user_names#new', as: :new_client_user_name
    post 'clients/user_name' => 'clients/user_names#create', as: :client_user_name
    get 'clients/newsletter' => 'clients/newletters#show', as: :client_newletter
    put 'clients/newsletter' => 'clients/newletters#update', as: :client_newletter
    get 'clients/newsletter/confirmation' => 'clients/newletters#confirmation', as: :confirmation_client_newletter
    get 'clients/pre_order/new' => 'clients/back_ordereds#new', as: :new_client_back_ordered
    get 'clients/pre_order/confirmation' => 'clients/back_ordereds#confirmation', as: :confirmation_client_back_ordered
    post 'clients/pre_order' => 'clients/back_ordereds#create', as: :client_back_ordered
    #put 'clients/back_ordered' => 'clients/back_ordereds#update', as: :client_back_ordered
  end

  devise_for :support_users, controllers: { sessions: "support_users/sessions", passwords: "support_users/passwords" }
  as :support_user do
    get 'admin/profile' => 'admin/profiles#edit', :as => 'edit_user_registration'
    put 'admin/profile' => 'admin/profiles#update', :as => 'user_registration'
  end

  resources :videos, only: [:index, :show] do
    put :disorder_filter, on: :collection
  end
  resources :products, only: [:index, :show] do
    collection do
      put :disorder_filter
      get :search
    end
  end

  resources :countries do
    resources :provinces, only: [:index]
  end

  get '/article/:article_category_permalink/:id', to: 'articles#show', as: :article
  resources :articles, only: [:index] do
    put :disorder_filter, on: :collection
  end

  resources :studies, only: [:index] do
    put :disorder_filter, on: :collection
  end

  resources :addresses, only: [:destroy]

  namespace :myaccount do
    resources :orders, only: [:index, :show]
    resources :payment_profiles, only: :destroy
    resources :pre_orders, only: [:index, :show, :destroy]

    scope module: 'auto_ship' do
      resources :auto_ships, path: "flexible_autoship", only: [:index, :show, :create, :update, :destroy] do
        member do
          put :update_items
          get :invoice
        end
        resources :order_items, only: [:index, :create, :destroy]

        resource :address, only: [:show, :create, :update] do
          member do
            put :select
          end
        end

        member do
          post :generate_order
        end

        resource :shipping_method, only: [:show, :create]

        resource :payment_profile, only: [:show, :create]
      end
    end
  end

  namespace :shopping do
    resource :cart, only: [:show, :update] do
      resources :cart_items, only: [:create, :destroy]
      #TODO: change to post/put
      get :checkout
      get :licence_field
      post :update_price
    end
    resources :addresses do
      put :select, on: :member
    end
    resources :shipping_methods, only: [:index] do
      put :select, on: :collection
    end
    resource :payment, only: [:show, :create]
    resource :guest, only: [:destroy]
    resources :feedbacks, only: [:new, :create]
  end

  namespace :admin do
    match "/root", to: "application#root"
    resources :support_centers
    resources :notes
    resources :emails, only: [:index, :destroy] do
      collection do
        post :export
      end
    end
    resources :support_users do
      resources :commission_rates, except: [:show]
    end

    resources :shipping_promotions, except: [:show]
    resources :product_promotions, except: [:show]
    resources :referral_codes, except: [:show]

    resources :faqs do
      member do
        match :preview, via: [:post, :put]
      end
    end

    resources :products do
      collection do
        get :search
        match "/upload_label_file", to: "products#upload_label_file", via: [:post, :put]
      end
      member do
        match :preview, via: [:post, :put]
      end
    end
    resources :articles do
      collection do
        match "/upload_image", to: "articles#upload_image", via: [:post, :put]
        match "/upload_author_image", to: "articles#upload_author_image", via: [:post, :put]
      end
      member do
        match :preview, via: [:post, :put]
      end
    end
    resources :videos do
      collection do
        match "/upload_image", to: "videos#upload_image", via: [:post, :put]
      end

      member do
        match :preview, via: [:post, :put]
      end
    end

    resources :studies do
      collection do
        match "/upload_journal_cover", to: "studies#upload_journal_cover", via: [:post, :put]
        match "/upload_pdf", to: "studies#upload_pdf", via: [:post, :put]
      end

      member do
        match :preview, via: [:post, :put]
      end
    end

    resources :documents, except: [:show] do
      collection do
        match "/upload_file", to: "documents#upload_file", via: [:post, :put]
      end
    end

    resources :product_images do
      collection do
        match "/upload_image", to: "product_images#upload_image", via: [:post, :put]
      end
    end

    resources :variants, only: :index
    resources :treatments
    resources :clients, except: [:show] do
      member do
        post :select
        post :impersonate
        delete :cancel_select
        put :receive_newletter
      end

      resources :payment_profiles, only: :destroy

      scope module: 'auto_ship' do
        resources :auto_ships, path: "auto_ship", only: [:index, :show, :create, :update, :destroy] do
          member do
            put :update_items
            get :invoice
          end

          resources :auto_ship_adjustments, except: [:index, :show]

          resources :order_items, only: [:index, :create, :destroy]

          resource :address, only: [:show, :create, :update] do
            member do
              put :select
            end
          end

          resource :shipping_method, only: [:show, :create]

          resource :payment_profile, only: [:show, :create]
        end
      end
    end
    resources :orders do
      resources :order_notes
      resource :invoice
      member do
        post :refund_all
      end
      collection do
        get :export_list
        get :invoices
        post :export_shipping_for_canada
        post :export_shipping_for_other_country
        post :ready_all
      end
      member do
        put :update_total
        put :cancel
      end
      resources :order_items do
        member do
          get :new_return
          post :return
        end
      end
      resources :payments do
        member do
          post :void
          get :new_refund
          post :refund
        end
        collection do
          post :pay_by_credit
        end
      end
      resource :shipments do
        collection do
          get :edit_shipping_method
          put :ready
          put :pickup
        end
      end
      resources :shipping_methods do
        put :select, on: :collection
      end
      resources :addresses do
        member do
          put :select
        end
      end
      resources :order_adjustments
    end

    namespace :report do
      resources :sales_and_orders, only: [:index]
      resources :product_sales, only: [:index]
      resources :call_lists, only: [:index]
      resources :auto_ship_reports, only: [:index]
      resources :pre_orders, only: [:index]
      resources :new_customer_pre_orders, only: [:index] do
        collection do
          get :export
        end
      end
      resources :commissions, only: [:index]
      resources :locations, only: [:index] do
        collection do
          get :to_client
          get :back_to
          get :export
        end
      end
    end
    namespace :configuration do
      resource  :country_currency, only: [:show, :create]
      resources :ta_levels, except: [:show]
      resources :introduction_types
      resources :call_frequencies
      resources :country_shipping_adjustments
      resources :registration_methods
      resources :contact_times
      resources :commission_rates, except: [:show]
      resources :disorders
      resources :handling_charges
      #resources :settings
      resource :auto_ship_settings
      resources :transfer_pages, except: [:show]
      resources :global_languages, only: [:index, :create, :destroy]
      resources :limiting_factors, except: [:show]
      resources :diagnoses, except: [:show]
      resources :seos, only: [:index, :edit, :update]
      resources :questions
      resources :age_groups
      resources :genders
      resources :symptoms
      resources :side_effects
      resources :withdrawal_effects
      resources :questionnaires
      resources :user_groups
      resources :drugs
      resources :doctors, only: [:index, :new, :edit]

    end
  end

  post "select_language", to: "sites#select_language", as: :select_language
  match ":transfer_name" => "sites#transfer_page"
end
