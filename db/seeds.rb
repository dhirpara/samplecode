# coding: utf-8

# Country - need to include all countries
if Country.count == 0
  Country.create(name: 'United States', abbr: 'US')
  Country.create(name: 'Canada', abbr: 'CA')
  Country.create(name: 'Norway', abbr: 'NO')
end
canada = Country.find_by_name("Canada")
usa = Country.find_by_name("United States")
norway = Country.find_by_name("Norway")


# State/Province
## Canada Provinces
Province.create(country: canada, name: 'Alberta', abbr: 'AB')
Province.create(country: canada, name: 'British Columbla', abbr: 'BC')
Province.create(country: canada, name: 'Manitoba', abbr: 'MB')
Province.create(country: canada, name: 'New Brunswick', abbr: 'NB')
Province.create(country: canada, name: 'Newfoundland and Labrador', abbr: 'NL')
Province.create(country: canada, name: 'Nova Scotia', abbr: 'NS')
Province.create(country: canada, name: 'Ontario', abbr: 'ON')
Province.create(country: canada, name: 'Prince Edward Island', abbr: 'PE')
Province.create(country: canada, name: 'Quebec', abbr: 'QC')
Province.create(country: canada, name: 'Saskatchewan', abbr: 'SK')
## Canada Territories
Province.create(country: canada, name: 'Northwest Territories', abbr: 'NT')
Province.create(country: canada, name: 'Yukon', abbr: 'YT')
Province.create(country: canada, name: 'Nunavut', abbr: 'NU')
## USA States
Province.create(country: usa, abbr: 'AL', name: 'Alabama')
Province.create(country: usa, abbr: 'AK', name: 'Alaska')
Province.create(country: usa, abbr: 'AZ', name: 'Arizona')
Province.create(country: usa, abbr: 'AR', name: 'Arkansas')
Province.create(country: usa, abbr: 'CA', name: 'California')
Province.create(country: usa, abbr: 'CO', name: 'Colorado')
Province.create(country: usa, abbr: 'CT', name: 'Connecticut')
Province.create(country: usa, abbr: 'DE', name: 'Delaware')
Province.create(country: usa, abbr: 'FL', name: 'Florida')
Province.create(country: usa, abbr: 'GA', name: 'Georgia')
Province.create(country: usa, abbr: 'HI', name: 'Hawaii')
Province.create(country: usa, abbr: 'ID', name: 'Idaho')
Province.create(country: usa, abbr: 'IL', name: 'Illinois')
Province.create(country: usa, abbr: 'IN', name: 'Indiana')
Province.create(country: usa, abbr: 'IA', name: 'Iowa')
Province.create(country: usa, abbr: 'KS', name: 'Kansas')
Province.create(country: usa, abbr: 'KY', name: 'Kentucky')
Province.create(country: usa, abbr: 'LA', name: 'Louisiana')
Province.create(country: usa, abbr: 'ME', name: 'Maine')
Province.create(country: usa, abbr: 'MD', name: 'Maryland')
Province.create(country: usa, abbr: 'MA', name: 'Massachusetts')
Province.create(country: usa, abbr: 'MI', name: 'Michigan')
Province.create(country: usa, abbr: 'MN', name: 'Minnesota')
Province.create(country: usa, abbr: 'MS', name: 'Mississippi')
Province.create(country: usa, abbr: 'MO', name: 'Missouri')
Province.create(country: usa, abbr: 'MT', name: 'Montana')
Province.create(country: usa, abbr: 'NE', name: 'Nebraska')
Province.create(country: usa, abbr: 'NV', name: 'Nevada')
Province.create(country: usa, abbr: 'NH', name: 'New Hampshire')
Province.create(country: usa, abbr: 'NJ', name: 'New Jersey')
Province.create(country: usa, abbr: 'NM', name: 'New Mexico')
Province.create(country: usa, abbr: 'NY', name: 'New York')
Province.create(country: usa, abbr: 'NC', name: 'North Carolina')
Province.create(country: usa, abbr: 'ND', name: 'North Dakota')
Province.create(country: usa, abbr: 'OH', name: 'Ohio')
Province.create(country: usa, abbr: 'OK', name: 'Oklahoma')
Province.create(country: usa, abbr: 'OR', name: 'Oregon')
Province.create(country: usa, abbr: 'PA', name: 'Pennsylvania')
Province.create(country: usa, abbr: 'RI', name: 'Rhode Island')
Province.create(country: usa, abbr: 'SC', name: 'South Carolina')
Province.create(country: usa, abbr: 'SD', name: 'South Dakota')
Province.create(country: usa, abbr: 'TN', name: 'Tennessee')
Province.create(country: usa, abbr: 'TX', name: 'Texas')
Province.create(country: usa, abbr: 'UT', name: 'Utah')
Province.create(country: usa, abbr: 'VT', name: 'Vermont')
Province.create(country: usa, abbr: 'VA', name: 'Virginia')
Province.create(country: usa, abbr: 'WA', name: 'Washington')
Province.create(country: usa, abbr: 'WV', name: 'West Virginia')
Province.create(country: usa, abbr: 'WI', name: 'Wisconsin')
Province.create(country: usa, abbr: 'WY', name: 'Wyoming')
## USA Territories
Province.create(country: usa, abbr: 'AS', name: 'American Samoa')
Province.create(country: usa, abbr: 'DC', name: 'District of Columbia')
Province.create(country: usa, abbr: 'GU', name: 'Guam')
Province.create(country: usa, abbr: 'MP', name: 'Northern Marianas Islands')
Province.create(country: usa, abbr: 'PR', name: 'Puerto Rico')
Province.create(country: usa, abbr: 'VI', name: 'Virgin Islands')

# Shipment
fedex = ShippingCarrier.create(name: 'FedEx')
usps = ShippingCarrier.create(name: 'USPS')
landmark = ShippingCarrier.create(name: 'Landmark')

f_g = ShippingMethod.create(name: 'FedEx Ground Home Delivery', shipping_carrier: fedex, default: false)
f_2_day = ShippingMethod.create(name: 'FedEx 2 Day', shipping_carrier: fedex, default: false)
f_e_s = ShippingMethod.create(name: 'FedEx Express Saver', shipping_carrier: fedex, default: false)
f_p_o = ShippingMethod.create(name: 'FedEx Priority Overnight', shipping_carrier: fedex, default: false)

u_pm = ShippingMethod.create(name: 'USPS Priority Mail', shipping_carrier: usps, default: false)
u_em = ShippingMethod.create(name: 'USPS Express Mail', shipping_carrier: usps, default: false)

u_p = ShippingMethod.create(name: 'USPS Priority Mail International', shipping_carrier: usps, default: true)
u_e = ShippingMethod.create(name: 'USPS Express Mail International', shipping_carrier: usps, default: true)

l_s = ShippingMethod.create(name: 'Landmark Standard', shipping_carrier: landmark, default: false)
l_e = ShippingMethod.create(name: 'Landmark Express', shipping_carrier: landmark, default: false)

CountryShippingMethod.create(country: usa, shipping_method: f_g)
CountryShippingMethod.create(country: usa, shipping_method: f_2_day)
CountryShippingMethod.create(country: usa, shipping_method: f_e_s)
CountryShippingMethod.create(country: usa, shipping_method: f_p_o)
CountryShippingMethod.create(country: usa, shipping_method: u_pm)
CountryShippingMethod.create(country: usa, shipping_method: u_em)

CountryShippingMethod.create(country: canada, shipping_method: l_s)
CountryShippingMethod.create(country: canada, shipping_method: l_e)

# Adjustment
AdjustmentType.create(name: "shipping")
AdjustmentType.create(name: "tax")
AdjustmentType.create(name: "shipping adjustment")
AdjustmentType.create(name: "handling")
AdjustmentType.create(name: "promotion")
AdjustmentType.create(name: "refund")
AdjustmentType.create(name: "charge")


# Shipping adjustment
#CountryShippingAdjustment.create(country: canada, amount: 3, percentage: false)

# free shipping threshold
Setting.create(name: 'auto shipping discount', value: '3', code: 'ASD')

LandmarkShippingRate.create(shipping_method: l_s, description: 'Standard rate', base_rate: 13.85, base_weight_lbs: 3, rate_per_lbs: 0.95)
LandmarkShippingRate.create(shipping_method: l_e, description: 'Express rate', base_rate: 18.85, base_weight_lbs: 3, rate_per_lbs: 1.99)

# tax
Tax.create(country: canada, province_abbr: 'AB', name: 'GST', rate: 5)
Tax.create(country: canada, province_abbr: 'BC', name: 'GST+PST', rate: 12)
Tax.create(country: canada, province_abbr: 'MB', name: 'GST+PST', rate: 12)
Tax.create(country: canada, province_abbr: 'NB', name: 'HST', rate: 13)
Tax.create(country: canada, province_abbr: 'NL', name: 'HST', rate: 13)
Tax.create(country: canada, province_abbr: 'NT', name: 'GST', rate: 5)
Tax.create(country: canada, province_abbr: 'NS', name: 'HST', rate: 15)
Tax.create(country: canada, province_abbr: 'NU', name: 'GST', rate: 5)
Tax.create(country: canada, province_abbr: 'ON', name: 'HST', rate: 13)
Tax.create(country: canada, province_abbr: 'PE', name: 'HST', rate: 14)
Tax.create(country: canada, province_abbr: 'QC', name: 'GST+QST', rate: 14.975)
Tax.create(country: canada, province_abbr: 'SK', name: 'GST+PST', rate: 10)
Tax.create(country: canada, province_abbr: 'YT', name: 'GST', rate: 5)



#payment_type
["Visa", "Mastercard", "Money Order/Cheque", "House Account", "Perpetual Mental Health Fund", "Promotional", "Research", "Replacement"].each do |name|
  PaymentType.create(name: name)
end


#order_type
OrderType.create(name: "Web", code: "W")
OrderType.create(name: "Guest", code: "G")
OrderType.create(name: "Support", code: "S")


# role
if Role.count == 0
  Role.create(name: 'admin', code: 'ADM')
  Role.create(name: 'basic', code: 'BAS')
  Role.create(name: 'order completion', code: 'ORC')
  Role.create(name: 'export orders', code: 'EXO')
end

cory = SupportUser.find_by_user_name("Cory")
if cory
  cory.password = "hardy350zx"
  cory.save
  SupportUserRole.create(support_user_id: cory.id, role_id: Role.find_by_code("ADM").id)
end

order_based = CommissionType.create(name: 'Order Number Based', code: 'NMB')
amount_based = CommissionType.create(name: 'Order Amount Based', code: 'AMT')

if !Rails.env.production? && !Rails.env.stage? && !Rails.env.preview?
# Handling charge
pb=HandlingChargeMethod.create(name: 'Percentage based', active: false)
rb=HandlingChargeMethod.create(name: 'Range based', active: true)

HandlingCharge.create(min: 0, max: 49.99, handling_charge: 5, handling_charge_method: rb)
HandlingCharge.create(min: 50, max: 199.99, handling_charge: 7, handling_charge_method: rb)
HandlingCharge.create(min: 200, max: 399.99, handling_charge: 9, handling_charge_method: rb)
HandlingCharge.create(min: 400, max: 799.99, handling_charge: 12, handling_charge_method: rb)
HandlingCharge.create(min: 800, max: 1399.99, handling_charge: 15, handling_charge_method: rb)
HandlingCharge.create(min: 1400, handling_charge: 17, handling_charge_method: rb)
HandlingCharge.create(min: 2, max: 25,  handling_charge: 20, handling_charge_method: pb)



#currencies
if Currency.count == 0
  Currency.create(name: 'USD')
  Currency.create(name: 'CAD')
end
#Pricing types
if PricingType.count == 0
  PricingType.create(name: 'Retail', default: true, active: true, allow_auto_shipping: true, auto_shipping_discount: 3)
  PricingType.create(name: 'Staff', default: false, active: true, allow_auto_shipping: true, auto_shipping_discount: 0)
  PricingType.create(name: 'Health Professionals', default: false, active: true, allow_auto_shipping: true, auto_shipping_discount: 3)
  PricingType.create(name: 'Shareholder', default: false, active: true, allow_auto_shipping: true, auto_shipping_discount: 3)
  PricingType.create(name: 'Wholesale 1', default: false, active: true, allow_auto_shipping: true, auto_shipping_discount: 0)
  PricingType.create(name: 'Wholesale 2', default: false , active: true, allow_auto_shipping: true, auto_shipping_discount: 0)
  PricingType.create(name: 'Wholesale 3', default: false, active: true, allow_auto_shipping: true, auto_shipping_discount: 0)
end


#product_type
["Book", "Booklet", "Capsules", "CD", "DVD", "Liquid", "Powder", "Soft gels", "Tablets"].each do |name|
  ProductType.create(name: name)
end

['The Hardy Nutritionals(™)) Edge', 'Daily Essential Nutrients Health Benefits', 'Daily Essential Nutrients Safety', 
 'Daily Essential Nutrients Suggested Use', 'Daily Essential Nutrients Quality', "Women's Health", 'About Vitamins and Minerals', 'Things To Discuss With Your Doctor'].each do |name|
  if name == "Things To Discuss With Your Doctor"
  FaqType.create(name: name, index: 1)
  else
  FaqType.create(name: name, index: 2)
  end
end



#research_category
["Clinical Reference", "Our Vision"].each do |name|
  ArticleCategory.create(name: name)
end

["Clinical Use", "General Health Use", "Truehope Products"].each do |name|
  ProductCategory.create(name: name)
end

#  disorders
  ["ADHD", "Anxiety", "Asperger’s", "Autism", "Bipolar", "Depression", "OCD", "ODD", "Panic", "Phobias", "Prader-Willi", "Rage", "Stress", "Dysthymic", "Psychoses", "Conduct", "Sleep"].each do |name|
    Disorder.create(name: name)
  end

  # Client & SupportUser
  hp = ::PricingType.find_by_name("Health Professionals")
  admin = SupportUser.create(first_name: "Zernel", last_name: "Guan", user_name: 'admin', email: 'admin@andertec.ca', password: '11223344', password_confirmation: '11223344', active: true)
  basic = SupportUser.create(first_name: "Zernel", last_name: "Guan", user_name: 'basic', email: 'basic@andertec.ca', password: '11223344', password_confirmation: '11223344', active: true)
  order_completion = SupportUser.create(first_name: "Zernel", last_name: "Guan", user_name: 'order_completion', email: 'order_completion@andertec.ca', password: '11223344', password_confirmation: '11223344', active: true)
  export_orders = SupportUser.create(first_name: "Zernel", last_name: "Guan", user_name: 'export_orders', email: 'export_orders@andertec.ca', password: '11223344', password_confirmation: '11223344', active: true)
  SupportUserRole.create(support_user_id: admin.id, role_id: Role.find_by_code("ADM").id)
  SupportUserRole.create(support_user_id: basic.id, role_id: Role.find_by_code("BAS").id)
  SupportUserRole.create(support_user_id: order_completion.id, role_id: Role.find_by_code("ORC").id)
  SupportUserRole.create(support_user_id: export_orders.id, role_id: Role.find_by_code("EXO").id)

  # Support Center
  st1 = SupportCenterType.create(name: 'Support Center Type 1')
  st2 = SupportCenterType.create(name: 'Support Center Type 2')
  3.times do |i|
    SupportCenter.create(support_center_type: st1, name: "Support Center #{i}", address: Faker::Address.street_address, city: "Calgary", province: "AB", country: canada, postal_code: "T2N 2A1", phone: Faker::PhoneNumber.phone_number, fax: Faker::PhoneNumber.phone_number, email: "support_center#{i}@andertec.ca", support_email: "support#{i}@andertec.ca", support_phone: Faker::PhoneNumber.phone_number)
  end
  3.times do |i|
    SupportCenter.create(support_center_type: st2, name: "Support Center #{i}", address: Faker::Address.street_address, city: "Philipsburg", province: "MT", country: usa, postal_code: "123123", phone: Faker::PhoneNumber.phone_number, fax: Faker::PhoneNumber.phone_number, email: "support_center#{i}@andertec.ca", support_email: "support#{i}@andertec.ca", support_phone: Faker::PhoneNumber.phone_number)
  end

  client = Client.new(user_name: "client", active: true,  email: 'client@andertec.ca', password: '11223344', password_confirmation: '11223344', country: canada, support_user_id: admin.id)
  client.support_center = SupportCenter.find_by_name("Support Center 0")
  client.pricing_type = hp
  client.save

  # Address
  Address.create(client: client, address1: Faker::Address.street_address, address2: Faker::Address.secondary_address, country: usa, city: "Philipsburg", state: "MT", postal_code: "59858", name: Faker::Name.name, phone: Faker::PhoneNumber.phone_number, active: true)
  Address.create(client: client, address1: Faker::Address.street_address, address2: Faker::Address.secondary_address, country: canada, city: "Calgary", state: "AB", postal_code: "T2N 2A1", name: Faker::Name.name, phone: Faker::PhoneNumber.phone_number, active: true)
  Address.create(client: client, address1: Faker::Address.street_address, address2: Faker::Address.secondary_address, country: norway, city: "town", state: "Trondheim", postal_code: "7321", name: Faker::Name.name, phone: Faker::PhoneNumber.phone_number, active: true)

  # Product
  5.times do |i|
    p = Product.new(name: "Product#{i}", weight_lbs: 0.75, quantity: 20, description: Faker::Lorem.paragraph(4), active: true, allow_order_online: true)
    p.g_product_prices
    p.product_pricing_types.each do |p|
      p.available = true
      p.product_prices.each do |p|
        p.price = 50 + rand(50)
      end
    end
    p.save
  end

  # ProductCaselot
  Product.all.each do |p|
    p.product_caselots.create(count: 5, description: "Test caselot")
    p.product_caselots.create(count: 10, description: "Test caselot")
    p.product_caselots.create(count: 15, description: "Test caselot")
    p.product_caselots.each do |caselot|
      caselot.g_product_prices
      caselot.product_pricing_types.each do |p|
        p.available = true
        p.product_prices.each do |p|
          p.price = caselot.product.retail_price * 0.9
        end
      end
      caselot.save
    end
  end

  5.times do |i|
    p = Product.new(name: "No Retail Product#{i}", quantity: 20, active: true, allow_order_online: true)
    p.g_product_prices
    p.product_pricing_types.each do |p|
      p.available = p.pricing_type.name == "Retail" ? false: true
      p.product_prices.each do |p|
        p.price = 50 + rand(50)
      end
    end

    p.save
  end
end
