class AddPackageNumberToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :package_number, :integer
  end
end
