class CreateHandlingChargeMethods < ActiveRecord::Migration
  def change
    create_table :handling_charge_methods do |t|
      t.string :name
      t.boolean :active
      t.timestamps
    end
  end
end
