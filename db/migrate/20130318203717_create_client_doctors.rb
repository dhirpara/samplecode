class CreateClientDoctors < ActiveRecord::Migration
  def change
    create_table :client_doctors do |t|
      t.integer :client_id
      t.integer :name
      t.string :doctor_type
      t.string :work_phone
      t.string :home_phone
      t.string :fax
      t.string :email
      t.string :address
      t.string :city
      t.string :state
      t.string :postal_code
      t.string :support_status
      t.boolean :active
      t.timestamps
    end
  end
end
