class AddTrnOrderNumberToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :trn_order_number, :string
    add_column :payments, :can_void, :boolean
  end
end
