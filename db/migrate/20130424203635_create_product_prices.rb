class CreateProductPrices < ActiveRecord::Migration
  def change
    create_table :product_prices do |t|
      t.integer :priceable_id
      t.string :priceable_type
      t.integer :currency_id
      t.integer :pricing_type_id
      t.decimal :price, :precision => 8, :scale => 2
      t.boolean :available
      t.timestamps
    end
  end
end
