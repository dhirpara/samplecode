class AddTrnColumnsToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :trn_id, :string
    add_column :payments, :trn_type, :string, limit: 2

    Payment.all.each do |p|
      if p.confirmation_code.present?
        p.trn_id = p.confirmation_code.split(";")[0]
        p.trn_type = p.confirmation_code.split(";")[2]
        p.save
      end
    end
  end
end
