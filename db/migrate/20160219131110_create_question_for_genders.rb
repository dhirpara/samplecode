class CreateQuestionForGenders < ActiveRecord::Migration
  def change
    create_table :question_for_genders do |t|
      t.references :gender
      t.references :question

      t.timestamps
    end
    add_index :question_for_genders, :gender_id
    add_index :question_for_genders, :question_id
  end
end
