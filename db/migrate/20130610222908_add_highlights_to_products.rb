class AddHighlightsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :highlights, :text
  end
end
