class ChangeTimeZoneType < ActiveRecord::Migration
  def change
    remove_column :support_users, :time_zone
    add_column :support_users, :time_zone, :integer
  end
end
