class CreateSymptoms < ActiveRecord::Migration
  def change
    create_table :symptoms do |t|
      t.integer :disid
      t.text :question

      t.timestamps
    end
  end
end
