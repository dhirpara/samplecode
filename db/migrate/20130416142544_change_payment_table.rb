class ChangePaymentTable < ActiveRecord::Migration
  def change
    remove_column :payments, :creditcard_id
    add_column :payments, :confirmation_code, :string
  end
end
