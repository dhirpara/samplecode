class AddDescriptionToWithdrawalEffect < ActiveRecord::Migration
  def change
    add_column :withdrawal_effects, :description, :text
  end
end
