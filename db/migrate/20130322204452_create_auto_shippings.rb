class CreateAutoShippings < ActiveRecord::Migration
  def change
    create_table :auto_shippings do |t|
      t.integer :client_id
      t.date :start_date
      t.integer :interval
      t.date :next_order_date
      t.boolean :active
      t.integer :support_user_id
      t.timestamps
    end

    add_index :auto_shippings, :client_id
    add_index :auto_shippings, :support_user_id
  end
end
