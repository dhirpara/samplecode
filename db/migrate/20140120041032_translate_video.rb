class TranslateVideo < ActiveRecord::Migration
  def up
    Video.create_translation_table!({
      title: :string,
      description: :text,
      seo_title: :string,
      seo_keywords: :text
    })

  end

  def down
    Video.drop_translation_table! :migrate_data => true
  end
end
