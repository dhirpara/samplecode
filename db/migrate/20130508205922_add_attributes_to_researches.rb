class AddAttributesToResearches < ActiveRecord::Migration
  def change
    add_column :researches, :image, :string
    add_column :researches, :author, :string
    add_column :researches, :author_image, :string
    add_column :researches, :publish_date, :date
  end
end
