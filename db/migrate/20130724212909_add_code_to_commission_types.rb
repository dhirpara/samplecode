class AddCodeToCommissionTypes < ActiveRecord::Migration
  def change
    add_column :commission_types, :code, :string
  end
end
