class AddSupportUserIdToCommissionRates < ActiveRecord::Migration
  def change
    add_column :commission_rates, :support_user_id, :integer
  end
end
