class CreateFamilies < ActiveRecord::Migration
  def change
    create_table :families do |t|
      t.integer :cid_head
      t.string :name
      t.datetime :date_created
      t.integer :ssid_created

      t.timestamps
    end
  end
end
