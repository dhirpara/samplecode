class AddBillingAddressToPaymentProfile < ActiveRecord::Migration
  def change
    add_column :payment_profiles, :address1, :string
    add_column :payment_profiles, :address2, :string
    add_column :payment_profiles, :city, :string
    add_column :payment_profiles, :state, :string
    add_column :payment_profiles, :name, :string
    add_column :payment_profiles, :postal_code, :string
    add_column :payment_profiles, :phone, :string
    add_column :payment_profiles, :country_id, :integer
    add_index  :payment_profiles, :country_id
  end
end
