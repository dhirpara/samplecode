class ChangeAttributesInProducts < ActiveRecord::Migration
  def change
    change_column :products, :shipping_description, :string
    add_column :products, :featured, :boolean
  end

end
