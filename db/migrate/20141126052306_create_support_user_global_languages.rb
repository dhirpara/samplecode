class CreateSupportUserGlobalLanguages < ActiveRecord::Migration
  def change
    create_table :support_user_global_languages do |t|
      t.belongs_to :support_user
      t.belongs_to :global_language

      t.timestamps
    end
    add_index :support_user_global_languages, :support_user_id
    add_index :support_user_global_languages, :global_language_id
  end
end
