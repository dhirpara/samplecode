class TranslateArticle < ActiveRecord::Migration
  def up
    Article.create_translation_table!({
      title: :string,
      content: :text,
      seo_title: :string,
      seo_keywords: :text
    })
  end

  def down
    Article.drop_translation_table! :migrate_data => true
  end
end
