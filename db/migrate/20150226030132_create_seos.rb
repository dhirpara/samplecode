class CreateSeos < ActiveRecord::Migration
  def change
    create_table :seos do |t|
      t.string :url
      t.string :title
      t.string :keywords
      t.string :description

      t.timestamps
    end
  end
end
