class CreateOrderReports < ActiveRecord::Migration
  def change
    create_table :order_reports do |t|
      t.integer :order_id
      t.decimal :subtotal,          :precision => 8, :scale => 2
      t.decimal :tax,          :precision => 8, :scale => 2
      t.decimal :shipping,          :precision => 8, :scale => 2
      t.decimal :handling,          :precision => 8, :scale => 2
      t.decimal :other,          :precision => 8, :scale => 2
      t.decimal :total,          :precision => 8, :scale => 2
      t.decimal :received_total,          :precision => 8, :scale => 2

      t.timestamps
    end

    Order.where(state: "complete").each do |order|
      order_report = OrderReport.where("order_id = ? and DATE(created_at) = DATE(?)", order.id, Time.zone.now).first || OrderReport.new
      order_report.assign_attributes({
        order_id: order.id,
        subtotal: order.subtotal,
        tax: order.db_tax_charge,
        shipping: order.db_shipping_charge,
        handling: order.db_handling_charge,
        other: order.db_other_charge,
        total: order.total,
        received_total: order.received_payment_total
      })

      order_report.created_at = order.completed_at
      order_report.save
    end
  end
end
