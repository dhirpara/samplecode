class AddPermalinkToResearch < ActiveRecord::Migration
  def change
    add_column :researches, :permalink, :string
  end
end
