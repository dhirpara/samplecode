class AddShippedAtToShipment < ActiveRecord::Migration
  def change
    add_column :shipments, :shipped_at, :datetime
  end
end
