class ChangeValueToAmount < ActiveRecord::Migration
  def change
    rename_column :country_shipping_adjustments, :value, :amount
  end
end
