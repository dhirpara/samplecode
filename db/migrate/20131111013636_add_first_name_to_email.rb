class AddFirstNameToEmail < ActiveRecord::Migration
  def change
    add_column :emails, :first_name, :string
  end
end
