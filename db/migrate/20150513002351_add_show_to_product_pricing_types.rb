class AddShowToProductPricingTypes < ActiveRecord::Migration
  def change
    add_column :product_pricing_types, :show, :boolean, default: false
  end
end
