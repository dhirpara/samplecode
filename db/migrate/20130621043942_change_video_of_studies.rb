class ChangeVideoOfStudies < ActiveRecord::Migration
  def change
    remove_column :studies, :video
    add_column :studies, :video_id, :integer
  end
end
