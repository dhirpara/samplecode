class CreateStudyDisorders < ActiveRecord::Migration
  def change
    create_table :study_disorders do |t|
      t.integer :study_id
      t.integer :disorder_id

      t.timestamps
    end
  end
end
