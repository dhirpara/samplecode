class ChangeShippingStatusIdToState < ActiveRecord::Migration
  def change
    rename_column :shipments, :shipment_status_id, :state
    change_column :shipments, :state, :string
  end
end
