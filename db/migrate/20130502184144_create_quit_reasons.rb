class CreateQuitReasons < ActiveRecord::Migration
  def change
    create_table :quit_reasons do |t|
      t.string :name
      t.boolean :deleted
      t.timestamps
    end
  end
end
