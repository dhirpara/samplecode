class CreateAutoShipReports < ActiveRecord::Migration
  def change
    create_table :auto_ship_reports do |t|
      t.belongs_to :client
      t.belongs_to :country
      t.belongs_to :auto_shipping
      t.belongs_to :order
      t.string :email
      t.date :date_of_shipment
      t.integer :interval
      t.string :state
      t.decimal :total,          :precision => 8, :scale => 2

      t.timestamps
    end
    add_index :auto_ship_reports, :client_id
    add_index :auto_ship_reports, :country_id
  end
end
