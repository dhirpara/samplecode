class AddCreditcardIdToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :creditcard_id, :integer
    add_index :payments, :creditcard_id
  end
end
