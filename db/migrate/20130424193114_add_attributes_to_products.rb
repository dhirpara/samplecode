class AddAttributesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :type_id, :integer
    add_column :products, :quantity, :integer
    add_column :products, :unit, :string
    add_column :products, :description, :string
    add_column :products, :shipping_description, :string
    add_column :products, :weight_lbs, :float
    add_column :products, :product_code, :string
    add_column :products, :tariff_code, :string
    add_column :products, :customs_value, :decimal, :precision => 8, :scale => 2
    add_column :products, :chargeable, :boolean
    add_column :products, :allow_order_online, :boolean
    add_column :products, :deleted, :boolean
  end
end
