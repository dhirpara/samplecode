class AddLastNameToEmails < ActiveRecord::Migration
  def change
    add_column :emails, :last_name, :string
  end
end
