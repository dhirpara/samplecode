class CreateSupportCenterTypes < ActiveRecord::Migration
  def change
    create_table :support_center_types do |t|
      t.string :name #CSCT_typename
      t.timestamps
    end
  end
end
