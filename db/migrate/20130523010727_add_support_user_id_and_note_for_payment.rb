class AddSupportUserIdAndNoteForPayment < ActiveRecord::Migration
  def change
    add_column :payments, :support_user_id, :integer
    add_index :payments, :support_user_id
    change_column :payments, :note, :text
  end
end
