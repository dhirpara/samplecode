class ChangeOrderToIndex < ActiveRecord::Migration
  def change
    rename_column :support_groups, :order, :index
    rename_column :introduction_types, :order, :index
  end
end
