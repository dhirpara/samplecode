class AddAltToImage < ActiveRecord::Migration
  def change
    add_column :product_images, :alt, :string
    add_column :articles, :author_image_alt, :string
    add_column :articles, :image_alt, :string
    add_column :videos, :image_alt, :string
    add_column :studies, :journal_cover_alt, :string
  end
end
