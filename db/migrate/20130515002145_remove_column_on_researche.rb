class RemoveColumnOnResearche < ActiveRecord::Migration
  def change
    remove_column :researches, :two_column_featured
    remove_column :researches, :three_column_featured
    add_column :researches, :column_number, :integer
  end
end
