class ChangeAutoShipSettingActiveDefaultToFalseTwo < ActiveRecord::Migration
  def change
    change_column :auto_shippings, :active, :boolean, default: false
  end
end
