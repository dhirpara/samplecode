class ChangeClients < ActiveRecord::Migration
  def change
    remove_column :clients, :quit_reason
    add_column :clients, :quit_reason_id, :integer
    add_column :clients, :age_category_id, :integer
    add_index :clients, :quit_reason_id
    add_index :clients, :age_category_id
  end
end
