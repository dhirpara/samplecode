class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :user_name
      t.string :password
      t.string :first_name
      t.string :last_name
      t.string :middle_name
      t.date :date_of_birth #dob
      t.string :gender, :limit=>1
      t.string :guardian
      t.boolean :active
      t.boolean :deleted
      t.string :address_1
      t.string :address_2 #apt_num
      t.string :address_3 #Address3
      t.string :city
      t.string :state
      t.integer :country_id
      t.string :postal_code
      t.string :home_phone #c_hphone
      t.string :work_phone #c_whome
      t.string :cell_phone #cell
      t.string :pager
      t.string :fax
      t.string :email
      t.string :work_email # c_wemail
      t.string :contact_day #best day to contact
      t.integer :contact_time_id #contact_times  - best time to contact
      t.integer :time_zone
      t.datetime :contact_date #actually date time of contact
      t.date :next_contact_date
      t.date :start_date
      t.date :start_supplement
      t.date :date_active
      t.integer :introduction_type_id #table introduction_types
      t.integer :register_method_id # registredby, new table register_methods
      t.date :quit_date
      t.string :quit_reason
      t.text :general_comments
      t.boolean :admin_order
      t.boolean :admin_support
      t.boolean :allow_order
      t.boolean :require_data
      t.date :data_start_date
      t.date :data_stop_date
      t.float :discount
      t.boolean :main_participant
      t.integer :call_frequency_id #client_call_frequency new table call_frequencies
      t.boolean :self_registered
      t.boolean :cne_professional
      t.boolean :mental_wellness
      t.text :mental_wellness_details
      t.boolean :single_order_client
      t.string :encrypted_password, :null => false, :default => ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip



      t.integer :pricing_type_id #C_CTYID  pricing_types(Clients_Types)
      t.integer :client_family_id #C_FID  client_families(Families)
      t.integer :support_center_id #C_CSCID support_centers ( Clients_Support_Centers)
      t.integer :support_group_id #CSGID support_groups (Clients_Support_Groups)
      t.integer :language_id #LANID languages (Languages)
      t.integer :support_user_id
      t.timestamps
    end

    add_index :clients, :email,                :unique => true
    add_index :clients, :reset_password_token, :unique => true
    add_index :clients, :country_id
    add_index :clients, :contact_time_id
    add_index :clients, :introduction_type_id
    add_index :clients, :register_method_id
    add_index :clients, :pricing_type_id
    add_index :clients, :client_family_id
    add_index :clients, :support_center_id
    add_index :clients, :support_group_id
    add_index :clients, :language_id
    add_index :clients, :support_user_id
  end
end
