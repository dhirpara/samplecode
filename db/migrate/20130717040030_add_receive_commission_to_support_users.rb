class AddReceiveCommissionToSupportUsers < ActiveRecord::Migration
  def change
    add_column :support_users, :receive_commission, :boolean, default: false
  end
end
