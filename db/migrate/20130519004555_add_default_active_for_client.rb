class AddDefaultActiveForClient < ActiveRecord::Migration
  def change
    change_column :clients, :active, :boolean, default: false
  end
end
