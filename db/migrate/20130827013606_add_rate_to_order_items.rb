class AddRateToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :rate, :decimal, precision: 8, scale: 2, default: 1
  end
end
