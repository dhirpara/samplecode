class AddPriorityToCallFrequency < ActiveRecord::Migration
  def change
    add_column :call_frequencies, :priority, :integer
  end
end
