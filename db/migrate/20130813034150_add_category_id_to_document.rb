class AddCategoryIdToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :article_category_id, :integer
    add_column :documents, :name, :string
  end
end
