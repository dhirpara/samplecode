class TranslateStudy < ActiveRecord::Migration
  def up
    Study.create_translation_table!({
      title: :string,
      brief_description: :text,
      article_title: :string
    })

  end

  def down
    Study.drop_translation_table! :migrate_data => true
  end
end
