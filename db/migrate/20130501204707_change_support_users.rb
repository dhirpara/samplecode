class ChangeSupportUsers < ActiveRecord::Migration
  def change
    remove_column :support_users, :admin
    remove_column :support_users, :super_admin
    add_column :support_users, :role_id, :integer
    add_index :support_users, :role_id
  end

end
