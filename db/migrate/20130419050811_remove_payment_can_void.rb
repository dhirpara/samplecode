class RemovePaymentCanVoid < ActiveRecord::Migration
  def change
    remove_column :payments, :can_void
    remove_column :payments, :trn_order_number
    add_column :payments, :ref_trn_id, :string
  end
end
