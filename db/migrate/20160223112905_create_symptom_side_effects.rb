class CreateSymptomSideEffects < ActiveRecord::Migration
  def change
    create_table :symptom_side_effects do |t|
      t.references :symptom
      t.references :side_effect

      t.timestamps
    end
    add_index :symptom_side_effects, :symptom_id
    add_index :symptom_side_effects, :side_effect_id
  end
end
