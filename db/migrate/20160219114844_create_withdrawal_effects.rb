class CreateWithdrawalEffects < ActiveRecord::Migration
  def change
    create_table :withdrawal_effects do |t|
      t.text :name

      t.timestamps
    end
  end
end
