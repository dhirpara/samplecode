class ChangeOrderItemsVariantIdToProductId < ActiveRecord::Migration
  def change
    rename_column :order_items, :variant_id, :product_id
    #rename_index :order_items, :variant_id, :product_id
    remove_index :order_items, :variant_id
    add_index :order_items, :product_id
  end
end
