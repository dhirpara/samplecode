class AddIndexToFaqs < ActiveRecord::Migration
  def change
    add_column :faqs, :index, :integer
  end
end
