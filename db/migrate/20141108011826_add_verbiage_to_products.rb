class AddVerbiageToProducts < ActiveRecord::Migration
  def change
    add_column :products, :verbiage, :text

    Product.all.each do |p|
      p.verbiage = "<p>***We've just had a massive response from TV coverage!***</p>
          <ul>
            <li><strong>Existing customers:</strong> log into your account to place your order &ndash; just like usual!</li>
            <li><strong>New customers:</strong> click on the \"PRE-ORDER NOW\" button (below) to get in the que.</li>
          </ul>"
      p.save
    end
  end
end
