class CreateProvinces < ActiveRecord::Migration
  def change
    create_table :provinces do |t|
      t.string :name
      t.string :abbr, limit: 2
      t.timestamps
    end
  end
end
