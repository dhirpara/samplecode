class AddAbbrToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :abbr, :string, limit: 2
  end
end
