class CreateHandlingCharges < ActiveRecord::Migration
  def change
    create_table :handling_charges do |t|
      t.decimal :min_order_total, :precision => 8, :scale => 2
      t.decimal :max_order_total, :precision => 8, :scale => 2
      t.decimal :handling_charge, :precision => 8, :scale => 2
      t.timestamps
    end
  end
end
