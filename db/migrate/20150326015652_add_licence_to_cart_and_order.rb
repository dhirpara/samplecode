class AddLicenceToCartAndOrder < ActiveRecord::Migration
  def change
    add_column :carts, :licence, :string
    add_column :orders, :licence, :string
    add_column :referral_codes, :need_licence, :boolean, default: :false
  end
end
