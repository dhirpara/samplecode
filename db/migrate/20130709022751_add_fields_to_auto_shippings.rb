class AddFieldsToAutoShippings < ActiveRecord::Migration
  def change
    add_column :auto_shippings, :shipping_method_id, :integer
    add_column :auto_shippings, :shipping_address_id, :integer
    add_column :auto_shippings, :payment_address_id, :integer
  end
end
