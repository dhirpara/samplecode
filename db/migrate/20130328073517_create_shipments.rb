class CreateShipments < ActiveRecord::Migration
  def change
    create_table :shipments do |t|
      t.integer :order_id
      t.float :weight_lbs
      t.integer :shipment_status_id
      t.integer :shipping_method_id

      t.timestamps
    end
    add_index :shipments, :order_id
    add_index :shipments, :shipment_status_id
    add_index :shipments, :shipping_method_id
  end
end
