class AddCurrencyIdToPaymentProfiles < ActiveRecord::Migration
  def change
    add_column :payment_profiles, :currency_id, :integer

    PaymentProfile.all.each do |p|
      if payment = Client.find(p.client_id).payments.select{|a| a.is_cc_type? && a.received && a.payment_profile_id.present?}.sort_by(&:id).last
        p.currency_id = payment.order.currency_id
        p.save
      else
        p.destroy
      end
    end

    if country = Country.find_by_id(2)
      currency = Currency.create(name: "USA")
      country.update_attributes(currency_id: currency.id)
    end
  end
end
