class CreateRegistrationMethods < ActiveRecord::Migration
  def change
    create_table :registration_methods do |t|
      t.string :name
      t.timestamps
    end
  end
end
