class CreatePreOrderReports < ActiveRecord::Migration
  def change
    create_table :pre_orders do |t|
      t.belongs_to :order
      t.belongs_to :payment_profile

      t.timestamps
    end
    add_index :pre_orders, :order_id
    add_index :pre_orders, :payment_profile_id
  end
end
