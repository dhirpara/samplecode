class CreateClientLimtingFactors < ActiveRecord::Migration
  def change
    create_table :client_limiting_factors do |t|
      t.belongs_to :client
      t.belongs_to :limiting_factor

      t.timestamps
    end
    add_index :client_limiting_factors, :client_id
    add_index :client_limiting_factors, :limiting_factor_id
  end
end
