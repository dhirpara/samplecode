class CreateClientDiagnoses < ActiveRecord::Migration
  def change
    drop_table :diagnoses

    create_table :diagnoses do |t|
      t.string :name
    end

    create_table :client_diagnoses do |t|
      t.belongs_to :client
      t.belongs_to :diagnosis

      t.timestamps
    end
    add_index :client_diagnoses, :client_id
    add_index :client_diagnoses, :diagnosis_id
  end
end
