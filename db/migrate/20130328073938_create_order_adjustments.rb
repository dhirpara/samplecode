class CreateOrderAdjustments < ActiveRecord::Migration
  def change
    create_table :order_adjustments do |t|
      t.integer :adjustable_id
      t.string :adjustable_type
      t.decimal :amount, :precision => 8, :scale => 2
      t.integer :adjustment_type_id
      t.string :note
      t.timestamps
    end
    add_index :order_adjustments, :adjustable_id
  end
end
