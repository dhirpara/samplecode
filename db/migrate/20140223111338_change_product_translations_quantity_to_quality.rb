class ChangeProductTranslationsQuantityToQuality < ActiveRecord::Migration
  def change
    rename_column :product_translations, :quantity, :quality
  end
end
