class ChangeOrderTypeToOrderTypeId < ActiveRecord::Migration
  def change
    remove_column :orders, :type
    add_column :orders, :order_type_id, :integer
    add_index :orders, :order_type_id
  end
end
