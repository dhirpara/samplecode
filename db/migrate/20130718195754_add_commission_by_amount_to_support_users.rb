class AddCommissionByAmountToSupportUsers < ActiveRecord::Migration
  def change
    add_column :support_users, :commission_by_amount, :boolean, default: false
  end
end
