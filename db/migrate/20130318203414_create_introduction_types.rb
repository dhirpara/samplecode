class CreateIntroductionTypes < ActiveRecord::Migration
  def change
    create_table :introduction_types do |t|
      t.string :name
      t.integer :order
      t.boolean :active
      t.timestamps
    end
  end
end
