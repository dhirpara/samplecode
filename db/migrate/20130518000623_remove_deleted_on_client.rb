class RemoveDeletedOnClient < ActiveRecord::Migration
  def change
    remove_column :clients, :deleted
  end
end
