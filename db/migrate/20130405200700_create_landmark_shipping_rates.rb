class CreateLandmarkShippingRates < ActiveRecord::Migration
  def change
    create_table :landmark_shipping_rates do |t|
      t.integer :shipping_method_id
      t.string :description
      t.decimal :base_rate, :precision => 8, :scale => 2
      t.integer :base_weight_lbs
      t.decimal :rate_per_lbs, :precision => 8, :scale => 2
      t.timestamps
    end
  end
end
