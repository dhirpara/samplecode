class TranslateProductCategory < ActiveRecord::Migration
  def up
    ProductCategory.create_translation_table!({name: :string})
  end

  def down
    ProductCategory.drop_translation_table!
  end
end
