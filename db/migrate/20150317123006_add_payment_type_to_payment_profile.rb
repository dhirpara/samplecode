class AddPaymentTypeToPaymentProfile < ActiveRecord::Migration
  def change
    add_column :payment_profiles, :payment_type_id, :integer
  end
end
