class AddDeletedToClient < ActiveRecord::Migration
  def change
    add_column :clients, :deleted, :boolean, default: false
  end
end
