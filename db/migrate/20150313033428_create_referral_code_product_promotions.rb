class CreateReferralCodeProductPromotions < ActiveRecord::Migration
  def change
    create_table :referral_code_product_promotions do |t|
      t.belongs_to :referral_code
      t.belongs_to :product_promotion

      t.timestamps
    end
    add_index :referral_code_product_promotions, :referral_code_id
    add_index :referral_code_product_promotions, :product_promotion_id

    add_column :product_promotions, :name, :string
  end
end
