class CreateGlobalLanguages < ActiveRecord::Migration
  def change
    create_table :global_languages do |t|
      t.string :name
      t.string :abbr
      t.boolean :active, default: false

      t.timestamps
    end

    [
      {name: "Arabic", abbr: "ar"},
      {name: "Azerbaijani", abbr: "az"},
      {name: "Bulgarian", abbr: "bg"},
      {name: "Catalan", abbr: "ca"},
      {name: "Czech", abbr: "cs"},
      {name: "Danish", abbr: "da"},
      {name: "German", abbr: "de"},
      {name: "Greek", abbr: "el"},
      {name: "English", abbr: "en"},
      {name: "Spanish", abbr: "es"},
      {name: "Estonian", abbr: "et"},
      {name: "Persian", abbr: "fa"},
      {name: "Finnish", abbr: "fi"},
      {name: "French", abbr: "fr"},
      {name: "Hebrew", abbr: "he"},
      {name: "Croatian", abbr: "hr"},
      {name: "Hungarian", abbr: "hu"},
      {name: "Indonesian", abbr: "id"},
      {name: "Icelandic", abbr: "is"},
      {name: "Italian", abbr: "it"},
      {name: "Japanese", abbr: "ja"},
      {name: "Korean", abbr: "ko"},
      {name: "Lithuanian", abbr: "lt"},
      {name: "Latvian", abbr: "lv"},
      {name: "Burmese", abbr: "my"},
      {name: "Norwegian", abbr: "nb"},
      {name: "Dutch", abbr: "nl"},
      {name: "Norwegian", abbr: "no"},
      {name: "Polish", abbr: "pl"},
      {name: "Portuguese", abbr: "pt"},
      {name: "Romanian", abbr: "ro"},
      {name: "Russian", abbr: "ru"},
      {name: "Slovak", abbr: "sk"},
      {name: "Slovenian", abbr: "sl"},
      {name: "Swedish", abbr: "sv"},
      {name: "Thai", abbr: "th"},
      {name: "Turkish", abbr: "tr"},
      {name: "Ukrainian", abbr: "uk"},
      {name: "Vietnamese", abbr: "vi"},
      {name: "Chinese(Simplified)", abbr: "zh-CN"},
      {name: "Chinese(Traditional)", abbr: "zh-TW"}
    ].each do |attributes|
      GlobalLanguage.create(attributes)
    end

    GlobalLanguage.where(abbr: ["en", "zh-CN", "zh-TW"]).each{|l| l.update_attributes(active: true)}
  end
end
