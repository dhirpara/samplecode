class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.integer :client_id
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :first_name
      t.string :last_name
      t.string :postal_code
      t.integer :country_id
      t.boolean :active
      t.timestamps
    end
    add_index :addresses, :country_id
    add_index :addresses, :client_id
  end
end
