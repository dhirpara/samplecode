class CreatePaymentTypes < ActiveRecord::Migration
  def change
    create_table :payment_types do |t|
      t.string :name

      t.timestamps
    end
    add_column :payments, :payment_type_id, :integer
    add_column :payments, :received, :boolean
    add_index :payments, :payment_type_id
  end
end
