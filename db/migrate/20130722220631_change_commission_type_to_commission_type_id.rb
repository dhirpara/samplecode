class ChangeCommissionTypeToCommissionTypeId < ActiveRecord::Migration
  def change
    remove_column :commission_rates, :commission_type
    add_column :commission_rates, :commission_type_id, :integer
  end

end
