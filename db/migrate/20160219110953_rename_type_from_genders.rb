class RenameTypeFromGenders < ActiveRecord::Migration
  def up
  	rename_column :genders, :type, :gender_type
  end

  def down
  end
end
