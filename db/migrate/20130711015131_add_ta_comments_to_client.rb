class AddTaCommentsToClient < ActiveRecord::Migration
  def change
    add_column :clients, :ta_level_id, :integer
    add_index  :clients, :ta_level_id
    add_column :clients, :ta_comments, :string
  end
end
