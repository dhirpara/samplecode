class DropShipmentStatuses < ActiveRecord::Migration
  def change
    drop_table :shipment_statuses
  end
end
