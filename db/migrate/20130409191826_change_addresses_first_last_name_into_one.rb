class ChangeAddressesFirstLastNameIntoOne < ActiveRecord::Migration
  def change
    remove_column :addresses, :last_name
    rename_column :addresses, :first_name, :name
  end
end
