class AddIndexToFaqCategories < ActiveRecord::Migration
  def change
    add_column :faq_types, :index, :integer
  end
end
