class ChangeProductColumn < ActiveRecord::Migration
  def change
    change_column :products, :description, :text
    change_column :products, :shipping_description, :text
    rename_column :products, :deleted, :active
  end
end
