class RemovePasswordFromClients < ActiveRecord::Migration
  def change
    remove_column :clients, :password
  end
end
