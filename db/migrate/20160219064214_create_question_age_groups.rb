class CreateQuestionAgeGroups < ActiveRecord::Migration
  def change
    create_table :question_age_groups do |t|
      t.references :question
      t.references :age_group

      t.timestamps
    end
    add_index :question_age_groups, :question_id
    add_index :question_age_groups, :age_group_id
  end
end
