class CreateContactTimes < ActiveRecord::Migration
  def change
    create_table :contact_times do |t|
      t.string :description
      t.timestamps
    end
  end
end
