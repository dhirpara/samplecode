class AddDeclinedToPreOrders < ActiveRecord::Migration
  def change
    add_column :pre_orders, :declined, :boolean, default: false
    add_index :pre_orders, :declined
  end
end
