class CreateAgeCategories < ActiveRecord::Migration
  def change
    create_table :age_categories do |t|
      t.string :name
      t.boolean :deleted
      t.timestamps
    end
  end
end
