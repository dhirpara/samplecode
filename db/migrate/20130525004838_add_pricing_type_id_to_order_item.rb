class AddPricingTypeIdToOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :pricing_type_id, :integer
    add_index :order_items, :pricing_type_id
  end
end
