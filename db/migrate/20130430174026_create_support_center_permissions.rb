class CreateSupportCenterPermissions < ActiveRecord::Migration
  def change
    create_table :support_center_permissions do |t|
      t.integer :support_user_id #CSCPS_SSID
      t.integer :support_center_id #CSCPS_CSCID
      t.timestamps
    end

    add_index :support_center_permissions, :support_user_id
    add_index :support_center_permissions, :support_center_id
  end
end
