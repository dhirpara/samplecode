class AddAutoShippingAttributesToPricingTypes < ActiveRecord::Migration
  def change
    add_column :pricing_types, :allow_auto_shipping, :boolean
    add_column :pricing_types, :auto_shipping_discount, :integer
  end
end
