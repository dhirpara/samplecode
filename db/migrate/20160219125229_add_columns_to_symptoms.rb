class AddColumnsToSymptoms < ActiveRecord::Migration
  def change
    add_column :symptoms, :name, :string
    add_column :symptoms, :description, :text
  end
end
