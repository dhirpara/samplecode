class AddIndexToResearches < ActiveRecord::Migration
  def change
    add_column :researches, :index, :integer
  end
end
