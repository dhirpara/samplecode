class CreateShippingPromotions < ActiveRecord::Migration
  def change
    create_table :shipping_promotions do |t|
      t.decimal :min_amount,           :precision => 8, :scale => 2
      t.decimal :amount_off,           :precision => 8, :scale => 2
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
