class TranslateDisorder < ActiveRecord::Migration
  def up
    Disorder.create_translation_table!({
      name: :string
    })

  end

  def down
    Disorder.drop_translation_table! :migrate_data => true
  end
end
