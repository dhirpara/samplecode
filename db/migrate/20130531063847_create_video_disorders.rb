class CreateVideoDisorders < ActiveRecord::Migration
  def change
    create_table :video_disorders do |t|
      t.integer :video_id
      t.integer :disorder_id

      t.timestamps
    end

    add_index :video_disorders, :video_id
    add_index :video_disorders, :disorder_id
  end
end
