class AddAddressIdToShipment < ActiveRecord::Migration
  def change
    remove_column :orders, :ship_address_id
    add_column :shipments, :address_id, :integer
  end
end
