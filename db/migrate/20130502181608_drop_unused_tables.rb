class DropUnusedTables < ActiveRecord::Migration
  def change
    drop_table :product_promotions
    drop_table :client_families
    drop_table :support_groups
  end

end
