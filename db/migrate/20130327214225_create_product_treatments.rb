class CreateProductTreatments < ActiveRecord::Migration
  def change
    create_table :product_treatments do |t|
      t.integer :product_id
      t.integer :treatment_id
      t.timestamps
    end

    add_index :product_treatments, :product_id
    add_index :product_treatments, :treatment_id
  end
end
