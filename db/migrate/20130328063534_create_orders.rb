class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :client_id
      t.string :number
      t.string :state
      t.integer :payment_status_id
      t.datetime :completed_at
      t.integer :support_user_id
      t.integer :auto_shipping_id
      t.integer :ship_address_id
      t.timestamps
    end

    add_index :orders, :client_id
    add_index :orders, :payment_status_id
    add_index :orders, :support_user_id
    add_index :orders, :auto_shipping_id
    add_index :orders, :ship_address_id
  end
end
