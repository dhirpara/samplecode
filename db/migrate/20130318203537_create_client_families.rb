class CreateClientFamilies < ActiveRecord::Migration
  def change
    create_table :client_families do |t|
      t.string :name
      t.integer :head_client_id
      t.integer :support_member_id
      t.timestamps
    end
  end
end
