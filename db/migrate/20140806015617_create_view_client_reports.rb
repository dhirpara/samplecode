class CreateViewClientReports < ActiveRecord::Migration
  def change
    create_table :view_client_reports do |t|
      t.belongs_to :client
      t.datetime :datetime
      t.belongs_to :support_user

      t.timestamps
    end
    add_index :view_client_reports, :client_id
    add_index :view_client_reports, :support_user_id
  end
end
