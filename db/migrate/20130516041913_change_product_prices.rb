class ChangeProductPrices < ActiveRecord::Migration
  def change
    remove_column :product_prices, :priceable_id
    remove_column :product_prices, :priceable_type
    remove_column :product_prices, :available
    rename_column :product_prices, :pricing_type_id, :product_pricing_type_id
  end
end
