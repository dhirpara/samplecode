class CreateSupportCenters < ActiveRecord::Migration
  def change
    create_table :support_centers do |t|
      t.integer :support_center_type_id #CSC_CSCTID
      t.string :code
      t.string :name
      t.string :address
      t.string :city
      t.string :province #CSC_State
      t.integer :country_id #CSC_COID
      t.string :postal_code
      t.string :phone
      t.string :fax
      t.string :email
      t.string :support_email
      t.string :support_phone
      t.boolean :deleted
      t.timestamps
    end

    add_index :support_centers, :support_center_type_id
  end
end
