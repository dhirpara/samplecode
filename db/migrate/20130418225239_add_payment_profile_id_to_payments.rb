class AddPaymentProfileIdToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :payment_profile_id, :integer
    add_index :payments, :payment_profile_id
  end
end
