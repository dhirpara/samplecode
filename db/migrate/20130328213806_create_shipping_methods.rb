class CreateShippingMethods < ActiveRecord::Migration
  def change
    create_table :shipping_methods do |t|
      t.string :name
      t.integer :shipping_carrier_id
      t.boolean :default
      t.timestamps
    end

    add_index :shipping_methods, :shipping_carrier_id
  end
end
