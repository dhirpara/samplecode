class AddDeletedAtToReferralCodes < ActiveRecord::Migration
  def change
    add_column :referral_codes, :deleted_at, :datetime
    add_index :referral_codes, :deleted_at
  end
end
