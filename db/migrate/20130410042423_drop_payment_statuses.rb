class DropPaymentStatuses < ActiveRecord::Migration
  def change
    drop_table :payment_statuses
    remove_column :orders, :payment_status_id
    add_column :orders, :payment_state, :string
  end
end
