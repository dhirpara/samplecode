class ChangeAutoShippingColumn < ActiveRecord::Migration
  def change
    rename_column :auto_shippings, :shipping_address_id, :address_id
    remove_column :auto_shippings, :payment_address_id
  end
end
