class CreateCountryShippingAdjustments < ActiveRecord::Migration
  def change
    create_table :country_shipping_adjustments do |t|
      t.integer :country_id
      t.decimal :value, :precision => 8, :scale => 2
      t.boolean :percentage
      t.timestamps
    end
  end
end
