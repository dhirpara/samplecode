class CreateCommissionReports < ActiveRecord::Migration
  def change
    create_table :commission_reports do |t|
      t.belongs_to :order
      t.belongs_to :support_user
      t.date :date
      t.decimal :amount,     :precision => 8, :scale => 2
      t.string :description
      t.integer :rate

      t.timestamps
    end
    add_index :commission_reports, :order_id
    add_index :commission_reports, :support_user_id
  end
end
