class ChangeStateToProvince < ActiveRecord::Migration
  def change
    rename_column :clients, :state, :province
    rename_column :client_doctors, :state, :province
  end
end
