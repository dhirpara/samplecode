class CreateSupportUsers < ActiveRecord::Migration
  def change
    create_table :support_users do |t|
      t.string :first_name
      t.string :last_name
      t.string :username
      t.string :password
      t.boolean :support_desk
      t.boolean :admin
      t.boolean :super_admin
      t.boolean :data_enter
      t.boolean :admin_orders
      t.boolean :order_desk
      t.boolean :time_zone
      t.integer :client_id
      t.integer :support_center_id

      t.string :email,              :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      t.timestamps
    end

    add_index :support_users, :email,                :unique => true
    add_index :support_users, :reset_password_token, :unique => true
    add_index :support_users, :support_center_id
  end
end
