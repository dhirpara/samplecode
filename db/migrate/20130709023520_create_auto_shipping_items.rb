class CreateAutoShippingItems < ActiveRecord::Migration
  def change
    create_table :auto_shipping_items do |t|
      t.integer :product_id
      t.integer :auto_shipping_id
      t.integer :quantity

      t.timestamps
    end
  end
end
