class CreateCreditcards < ActiveRecord::Migration
  def change
    create_table :creditcards do |t|
      t.string :first_name
      t.string :last_name
      t.string :last_digits
      t.integer :month
      t.integer :year
      t.string :verification_value
      t.integer :address_id
      t.timestamps
    end
  end
end
