class CreateTaxes < ActiveRecord::Migration
  def change
    create_table :taxes do |t|
      t.integer :country_id
      t.string :province_abbr, :limit=>2
      t.string :name
      t.integer :rate
      t.timestamps
    end
  end
end
