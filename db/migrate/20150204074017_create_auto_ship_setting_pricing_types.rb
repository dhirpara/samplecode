class CreateAutoShipSettingPricingTypes < ActiveRecord::Migration
  def change
    create_table :auto_ship_setting_pricing_types do |t|
      t.belongs_to :auto_ship_setting
      t.belongs_to :pricing_type

      t.timestamps
    end
    add_index :auto_ship_setting_pricing_types, :auto_ship_setting_id
    add_index :auto_ship_setting_pricing_types, :pricing_type_id
  end
end
