class ChangeProductPromotionColumn < ActiveRecord::Migration
  def change
    create_table :product_promotions do |t|
      t.integer :need_purchase_product_id
      t.integer :need_purchase_quantity
      t.integer :promotion_product_id
      t.decimal :promotion_quantity_or_rate
      t.integer :product_promotion_method_id
      t.integer :repeated_time
      t.date :start_date
      t.date :end_date
    end
  end
end
