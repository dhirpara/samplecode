class ChangeReceivedTotalToPaymentReceived < ActiveRecord::Migration
  def change
    rename_column :order_reports, :received_total, :payment_received

    OrderReport.all.each do |r|
      r.update_attributes(payment_received: PaymentReport.where("order_id =? and payment_type_id in (?) and DATE(created_at) = DATE(?)", r.order_id, PaymentType.actual_type_id, r.created_at).sum(&:amount))
    end
  end
end
