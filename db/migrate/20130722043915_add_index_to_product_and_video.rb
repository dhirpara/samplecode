class AddIndexToProductAndVideo < ActiveRecord::Migration
  def change
    add_column :products, :index, :integer
    add_column :videos, :index, :integer
  end
end
