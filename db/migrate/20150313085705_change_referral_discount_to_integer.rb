class ChangeReferralDiscountToInteger < ActiveRecord::Migration
  def change
    remove_column :referral_codes, :discount
    add_column :referral_codes, :discount, :integer
  end
end
