class ChangeResearchIdToArticleIdOnArticleDisorders < ActiveRecord::Migration
  def change
    rename_column :article_disorders, :research_id, :article_id
  end
end
