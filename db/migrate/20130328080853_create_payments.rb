class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :order_id
      t.decimal :amount, :precision => 8, :scale => 2
      t.string :error
      t.string :error_code
      t.string :message
      t.string :note
      t.timestamps
    end
    add_index :payments, :order_id
  end
end
