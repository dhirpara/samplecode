class CreatePromotionPricingTypes < ActiveRecord::Migration
  def change
    create_table :promotion_pricing_types do |t|
      t.integer :promotion_id
      t.string :promotion_type
      t.belongs_to :pricing_type

      t.timestamps
    end

    add_index :promotion_pricing_types, :pricing_type_id
  end
end
