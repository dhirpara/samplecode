class AddCurrenyIdToClientsAndOrders < ActiveRecord::Migration
  def change
    add_column :clients, :currency_id, :integer
    add_column :orders, :currency_id, :integer
    add_index :clients, :currency_id
    add_index :orders, :currency_id
  end
end
