class TranslateProducts < ActiveRecord::Migration
  def up
    Product.create_translation_table!({
      name: :string,
      seo_title: :string,
      seo_keywords: :text,
      highlights: :text,
      description: :text,
      suggested_use: :text,
      supplement_facts: :text,
      quantity: :text
    })
  end

  def down
    Product.drop_translation_table! :migrate_data => true
  end
end
