class CreateDrugs < ActiveRecord::Migration
  def change
    create_table :drugs do |t|
      t.integer :cid
      t.integer :dlid
      t.text :label
      t.float :amount
      t.string :unit
      t.text :comments
      t.string :previous
      t.datetime :date_stopped
      t.integer :ssid
      t.boolean :deleted, default: false
      t.integer :ssid_deleted
      t.datetime :delete_date

      t.timestamps
    end
  end
end
