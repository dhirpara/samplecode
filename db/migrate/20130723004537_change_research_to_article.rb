class ChangeResearchToArticle < ActiveRecord::Migration
  def change
    rename_column :researches, :research_category_id, :article_category_id
    rename_table :researches, :articles
    rename_table :research_disorders, :article_disorders
    rename_table :research_categories, :article_categories
  end
end
