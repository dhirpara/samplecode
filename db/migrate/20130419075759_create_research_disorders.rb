class CreateResearchDisorders < ActiveRecord::Migration
  def change
    create_table :research_disorders do |t|
      t.integer :research_id
      t.integer :disorder_id

      t.timestamps
    end
    add_index :research_disorders, :research_id
    add_index :research_disorders, :disorder_id
  end
end
