class AddUseForReferralCodeCheckboxToPromotion < ActiveRecord::Migration
  def change
    add_column :product_promotions, :use_only_for_referral_code, :boolean, default: false
  end
end
