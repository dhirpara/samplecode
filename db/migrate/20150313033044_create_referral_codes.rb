class CreateReferralCodes < ActiveRecord::Migration
  def change
    create_table :referral_codes do |t|
      t.string :code
      t.string :url
      t.string :discount
      t.boolean :percentage, default: false
      t.belongs_to :support_user
      t.belongs_to :pricing_type
      t.date :start_date
      t.date :end_date
      t.integer :number_of_times

      t.timestamps
    end
    add_index :referral_codes, :support_user_id
    add_index :referral_codes, :pricing_type_id
  end
end
