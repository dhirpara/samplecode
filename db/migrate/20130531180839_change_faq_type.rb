class ChangeFaqType < ActiveRecord::Migration
  def change
    remove_column :faqs, :faq_type
    add_column :faqs, :faq_type_id, :integer
  end
end
