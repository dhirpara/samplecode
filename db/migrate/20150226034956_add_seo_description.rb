class AddSeoDescription < ActiveRecord::Migration
  def up
    add_column :products, :seo_description, :text
    Product.add_translation_fields! seo_description: :text
    add_column :videos, :seo_description, :text
    Video.add_translation_fields! seo_description: :text
    add_column :articles, :seo_description, :text
    Article.add_translation_fields! seo_description: :text
  end

  def down
    remove_column :products, :seo_description
    remove_column :product_translations, :seo_description
    remove_column :videos, :seo_description
    remove_column :video_translations, :seo_description
    remove_column :articles, :seo_description
    remove_column :article_translations, :seo_description
  end
end
