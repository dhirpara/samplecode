class CreateOrderNotes < ActiveRecord::Migration
  def change
    create_table :order_notes do |t|
      t.integer :client_id #ON_CID
      t.text :note
      t.integer :create_support_user_id #ON_SSIDCreated
      t.integer :read_support_user_id #ON_SSIDRead
      t.date :date_read #ON_DateRead
      t.boolean :deleted
      t.timestamps
    end

    add_index :order_notes, :client_id
    add_index :order_notes, :create_support_user_id
    add_index :order_notes, :read_support_user_id
  end
end
