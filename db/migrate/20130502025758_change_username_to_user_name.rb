class ChangeUsernameToUserName < ActiveRecord::Migration
  def change
    rename_column :support_users, :username, :user_name
  end
end
