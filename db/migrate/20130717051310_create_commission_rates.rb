class CreateCommissionRates < ActiveRecord::Migration
  def change
    create_table :commission_rates do |t|
      t.float :min
      t.float :max
      t.float :rate
      t.string :commission_type

      t.timestamps
    end
  end
end
