class AddEmailToAutoShipping < ActiveRecord::Migration
  def change
    add_column :auto_shippings, :email, :string
  end
end
