class AddPriorityToPaymentTypes < ActiveRecord::Migration
  def change
    add_column :payment_types, :index, :integer

    PaymentType.all.each_with_index do |p, i|
      p.index = (i + 1) * 10
      p.save
    end

    pt = PaymentType.find_by_name("Discover")
    if pt
      pt.index = 25
      pt.save
    end
  end
end
