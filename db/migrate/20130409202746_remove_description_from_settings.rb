class RemoveDescriptionFromSettings < ActiveRecord::Migration
  def change
    remove_column :settings, :description
    change_column :settings, :code, :string, limit: 3
  end
end
