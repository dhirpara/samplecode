class CreateCategories < ActiveRecord::Migration
  def change
    create_table :research_categories do |t|
      t.string :name

      t.timestamps
    end

    add_column :researches, :research_category_id, :integer
    add_index :researches, :research_category_id
  end
end
