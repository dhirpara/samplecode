class CreatePricingTypes < ActiveRecord::Migration
  def change
    create_table :pricing_types do |t|
      t.string :name
      t.integer :discount_percentage
      t.boolean :default
      t.boolean :active
      t.timestamps
    end
  end
end
