class CreateOrderItemCaselots < ActiveRecord::Migration
  def change
    create_table :order_item_caselots do |t|
      t.integer :order_item_id
      t.integer :count
      t.integer :quantity
      t.decimal :unit_price

      t.timestamps
    end
    add_index :order_item_caselots, :order_item_id
  end
end
