class AddShowInNavigationToDisorders < ActiveRecord::Migration
  def change
    add_column :disorders, :show_in_navigation, :boolean, default: true
  end
end
