class ArticleDisorders < ActiveRecord::Migration
  def change
    create_table :article_disorders do |t|
      t.integer :article_id
      t.integer :disorder_id

      t.timestamps
    end
  end
end
