class ChangeAutoShipSettingActiveDefaultToTrue < ActiveRecord::Migration
  def change
    change_column :auto_shippings, :active, :boolean, default: true
  end
end
