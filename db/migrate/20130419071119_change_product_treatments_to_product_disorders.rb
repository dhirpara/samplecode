class ChangeProductTreatmentsToProductDisorders < ActiveRecord::Migration
  def change
    remove_column :product_treatments, :treatment_id
    rename_table :product_treatments, :product_disorders
    add_column :product_disorders, :disorder_id, :integer
    add_index :product_disorders, :disorder_id
    remove_column :products, :disorder_id
  end
end
