class ChangePaymentMessageType < ActiveRecord::Migration
  def change
    change_column :payments, :message, :text
  end
end
