class CreateStudies < ActiveRecord::Migration
  def change
    create_table :studies do |t|
      t.string :title
      t.string :image
      t.string :pdf
      t.string :link
      t.text :brief_description

      t.timestamps
    end
  end
end
