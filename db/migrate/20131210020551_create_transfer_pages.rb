class CreateTransferPages < ActiveRecord::Migration
  def change
    create_table :transfer_pages do |t|
      t.string :name
      t.string :url

      t.timestamps
    end
  end
end
