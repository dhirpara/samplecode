class CreateLimtingFactors < ActiveRecord::Migration
  def change
    create_table :limiting_factors do |t|
      t.string :name

      t.timestamps
    end
  end
end
