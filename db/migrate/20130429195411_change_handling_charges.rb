class ChangeHandlingCharges < ActiveRecord::Migration
  def change
    add_column :handling_charges, :handling_charge_method_id, :integer
    rename_column :handling_charges, :min_order_total, :min
    rename_column :handling_charges, :max_order_total, :max
    add_index :handling_charges, :handling_charge_method_id
  end
end
