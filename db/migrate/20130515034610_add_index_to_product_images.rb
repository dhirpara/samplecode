class AddIndexToProductImages < ActiveRecord::Migration
  def change
    add_column :product_images, :index, :integer
  end
end
