class ChangeColumnsInCommissionRates < ActiveRecord::Migration
  def change
    change_column :commission_rates, :min, :integer
    change_column :commission_rates, :max, :integer
    change_column :commission_rates, :rate, :integer
  end
end
