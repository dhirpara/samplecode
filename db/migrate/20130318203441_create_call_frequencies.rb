class CreateCallFrequencies < ActiveRecord::Migration
  def change
    create_table :call_frequencies do |t|
      t.string :name
      t.timestamps
    end
  end
end
