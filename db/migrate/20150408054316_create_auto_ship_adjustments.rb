class CreateAutoShipAdjustments < ActiveRecord::Migration
  def change
    create_table :auto_ship_adjustments do |t|
      t.belongs_to :auto_shipping
      t.decimal :amount,             :precision => 8, :scale => 2
      t.belongs_to :adjustment_type
      t.string :note

      t.timestamps
    end
    add_index :auto_ship_adjustments, :auto_shipping_id
    add_index :auto_ship_adjustments, :adjustment_type_id
  end
end
