class AddLastDigitsToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :last_digits, :string
  end
end
