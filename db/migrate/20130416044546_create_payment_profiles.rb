class CreatePaymentProfiles < ActiveRecord::Migration
  def change
    drop_table :creditcards
    create_table :payment_profiles do |t|
      t.string :customer_code
      t.string :last_digits
      t.integer :client_id

      t.timestamps
    end
    add_index :payment_profiles, :client_id
  end
end
