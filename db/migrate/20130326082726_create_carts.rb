class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.integer :shopper_id
      t.string :shopper_type

      t.timestamps
    end
    add_index :carts, :shopper_id
  end
end
