class RemoveEmailIndexFromClients < ActiveRecord::Migration
  def change
    remove_index :clients, :email
  end
end
