class CreateProductPricingTypes < ActiveRecord::Migration
  def change
    create_table :product_pricing_types do |t|
      t.integer :pricing_type_id
      t.integer :priceable_id
      t.string :priceable_type
      t.boolean :available, default: false

      t.timestamps
    end
    add_index :product_pricing_types, :pricing_type_id
    add_index :product_pricing_types, :priceable_type
  end
end
