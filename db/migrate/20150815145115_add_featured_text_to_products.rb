class AddFeaturedTextToProducts < ActiveRecord::Migration
  def change
    add_column :products, :featured_text, :text
  end
end
