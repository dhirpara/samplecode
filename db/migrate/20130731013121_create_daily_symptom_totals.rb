class CreateDailySymptomTotals < ActiveRecord::Migration
  def change
    create_table :daily_symptom_totals do |t|
      t.integer :cid
      t.integer :diaid
      t.integer :total
      t.datetime :date
      t.datetime :date_entered

      t.timestamps
    end
  end
end
