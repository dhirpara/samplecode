class AddAmountOwingToPaymentReports < ActiveRecord::Migration
  def change
    add_column :payment_reports, :amount_owing, :decimal, :precision => 8, :scale => 2
  end
end
