class AddDefaultOnSupportCenter < ActiveRecord::Migration
  def change
    add_column :support_centers, :default, :boolean, default: false
  end
end
