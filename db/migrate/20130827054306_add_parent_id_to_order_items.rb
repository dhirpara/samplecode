class AddParentIdToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :parent_id, :integer
    add_column :cart_items, :parent_id, :integer
  end
end
