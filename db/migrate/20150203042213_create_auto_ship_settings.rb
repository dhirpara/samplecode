class CreateAutoShipSettings < ActiveRecord::Migration
  def change
    create_table :auto_ship_settings do |t|
      t.integer :discount

      t.timestamps
    end
    AutoShipSetting.create(discount: 5)
  end
end
