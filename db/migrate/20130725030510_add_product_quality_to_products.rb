class AddProductQualityToProducts < ActiveRecord::Migration
  def change
    add_column :products, :quality, :text
  end
end
