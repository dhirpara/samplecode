class AddColumnsToDrugs < ActiveRecord::Migration
  def change
    add_column :drugs, :brand_name, :string
    add_column :drugs, :drug_name, :string
    add_column :drugs, :current_dosage, :string
    add_column :drugs, :api_link, :string
    add_column :drugs, :menufacture, :string
    add_column :drugs, :herbal, :boolean
    add_column :drugs, :sleep, :integer
    add_column :drugs, :exercise, :integer
    add_column :drugs, :water, :string
    

  end
end
