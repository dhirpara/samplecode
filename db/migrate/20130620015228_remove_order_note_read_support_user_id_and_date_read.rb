class RemoveOrderNoteReadSupportUserIdAndDateRead < ActiveRecord::Migration
  def change
    remove_column :order_notes, :read_support_user_id
    remove_column :order_notes, :date_read
  end
end
