class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.integer :cartable_id
      t.string :cartable_type
      t.integer :cart_id
      t.integer :quantity

      t.timestamps
    end
    add_index :cart_items, :cartable_id
    add_index :cart_items, :cart_id
  end
end
