class ChangeProductPromotionMethodName < ActiveRecord::Migration
  def change
    rename_column :product_promotions, :product_promotion_method_id, :promotion_method_id
  end
end
