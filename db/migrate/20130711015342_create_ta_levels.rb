class CreateTaLevels < ActiveRecord::Migration
  def change
    create_table :ta_levels do |t|
      t.string :name
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
