class AddSeoToRelativeTable < ActiveRecord::Migration
  def change
    add_column :products, :seo_title, :string
    add_column :products, :seo_keywords, :text

    add_column :videos, :seo_title, :string
    add_column :videos, :seo_keywords, :text

    add_column :researches, :seo_title, :string
    add_column :researches, :seo_keywords, :text
  end
end
