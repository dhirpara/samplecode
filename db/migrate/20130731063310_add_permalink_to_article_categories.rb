class AddPermalinkToArticleCategories < ActiveRecord::Migration
  def change
    add_column :article_categories, :permalink, :string

    ArticleCategory.all.each do |a|
      a.permalink = a.name.parameterize
      a.save
    end
  end
end
