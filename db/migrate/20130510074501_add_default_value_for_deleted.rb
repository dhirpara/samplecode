class AddDefaultValueForDeleted < ActiveRecord::Migration
  def change
    change_column :contact_times , :deleted, :boolean, default: false
    change_column :call_frequencies, :deleted, :boolean, default: false
    change_column :country_shipping_adjustments, :deleted, :boolean, default: false
    change_column :registration_methods, :deleted, :boolean, default: false
    change_column :introduction_types, :deleted, :boolean, default: false
    change_column :disorders, :deleted, :boolean, default: false
    change_column :handling_charges, :deleted, :boolean, default: false

    remove_column :introduction_types, :active
  end
end
