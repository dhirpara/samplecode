class RemoveDiscountPercentageFromPricingTypes < ActiveRecord::Migration
  def change
    remove_column :pricing_types, :discount_percentage
  end
end
