class CreateSupportGroups < ActiveRecord::Migration
  def change
    create_table :support_groups do |t|
      t.string :name
      t.integer :order
      t.boolean :active
      t.timestamps
    end
  end
end
