class AddRateToCartItems < ActiveRecord::Migration
  def change
    add_column :cart_items, :rate, :decimal, precision: 8, scale: 2, default: 1
  end
end
