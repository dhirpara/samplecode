class AddFeaturedAttributesToResearches < ActiveRecord::Migration
  def change
    add_column :researches, :two_column_featured, :boolean
    add_column :researches, :three_column_featured, :boolean
  end
end
