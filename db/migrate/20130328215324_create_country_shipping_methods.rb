class CreateCountryShippingMethods < ActiveRecord::Migration
  def change
    create_table :country_shipping_methods do |t|
      t.integer :country_id
      t.integer :shipping_method_id
      t.timestamps
    end

    add_index :country_shipping_methods, :country_id
    add_index :country_shipping_methods, :shipping_method_id

  end
end
