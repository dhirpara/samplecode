class CreateDiagnoses < ActiveRecord::Migration
  def change
    create_table :diagnoses do |t|
      t.integer :cid
      t.integer :disid
      t.text :notes
      t.datetime :date
      t.string :tracked
      t.string :doctor_diagnosed
      t.integer :ssid
      t.boolean :deleted, default: false
      t.integer :ssid_deleted
      t.datetime :deleted_date

      t.timestamps
    end
  end
end
