class AddTargetIdToShippingPromotions < ActiveRecord::Migration
  def change
    add_column :shipping_promotions, :area_id, :integer
  end
end
