class AddTypeToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :type, :string, limit: 1
  end
end
