class CreateReferralCodeOrders < ActiveRecord::Migration
  def change
    create_table :referral_code_orders do |t|
      t.belongs_to :referral_code
      t.belongs_to :order

      t.timestamps
    end
    add_index :referral_code_orders, :referral_code_id
    add_index :referral_code_orders, :order_id
  end
end
