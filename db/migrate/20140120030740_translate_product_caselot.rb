class TranslateProductCaselot < ActiveRecord::Migration
  def up
    ProductCaselot.create_translation_table!({
      description: :string,
    })
  end

  def down
    ProductCaselot.drop_translation_table! :migrate_data => true
  end
end
