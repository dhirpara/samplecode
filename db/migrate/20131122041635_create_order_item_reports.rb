class CreateOrderItemReports < ActiveRecord::Migration
  def change
    create_table :order_item_reports do |t|
      t.integer :order_item_id
      t.integer :quantity

      t.timestamps
    end

    Order.where(state: "complete").each do |o|
      o.order_items.each do |i|
        r = OrderItemReport.new(order_item: i, quantity: i.quantity)
        r.created_at = i.created_at
        r.save
      end
    end
  end
end
