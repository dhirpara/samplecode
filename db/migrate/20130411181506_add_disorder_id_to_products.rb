class AddDisorderIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :disorder_id, :integer
    add_index :products, :disorder_id
  end
end
