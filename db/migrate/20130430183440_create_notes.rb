class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.integer :client_id #N_CID
      t.text :note
      t.integer :support_user_id #N_SSIDWritten
      t.boolean :deleted
      t.timestamps  #N_DateTime -> created_at
    end

    add_index :notes, :client_id
    add_index :notes, :support_user_id

  end
end
