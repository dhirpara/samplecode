class RenameTreatmentsToDisorders < ActiveRecord::Migration
  def change
    rename_table :treatments, :disorders
  end
end
