class AddReferralCodeToOrder < ActiveRecord::Migration
  def change
    add_column :carts, :referral_code_id, :integer
    add_column :carts, :pricing_type_id, :integer

    add_column :orders, :referral_code_id, :integer
    add_column :orders, :pricing_type_id, :integer
  end
end
