class AddPaymentPrfileIdToAutoShippings < ActiveRecord::Migration
  def change
    add_column :auto_shippings, :payment_profile_id, :integer
  end
end
