class BackOrder < ActiveRecord::Migration
  def change
    add_column :products, :back_ordered, :boolean, default: false
    add_column :clients, :back_ordered_time, :datetime
    add_column :clients, :back_ordered_customer, :boolean, default: false
  end
end
