class AddIndexToStudies < ActiveRecord::Migration
  def change
    add_column :studies, :index, :integer
  end
end
