class ChangePromotionsProductIdType < ActiveRecord::Migration
  def change
    remove_column :product_promotions, :product_id
    add_column :product_promotions, :product_id, :integer
  end
end
