class AddGuestToClients < ActiveRecord::Migration
  def change
    add_column :clients, :guest, :boolean, default: false
  end
end
