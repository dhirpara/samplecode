class CreateBackOrderReports < ActiveRecord::Migration
  def change
    create_table :back_order_reports do |t|
      t.belongs_to :client
      t.belongs_to :product
      t.integer :quantity

      t.timestamps
    end

    remove_column :clients, :back_ordered_time
  end
end
