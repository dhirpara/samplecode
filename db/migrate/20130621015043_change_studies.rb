class ChangeStudies < ActiveRecord::Migration
  def change
    rename_column :studies, :image, :journal_cover
    add_column :studies, :article_title, :string
    add_column :studies, :reference, :string
    add_column :studies, :video, :string
  end
end
