class CreatePaymentReports < ActiveRecord::Migration
  def change
    create_table :payment_reports do |t|
      t.integer :order_id
      t.integer :support_user_id
      t.integer :payment_type_id
      t.integer :payment_id
      t.decimal :amount, :precision => 8, :scale => 2
      t.text :note

      t.timestamps
    end

    Payment.where(received: true).each do |payment|
      payment_amount = ["R", "VP"].include?(payment.trn_type) ? - payment.amount : payment.amount

      pr = PaymentReport.new(amount: payment_amount, note: payment.note, order_id: payment.order_id, payment_id: payment.id,
                             payment_type_id: payment.payment_type_id, support_user_id: payment.support_user_id)
      pr.created_at = payment.completed_at
      pr.save
    end
  end
end
