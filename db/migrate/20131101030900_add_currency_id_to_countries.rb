class AddCurrencyIdToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :currency_id, :integer

    Country.all.each do |c|
      if c.abbr == "CA"
        c.update_attributes(currency_id: Currency.find_by_name("CAD").id)
      else
        c.update_attributes(currency_id: Currency.find_by_name("USD").id)
      end
    end

    Client.update_all(currency_id: nil)
  end
end
