class CreateVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|
      t.string :unit
      t.decimal :price, :precision => 8, :scale => 2
      t.integer :product_id

      t.timestamps
    end
    add_index :variants, :product_id
  end
end
