class AddDeletedColumnToConfigurationTable < ActiveRecord::Migration
  def change
    add_column :contact_times , :deleted, :boolean
    add_column :call_frequencies, :deleted, :boolean
    add_column :country_shipping_adjustments, :deleted, :boolean
    add_column :registration_methods, :deleted, :boolean
    add_column :introduction_types, :deleted, :boolean
    add_column :disorders, :deleted, :boolean
    add_column :handling_charges, :deleted, :boolean
  end
end
