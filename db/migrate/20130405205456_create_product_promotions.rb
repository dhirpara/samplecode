class CreateProductPromotions < ActiveRecord::Migration
  def change
    create_table :product_promotions do |t|
      t.string :product_id
      t.integer :discount_percentage
      t.date :start_date
      t.date :end_date
      t.timestamps
    end
  end
end
