class RemoveChargeableFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :chargeable
  end
end
