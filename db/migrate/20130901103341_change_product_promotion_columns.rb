class ChangeProductPromotionColumns < ActiveRecord::Migration
  def change
    rename_column :product_promotions, :need_purchase_product_id, :x_product_id
    rename_column :product_promotions, :need_purchase_quantity, :x_quantity
    rename_column :product_promotions, :promotion_product_id, :y_product_id
    rename_column :product_promotions, :promotion_quantity_or_rate, :promotion_quantity_or_rate_off
  end
end
