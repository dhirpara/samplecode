class CreateSupportUserRoles < ActiveRecord::Migration
  def change
    remove_column :support_users, :role_id
    create_table :support_user_roles do |t|
      t.integer :support_user_id
      t.integer :role_id

      t.timestamps
    end
  end
end
