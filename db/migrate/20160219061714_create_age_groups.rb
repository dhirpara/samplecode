class CreateAgeGroups < ActiveRecord::Migration
  def change
    create_table :age_groups do |t|
      t.string :name_of_group
      t.string :range_of_group

      t.timestamps
    end
  end
end
