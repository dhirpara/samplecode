class AddRetailPriceToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :retail_price, :decimal, precision: 8, scale: 2
  end
end
