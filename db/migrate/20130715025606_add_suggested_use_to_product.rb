class AddSuggestedUseToProduct < ActiveRecord::Migration
  def change
    add_column :products, :suggested_use, :text
    add_column :products, :supplement_facts, :text
    add_column :products, :label_file, :string
  end
end
