class AddActiveToSupportUser < ActiveRecord::Migration
  def change
    add_column :support_users, :active, :boolean, default: false
  end
end
