class AddDefaultDeletedValueForSupportCenter < ActiveRecord::Migration
  def change
    change_column :support_centers, :deleted, :boolean, default: false
  end
end
