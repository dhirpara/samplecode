class AddDeletedAtToPaymentProfile < ActiveRecord::Migration
  def change
    add_column :payment_profiles, :deleted_at, :datetime
  end
end
