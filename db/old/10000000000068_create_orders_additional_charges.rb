class CreateOrdersAdditionalCharges < ActiveRecord::Migration
  def change
    create_table :orders_additional_charges do |t|
        
t.integer :OAC_CURRID
t.integer :OAC_DCID
t.decimal :OAC_MaxHandelingFee
t.decimal :OAC_CustomsFee
t.float :OAC_PackingWeight
t.string :OAC_Deleted
    end
  end
end
