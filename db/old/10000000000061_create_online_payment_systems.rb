class CreateOnlinePaymentSystems < ActiveRecord::Migration
  def change
    create_table :online_payment_systems do |t|
        
t.string :ops_name
t.string :ops_homeURL
t.string :ops_description
t.binary :ops_API_Login
t.binary :ops_transactionKey
t.string :ops_transactionURL
    end
  end
end
