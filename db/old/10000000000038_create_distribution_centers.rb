class CreateDistributionCenters < ActiveRecord::Migration
  def change
    create_table :distribution_centers do |t|
        
t.string :DC_Name
t.string :DC_AddressName
t.string :DC_Address
t.string :DC_City
t.string :DC_State
t.string :DC_ZipCode
t.string :DC_Country
t.string :DC_Deleted
    end
  end
end
