class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
        
t.string :CO_Name
t.string :CO_PostalName
t.integer :CO_CURRID
t.integer :CO_DCID
t.string :CO_Deleted
t.integer :CO_DefaultLanguageID
t.integer :CO_DefaultClientTypeID
t.integer :CO_DefaultOrdersCenter
t.text :CO_OrdersNotes
t.string :CO_AllowOnlineOrdersFrom
    end
  end
end
