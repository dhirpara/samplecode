class CreateAutoShippings < ActiveRecord::Migration
  def change
    create_table :auto_shippings do |t|
        
t.integer :AS_CID
t.integer :AS_CPID
t.integer :AS_CSID
t.integer :AS_DCID
t.integer :AS_CURRID
t.integer :AS_PLID
t.integer :AS_SMID
t.integer :AS_SMPID
t.integer :AS_ShipWeight
t.datetime :AS_DateStarted
t.integer :AS_Interval
t.datetime :AS_NextOrderDate
t.string :AS_ChargeCustoms
t.string :AS_Active
t.integer :AS_SSIDCreated
t.integer :AS_SSIDUpdated
    end
  end
end
