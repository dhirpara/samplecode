class CreateSupports < ActiveRecord::Migration
  def change
    create_table :supports do |t|
        
t.integer :S_CID
t.string :S_Name
t.string :S_Relationship
t.string :S_HPhone
t.string :S_WPhone
t.string :S_Email
t.string :S_Cell
t.string :S_Fax
t.string :S_Address
t.string :S_City
t.string :S_State
t.string :S_Country
t.string :S_Postal
t.string :S_Deleted
    end
  end
end
