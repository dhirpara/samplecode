class CreateClientsSupportCenters < ActiveRecord::Migration
  def change
    create_table :clients_support_centers do |t|
        
t.datetime :CSC_DateCreated
t.integer :CSC_SASID
t.integer :CSC_CSCTID
t.string :CSC_Code
t.string :CSC_Name
t.string :CSC_Address
t.string :CSC_City
t.string :CSC_State
t.integer :CSC_COID
t.string :CSC_PostalCode
t.string :CSC_Phone
t.string :CSC_Fax
t.string :CSC_Email
t.string :CSC_SupportEmail
t.string :CSC_SupportPhone
t.string :CSC_Deleted
t.string :CSC_ProvideSupport
t.string :CSC_ProductSupport
t.integer :CSC_CTYID_Default
t.integer :CSC_MainCID
    end
  end
end
