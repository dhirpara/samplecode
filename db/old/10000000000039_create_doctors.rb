class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
        
t.integer :DR_CID
t.string :DR_Name
t.string :DR_Type
t.string :DR_WPhone
t.string :DR_HPhone
t.string :DR_Fax
t.string :DR_Email
t.string :DR_Address
t.string :DR_City
t.string :DR_State
t.string :DR_PostalCode
t.string :DR_SupportStatus
t.string :DR_Deleted
    end
  end
end
