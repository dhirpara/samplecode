class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
        
t.integer :N_CID
t.text :N_Note
t.datetime :N_DateTime
t.datetime :N_Date
t.float :N_Duration
t.integer :N_SSIDWritten
t.integer :N_SSTime
t.string :N_Modified
t.datetime :N_ModifiedDate
t.float :N_ModifiedTime
t.integer :N_SSIDModified
t.string :N_Deleted
t.string :N_SSIDDelete
t.integer :N_SSIDRevised
t.datetime :N_RevisedDate
    end
  end
end
