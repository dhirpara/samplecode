class CreateOrderProducts < ActiveRecord::Migration
  def change
    create_table :order_products do |t|
        
t.integer :OP_OID
t.integer :OP_PID
t.integer :OP_Quantity
t.integer :OP_ProductNum
t.string :OP_Adjusted
t.integer :OP_AdjustedQty
t.datetime :OP_AdjustDate
    end
  end
end
