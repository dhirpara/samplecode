class CreateOrdersCupons < ActiveRecord::Migration
  def change
    create_table :orders_cupons do |t|
        
t.string :OCU_Number
t.string :OCU_Name
t.decimal :OCU_Value
t.integer :OCU_CURRID
t.datetime :OCU_DateCreated
t.string :OCU_UseOnce
t.string :OCU_Used
t.integer :OCU_SSIDCreated
t.string :OCU_Deleted
    end
  end
end
