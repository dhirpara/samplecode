class CreateProductsPrices < ActiveRecord::Migration
  def change
    create_table :products_prices do |t|
        
t.integer :PP_CTYID
t.integer :PP_PBPID
t.float :PP_Price
t.integer :PP_QuantityMin
t.integer :PP_QuantityMax
t.integer :PP_QuantityMultiples
t.decimal :PP_HandleFee
t.string :PP_Deleted
    end
  end
end
