class CreateDrugs < ActiveRecord::Migration
  def change
    create_table :drugs do |t|
        
t.integer :DRU_CID
t.integer :DRU_DLID
t.text :DRU_Label
t.float :DRU_Amount
t.string :DRU_Unit
t.text :DRU_Comments
t.string :DRU_Previous
t.datetime :DRU_DateStopped
t.integer :DRU_SSID
t.string :DRU_Deleted
t.integer :DRU_SSIDDeleted
t.datetime :DRU_DeleteDate
    end
  end
end
