class CreateOrderNotes < ActiveRecord::Migration
  def change
    create_table :order_notes do |t|
        
t.integer :ON_CID
t.datetime :ON_Date
t.text :ON_Note
t.integer :ON_SSIDCreated
t.string :ON_Read
t.integer :ON_SSIDRead
t.datetime :ON_DateRead
t.string :ON_Deleted
t.integer :ON_SSIDDeleted
t.datetime :ON_DateDeleted
    end
  end
end
