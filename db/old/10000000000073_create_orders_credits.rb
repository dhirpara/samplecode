class CreateOrdersCredits < ActiveRecord::Migration
  def change
    create_table :orders_credits do |t|
        
t.integer :OC_OID
t.datetime :OC_CreditDate
t.decimal :OC_Amount
t.string :OC_TransferFrom
t.string :OC_TransferTo
t.integer :OC_TransferOrderNum
t.integer :OC_SSID
t.string :OC_Deleted
t.integer :OC_SSIDDeleted
    end
  end
end
