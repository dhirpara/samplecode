class CreateDiagnoses < ActiveRecord::Migration
  def change
    create_table :diagnoses do |t|
        
t.integer :DIA_CID
t.integer :DIA_DISID
t.text :DIA_Notes
t.datetime :DIA_Date
t.string :DIA_Tracked
t.string :DIA_DoctorDiagnosed
t.integer :DIA_SSID
t.string :DIA_Deleted
t.integer :DIA_SSIDDeleted
t.datetime :DIA_DeletedDate
    end
  end
end
