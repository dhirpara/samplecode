class CreateOrdersPayments < ActiveRecord::Migration
  def change
    create_table :orders_payments do |t|
        
t.integer :OPAY_OID
t.string :OPAY_ExtendedOID
t.integer :OPAY_CPID
t.string :OPAY_PaymentWhenOrderPlaced
t.string :OPAY_SelfPlacedPayment
t.datetime :OPAY_PaymentDate
t.decimal :OPAY_Amount
t.string :OPAY_Auth
t.string :OPAY_RealTimeTransaction
t.string :OPAY_RealTimeApproved
t.string :OPAY_ReceiptID
t.string :OPAY_TransactionID
t.string :OPAY_TransactionType
t.string :OPAY_TransactionTime
t.string :OPAY_TransactionDate
t.string :OPAY_ResponseCode
t.string :OPAY_ISOCode
t.string :OPAY_ResponseMessage
t.string :OPAY_ReferenceNumber
t.string :OPAY_Ticket
t.string :OPAY_Notes
t.integer :OPAY_SSID
t.string :OPAY_Deleted
t.integer :OPAY_SSIDDeleted
t.datetime :OPAY_Transaction_DateTime
    end
  end
end
