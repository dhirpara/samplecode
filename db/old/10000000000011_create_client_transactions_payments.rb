class CreateClientTransactionsPayments < ActiveRecord::Migration
  def change
    create_table :client_transactions_payments do |t|
        
t.integer :CTP_CID
t.integer :CTP_OID
t.integer :CTP_CPID
t.integer :CTP_PMID
t.datetime :CTP_Date
t.decimal :CTP_Amount
t.string :CTP_Auth
t.integer :CTP_SSID
    end
  end
end
