class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
        
t.integer :C_NCID
t.integer :C_CTYID
t.string :C_UserName
t.string :C_Password
t.string :C_FirstName
t.string :C_LastName
t.string :C_MiddleName
t.datetime :C_DOB
t.string :C_Gender
t.string :C_Guardian
t.string :C_Active
t.string :C_TestAccount
t.string :C_Deleted
t.string :C_Address
t.string :C_AptNum
t.string :C_Address3
t.string :C_City
t.string :C_State
t.string :C_PostalCode
t.string :C_Country
t.integer :C_COID
t.string :C_HPhone
t.string :C_WPhone
t.string :C_Cell
t.string :C_Pager
t.string :C_Email
t.string :C_WEmail
t.integer :C_FID
t.string :C_ContactDay
t.string :C_ContactTime
t.datetime :C_NextContactdate
t.string :C_Fax
t.integer :C_TimeZone
t.datetime :C_ContactDate
t.integer :C_CTID
t.datetime :C_StartDate
t.datetime :C_StartSupplement
t.datetime :C_DateActive
t.string :C_Introduction
t.string :C_RegisteredBy
t.datetime :C_QuitDate
t.datetime :C_SSIDQuitDate
t.integer :C_SSIDQuit
t.integer :C_QRID
t.string :C_QuitReason
t.text :C_GeneralComments
t.string :C_Category
t.string :C_AdminOrder
t.string :C_AdminSupport
t.string :C_AllowOrder
t.string :C_RequireData
t.datetime :C_DataStartDate
t.datetime :C_DataStopDate
t.float :C_Discount
t.string :C_MainParticipant
t.integer :C_TALevel
t.string :C_TAComments
t.string :C_CallFrequency
t.integer :C_SSIDCreated
t.integer :C_SSIDModified
t.datetime :C_ModifiedDate
t.string :C_SelfRegistered
t.float :C_RegistrationTime
t.string :C_RegistrationIP
t.string :C_THAuthorized
t.string :C_MYTHMessagePost
t.string :C_MYTHMessageRead
t.string :C_MYTHChat
t.string :C_MYTHOrder
t.integer :C_CSCID
t.integer :C_CTAID
t.integer :C_CSGID
t.integer :C_LANID
t.string :C_CNEProfessional
t.integer :C_CSTID
t.string :C_MentalWellness
t.text :C_MentalWellnessDetails
t.string :C_SingleOrderClient
t.integer :C_CACID
t.integer :C_CITID
t.binary :C_eFirstName
t.binary :C_eLastName
t.binary :C_eDOB
t.binary :C_eHPhone
t.binary :C_eWPhone
t.binary :C_eCell
t.binary :C_eEmail
t.binary :C_eWEmail
t.string :C_ePassword
t.string :C_AffiliateReferal
t.integer :C_AffiliateID
t.string :C_AffiliateURL
t.datetime :C_ReferalDate
t.integer :C_AffiliateModifySSID
t.integer :C_Old_CSGID
t.integer :C_SupportTeam
t.string :C_ValidationSecret
t.integer :C_AuthSSID
t.datetime :C_AuthDate
t.integer :C_MBUID
t.string :C_MessageBoardName
t.string :C_Hide
    end
  end
end
