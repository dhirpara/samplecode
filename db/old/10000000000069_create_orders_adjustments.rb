class CreateOrdersAdjustments < ActiveRecord::Migration
  def change
    create_table :orders_adjustments do |t|
        
t.integer :OA_OID
t.datetime :OA_AdjustmentDate
t.decimal :OA_Shipping
t.decimal :OA_Handling
t.decimal :OA_Customs
t.string :OA_Reason
t.integer :OA_SSID
    end
  end
end
