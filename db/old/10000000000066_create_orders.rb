class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
        
t.integer :O_CID
t.integer :O_OCID
t.integer :O_CSCID
t.string :O_OCCode
t.string :O_Imported
t.integer :O_OrderNum
t.datetime :O_DateCreated
t.integer :O_SSIDCreated
t.string :O_SelfPlaced
t.string :O_Completed
t.datetime :O_CompletedDate
t.integer :O_SSIDCompleted
t.string :O_Printed
t.integer :O_OPRID
t.string :O_Sent
t.datetime :O_SentDate
t.integer :O_SSIDSent
t.integer :O_CSID
t.integer :O_SMID
t.integer :O_SMPID
t.integer :O_ShipWeight
t.decimal :O_Shipping
t.decimal :O_Handling
t.decimal :O_Customs
t.integer :O_CURRID
t.decimal :O_Discount
t.decimal :O_DiscountTotal
t.string :O_FirstOrder
t.string :O_WaitForMO
t.decimal :O_BalanceOnOrder
t.decimal :O_ActualShippingCost
t.string :O_TrackingNumber
t.string :O_Deleted
t.integer :O_SSIDDeleted
t.string :O_WaitingToComplete
t.string :O_AutoShipOrder
t.string :O_ShipFromWarehouse
t.string :O_DeleteReason
t.datetime :O_DeleteDate
t.integer :O_SCID
    end
  end
end
