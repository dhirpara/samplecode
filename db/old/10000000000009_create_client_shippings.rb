class CreateClientShippings < ActiveRecord::Migration
  def change
    create_table :client_shippings do |t|
        
t.integer :CS_CID
t.string :CS_Imported
t.integer :CS_OldID
t.string :CS_CompanyName
t.string :CS_Address
t.string :CS_Address2
t.string :CS_Address3
t.string :CS_City
t.string :CS_State
t.integer :CS_COID
t.string :CS_Country
t.string :CS_PostalCode
t.string :CS_Deleted
t.string :CS_ATTN
t.string :CS_IncludeATTN
    end
  end
end
