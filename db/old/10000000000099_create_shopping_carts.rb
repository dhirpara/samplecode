class CreateShoppingCarts < ActiveRecord::Migration
  def change
    create_table :shopping_carts do |t|
        
t.integer :SC_ID1, limit: 8
t.string :SC_ID2
t.string :SC_IPAddress
t.datetime :SC_Date
t.integer :SC_CID
t.integer :SC_CTYID
t.integer :CS_CURRID
t.string :SC_CompletePurchase
t.string :SC_Deleted
t.integer :SC_OrderDCID
t.integer :SC_OrderPLID
t.integer :SC_OrderOCID
t.string :SC_OrderOCCode
t.integer :SC_OrderProductWeight
t.decimal :SC_OrderProductHandling
t.decimal :SC_OrderMaxHandling
t.decimal :SC_OrderCustoms
t.string :SC_OrderCurrencyName
t.integer :SC_OrderCSID
t.string :SC_OrderAddressName
t.string :SC_OrderAddressCompanyName
t.string :SC_OrderAddress1
t.string :SC_OrderAddress2
t.string :SC_OrderAddress3
t.string :SC_OrderPostalCode
t.string :SC_OrderState
t.integer :SC_OrderCOID
t.string :SC_OrderSMID
t.string :SC_OrderSMPID
    end
  end
end
