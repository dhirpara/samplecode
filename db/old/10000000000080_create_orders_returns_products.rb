class CreateOrdersReturnsProducts < ActiveRecord::Migration
  def change
    create_table :orders_returns_products do |t|
        
t.integer :ORP_OID
t.integer :ORP_PPID
t.integer :ORP_Quantity
t.datetime :ORP_ReturnDate
t.string :ORP_ReturnReason
t.integer :ORP_SSIDReturned
    end
  end
end
