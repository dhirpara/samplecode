class CreateDrugLookups < ActiveRecord::Migration
  def change
    create_table :drug_lookups do |t|
        
t.string :DL_Brand
t.string :DL_CommonName
t.string :DL_Name
t.integer :DL_DCID
t.string :DL_Class
t.string :DL_Manufacturer
t.string :DL_Form
t.string :DL_Herbal
t.string :DL_Dosage
t.string :DL_Unit
t.text :DL_Effects
t.text :DL_SideEffects
t.text :DL_Withdrawals
t.text :DL_Interaction
t.string :DL_RedFlag
t.text :DL_rxListLink
t.string :DL_Deleted
    end
  end
end
