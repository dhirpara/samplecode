class CreateOrdersAdjustmentsProducts < ActiveRecord::Migration
  def change
    create_table :orders_adjustments_products do |t|
        
t.integer :OAP_OID
t.integer :OAP_PPID
t.datetime :OAP_AdjustmentDate
t.decimal :OAP_Amount
t.string :OAP_Reason
t.integer :OAP_SSID
    end
  end
end
