class CreateSupportStaffs < ActiveRecord::Migration
  def change
    create_table :support_staffs do |t|
        
t.string :SS_FirstName
t.string :SS_LastName
t.string :SS_UserName
t.string :SS_Password
t.string :SS_SupportDesk
t.string :SS_Admin
t.string :SS_SuperAdmin
t.string :SS_DataEnter
t.string :SS_AdminOrders
t.string :SS_OrderDesk
t.integer :SS_TimeZone
t.string :SS_Deleted
t.integer :SS_CID
t.integer :SS_CSCID
t.integer :SS_SSTID
t.string :SS_AllowRemoteAccess
t.string :SS_ViewAll
t.string :SS_ViewOnly
t.datetime :SS_DateCreated
t.integer :SS_OCID
t.integer :SS_LoginAttempts
t.string :SS_LoginSuspend
t.integer :SS_InterFace
t.string :SS_QtaskUser
    end
  end
end
