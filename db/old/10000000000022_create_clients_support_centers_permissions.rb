class CreateClientsSupportCentersPermissions < ActiveRecord::Migration
  def change
    create_table :clients_support_centers_permissions do |t|
        
t.integer :CSCP_SSID
t.integer :CSCP_CSCID
t.datetime :CSCP_DateGranted
t.datetime :CSCP_DateRemoved
t.integer :CSCP_SSIDGranted
t.integer :CSCP_SSIDRemoved
t.string :CSCP_Deleted
    end
  end
end
