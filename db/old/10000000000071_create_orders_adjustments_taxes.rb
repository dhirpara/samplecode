class CreateOrdersAdjustmentsTaxes < ActiveRecord::Migration
  def change
    create_table :orders_adjustments_taxes do |t|
        
t.integer :OAT_OID
t.integer :OAT_TID
t.datetime :OAT_AdjustmentDate
t.decimal :OAT_Amount
t.string :OAT_Reason
t.integer :OAT_SSID
    end
  end
end
