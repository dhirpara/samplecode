class CreateOrderAdjustments < ActiveRecord::Migration
  def change
    create_table :order_adjustments do |t|
        
t.integer :OA_OID
t.datetime :OA_Date
t.decimal :OA_Shipping
t.decimal :OA_Handling
t.decimal :OA_Customs
t.decimal :OA_Discount
t.decimal :OA_Product
t.decimal :OA_Tax
t.string :OA_Reason
t.string :OA_Authorization
t.integer :OA_SSID
    end
  end
end
