class CreateClientPayments < ActiveRecord::Migration
  def change
    create_table :client_payments do |t|
        
t.integer :CP_CID
t.string :CP_Imported
t.integer :CP_OldID
t.integer :CP_PMID
t.string :CP_NameOnCard
t.string :CP_CreditNum
t.string :CP_CreditExpiry
t.string :CP_Deleted
t.string :CP_SelfEntered
t.binary :CP_CreditNumber
    end
  end
end
