class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
        
t.string :P_Name
t.string :P_Type
t.integer :P_Amount
t.string :P_Unit
t.string :P_Description
t.string :P_ShippingDescription
t.float :P_UnitWeight
t.string :P_Code
t.string :P_TarrifCode
t.string :P_ChargeableItem
t.string :P_SelfOrderProduct
t.integer :P_Order
t.string :P_Deleted
t.string :P_myTruehopeImage
t.string :P_myTruehopeImageName
t.integer :P_myTruehopeImageHeight
t.integer :P_myTruehopeImageWidth
t.string :P_myTruehopeImageThumbName
t.integer :P_myTruehopeImageThumbHeight
t.integer :P_myTruehopeImageThumbWidth
t.text :P_myTruehopeProductInfo
t.string :P_myTruehopeProductPage
t.string :P_myTruehopeProductPageLocation
t.integer :P_Category
t.string :P_Caselot
t.integer :P_CaselotCount
t.integer :P_CommissionCategory
t.string :P_InternalOrders
t.decimal :P_CustomsValue
    end
  end
end
