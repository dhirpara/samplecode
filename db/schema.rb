# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160223112905) do

  create_table "addresses", :force => true do |t|
    t.integer  "client_id"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "name"
    t.string   "postal_code"
    t.integer  "country_id"
    t.boolean  "active"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "phone"
  end

  add_index "addresses", ["client_id"], :name => "index_addresses_on_client_id"
  add_index "addresses", ["country_id"], :name => "index_addresses_on_country_id"

  create_table "adjustment_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "age_categories", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "age_groups", :force => true do |t|
    t.string   "name_of_group"
    t.string   "range_of_group"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "article_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "permalink"
  end

  create_table "article_disorders", :force => true do |t|
    t.integer  "article_id"
    t.integer  "disorder_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "article_translations", :force => true do |t|
    t.integer  "article_id"
    t.string   "locale",          :null => false
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "title"
    t.text     "content"
    t.string   "seo_title"
    t.text     "seo_keywords"
    t.text     "seo_description"
  end

  add_index "article_translations", ["article_id"], :name => "index_article_translations_on_article_id"
  add_index "article_translations", ["locale"], :name => "index_article_translations_on_locale"

  create_table "articles", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "image"
    t.string   "author"
    t.string   "author_image"
    t.date     "publish_date"
    t.integer  "article_category_id"
    t.integer  "column_number"
    t.string   "permalink"
    t.integer  "index"
    t.string   "seo_title"
    t.text     "seo_keywords"
    t.text     "seo_description"
    t.string   "author_image_alt"
    t.string   "image_alt"
  end

  add_index "articles", ["article_category_id"], :name => "index_researches_on_research_category_id"

  create_table "auto_ship_adjustments", :force => true do |t|
    t.integer  "auto_shipping_id"
    t.decimal  "amount",             :precision => 8, :scale => 2
    t.integer  "adjustment_type_id"
    t.string   "note"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
  end

  add_index "auto_ship_adjustments", ["adjustment_type_id"], :name => "index_auto_ship_adjustments_on_adjustment_type_id"
  add_index "auto_ship_adjustments", ["auto_shipping_id"], :name => "index_auto_ship_adjustments_on_auto_shipping_id"

  create_table "auto_ship_reports", :force => true do |t|
    t.integer  "client_id"
    t.integer  "country_id"
    t.integer  "auto_shipping_id"
    t.integer  "order_id"
    t.string   "email"
    t.date     "date_of_shipment"
    t.integer  "interval"
    t.string   "state"
    t.decimal  "total",            :precision => 8, :scale => 2
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  add_index "auto_ship_reports", ["client_id"], :name => "index_auto_ship_reports_on_client_id"
  add_index "auto_ship_reports", ["country_id"], :name => "index_auto_ship_reports_on_country_id"

  create_table "auto_ship_setting_pricing_types", :force => true do |t|
    t.integer  "auto_ship_setting_id"
    t.integer  "pricing_type_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "auto_ship_setting_pricing_types", ["auto_ship_setting_id"], :name => "index_auto_ship_setting_pricing_types_on_auto_ship_setting_id"
  add_index "auto_ship_setting_pricing_types", ["pricing_type_id"], :name => "index_auto_ship_setting_pricing_types_on_pricing_type_id"

  create_table "auto_ship_settings", :force => true do |t|
    t.integer  "discount"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "auto_shipping_items", :force => true do |t|
    t.integer  "product_id"
    t.integer  "auto_shipping_id"
    t.integer  "quantity"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "caselot_count"
  end

  create_table "auto_shippings", :force => true do |t|
    t.integer  "client_id"
    t.date     "start_date"
    t.integer  "interval"
    t.date     "next_order_date"
    t.boolean  "active",             :default => false
    t.integer  "support_user_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.integer  "shipping_method_id"
    t.integer  "address_id"
    t.string   "email"
    t.integer  "payment_profile_id"
  end

  add_index "auto_shippings", ["client_id"], :name => "index_auto_shippings_on_client_id"
  add_index "auto_shippings", ["support_user_id"], :name => "index_auto_shippings_on_support_user_id"

  create_table "back_order_reports", :force => true do |t|
    t.integer  "client_id"
    t.integer  "product_id"
    t.integer  "quantity"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "call_frequencies", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "deleted",    :default => false
    t.integer  "priority"
  end

  create_table "cart_items", :force => true do |t|
    t.integer  "cartable_id"
    t.string   "cartable_type"
    t.integer  "cart_id"
    t.integer  "quantity"
    t.datetime "created_at",                                                   :null => false
    t.datetime "updated_at",                                                   :null => false
    t.integer  "parent_id"
    t.decimal  "rate",          :precision => 8, :scale => 2, :default => 1.0
  end

  add_index "cart_items", ["cart_id"], :name => "index_cart_items_on_cart_id"
  add_index "cart_items", ["cartable_id"], :name => "index_cart_items_on_cartable_id"

  create_table "carts", :force => true do |t|
    t.integer  "shopper_id"
    t.string   "shopper_type"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "referral_code_id"
    t.integer  "pricing_type_id"
    t.string   "licence"
  end

  add_index "carts", ["shopper_id"], :name => "index_carts_on_shopper_id"

  create_table "client_diagnoses", :force => true do |t|
    t.integer  "client_id"
    t.integer  "diagnosis_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "client_diagnoses", ["client_id"], :name => "index_client_diagnoses_on_client_id"
  add_index "client_diagnoses", ["diagnosis_id"], :name => "index_client_diagnoses_on_diagnosis_id"

  create_table "client_doctors", :force => true do |t|
    t.integer  "client_id"
    t.integer  "name"
    t.string   "doctor_type"
    t.string   "work_phone"
    t.string   "home_phone"
    t.string   "fax"
    t.string   "email"
    t.string   "address"
    t.string   "city"
    t.string   "province"
    t.string   "postal_code"
    t.string   "support_status"
    t.boolean  "active"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "client_limiting_factors", :force => true do |t|
    t.integer  "client_id"
    t.integer  "limiting_factor_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "client_limiting_factors", ["client_id"], :name => "index_client_limiting_factors_on_client_id"
  add_index "client_limiting_factors", ["limiting_factor_id"], :name => "index_client_limiting_factors_on_limiting_factor_id"

  create_table "clients", :force => true do |t|
    t.string   "user_name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "middle_name"
    t.date     "date_of_birth"
    t.string   "gender",                  :limit => 1
    t.string   "guardian"
    t.boolean  "active",                               :default => false
    t.string   "address_1"
    t.string   "address_2"
    t.string   "address_3"
    t.string   "city"
    t.string   "province"
    t.integer  "country_id"
    t.string   "postal_code"
    t.string   "home_phone"
    t.string   "work_phone"
    t.string   "cell_phone"
    t.string   "pager"
    t.string   "fax"
    t.string   "email"
    t.string   "work_email"
    t.string   "contact_day"
    t.integer  "contact_time_id"
    t.integer  "time_zone"
    t.datetime "contact_date"
    t.date     "next_contact_date"
    t.date     "start_date"
    t.date     "start_supplement"
    t.date     "date_active"
    t.integer  "introduction_type_id"
    t.integer  "register_method_id"
    t.date     "quit_date"
    t.text     "general_comments"
    t.boolean  "admin_order"
    t.boolean  "admin_support"
    t.boolean  "allow_order"
    t.boolean  "require_data"
    t.date     "data_start_date"
    t.date     "data_stop_date"
    t.float    "discount"
    t.boolean  "main_participant"
    t.integer  "call_frequency_id"
    t.boolean  "self_registered"
    t.boolean  "cne_professional"
    t.boolean  "mental_wellness"
    t.text     "mental_wellness_details"
    t.boolean  "single_order_client"
    t.string   "encrypted_password",                   :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "pricing_type_id"
    t.integer  "client_family_id"
    t.integer  "support_center_id"
    t.integer  "support_group_id"
    t.integer  "language_id"
    t.integer  "support_user_id"
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.integer  "currency_id"
    t.integer  "quit_reason_id"
    t.integer  "age_category_id"
    t.boolean  "deleted",                              :default => false
    t.boolean  "guest",                                :default => false
    t.integer  "ta_level_id"
    t.string   "ta_comments"
    t.boolean  "back_ordered_customer",                :default => false
  end

  add_index "clients", ["age_category_id"], :name => "index_clients_on_age_category_id"
  add_index "clients", ["client_family_id"], :name => "index_clients_on_client_family_id"
  add_index "clients", ["contact_time_id"], :name => "index_clients_on_contact_time_id"
  add_index "clients", ["country_id"], :name => "index_clients_on_country_id"
  add_index "clients", ["currency_id"], :name => "index_clients_on_currency_id"
  add_index "clients", ["introduction_type_id"], :name => "index_clients_on_introduction_type_id"
  add_index "clients", ["language_id"], :name => "index_clients_on_language_id"
  add_index "clients", ["pricing_type_id"], :name => "index_clients_on_pricing_type_id"
  add_index "clients", ["quit_reason_id"], :name => "index_clients_on_quit_reason_id"
  add_index "clients", ["register_method_id"], :name => "index_clients_on_register_method_id"
  add_index "clients", ["reset_password_token"], :name => "index_clients_on_reset_password_token", :unique => true
  add_index "clients", ["support_center_id"], :name => "index_clients_on_support_center_id"
  add_index "clients", ["support_group_id"], :name => "index_clients_on_support_group_id"
  add_index "clients", ["support_user_id"], :name => "index_clients_on_support_user_id"
  add_index "clients", ["ta_level_id"], :name => "index_clients_on_ta_level_id"

  create_table "commission_rates", :force => true do |t|
    t.integer  "min"
    t.integer  "max"
    t.integer  "rate"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "commission_type_id"
    t.integer  "support_user_id"
  end

  create_table "commission_reports", :force => true do |t|
    t.integer  "order_id"
    t.integer  "support_user_id"
    t.date     "date"
    t.decimal  "amount",          :precision => 8, :scale => 2
    t.string   "description"
    t.integer  "rate"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  add_index "commission_reports", ["order_id"], :name => "index_commission_reports_on_order_id"
  add_index "commission_reports", ["support_user_id"], :name => "index_commission_reports_on_support_user_id"

  create_table "commission_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "code"
  end

  create_table "contact_times", :force => true do |t|
    t.string   "description"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "deleted",     :default => false
  end

  create_table "countries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "abbr",        :limit => 2
    t.integer  "currency_id"
  end

  create_table "country_shipping_adjustments", :force => true do |t|
    t.integer  "country_id"
    t.decimal  "amount",     :precision => 8, :scale => 2
    t.boolean  "percentage"
    t.datetime "created_at",                                                  :null => false
    t.datetime "updated_at",                                                  :null => false
    t.boolean  "deleted",                                  :default => false
  end

  create_table "country_shipping_methods", :force => true do |t|
    t.integer  "country_id"
    t.integer  "shipping_method_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "country_shipping_methods", ["country_id"], :name => "index_country_shipping_methods_on_country_id"
  add_index "country_shipping_methods", ["shipping_method_id"], :name => "index_country_shipping_methods_on_shipping_method_id"

  create_table "currencies", :force => true do |t|
    t.string "name"
  end

  create_table "daily_symptom_totals", :force => true do |t|
    t.integer  "cid"
    t.integer  "diaid"
    t.integer  "total"
    t.datetime "date"
    t.datetime "date_entered"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "diagnoses", :force => true do |t|
    t.string "name"
  end

  create_table "disorder_translations", :force => true do |t|
    t.integer  "disorder_id"
    t.string   "locale",      :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "name"
  end

  add_index "disorder_translations", ["disorder_id"], :name => "index_disorder_translations_on_disorder_id"
  add_index "disorder_translations", ["locale"], :name => "index_disorder_translations_on_locale"

  create_table "disorders", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.boolean  "deleted",            :default => false
    t.boolean  "show_in_navigation", :default => true
  end

  create_table "document_translations", :force => true do |t|
    t.integer  "document_id"
    t.string   "locale",      :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "name"
  end

  add_index "document_translations", ["document_id"], :name => "index_document_translations_on_document_id"
  add_index "document_translations", ["locale"], :name => "index_document_translations_on_locale"

  create_table "documents", :force => true do |t|
    t.string   "file"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "article_category_id"
    t.string   "name"
  end

  create_table "drugs", :force => true do |t|
    t.integer  "cid"
    t.integer  "dlid"
    t.text     "label"
    t.float    "amount"
    t.string   "unit"
    t.text     "comments"
    t.string   "previous"
    t.datetime "date_stopped"
    t.integer  "ssid"
    t.boolean  "deleted",        :default => false
    t.integer  "ssid_deleted"
    t.datetime "delete_date"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.string   "brand_name"
    t.string   "drug_name"
    t.string   "current_dosage"
    t.string   "api_link"
    t.string   "menufacture"
    t.boolean  "herbal"
    t.integer  "sleep"
    t.integer  "exercise"
    t.string   "water"
  end

  create_table "emails", :force => true do |t|
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "first_name"
    t.string   "token"
    t.string   "last_name"
  end

  create_table "families", :force => true do |t|
    t.integer  "cid_head"
    t.string   "name"
    t.datetime "date_created"
    t.integer  "ssid_created"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "faq_translations", :force => true do |t|
    t.integer  "faq_id"
    t.string   "locale",     :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "question"
    t.text     "answer"
  end

  add_index "faq_translations", ["faq_id"], :name => "index_faq_translations_on_faq_id"
  add_index "faq_translations", ["locale"], :name => "index_faq_translations_on_locale"

  create_table "faq_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "index"
  end

  create_table "faqs", :force => true do |t|
    t.text     "question"
    t.text     "answer"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "faq_type_id"
    t.integer  "index"
  end

  create_table "genders", :force => true do |t|
    t.string   "gender_type"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "global_languages", :force => true do |t|
    t.string   "name"
    t.string   "abbr"
    t.boolean  "active",     :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "handling_charge_methods", :force => true do |t|
    t.string   "name"
    t.boolean  "active"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "handling_charges", :force => true do |t|
    t.decimal  "min",                       :precision => 8, :scale => 2
    t.decimal  "max",                       :precision => 8, :scale => 2
    t.decimal  "handling_charge",           :precision => 8, :scale => 2
    t.datetime "created_at",                                                                 :null => false
    t.datetime "updated_at",                                                                 :null => false
    t.integer  "handling_charge_method_id"
    t.boolean  "deleted",                                                 :default => false
  end

  add_index "handling_charges", ["handling_charge_method_id"], :name => "index_handling_charges_on_handling_charge_method_id"

  create_table "introduction_types", :force => true do |t|
    t.string   "name"
    t.integer  "index"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "deleted",    :default => false
  end

  create_table "landmark_shipping_rates", :force => true do |t|
    t.integer  "shipping_method_id"
    t.string   "description"
    t.decimal  "base_rate",          :precision => 8, :scale => 2
    t.integer  "base_weight_lbs"
    t.decimal  "rate_per_lbs",       :precision => 8, :scale => 2
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
  end

  create_table "languages", :force => true do |t|
    t.string   "name"
    t.integer  "order"
    t.boolean  "active"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "limiting_factors", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "notes", :force => true do |t|
    t.integer  "client_id"
    t.text     "note"
    t.integer  "support_user_id"
    t.boolean  "deleted",         :default => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "notes", ["client_id"], :name => "index_notes_on_client_id"
  add_index "notes", ["support_user_id"], :name => "index_notes_on_support_user_id"

  create_table "order_adjustments", :force => true do |t|
    t.integer  "adjustable_id"
    t.string   "adjustable_type"
    t.decimal  "amount",             :precision => 8, :scale => 2
    t.integer  "adjustment_type_id"
    t.string   "note"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
  end

  add_index "order_adjustments", ["adjustable_id"], :name => "index_order_adjustments_on_adjustable_id"

  create_table "order_item_reports", :force => true do |t|
    t.integer  "order_item_id"
    t.integer  "quantity"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "order_items", :force => true do |t|
    t.integer  "order_id"
    t.integer  "product_id"
    t.decimal  "price",           :precision => 8, :scale => 2
    t.integer  "quantity"
    t.datetime "created_at",                                                     :null => false
    t.datetime "updated_at",                                                     :null => false
    t.integer  "caselot_count"
    t.integer  "pricing_type_id"
    t.decimal  "retail_price",    :precision => 8, :scale => 2
    t.decimal  "rate",            :precision => 8, :scale => 2, :default => 1.0
    t.integer  "parent_id"
  end

  add_index "order_items", ["order_id"], :name => "index_order_items_on_order_id"
  add_index "order_items", ["pricing_type_id"], :name => "index_order_items_on_pricing_type_id"
  add_index "order_items", ["product_id"], :name => "index_order_items_on_product_id"

  create_table "order_notes", :force => true do |t|
    t.integer  "client_id"
    t.text     "note"
    t.integer  "create_support_user_id"
    t.boolean  "deleted"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "order_notes", ["client_id"], :name => "index_order_notes_on_client_id"
  add_index "order_notes", ["create_support_user_id"], :name => "index_order_notes_on_create_support_user_id"

  create_table "order_reports", :force => true do |t|
    t.integer  "order_id"
    t.decimal  "subtotal",         :precision => 8, :scale => 2
    t.decimal  "tax",              :precision => 8, :scale => 2
    t.decimal  "shipping",         :precision => 8, :scale => 2
    t.decimal  "handling",         :precision => 8, :scale => 2
    t.decimal  "other",            :precision => 8, :scale => 2
    t.decimal  "total",            :precision => 8, :scale => 2
    t.decimal  "payment_received", :precision => 8, :scale => 2
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  create_table "order_types", :force => true do |t|
    t.string "name"
    t.string "code", :limit => 1
  end

  create_table "orders", :force => true do |t|
    t.integer  "client_id"
    t.string   "number"
    t.string   "state"
    t.datetime "completed_at"
    t.integer  "support_user_id"
    t.integer  "auto_shipping_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "payment_state"
    t.integer  "currency_id"
    t.integer  "order_type_id"
    t.integer  "package_number"
    t.integer  "referral_code_id"
    t.integer  "pricing_type_id"
    t.string   "licence"
  end

  add_index "orders", ["auto_shipping_id"], :name => "index_orders_on_auto_shipping_id"
  add_index "orders", ["client_id"], :name => "index_orders_on_client_id"
  add_index "orders", ["currency_id"], :name => "index_orders_on_currency_id"
  add_index "orders", ["order_type_id"], :name => "index_orders_on_order_type_id"
  add_index "orders", ["support_user_id"], :name => "index_orders_on_support_user_id"

  create_table "payment_profiles", :force => true do |t|
    t.string   "customer_code"
    t.string   "last_digits"
    t.integer  "client_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "name"
    t.string   "postal_code"
    t.string   "phone"
    t.integer  "country_id"
    t.integer  "currency_id"
    t.integer  "payment_type_id"
    t.datetime "deleted_at"
  end

  add_index "payment_profiles", ["client_id"], :name => "index_payment_profiles_on_client_id"
  add_index "payment_profiles", ["country_id"], :name => "index_payment_profiles_on_country_id"

  create_table "payment_reports", :force => true do |t|
    t.integer  "order_id"
    t.integer  "support_user_id"
    t.integer  "payment_type_id"
    t.integer  "payment_id"
    t.decimal  "amount",          :precision => 8, :scale => 2
    t.text     "note"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.decimal  "amount_owing",    :precision => 8, :scale => 2
  end

  create_table "payment_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "index"
  end

  create_table "payments", :force => true do |t|
    t.integer  "order_id"
    t.decimal  "amount",                          :precision => 8, :scale => 2
    t.string   "error"
    t.string   "error_code"
    t.text     "message"
    t.text     "note"
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
    t.string   "confirmation_code"
    t.integer  "payment_profile_id"
    t.string   "ref_trn_id"
    t.integer  "payment_type_id"
    t.boolean  "received"
    t.integer  "support_user_id"
    t.datetime "completed_at"
    t.string   "last_digits"
    t.text     "source"
    t.string   "trn_id"
    t.string   "trn_type",           :limit => 2
  end

  add_index "payments", ["order_id"], :name => "index_payments_on_order_id"
  add_index "payments", ["payment_profile_id"], :name => "index_payments_on_payment_profile_id"
  add_index "payments", ["payment_type_id"], :name => "index_payments_on_payment_type_id"
  add_index "payments", ["support_user_id"], :name => "index_payments_on_support_user_id"

  create_table "pre_orders", :force => true do |t|
    t.integer  "order_id"
    t.integer  "payment_profile_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.boolean  "declined",           :default => false
  end

  add_index "pre_orders", ["declined"], :name => "index_pre_orders_on_declined"
  add_index "pre_orders", ["order_id"], :name => "index_pre_orders_on_order_id"
  add_index "pre_orders", ["payment_profile_id"], :name => "index_pre_orders_on_payment_profile_id"

  create_table "pricing_types", :force => true do |t|
    t.string   "name"
    t.boolean  "default"
    t.boolean  "active"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.boolean  "allow_auto_shipping"
    t.integer  "auto_shipping_discount"
  end

  create_table "product_caselot_translations", :force => true do |t|
    t.integer  "product_caselot_id"
    t.string   "locale",             :null => false
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "description"
  end

  add_index "product_caselot_translations", ["locale"], :name => "index_product_caselot_translations_on_locale"
  add_index "product_caselot_translations", ["product_caselot_id"], :name => "index_product_caselot_translations_on_product_caselot_id"

  create_table "product_caselots", :force => true do |t|
    t.integer  "count"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "product_id"
  end

  add_index "product_caselots", ["product_id"], :name => "index_product_caselots_on_product_id"

  create_table "product_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "product_category_translations", :force => true do |t|
    t.integer  "product_category_id"
    t.string   "locale",              :null => false
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "name"
  end

  add_index "product_category_translations", ["locale"], :name => "index_product_category_translations_on_locale"
  add_index "product_category_translations", ["product_category_id"], :name => "index_product_category_translations_on_product_category_id"

  create_table "product_disorders", :force => true do |t|
    t.integer  "product_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "disorder_id"
  end

  add_index "product_disorders", ["disorder_id"], :name => "index_product_disorders_on_disorder_id"
  add_index "product_disorders", ["product_id"], :name => "index_product_treatments_on_product_id"

  create_table "product_images", :force => true do |t|
    t.string   "image"
    t.integer  "product_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "index"
    t.string   "alt"
  end

  add_index "product_images", ["product_id"], :name => "index_product_images_on_product_id"

  create_table "product_prices", :force => true do |t|
    t.integer  "currency_id"
    t.integer  "product_pricing_type_id"
    t.decimal  "price",                   :precision => 8, :scale => 2
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
  end

  create_table "product_pricing_types", :force => true do |t|
    t.integer  "pricing_type_id"
    t.integer  "priceable_id"
    t.string   "priceable_type"
    t.boolean  "available",       :default => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.boolean  "show",            :default => false
  end

  add_index "product_pricing_types", ["priceable_type"], :name => "index_product_pricing_types_on_priceable_type"
  add_index "product_pricing_types", ["pricing_type_id"], :name => "index_product_pricing_types_on_pricing_type_id"

  create_table "product_promotions", :force => true do |t|
    t.integer "x_product_id"
    t.integer "x_quantity"
    t.integer "y_product_id"
    t.decimal "promotion_quantity_or_rate_off"
    t.integer "promotion_method_id"
    t.integer "repeated_time"
    t.date    "start_date"
    t.date    "end_date"
    t.string  "name"
    t.boolean "use_only_for_referral_code",     :default => false
  end

  create_table "product_translations", :force => true do |t|
    t.integer  "product_id"
    t.string   "locale",           :null => false
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "name"
    t.string   "seo_title"
    t.text     "seo_keywords"
    t.text     "highlights"
    t.text     "description"
    t.text     "suggested_use"
    t.text     "supplement_facts"
    t.text     "quality"
    t.text     "seo_description"
  end

  add_index "product_translations", ["locale"], :name => "index_product_translations_on_locale"
  add_index "product_translations", ["product_id"], :name => "index_product_translations_on_product_id"

  create_table "product_type_translations", :force => true do |t|
    t.integer  "product_type_id"
    t.string   "locale",          :null => false
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "name"
  end

  add_index "product_type_translations", ["locale"], :name => "index_product_type_translations_on_locale"
  add_index "product_type_translations", ["product_type_id"], :name => "index_product_type_translations_on_product_type_id"

  create_table "product_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                                                            :null => false
    t.datetime "updated_at",                                                            :null => false
    t.integer  "type_id"
    t.integer  "quantity"
    t.string   "unit"
    t.text     "description"
    t.string   "shipping_description"
    t.float    "weight_lbs"
    t.string   "product_code"
    t.string   "tariff_code"
    t.decimal  "customs_value",        :precision => 8, :scale => 2
    t.boolean  "allow_order_online"
    t.boolean  "active"
    t.boolean  "featured"
    t.text     "highlights"
    t.integer  "product_category_id"
    t.text     "suggested_use"
    t.text     "supplement_facts"
    t.string   "label_file"
    t.string   "seo_title"
    t.text     "seo_keywords"
    t.integer  "index"
    t.text     "quality"
    t.boolean  "back_ordered",                                       :default => false
    t.text     "verbiage"
    t.text     "seo_description"
    t.boolean  "pre_order",                                          :default => false
    t.text     "featured_text"
  end

  create_table "promotion_pricing_types", :force => true do |t|
    t.integer  "promotion_id"
    t.string   "promotion_type"
    t.integer  "pricing_type_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "promotion_pricing_types", ["pricing_type_id"], :name => "index_promotion_pricing_types_on_pricing_type_id"

  create_table "provinces", :force => true do |t|
    t.string   "name"
    t.string   "abbr",       :limit => 2
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "country_id"
  end

  create_table "question_age_groups", :force => true do |t|
    t.integer  "question_id"
    t.integer  "age_group_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "question_age_groups", ["age_group_id"], :name => "index_question_age_groups_on_age_group_id"
  add_index "question_age_groups", ["question_id"], :name => "index_question_age_groups_on_question_id"

  create_table "question_for_genders", :force => true do |t|
    t.integer  "gender_id"
    t.integer  "question_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "question_for_genders", ["gender_id"], :name => "index_question_for_genders_on_gender_id"
  add_index "question_for_genders", ["question_id"], :name => "index_question_for_genders_on_question_id"

  create_table "questionnaires", :force => true do |t|
    t.string   "title"
    t.text     "comment"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "questions", :force => true do |t|
    t.text     "title"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "quit_reasons", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "referral_code_orders", :force => true do |t|
    t.integer  "referral_code_id"
    t.integer  "order_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "referral_code_orders", ["order_id"], :name => "index_referral_code_orders_on_order_id"
  add_index "referral_code_orders", ["referral_code_id"], :name => "index_referral_code_orders_on_referral_code_id"

  create_table "referral_code_product_promotions", :force => true do |t|
    t.integer  "referral_code_id"
    t.integer  "product_promotion_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "referral_code_product_promotions", ["product_promotion_id"], :name => "index_referral_code_product_promotions_on_product_promotion_id"
  add_index "referral_code_product_promotions", ["referral_code_id"], :name => "index_referral_code_product_promotions_on_referral_code_id"

  create_table "referral_codes", :force => true do |t|
    t.string   "code"
    t.string   "url"
    t.boolean  "percentage",      :default => false
    t.integer  "support_user_id"
    t.integer  "pricing_type_id"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "number_of_times"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "discount"
    t.datetime "deleted_at"
    t.boolean  "need_licence",    :default => false
  end

  add_index "referral_codes", ["deleted_at"], :name => "index_referral_codes_on_deleted_at"
  add_index "referral_codes", ["pricing_type_id"], :name => "index_referral_codes_on_pricing_type_id"
  add_index "referral_codes", ["support_user_id"], :name => "index_referral_codes_on_support_user_id"

  create_table "registration_methods", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "deleted",    :default => false
  end

  create_table "roles", :force => true do |t|
    t.string "name"
    t.string "code"
  end

  create_table "seo_translations", :force => true do |t|
    t.integer  "seo_id"
    t.string   "locale",      :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "title"
    t.text     "keywords"
    t.text     "description"
  end

  add_index "seo_translations", ["locale"], :name => "index_seo_translations_on_locale"
  add_index "seo_translations", ["seo_id"], :name => "index_seo_translations_on_seo_id"

  create_table "seos", :force => true do |t|
    t.string   "url"
    t.string   "title"
    t.string   "keywords"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "settings", :force => true do |t|
    t.string   "name"
    t.string   "value"
    t.string   "code",       :limit => 3
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "shipments", :force => true do |t|
    t.integer  "order_id"
    t.float    "weight_lbs"
    t.string   "state"
    t.integer  "shipping_method_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "address_id"
    t.datetime "shipped_at"
  end

  add_index "shipments", ["order_id"], :name => "index_shipments_on_order_id"
  add_index "shipments", ["shipping_method_id"], :name => "index_shipments_on_shipping_method_id"
  add_index "shipments", ["state"], :name => "index_shipments_on_shipment_status_id"

  create_table "shipping_carriers", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "shipping_methods", :force => true do |t|
    t.string   "name"
    t.integer  "shipping_carrier_id"
    t.boolean  "default"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "shipping_methods", ["shipping_carrier_id"], :name => "index_shipping_methods_on_shipping_carrier_id"

  create_table "shipping_promotions", :force => true do |t|
    t.decimal  "min_amount", :precision => 8, :scale => 2
    t.decimal  "amount_off", :precision => 8, :scale => 2
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "area_id"
  end

  create_table "side_effects", :force => true do |t|
    t.text     "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "studies", :force => true do |t|
    t.string   "title"
    t.string   "journal_cover"
    t.string   "pdf"
    t.string   "link"
    t.text     "brief_description"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "article_title"
    t.string   "reference"
    t.integer  "video_id"
    t.integer  "index"
    t.string   "journal_cover_alt"
  end

  create_table "study_disorders", :force => true do |t|
    t.integer  "study_id"
    t.integer  "disorder_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "study_translations", :force => true do |t|
    t.integer  "study_id"
    t.string   "locale",            :null => false
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "title"
    t.text     "brief_description"
    t.string   "article_title"
  end

  add_index "study_translations", ["locale"], :name => "index_study_translations_on_locale"
  add_index "study_translations", ["study_id"], :name => "index_study_translations_on_study_id"

  create_table "support_center_permissions", :force => true do |t|
    t.integer  "support_user_id"
    t.integer  "support_center_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "support_center_permissions", ["support_center_id"], :name => "index_support_center_permissions_on_support_center_id"
  add_index "support_center_permissions", ["support_user_id"], :name => "index_support_center_permissions_on_support_user_id"

  create_table "support_center_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "support_centers", :force => true do |t|
    t.integer  "support_center_type_id"
    t.string   "code"
    t.string   "name"
    t.string   "address"
    t.string   "city"
    t.string   "province"
    t.integer  "country_id"
    t.string   "postal_code"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.string   "support_email"
    t.string   "support_phone"
    t.boolean  "deleted",                :default => false
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "default",                :default => false
  end

  add_index "support_centers", ["support_center_type_id"], :name => "index_support_centers_on_support_center_type_id"

  create_table "support_user_global_languages", :force => true do |t|
    t.integer  "support_user_id"
    t.integer  "global_language_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "support_user_global_languages", ["global_language_id"], :name => "index_support_user_global_languages_on_global_language_id"
  add_index "support_user_global_languages", ["support_user_id"], :name => "index_support_user_global_languages_on_support_user_id"

  create_table "support_user_roles", :force => true do |t|
    t.integer  "support_user_id"
    t.integer  "role_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "support_users", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "user_name"
    t.string   "password"
    t.boolean  "support_desk"
    t.boolean  "data_enter"
    t.boolean  "admin_orders"
    t.boolean  "order_desk"
    t.integer  "client_id"
    t.integer  "support_center_id"
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "time_zone"
    t.boolean  "active",                 :default => false
    t.boolean  "receive_commission",     :default => false
    t.boolean  "commission_by_amount",   :default => false
  end

  add_index "support_users", ["email"], :name => "index_support_users_on_email", :unique => true
  add_index "support_users", ["reset_password_token"], :name => "index_support_users_on_reset_password_token", :unique => true
  add_index "support_users", ["support_center_id"], :name => "index_support_users_on_support_center_id"

  create_table "symptom_side_effects", :force => true do |t|
    t.integer  "symptom_id"
    t.integer  "side_effect_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "symptom_side_effects", ["side_effect_id"], :name => "index_symptom_side_effects_on_side_effect_id"
  add_index "symptom_side_effects", ["symptom_id"], :name => "index_symptom_side_effects_on_symptom_id"

  create_table "symptoms", :force => true do |t|
    t.integer  "disid"
    t.text     "question"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "name"
    t.text     "description"
  end

  create_table "ta_levels", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted",    :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "taxes", :force => true do |t|
    t.integer  "country_id"
    t.string   "province_abbr", :limit => 2
    t.string   "name"
    t.integer  "rate"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "transfer_pages", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "user_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "video_disorders", :force => true do |t|
    t.integer  "video_id"
    t.integer  "disorder_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "video_disorders", ["disorder_id"], :name => "index_video_disorders_on_disorder_id"
  add_index "video_disorders", ["video_id"], :name => "index_video_disorders_on_video_id"

  create_table "video_translations", :force => true do |t|
    t.integer  "video_id"
    t.string   "locale",          :null => false
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "title"
    t.text     "description"
    t.string   "seo_title"
    t.text     "seo_keywords"
    t.text     "seo_description"
  end

  add_index "video_translations", ["locale"], :name => "index_video_translations_on_locale"
  add_index "video_translations", ["video_id"], :name => "index_video_translations_on_video_id"

  create_table "videos", :force => true do |t|
    t.string   "title"
    t.string   "embed_link"
    t.string   "image"
    t.text     "description"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "seo_title"
    t.text     "seo_keywords"
    t.integer  "index"
    t.text     "seo_description"
    t.string   "image_alt"
  end

  create_table "view_client_reports", :force => true do |t|
    t.integer  "client_id"
    t.datetime "datetime"
    t.integer  "support_user_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "view_client_reports", ["client_id"], :name => "index_view_client_reports_on_client_id"
  add_index "view_client_reports", ["support_user_id"], :name => "index_view_client_reports_on_support_user_id"

  create_table "withdrawal_effects", :force => true do |t|
    t.text     "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.text     "description"
  end

end
