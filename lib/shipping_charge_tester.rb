#!/usr/bin/env ruby
require 'active_shipping'
include ActiveMerchant::Shipping

class ShippingMethodTest

  def self.test_usps

    origin = Location.new(:country => 'US',
                          :state => 'UT',
                          :city => 'Centerville',
                          :zip => '84014')
    dest = Location.new(:country => 'US',
                        :state => 'MT',
                        :city => 'Philipsburg',
                        :zip => '59858')

    packages = [
      Package.new((1.5*16),                 # 7.5 lbs, times 16 oz/lb.
                  [12, 12, 12],              # 15x10x4.5 inches
                  :units => :imperial)        # not grams, not centimetres
    ]


    usps = USPS.new(:login => '328ANDER4239')
    response = usps.find_rates(origin, dest, packages)
    usps_rates = response.rates.sort_by(&:price).collect {|rate| [rate.service_name, rate.price]}
    puts usps_rates.inspect
  end

  def self.test_fedex
    origin = Location.new(:country => 'US',
                          :state => 'UT',
                          :city => 'Centerville',
                          :zip => '84014')
    dest = Location.new(:country => 'US',
                        :state => 'OR',
                        :city => 'Lakeview',
                        :zip => '97630')

    packages = [
      Package.new((1*16),                 # 7.5 lbs, times 16 oz/lb.
                  [6, 6, 6],              # 15x10x4.5 inches
                  :units => :imperial)        # not grams, not centimetres
    ]

    #test
#    fedex = FedEx.new(:login => '118581109', :password => 'oybZLZRrsc9QLbv6o87Xcwpsl', :key => 'OjrcCd34Z2kDaG61', :account => '510087429', :test=>true)

#    response = fedex.find_rates(origin, dest, packages)

#    fedex_rates = response.rates.sort_by(&:price).collect {|rate| [rate.service_name, rate.price]}
#    puts fedex_rates.inspect

    #production
    fedex = FedEx.new(:login => 105189207, :password => 'GQjeyLgcyMixIp5MBAIXdoj68', :key => '9ca2Q8skgX3ZsKcx', :account => 334288536, :RequestedShipment => {:RateRequestTypes => 'LIST'})

    response = fedex.find_rates(origin, dest, packages)

    fedex_rates = response.rates.sort_by(&:price).collect {|rate| [rate.service_name, rate.price]}
    puts response.xml
  end

  def self.norway_usps

    origin = Location.new(:country => 'US',
                          :state => 'UT',
                          :city => 'Centerville',
                          :zip => '84014')
    dest = Location.new(:country => 'NO',
                        :state => 'Trondheim',
                        :city => 'Trondheim',
                        :zip => '7321')

    packages = [
      Package.new((1.5*16),                 # 7.5 lbs, times 16 oz/lb.
                  [12, 12, 12],              # 15x10x4.5 inches
                  :units => :imperial)        # not grams, not centimetres
    ]


    usps = USPS.new(:login => '328ANDER4239')
    response = usps.find_rates(origin, dest, packages)
    usps_rates = response.rates.sort_by(&:price).collect {|rate| [rate.service_name, rate.price]}
    puts usps_rates.inspect
  end

end


#ShippingMethodTest.test_usps
#ShippingMethodTest.test_fedex
#ShippingMethodTest.norway_usps
