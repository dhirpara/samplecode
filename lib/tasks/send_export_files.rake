task send_export_file: :environment do
  #orders = Order.includes(:shipment).where("shipments.state = ?", "ready")
  canada_orders = Order.includes(shipment: [address: :country]).where("shipments.state = ? and countries.abbr in (?)", "ready", ["CA", "AU", "NZ"] + Country::EUROPEAN_UNION)
  usa_and_international_orders = Order.includes(shipment: [address: :country]).where("shipments.state = ? and countries.abbr not in (?)", "ready", ["CA", "AU", "NZ"] + Country::EUROPEAN_UNION)
  if canada_orders.any?
    ExportFileMailer.canada_subscription(canada_orders).deliver
    canada_orders.each{|o| o.shipment.ship }
  end

  if usa_and_international_orders.any?
    ExportFileMailer.usa_and_international_subscription(usa_and_international_orders).deliver
    usa_and_international_orders.each{|o| o.shipment.ship }
  end
end
