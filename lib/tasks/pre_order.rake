task pre_order: :environment do
  PreOrder.pending.each do |pre_order|
    unless pre_order.order.order_items.any?{|i| i.product.pre_order? }
      pre_order.complete_order
    end
  end
end
