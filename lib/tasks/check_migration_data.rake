# coding: utf-8
namespace :dev do
  desc "check migration data"
  task check_migration_data: :environment do


    disorders_array = []
    ["ADHD", "Anxiety", "Asperger’s", "Autism", "Bipolar", "Depression", "OCD", "ODD", "Panic", "Phobias", "Prader-Willi", "PDD", "Stress", "Dysthymic", "Psychoses", "Conduct", "PTSD", "General Health", "Multi-nutrient Approach ", "Healthy Diet & Supplementation", "Superior Nutrition"].each_with_index do |name, index|
      disorders_array[index] = [index+1, name]
    end
    if Disorder.order(:id).map{|o| [o.id, o.name]} == disorders_array
      puts "Disorder checked"
    else
      puts "***Disorder need to check"
    end

    faq_types_array = []
    ["The Hardy Nutritionals™ Edge", "Health Benefits", "Safety", "Suggested Use", "Quality", "Women's Health", "About Vitamins and Minerals", "Things To Discuss With Your Doctor"].each_with_index do |name, index|
      faq_types_array[index] = [index+1, name]
    end
    if FaqType.order(:id).map{|o| [o.id, o.name]} == faq_types_array
      puts "FaqType checked"
    else
      puts "***FaqType need to check"
    end

    if HandlingChargeMethod.all.map{|o| [o.id, o.name]} == [[1, "Percentage based"], [2, 'Range based']]
      puts "HandlingChargeMethod checked"
    else
      puts "***HandlingChargeMethod need to check"
    end


    pricing_types_array = []
    ["Retail", 'Staff', 'Health Professionals', 'Shareholders', 'Wholesale 1', 'Wholesale 2', 'Wholesale 3'].each_with_index do |name, index|
      pricing_types_array[index] = [index+1, name]
    end
    if PricingType.order(:id).map{|o| [o.id, o.name]} == pricing_types_array
      puts "PricingType checked"
    else
      puts "***PricingType need to check"
    end

    if ProductCategory.order(:id).map{|o| [o.id, o.name]} == [[1, "Clinical Use"], [2, "General Health Use"], [3, "Truehope Products"]]
      puts "ProductCategory checked"
    else
      puts "***ProductCategory need to check"
    end

    product_types_array = [[1, "Book"], [2, "Booklet"], [3, "Capsules"], [4, "CD"], [5, "DVD"], [6, "Liquid"], [7, "Powder"], [8, "Soft gels"], [9, "Tablets"]]

    if ProductType.order(:id).map{|o| [o.id, o.name]} == product_types_array
      puts "ProductType checked"
    else
      puts "***ProductType need to check"
    end

    if ArticleCategory.order(:id).map{|o|[o.id, o.name]} == [[1, "Clinical Reference"], [2, "Our Vision"]]
      puts "ArticleCategory checked"
    else
      puts "***ArticleCategory need to check"
    end

    #不存在数据
    [CartItem, Cart, AutoShipping, OrderItem, Order, OrderAdjustment, Payment, PaymentProfile, Shipment].each do |klass|
      puts klass.count == 0 ? "#{klass} checked" : "**#{klass} has some problem, need to check"
    end

    #stage
    [ProductCaselot, ProductDisorder, ProductImage, ProductPrice, ProductPricingType, Product,
     Article, ArticleDisorder,
     Study, StudyDisorder, Video, VideoDisorder, Faq].each do |klass|
      puts klass.count > 0 ? "#{klass} checked" : "**#{klass} has some problem, need to check"
    end

    #sqlserver
    [Address, ClientDoctor, Client, Note, OrderNote,
      SupportCenterPermission, SupportCenterType, SupportCenter, SupportUserRole, SupportUser].each do |klass|
      puts klass.count > 0 ? "#{klass} checked" : "**#{klass} has some problem, need to check"
    end
    [
      { klass: AgeCategory,        count: 7        },
      { klass: CallFrequency,      count: 7        },
      { klass: ContactTime,        count: 22       },
      { klass: Country,            count: 250      },
      { klass: IntroductionType,   count: 27       },
      { klass: Language,           count: 5        },
      { klass: QuitReason,         count: 34       },
      { klass: RegistrationMethod, count: 7        }
    ].each do |hash|
      puts hash[:klass].count == hash[:count] ? "#{hash[:klass]} checked" : "**#{hash[:klass]} has some problem, need to check"
    end


    #seed
    [
      { klass: Tax                         , count: 13        },
      { klass: AdjustmentType              , count: 7         },
      { klass: CountryShippingAdjustment   , count: 1         },
      { klass: Currency                    , count: 2         },
      { klass: HandlingCharge              , count: 7         },
      { klass: LandmarkShippingRate        , count: 2         },
      { klass: OrderType                   , count: 3         },
      { klass: PaymentType                 , count: 8         },
      { klass: Province                    , count: 69        },
      { klass: Role                        , count: 4         },
      { klass: Setting                     , count: 1         },
      { klass: ShippingCarrier             , count: 3         },
      { klass: ShippingMethod              , count: 10        }
    ].each do |hash|
      puts hash[:klass].count == hash[:count] ? "#{hash[:klass]} checked" : "**#{hash[:klass]} has some problem, need to check"
    end
  end
end
