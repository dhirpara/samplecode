namespace :dev do
  desc "update call frequency"
  task update_call_frequency: :environment do
    [
      "Daily",
      "Weekly",
      "Bi - Weekly",
      "Tri - Weekly",
      "Monthly",
      "Bi - Monthly",
      "Tri - Monthly"
    ].each_with_index do |name, index|
      CallFrequency.find_by_name(name).update_attributes(priority: (index + 1) * 10)
    end
  end
end
