 # coding: utf-8

namespace :dev do
  task translate: :environment do
    #I18n.locale = "en"
    #[{
      #en_name: "Clinical Use",
      #zh_cn_name: "临床应用",
      #zh_tw_name: "臨床應用"
    #}, {
      #en_name: "General Health Use",
      #zh_cn_name: "一般健康用途",
      #zh_tw_name: "一般健康用途"
    #}, {
      #en_name: "Other Products",
      #zh_cn_name: "其他产品",
      #zh_tw_name: "其他產品"
    #}].each do |hash|
      #if p = ProductCategory.where(name: hash[:en_name]).first
        #I18n.locale = "zh-CN"
        #p.update_attributes(name: hash[:zh_cn_name])
        #puts p.name
        #I18n.locale = "zh-TW"
        #p.update_attributes(name: hash[:zh_tw_name])
        #puts p.name
      #end
    #end

    I18n.locale = "en"
    [{
      en_name: "Capsules",
      zh_cn_name: "粒",
      zh_tw_name: "粒"
    }, {
      en_name: "Tablets",
      zh_cn_name: "片",
      zh_tw_name: "片"
    }, {
      en_name: "Soft gels",
      zh_cn_name: "膠囊",
      zh_tw_name: "胶囊"
    }].each do |hash|
      if p = ProductType.where(name: hash[:en_name]).first
        I18n.locale = "zh-CN"
        p.update_attributes(name: hash[:zh_cn_name])
        puts p.name
        I18n.locale = "zh-TW"
        p.update_attributes(name: hash[:zh_tw_name])
        puts p.name
      end
    end

  end
end
