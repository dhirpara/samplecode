task auto_ship: :environment do
  AutoShipping.where(active: true, next_order_date: Date.today).each do |a|
    if a.can_generate_order?
      a.generate_order
    end
  end

  AutoShipping.where(active: true, next_order_date: Date.today + 2).each do |a|
    if a.can_generate_order?
      AutoShipMailer.remind(a).deliver
    end
  end
end
