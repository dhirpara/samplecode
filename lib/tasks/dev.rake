# coding: utf-8

namespace :dev do
  task genera_static_seo: :environment do
       [
      {
        url: "/",
        title: "Bipolar Disorder, Anxiety Disorders,OCD,ADHD,Daily Essential Nutrients", 
        keywords: "Bipolar Disorder, Anxiety Disorders,OCD,ADHD,Daily Essential Nutrients,Autism,Multivitamin,mental health disorders,anxiety disorders,mineral supplement,health supplement,depression, stress,Dr.David Hardy,Hardy Nutritionals"
      },{
        url: "/products",
        title: "Hardy Nutritionals | Product Gallery", 
        keywords: ""
      },{
        url: "/videos",
        title: "Hardy Nutritionals | Media Center", 
        keywords: ""
      },{
        url: "/faqs",
        title: "Hardy Nutritionals | FAQ", 
        keywords: ""
      },{
        url: "/studies",
        title: "Hardy Nutritionals | Studies",
        keywords: ""
      },{
        url: "/articles",
        title: "Hardy Nutritionals | Articles",
        keywords: ""
      },{
        url: "/contact_us",
        title: "Hardy Nutritionals | Contact Us", 
        keywords: ""
      }
    ].each do |hash|
      url = hash[:url]
      title = hash[:title]
      keywords = hash[:keywords]
      Seo.create(url: url, title: title, keywords: keywords)
end

  end

  task payment_report_sync_amount_owing: :environment do
    PaymentReport.all.each do |p|
      p.amount_owing = p.order.need_purchase_total
      p.save
    end
  end

  task generate_commission_report: :environment do
    ActiveRecord::Base.transaction do
      SupportUser.commissioned.each do |su|
        clients = Client.where(support_user_id: su.id)
        clients.each do |client|
          if client.orders.any?
            orders = client.commission_calculated_orders(10.years.ago, 1.years.from_now)
            #commissioned_orders << {client:client, orders: orders}

            orders.each do |order|
              puts order.id
              rate = (/(.*%)/.match(order.commission_rate)).try(:[], 1)
              description =(/.*%.*\((.*)\)/.match(order.commission_rate)).try(:[], 1)
              CommissionReport.create(order: order, support_user: su, date: order.completed_at, amount: order.actual_received_payment_total, description: description, rate: rate)
            end
          end
        end
      end
    end
  end

  task change_product_category: :environment do
    if pc = ProductCategory.find_by_name("Truehope Products")
      pc.update_attributes(name: "Other Products")
   end
 end

 task add_credit_payment_type: :environment do
   PaymentType.create(name: "Credit") unless PaymentType.find_by_name("Credit")
 end

 task remove_fake_email: :environment do
   Client.where("email ilike ?", "hardy%@example.com").update_all(email: "")
   (0..5).to_a.each do |n|
     Client.where("email ilike ?", "#{n}\\_%").each do |c|
       c.update_attributes(email: c.email.split("_").slice(2..-1).join("_"))
       puts c.email
     end
   end
 end

 namespace :shipping_method do
   task change_shippine_method_name: :environment do
     if shipping_method = ShippingMethod.find_by_name("FedEx Ground")
       shipping_method.update_attributes(name: "FedEx Ground Home Delivery")
     end

     if shipping_method = ShippingMethod.find_by_name("USPS Express Mail International")
       shipping_method.update_attributes(name: "USPS Priority Mail Express International")
     end

     if shipping_method = ShippingMethod.find_by_name("USPS Priority Mail")
       shipping_method.update_attributes(name: "USPS Priority Mail 1-Day")
     end

     if shipping_method = ShippingMethod.find_by_name("USPS Express Mail")
        shipping_method.update_attributes(name: "USPS Priority Mail Express 1-Day")
      end

      usa = Country.find_by_abbr("US")
      canada = Country.find_by_abbr("CA")

      unless ShippingMethod.find_by_name("USPS Priority Mail 2-Day")
        s = ShippingMethod.create(name: "USPS Priority Mail 2-Day", shipping_carrier_id: 2, default: false)
        CountryShippingMethod.create(country: usa, shipping_method: s)
      end

      unless ShippingMethod.find_by_name("USPS Priority Mail Express 2-Day")
        s = ShippingMethod.create(name: "USPS Priority Mail Express 2-Day", shipping_carrier_id: 2, default: false)
        CountryShippingMethod.create(country: usa, shipping_method: s)
      end

      unless ShippingMethod.find_by_name("Canpar")
        s = ShippingMethod.create(name: "Canpar", shipping_carrier_id: 3, default: false)
        CountryShippingMethod.create(country: canada, shipping_method: s)
      end

      Video.all.each do |v|
        v.image.recreate_versions!
      end

      Address.where(active: false).update_all(client_id: nil)
      OrderItem.all.each{|i| i.update_attributes(retail_price: i.product.retail_price(i.order.currency))}
      Tax.all.each do |tax|
        if tax.name == "GST"
          tax.update_attributes(name: "G.S.T")
        elsif tax.name == "GST+PST"
          tax.update_attributes(name: "G.S.T + P.S.T")
        elsif tax.name == "HST"
          tax.update_attributes(name: "H.S.T")
        elsif tax.name == "GST+QST"
          tax.update_attributes(name: "G.S.T + Q.S.T")
        end
      end
    end
  end

  namespace :commission_rate do
    task add_data: :environment do

      CommissionRate.delete_all
      CommissionType.delete_all

      order_based = CommissionType.create(name: 'Order Number Based', code: 'NMB')
      amount_based = CommissionType.create(name: 'Order Amount Based', code: 'AMT')

      CommissionRate.create(min: 1, max: 2, rate: 19, commission_type: order_based, support_user_id: 3)
      CommissionRate.create(min: 3, max: 20, rate: 3, commission_type: order_based, support_user_id: 3)
      CommissionRate.create(min: 21, max: 50, rate: 1, commission_type: order_based, support_user_id: 3)

      CommissionRate.create(min: 0, max: 500, rate: 19, commission_type: amount_based, support_user_id: 3)
      CommissionRate.create(min: 500, max: 1000, rate: 3, commission_type: amount_based, support_user_id: 3)
      CommissionRate.create(min: 1000, max: 2000, rate: 1, commission_type: amount_based, support_user_id: 3)

    end
  end

  namespace :cache do
    desc "Clear the cache"
    task :clear => :environment do
      Rails.cache.clear
    end
  end
end
