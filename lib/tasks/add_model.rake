namespace :dev do
  MIGRATION_VERSION = 10000000000000
  def table_name(name)
    model_file_name(name).pluralize
  end

  def model_file_name(name)
    if name == "Auto_Shipping_Tax"
      "auto_shipping_tax"
    elsif name == "Not Adjusted Tax"
      "not_adjusted_tax"
    elsif name == "Order_Tax"
      "order_tax"
    elsif name == "Orders_Adjustments_Tax"
      "orders_adjustments_tax"
    elsif name == "Orders_Tax"
      "orders_tax"
    elsif name == "State_Province_Tax"
      "state_province_tax"
    elsif name == "Tax"
      "tax"
    else

      name.gsub(" ","_").pluralize.singularize.underscore
    end
  end

  def model_name(name)
    if name == "Auto_Shipping_Tax" || name == "auto_shipping_tax"
      "AutoShippingTax"
    elsif name == "Not Adjusted Tax" || name == "not_adjusted_tax"
      "NotAdjustedTax"
    elsif name == "Order_Tax" || name =="order_tax"
      "OrderTax"
    elsif name == "Orders_Adjustments_Tax" || name == "orders_adjustments_tax"
      "OrdersAdjustmentsTax"
    elsif name == "Orders_Tax" || name == "orders_tax"
      "OrdersTax"
    elsif name == "State_Province_Tax" || name =="state_province_tax"
      "StateProvinceTax"
    elsif name == "Tax" || name == "tax"
      "Tax"
    else
      name.titleize.gsub(" ","").pluralize.singularize
    end
  end

  desc "create model"
  task create_model: :environment do
    ActiveRecord::Base.transaction do
      (ActiveRecord::Base.connection.tables.delete_if{|name| name == "schema_migrations"}).each do |name|
        puts name
        content =<<-CODE
class Sqlserver::#{model_name(name)} < ActiveRecord::Base
  self.establish_connection(ActiveRecord::Base.configurations['sqlserver'])
  set_table_name '#{name}'
end
        CODE
        File.open("#{Rails.root.to_path}/app/models/sqlserver/#{model_file_name(name)}.rb", "w+") do |f|
          f.write content
        end

        content =<<-CODE
class #{model_name(name)} < ActiveRecord::Base
end
        CODE
        File.open "#{Rails.root.to_path}/app/models/#{model_file_name(name)}.rb", "w+" do |f|
          f.write content
        end
      end
    end
  end

  desc "create migration"
  task create_migration: :environment do
    ActiveRecord::Base.transaction do
      i = 1
      (ActiveRecord::Base.connection.tables.delete_if{|name| name == "schema_migrations"}).each do |name|
        content=<<-CODE
class Create#{model_name(name).pluralize} < ActiveRecord::Migration
  def change
    create_table :#{table_name(name)} do |t|
        #{add_columns(name)}
    end
  end
end
        CODE
        File.open "#{Rails.root.to_path}/db/migrate/#{MIGRATION_VERSION + i}_create_#{table_name(name)}.rb", "w+" do |f|
          f.write content
        end
        i = i + 1
      end
    end
  end

  def add_columns(name)
    "Sqlserver::#{model_name(name)}".constantize.columns.map do |c|
      array = c.name.split("_")

      if array[1].try(:underscore) == "id"
        ""
      else
        if c.type == :string
          c.sql_type.match /varchar\((.*)\)/
          if $1.to_i > 255
            "t.text :#{c.name}"
          else
            "t.string :#{c.name}"
          end
        elsif c.sql_type == "bigint(8)"
          "t.integer :#{c.name}, limit: 8"
        else
          "t.#{c.type} :#{c.name}"
        end
      end
    end.join("\n")
  end

  desc "create data"
  task create_data: :environment do
    ActiveRecord::Base.transaction do
      files = Dir.foreach("#{Rails.root.to_path}/app/models/sqlserver").map do |file|
        file
      end
      files.shift
      files.shift
      files = files.map{|str| str.split(".")[0]}

      files.each do |name|
        puts name
        sql_table = "Sqlserver::#{model_name(name)}".constantize
        psql_table = "#{model_name(name)}".constantize

        sql_table.limit(100).each do |obj|
          new_psql_record = psql_table.new
          sql_table.columns.each do |column|
            array = column.name.split("_")
            if array[1].try(:underscore) == "id"
              new_psql_record.id = obj.send(column.name)
            else
              new_psql_record.send("#{column.name}=", obj.send(column.name))
            end
          end

          new_psql_record.save
          puts new_psql_record.id
        end

        if model_name(name).constantize.last
          ActiveRecord::Base.connection.execute("ALTER SEQUENCE #{table_name(name)}_id_seq START with #{model_name(name).constantize.last.id + 1} RESTART;")
        end
      end
    end
  end
end
