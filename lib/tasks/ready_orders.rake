task ready_orders: :environment do
  Shipment.includes(:order).where("orders.state = ? and shipments.state = ?", "complete", "pending").each(&:ready)
end
