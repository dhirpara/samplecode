namespace :dev do
  desc "data migration"
  task data_migration_client: :environment do
    clients_array = Sqlserver::Client.valid_clients.in_groups(6, false)
    clients_array.each_with_index do |array, index|
      migrate_clients(array, index)
    end
  end

  task data_migration_client_backup: :environment do
    clients_array = Sqlserver::Client.valid_clients.in_groups(6, false)
    (1..5).each do |num|
      migrate_clients(clients_array[num], num)
    end
  end


  def migrate_clients(array, index)
    ActiveRecord::Base.transaction do
      Sqlserver::Client.data_to_migrate(array, index)
    end
  end

  task data_migration_address: :environment do
    ActiveRecord::Base.transaction do
      Sqlserver::ClientShipping.data_to_migrate
    end
  end

  task data_migration_note: :environment do
    ids_array = Sqlserver::Client.valid_clients_ids.in_groups(15, false)
    ids_array.each_with_index do |array, index|
      migrate_notes(array, index)
    end
  end

  task data_migration_note_backup: :environment do
    ids_array = Sqlserver::Client.valid_clients_ids.in_groups(15, false)
    (9..14).each do |num|
      migrate_notes(ids_array[num], num)
    end
  end

  def migrate_notes(array, index)
    ActiveRecord::Base.transaction do
      Sqlserver::Note.data_to_migrate(array, index)
    end
  end

  task data_migration: :environment do
    #IntroductionType要优先于Client
    array = [
      "Sqlserver::ClientsAgeCategory",
      "Sqlserver::Doctor", #client
      "Sqlserver::ContactTime",
      "Sqlserver::Country",
      "Sqlserver::QuitReason",
      "Sqlserver::ClientIntroductionsType",
      "Sqlserver::Language",
      "Sqlserver::OrderNote",#client
      "Sqlserver::SupportStaff",
      "Sqlserver::ClientsSupportCenter",
      "Sqlserver::ClientsSupportCentersPermission",
      "Sqlserver::ClientsSupportCentersType",
      "Sqlserver::ClientsTaLevel"
    ]
    ActiveRecord::Base.transaction do
      add_role
      array.each do |name|
        puts name
        name.constantize.data_to_migrate
      end
    end
  end

  def add_role
    Role.create(name: 'admin', code: 'ADM')
    Role.create(name: 'basic', code: 'BAS')
    Role.create(name: 'order completion', code: 'ORC')
    Role.create(name: 'export orders', code: 'EXO')
  end

  #delay
  #"Sqlserver::Family",
  #"Sqlserver::Symptom",
  #"Sqlserver::Drug",
  #"Sqlserver::Diagnosis",
  #"Sqlserver::DailySymptomTotal",

end
